﻿using Logic.Hardware.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading.Tasks;

namespace Logic.Hardware
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="Logic.Hardware.Model.Win32_MotherboardModel" />
    public class Win32_Motherboard : Win32_MotherboardModel
    {
        /// <summary>
        /// Gets the Win32_MotherboardDevice data.
        /// </summary>
        public static void GetData()
        {
            using (var worker = new BackgroundWorker())
            {
                worker.WorkerReportsProgress = true;
                worker.WorkerSupportsCancellation = true;

                // Handles the DoWork event of the BackgroundWorker.
                worker.DoWork += delegate
                {
                    using (ManagementObjectSearcher mos = new ManagementObjectSearcher("SELECT * FROM Win32_MotherboardDevice"))
                    {
                        foreach (ManagementObject obj in mos?.Get())
                        {
                            
                        }
                    }
                };

                // Handles the RunWorkerCompleted event of the BackgroundWorker.
                worker.RunWorkerCompleted += delegate { ValuesInitialized = true; };

                // Start the BackgroundWorker.
                worker.RunWorkerAsync();
            }
        }
    }
}
