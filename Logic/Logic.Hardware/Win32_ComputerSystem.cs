﻿using Logic.Hardware.Model;
using System.ComponentModel;
using System.Management;

namespace Logic.Hardware
{
    // https://docs.microsoft.com/en-us/windows/win32/cimwin32prov/win32-computersystem

    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="Logic.Hardware.Model.Win32_ComputerSystemModel" />
    public class Win32_ComputerSystem : Win32_ComputerSystemModel
    {
        /// <summary>
        /// Gets the Win32_ComputerSystem data.
        /// </summary>
        public static void GetData()
        {
            // Reset value
            ValuesInitialized = false;

            using (var worker = new BackgroundWorker())
            {
                worker.WorkerReportsProgress = true;
                worker.WorkerSupportsCancellation = true;

                // Handles the DoWork event of the BackgroundWorker.
                worker.DoWork += delegate
                {
                    // Get computerSystem propertys
                    using (ManagementObjectSearcher mos = new ManagementObjectSearcher("SELECT * FROM Win32_ComputerSystem"))
                    {
                        foreach (ManagementObject obj in mos?.Get())
                        {
                            NumberOfProcessors = (uint)obj["NumberOfProcessors"];
                            PowerState = (ushort)obj["PowerState"];
                            PrimaryOwnerName = (string)obj["PrimaryOwnerName"];
                        }
                    }
                };

                // Handles the RunWorkerCompleted event of the BackgroundWorker.
                worker.RunWorkerCompleted += delegate { ValuesInitialized = true; };

                // Start the BackgroundWorker.
                worker.RunWorkerAsync();
            };
        }
    }
}
