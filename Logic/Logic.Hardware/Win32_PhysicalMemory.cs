﻿using Logic.Hardware.Model;
using System;
using System.ComponentModel;
using System.Management;

namespace Logic.Hardware
{
    /// <summary>
    /// The Win32_PhysicalMemory WMI class represents a physical memory device located on a computer system 
    /// and available to the operating system.
    /// </summary>
    public class Win32_PhysicalMemory : Win32_PhysicalMemoryModel
    {
        /// <summary>
        /// Gets the Win32_PhysicalMemory data.
        /// </summary>
        public static void GetData()
        {
            // Reset value
            ValuesInitialized = false;

            using (var worker = new BackgroundWorker())
            {
                worker.WorkerReportsProgress = true;
                worker.WorkerSupportsCancellation = true;
                
                // Handles the DoWork event of the BackgroundWorker.
                worker.DoWork += delegate
                {
                    // Initialize PeformaneCounter
                    InitializePerformanceCounter();

                    using (ManagementObjectSearcher mos = new ManagementObjectSearcher("SELECT * FROM Win32_PhysicalMemory"))
                    {
                        foreach (ManagementObject obj in mos?.Get())
                        {
                            CapacityInBytes += Convert.ToInt64(obj["Capacity"]);
                            ConfiguredClockSpeed = Convert.ToString(obj["ConfiguredClockSpeed"]);
                            Manufacturer = (string)obj["Manufacturer"];
                            FormFactor = Convert.ToString(obj["FormFactor"]);
                            MinVoltage = Convert.ToString(obj["MinVoltage"]);
                            MaxVoltage = Convert.ToString(obj["MaxVoltage"]);
                            ConfiguredVoltage = Convert.ToString(obj["ConfiguredVoltage"]);
                            SerialNumber = (string)obj["SerialNumber"];
                        }
                    }
                };

                // Handles the RunWorkerCompleted event of the BackgroundWorker.
                worker.RunWorkerCompleted += delegate { ValuesInitialized = true; };

                // Start the BackgroundWorker.
                worker.RunWorkerAsync();
            };
        }
    }
}
