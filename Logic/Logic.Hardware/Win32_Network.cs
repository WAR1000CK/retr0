﻿using Logic.Hardware.Model;
using System.ComponentModel;
using System.Linq;
using System.Management;
using System.Net.NetworkInformation;
using System.Net.Sockets;

namespace Logic.Hardware
{
    /// <summary>
    /// Die Klasse <see cref="Win32_Network"/> liefert Informationen über die verbauten Netzwerkkarten.
    /// </summary>
    public class Win32_Network : Win32_NetworkModel
    {
        /// <summary>
        /// 
        /// </summary>
        public static void GetData()
        {
            // Reset value
            ValuesInitialized = false;

            using (var worker = new BackgroundWorker())
            {
                worker.WorkerReportsProgress = true;
                worker.WorkerSupportsCancellation = true;

                // Handles the DoWork event of the BackgroundWorker.
                worker.DoWork += delegate
                {
                    foreach (NetworkInterface ni in NetworkInterface.GetAllNetworkInterfaces())
                    {
                        if (IdentifyConnectorPresent(ni))
                        {
                            if (ni.NetworkInterfaceType == NetworkInterfaceType.Wireless80211 || ni.NetworkInterfaceType == NetworkInterfaceType.Ethernet)
                            {
                                // Initialize performance counter
                                InitializePerformanceCounter(ni);

                                // Returns an object that describes the configuration of this network interface.
                                IPProperties = ni.GetIPProperties();
                                // Gets the IP statistics for this System.Net.NetworkInformation.NetworkInterface instance.
                                IPStatistics = ni.GetIPStatistics();
                                // Gets the IPv4 statistics for this System.Net.NetworkInformation.NetworkInterface instance.
                                IPv4Statistics = ni.GetIPv4Statistics();
                                // Returns the Media Access Control (MAC) address or physical address for this adapter.
                                PhysicalAddress = ni.GetPhysicalAddress().ToString();

                                // Gets the description of the interface.
                                Description = ni.Description;
                                // Gets the identifier of the NIC.
                                Id = ni.Id;
                                // Gets a System.Boolean value that indicates whether the network interface is set to receive data packets.
                                IsReceiveOnly = ni.IsReceiveOnly;
                                // Gets the name of the NIC.
                                Name = ni.Name;
                                // Gets the interface type.
                                NetworkInterfaceType = ni.NetworkInterfaceType;
                                // Gets the current operating state of the network connection.
                                OperationalStatus = ni.OperationalStatus;
                                // Gets the speed of the network interface.
                                Speed = BytesToString(ni.Speed);
                                // Gets a System.Boolean value that indicates whether the network interface is enabled to receive multicast packets.
                                SupportsMulticast = ni.SupportsMulticast;

                                // Initialize optional network data.
                                foreach (ManagementObject obj in ObjectCollection)
                                {
                                    // 
                                    ActiveMaximumTransmissionUnit = obj["ActiveMaximumTransmissionUnit"];
                                    // 
                                    AdminLocked = obj["AdminLocked"];
                                    // 
                                    ComponentID = obj["ComponentID"];
                                    // 
                                    ConnectorPresent = obj["ConnectorPresent"];
                                    // 
                                    DeviceWakeUpEnable = obj["DeviceWakeUpEnable"];
                                    // 
                                    DriverDate = obj["DriverDate"];
                                    // 
                                    DriverName = obj["DriverName"];
                                    // 
                                    DriverVersionString = obj["DriverVersionString"];
                                    // 
                                    DriverProvider = obj["DriverProvider"];
                                    // 
                                    ReceiveLinkSpeed = obj["ReceiveLinkSpeed"];
                                    // 
                                    Virtual = obj["Virtual"];
                                }
                            }
                        }
                    }

                    // Make sure the IPInterfaceProperties is not null
                    foreach (GatewayIPAddressInformation obj in IPProperties?.GatewayAddresses)
                    {
                        // Maps the System.Net.IPAddress object to an IPv4 address.
                        if (obj.Address.AddressFamily == AddressFamily.InterNetwork)
                            GatewayAddressIpv4 = obj.Address.MapToIPv4();
                        // Maps the System.Net.IPAddress object to an IPv6 address.
                        if (obj.Address.AddressFamily == AddressFamily.InterNetworkV6)
                            GatewayAddressIpv6 = obj.Address.MapToIPv6();
                    }

                    // Gets the Domain Name System (DNS) suffix associated with this interface.
                    DnsSuffix = IPProperties.DnsSuffix;
                    // Gets the addresses of the Domain Name System (DNS) servers for this interface.
                    DnsAddresse = IPProperties.DnsAddresses.Where(x => x.AddressFamily == AddressFamily.InterNetwork).FirstOrDefault();
                };

                // Handles the RunWorkerCompleted event of the BackgroundWorker.
                worker.RunWorkerCompleted += delegate { ValuesInitialized = true; };

                // Start the BackgroundWorker.
                worker.RunWorkerAsync();
            };
        }

        private static bool IdentifyConnectorPresent(NetworkInterface ni)
        {
            var scope = new ManagementScope(@"\\localhost\root\StandardCimv2");
            var query = new ObjectQuery($"SELECT * FROM MSFT_NetAdapter WHERE ConnectorPresent = True AND DeviceID = '{ni.Id}'");
            var searcher = new ManagementObjectSearcher(scope, query);
            ObjectCollection = searcher.Get();
            searcher.Dispose();

            return ObjectCollection.Count > 0;
        }
    }
}
