﻿using Logic.Hardware.Model;
using System;
using System.ComponentModel;
using System.Management;

namespace Logic.Hardware
{
    // https://docs.microsoft.com/en-us/windows/win32/cimwin32prov/win32-operatingsystem

    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="Logic.Hardware.Model.Win32_OperatingSystemModel" />
    public class Win32_OperatingSystem : Win32_OperatingSystemModel
    {
        /// <summary>
        /// Gets the Win32_OperatingSystem data.
        /// </summary>
        public static void GetData()
        {
            // Reset value
            ValuesInitialized = false;

            using (var worker = new BackgroundWorker())
            {
                worker.WorkerReportsProgress = true;
                worker.WorkerSupportsCancellation = true;

                // Handles the DoWork event of the BackgroundWorker.
                worker.DoWork += delegate
                {
                    // Get operatingSystem propertys
                    using (ManagementObjectSearcher mos = new ManagementObjectSearcher("SELECT * FROM Win32_OperatingSystem"))
                    {
                        foreach (ManagementObject obj in mos?.Get())
                        {
                            // TODO: Convert size
                            TotalVisibleMemorySize = $"{Convert.ToInt64(obj["TotalVisibleMemorySize"]) / (1024 * 1024)} GB";
                            //
                            FreePhysicalMemory = $"{Convert.ToInt64(obj["FreePhysicalMemory"]) / (1024 * 1024)} GB";
                            //
                            SizeStoredInPagingFiles = $"{Convert.ToInt64(obj["SizeStoredInPagingFiles"]) / (1024 * 1024)} GB";
                            //
                            FreeSpaceInPagingFiles = $"{Convert.ToInt64(obj["FreeSpaceInPagingFiles"]) / (1024 * 1024)} GB";
                            //
                            TotalVirtualMemorySize = $"{Convert.ToInt64(obj["TotalVirtualMemorySize"]) / (1024 * 1024)} GB";
                            //
                            FreeVirtualMemory = $"{Convert.ToInt64(obj["FreeVirtualMemory"]) / (1024 * 1024)} GB";
                            //
                            MaxNumberOfProcesses = (uint)obj["MaxNumberOfProcesses"];
                            //
                            MaxProcessMemorySize = BytesToString(Convert.ToInt64(obj["MaxProcessMemorySize"]));
                        }
                    }
                };

                // Handles the RunWorkerCompleted event of the BackgroundWorker.
                worker.RunWorkerCompleted += delegate { ValuesInitialized = true; };

                // Start the BackgroundWorker.
                worker.RunWorkerAsync();
            };
        }
    }
}
