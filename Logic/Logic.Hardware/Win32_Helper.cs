﻿using System;
using System.Reflection;

namespace Logic.Hardware
{
    /// <summary>
    /// 
    /// </summary>
    public class Win32_Helper
    {
        /// <summary>
        /// Gets the version.
        /// </summary>
        public static string Version { get => Assembly.GetExecutingAssembly().GetName().Version.ToString(); }

        /// <summary>
        /// The convertvalue
        /// </summary>
        protected const ushort CONVERTVALUE = 0x400;

        /// <summary>
        /// Byte to string.
        /// </summary>
        /// <param name="byteCount">The byte count.</param>
        /// <returns></returns>
        protected static string BytesToString(long byteCount)
        {
            string[] suffix = { " B", " KB", " MB", " GB", " TB", " PB", " EB", " ZB", " YB" };

            if (byteCount == 0)
                return $"0 {suffix[0]}";

            long bytes = Math.Abs(byteCount);
            int place = Convert.ToInt32(Math.Floor(Math.Log(bytes, CONVERTVALUE)));
            double num = Math.Round(bytes / Math.Pow(CONVERTVALUE, place), 1);

            return (Math.Sign(byteCount) * num).ToString() + suffix[place];
        }
    }
}
