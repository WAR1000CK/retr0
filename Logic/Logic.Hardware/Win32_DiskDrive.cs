﻿using Logic.Hardware.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Management;
using System.Text.RegularExpressions;

namespace Logic.Hardware
{
    // https://docs.microsoft.com/en-us/windows/win32/cimwin32prov/win32-diskdrive

    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="Logic.Hardware.Model.Win32_DiskDriveModel" />
    public class Win32_DiskDrive : Win32_DiskDriveModel
    {
        /// <summary>
        /// Gets the Win32_DiskDrive data.
        /// </summary>
        public static void GetData()
        {
            // Reset value
            ValuesInitialized = false;

            using (var worker = new BackgroundWorker())
            {
                worker.WorkerReportsProgress = true;
                worker.WorkerSupportsCancellation = true;

                // Handles the DoWork event of the BackgroundWorker.
                worker.DoWork += delegate
                {
                    // // Initialize performance counter category
                    Category = new PerformanceCounterCategory(CATEGORY);

                    // Get the correct instance of the appropriate hard drive
                    foreach (string inst in Category.GetInstanceNames())
                    {
                        if (Regex.IsMatch(inst, Pattern, RegexOptions.IgnoreCase))
                        {
                            Id = DriveID(inst);
                            Letter = DriveLetter(inst);

                            // Initialize PeformanceCounter for systemdrive
                            InitializePerformanceCounter(DriveID(inst), DriveLetter(inst));
                        }
                    }

                    // Loop all drives
                    foreach (DriveInfo drive in DriveInfo.GetDrives())
                    {
                        // Storage all drives for later use
                        DriveInfos.Add(drive);

                        // Filter systemdrive
                        if (Regex.IsMatch(drive.Name, Pattern, RegexOptions.IgnoreCase))
                        {
                            // 
                            Name = drive.Name;
                            // 
                            VolumeLabel = drive.VolumeLabel;
                            // 
                            RootDirectory = drive.RootDirectory.ToString();
                            // 
                            DriveType = drive.DriveType.ToString();
                            // 
                            DriveFormat = drive.DriveFormat;
                            // 
                            IsReady = drive.IsReady;
                            // 
                            TotalAvailableSize = BytesToString(drive.TotalSize);
                            // 
                            TotalFreeSpace = BytesToString(drive.TotalFreeSpace);
                            // 
                            AvailableFreeSpace = BytesToString(drive.AvailableFreeSpace);
                        }
                    }

                    // Get more drive propertys for systemdrive
                    using (ManagementObjectSearcher mos = new ManagementObjectSearcher("SELECT * FROM Win32_DiskDrive"))
                    {
                        foreach (ManagementObject obj in mos?.Get())
                        {
                            // Filter systemdrive
                            if (obj["DeviceID"] as string == $@"\\.\PHYSICALDRIVE{Id}")
                            {
                                // 
                                DeviceId = (string)obj["DeviceID"];
                                // 
                                Model = (string)obj["Model"];
                                // 
                                Description = (string)obj["Description"];
                                // 
                                Manufacturer = (string)obj["Manufacturer"];
                                // 
                                MediaType = (string)obj["MediaType"];
                                // 
                                FirmwareRevision = (string)obj["FirmwareRevision"];
                                // 
                                SerialNumber = (string)obj["SerialNumber"];
                                // 
                                Partitions = (uint)obj["Partitions"];
                                // 
                                Size = Convert.ToString(obj["Size"]);
                            }
                        }
                    }
                };

                // Handles the RunWorkerCompleted event of the BackgroundWorker.
                worker.RunWorkerCompleted += delegate { ValuesInitialized = true; };

                // Start the BackgroundWorker.
                worker.RunWorkerAsync();
            };
        }
    }
}
