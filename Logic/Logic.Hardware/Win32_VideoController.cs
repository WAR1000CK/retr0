﻿using Logic.Hardware.Model;
using System.ComponentModel;
using System.Management;

namespace Logic.Hardware
{
    /// <summary>
    /// The Win32_VideoController WMI class represents the capabilities and management capacity of the video controller 
    /// on a computer system running Windows.
    /// </summary>
    public class Win32_VideoController : Win32_VideoControllerModel
    {
        /// <summary>
        /// Gets the Win32_VideoController data.
        /// </summary>
        public static void GetData()
        {
            // Reset value
            ValuesInitialized = false;

            using (var worker = new BackgroundWorker())
            {
                worker.WorkerReportsProgress = true;
                worker.WorkerSupportsCancellation = true;

                // Handles the DoWork event of the BackgroundWorker.
                worker.DoWork += delegate
                {
                    ObjectQuery query = new ObjectQuery("SELECT * FROM Win32_VideoController WHERE DeviceID=\"VideoController1\"");
                    using (ManagementObjectSearcher mos = new ManagementObjectSearcher(query))
                    {
                        foreach (ManagementObject obj in mos?.Get())
                        {
                            AcceleratorCapabilities = obj["AcceleratorCapabilities"];
                            AdapterCompatibility = obj["AdapterCompatibility"];
                            AdapterDACType = obj["AdapterDACType"];
                            AdapterRAM = obj["AdapterRAM"];
                            Availability = obj["Availability"];
                            Caption = obj["Caption"];
                            ColorTableEntries = obj["ColorTableEntries"];
                            ConfigManagerErrorCode = obj["ConfigManagerErrorCode"];
                            ConfigManagerUserConfig = obj["ConfigManagerUserConfig"];
                            CreationClassName = obj["CreationClassName"];
                            CurrentBitsPerPixel = obj["CurrentBitsPerPixel"];
                            CurrentHorizontalResolution = obj["CurrentHorizontalResolution"];
                            CurrentNumberOfColors = obj["CurrentNumberOfColors"];
                            CurrentNumberOfColumns = obj["CurrentNumberOfColumns"];
                            CurrentNumberOfRows = obj["CurrentNumberOfRows"];
                            CurrentRefreshRate = obj["CurrentRefreshRate"];
                            CurrentScanMode = obj["CurrentScanMode"];
                            CurrentVerticalResolution = obj["CurrentVerticalResolution"];
                            Description = obj["Description"];
                            DeviceID = obj["DeviceID"];
                            DeviceSpecificPens = obj["DeviceSpecificPens"];
                            DitherType = obj["DitherType"];
                            DriverDate = obj["DriverDate"];
                            DriverVersion = obj["DriverVersion"];
                            ErrorCleared = obj["ErrorCleared"];
                            ErrorDescription = obj["ErrorDescription"];
                            ICMIntent = obj["ICMIntent"];
                            ICMMethod = obj["ICMMethod"];
                            InfFilename = obj["InfFilename"];
                            InfSection = obj["InfSection"];
                            InstallDate = obj["InstallDate"];
                            InstalledDisplayDrivers = obj["InstalledDisplayDrivers"];
                            LastErrorCode = obj["LastErrorCode"];
                            MaxMemorySupported = obj["MaxMemorySupported"];
                            MaxNumberControlled = obj["MaxNumberControlled"];
                            MaxRefreshRate = obj["MaxRefreshRate"];
                            MinRefreshRate = obj["MinRefreshRate"];
                            Monochrome = obj["Monochrome"];
                            Name = obj["Name"];
                            NumberOfColorPlanes = obj["NumberOfColorPlanes"];
                            NumberOfVideoPages = obj["NumberOfVideoPages"];
                            PNPDeviceID = obj["PNPDeviceID"];
                            PowerManagementCapabilities = obj["PowerManagementCapabilities"];
                            PowerManagementSupported = obj["PowerManagementSupported"];
                            ProtocolSupported = obj["ProtocolSupported"];
                            ReservedSystemPaletteEntries = obj["ReservedSystemPaletteEntries"];
                            SpecificationVersion = obj["SpecificationVersion"];
                            Status = obj["Status"];
                            StatusInfo = obj["StatusInfo"];
                            SystemCreationClassName = obj["SystemCreationClassName"];
                            SystemName = obj["SystemName"];
                            SystemPaletteEntries = obj["SystemPaletteEntries"];
                            TimeOfLastReset = obj["TimeOfLastReset"];
                            VideoArchitecture = obj["VideoArchitecture"];
                            VideoMemoryType = obj["VideoMemoryType"];
                            VideoMode = obj["VideoMode"];
                            VideoModeDescription = obj["VideoModeDescription"];
                            VideoProcessor = obj["VideoProcessor"];
                        }
                    }
                };

                // Handles the RunWorkerCompleted event of the BackgroundWorker.
                worker.RunWorkerCompleted += delegate { ValuesInitialized = true; };

                // Start the BackgroundWorker.
                worker.RunWorkerAsync();
            };
        }
    }
}
