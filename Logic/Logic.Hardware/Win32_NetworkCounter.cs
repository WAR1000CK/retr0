﻿using System.Diagnostics;
using System.Net.NetworkInformation;

namespace Logic.Hardware
{
    /// <summary>
    /// 
    /// </summary>
    public class Win32_NetworkCounter : Win32_Helper
    {
        #region Private fields

        private const string CATEGORY = "Network Interface";
        private static PerformanceCounter _upload;
        private static PerformanceCounter _download;
        private static PerformanceCounter _transfer;

        #endregion

        /// <summary>
        /// Initialize performance counter
        /// </summary>
        /// <param name="ni"></param>
        protected static void InitializePerformanceCounter(NetworkInterface ni)
        {
            _upload = new PerformanceCounter(CATEGORY, "Bytes Sent/sec", ni?.Description, true);
            _download = new PerformanceCounter(CATEGORY, "Bytes Received/sec", ni?.Description, true);
            _transfer = new PerformanceCounter(CATEGORY, "Bytes Total/sec", ni?.Description, true);
        }

        /// <summary>
        /// Gets the converted size Received/sec.
        /// </summary>
        public static string Download => BytesToString((long)_download.NextValue());

        /// <summary>
        /// Gets the converted size Sent/sec.
        /// </summary>
        public static string Upload => BytesToString((long)_upload.NextValue());

        /// <summary>
        /// Gets the converted size Total/sec.
        /// </summary>
        public static string Transfer => BytesToString((long)_transfer.NextValue());
    }
}
