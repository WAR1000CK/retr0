﻿using Logic.Hardware.Model;
using System;
using System.ComponentModel;
using System.Management;

namespace Logic.Hardware
{
    // https://docs.microsoft.com/en-us/windows/win32/cimwin32prov/win32-battery

    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="Logic.Hardware.Model.Win32_BatteryModel" />
    public class Win32_Battery : Win32_BatteryModel
    {
        /// <summary>
        /// Gets the Win32_Battery data.
        /// </summary>
        public static void GetData()
        {
            // Reset value
            ValuesInitialized = false;

            using (var worker = new BackgroundWorker())
            {
                worker.WorkerReportsProgress = true;
                worker.WorkerSupportsCancellation = true;

                // Handles the DoWork event of the BackgroundWorker.
                worker.DoWork += delegate
                {
                    // Get battery propertys
                    using (ManagementObjectSearcher mos = new ManagementObjectSearcher("SELECT * FROM Win32_Battery"))
                    {
                        foreach (ManagementObject obj in mos?.Get())
                        {
                            Description = (string)obj["Description"];
                            Name = (string)obj["Name"];
                            Availability = (string)obj["Availability"];
                            Status = Convert.ToString(obj["BatteryStatus"]);
                            Caption = (string)obj["Caption"];
                            Chemistry = (string)obj["Chemistry"];
                            DesignVoltage = $"{obj["DesignVoltage"]} mAh";
                            DeviceID = (string)obj["DeviceID"];
                            EstimatedChargeRemaining = $"{obj["EstimatedChargeRemaining"]} %";
                            EstimatedRunTime = (string)obj["EstimatedRunTime"];
                            PowerManagementCapabilities = (string)obj["PowerManagementCapabilities"];
                            PowerManagementSupported = (bool)obj["PowerManagementSupported"];
                            SystemName = (string)obj["SystemName"];
                        }
                    }
                };

                // Handles the RunWorkerCompleted event of the BackgroundWorker.
                worker.RunWorkerCompleted += delegate { ValuesInitialized = true; };

                // Start the BackgroundWorker.
                worker.RunWorkerAsync();
            };
        }
    }
}
