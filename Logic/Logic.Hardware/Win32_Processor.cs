﻿using Logic.Hardware.Model;
using System;
using System.ComponentModel;
using System.Management;

namespace Logic.Hardware
{
    /// <summary>
    /// The Win32_Processor WMI class represents a device that can interpret a sequence of instructions on a computer 
    /// running on a Windows operating system.
    /// </summary>
    public class Win32_Processor : Win32_ProcessorModel
    {
        /// <summary>
        /// Gets the Win32_Processor data.
        /// </summary>
        public static void GetData()
        {
            // Reset value
            ValuesInitialized = false;

            using (var worker = new BackgroundWorker())
            {
                worker.WorkerReportsProgress = true;
                worker.WorkerSupportsCancellation = true;

                // Handles the DoWork event of the BackgroundWorker.
                worker.DoWork += delegate
                {
                    // Initialize PeformanceCounter
                    InitializePerformanceCounter();

                    using (ManagementObjectSearcher mos = new ManagementObjectSearcher("SELECT * FROM Win32_Processor"))
                    {
                        foreach (ManagementObject obj in mos?.Get())
                        {
                            // 
                            Name = (string)obj["Name"];
                            // 
                            Architecture = (ushort)obj["Architecture"];
                            // 
                            MaxClockSpeed = Convert.ToString(obj["MaxClockSpeed"]);
                            // 
                            ExtClock = $"{(uint)obj["ExtClock"]} Mhz";
                            // 
                            NumberOfCores = (uint)obj["NumberOfCores"];
                            // 
                            NumberOfEnabledCore = (uint)obj["NumberOfEnabledCore"];
                            // 
                            NumberOfLogicalProcessors = (uint)obj["NumberOfLogicalProcessors"];
                            // 
                            ThreadCount = (uint)obj["ThreadCount"];
                            // 
                            L2CacheSize = Convert.ToString(obj["L2CacheSize"]);
                            // 
                            L3CacheSize = Convert.ToString(obj["L3CacheSize"]);
                            // 
                            ProcessorId = (string)obj["ProcessorId"];
                            // 
                            ProcessorType = (ushort)obj["ProcessorType"];
                            // 
                            SocketDesignation = (string)obj["SocketDesignation"];
                            // 
                            VirtualizationFirmwareEnabled = (bool)obj["VirtualizationFirmwareEnabled"] == true ? "Aktiviert" : "Deaktiviert";
                            // 
                            VMMonitorModeExtensions = (bool)obj["VMMonitorModeExtensions"] == true ? "Aktiviert" : "Deaktiviert";
                        }
                    }
                };

                // Handles the RunWorkerCompleted event of the BackgroundWorker.
                worker.RunWorkerCompleted += delegate { ValuesInitialized = true; };

                // Start the BackgroundWorker.
                worker.RunWorkerAsync();
            };
        }
    }
}
