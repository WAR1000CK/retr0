﻿namespace Logic.Hardware.Enums
{
    /// <summary>
    /// Auswahl der RAM Spannung
    /// </summary>
    public enum Voltage
    {
        /// <summary>
        /// Minimale Spannung
        /// </summary>
        Min = 1,

        /// <summary>
        /// Maximale Spannung
        /// </summary>
        Max = 2,

        /// <summary>
        /// Eingestellte Spannung
        /// </summary>
        Configured = 3
    }
}
