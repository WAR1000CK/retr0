﻿namespace Logic.Hardware.Enums
{
    /// <summary>
    /// Auswahl der Größeneinheiten
    /// </summary>
    public enum SizeUnit
    {
        /// <summary>
        /// Ausgabe in Byte
        /// </summary>
        Byte = 1,

        /// <summary>
        /// Ausgabe in Kilobyte
        /// </summary>
        KB = 2,

        /// <summary>
        /// Ausgabe in Megabyte
        /// </summary>
        MB = 3,

        /// <summary>
        /// Ausgabe in Gigabyte
        /// </summary>
        GB = 4,

        /// <summary>
        /// Ausgabe in Terabyte
        /// </summary>
        TB = 5,

        /// <summary>
        /// Ausgabe in Petabyte
        /// </summary>
        PB = 6,

        /// <summary>
        /// Ausgabe in Exabyte
        /// </summary>
        EB = 7
    }
}
