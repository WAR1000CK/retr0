﻿namespace Logic.Hardware.Enums
{
    /// <summary>
    /// Auswahl des CPU Cache Levels
    /// </summary>
    public enum CacheLevel
    {
        /// <summary>
        /// Enthält am häufigsten benötigte Befehle und Daten. Getrennt für Befehle und Daten.
        /// </summary>
        L1Cache = 1,

        /// <summary>
        /// Puffer für Arbeitsspeicher. Beschleunigt die gleichzeitige Ausführung mehrerer Programme.
        /// </summary>
        L2Cache = 2,

        /// <summary>
        /// Abgleich der Caches aller Kerne. Datenkonsistenz und Datenaustausch zwischen Kernen.
        /// </summary>
        L3Cache = 3
    }
}
