﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading.Tasks;

namespace Logic.Hardware.Model
{
    // https://docs.microsoft.com/en-us/windows/win32/cimwin32prov/win32-motherboarddevice

    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="Logic.Hardware.Win32_Helper" />
    public class Win32_MotherboardModel : Win32_Helper
    {
        /// <summary>
        /// Gets or sets a value indicating whether [values initialized].
        /// </summary>
        public static bool ValuesInitialized { get; protected set; } = false;

    }
}
