﻿namespace Logic.Hardware.Model
{
    /*
       Source:
       https://docs.microsoft.com/en-us/windows/win32/cimwin32prov/win32-videocontroller
    */
    /// <summary>
    /// The <see cref="Win32_VideoController"/> WMI class represents the capabilities and management capacity of the video 
    /// controller on a computer system running Windows.
    /// </summary>
    public class Win32_VideoControllerModel : Win32_Helper
    {
        public static bool ValuesInitialized { get; protected set; } = false;


        /*  AcceleratorCapabilities:
            Unknown (0)
            Other (1)
            Graphics Accelerator (2)
            3D Accelerator (3)
        */
        /// <summary>
        /// Array of graphics and 3-D capabilities of the video controller.
        /// </summary>
        public static object AcceleratorCapabilities { get; protected set; }

        /// <summary>
        /// General chipset used for this controller to compare compatibilities with the system.
        /// </summary>
        public static object AdapterCompatibility { get; protected set; }

        /// <summary>
        /// Name or identifier of the digital-to-analog converter (DAC) chip. 
        /// The character set of this property is alphanumeric.
        /// </summary>
        public static object AdapterDACType { get; protected set; }

        /// <summary>
        /// Memory size of the video adapter.
        /// <code>Example: 64000 bytes</code>
        /// </summary>
        public static object AdapterRAM { get; protected set; }

        /*  Availability:
            Other (1)
            Unknown (2)
            
            Running/Full Power (3)
                Running or Full Power
            
            Warning (4)
            In Test (5)
            Not Applicable (6)
            Power Off (7)
            Off Line (8)
                Offline
            
            Off Duty (9)
            Degraded (10)
            Not Installed (11)
            Install Error (12)
            Power Save - Unknown (13)
                The device is known to be in a power save mode, but its exact status is unknown.
            
            Power Save - Low Power Mode (14)
                The device is in a power save state but still functioning, and may exhibit degraded performance.
            
            Power Save - Standby (15)
                The device is not functioning, but could be brought to full power quickly.
            
            Power Cycle (16)
            Power Save - Warning (17)
                The device is in a warning state, though also in a power save mode.
            
            Paused (18)
                The device is paused.
            
            Not Ready (19)
                The device is not ready.
            
            Not Configured (20)
                The device is not configured.
            
            Quiesced (21)
                The device is quiet.
        */
        /// <summary>
        /// Availability and status of the device.
        /// </summary>
        public static object Availability { get; protected set; }

        /// <summary>
        /// Free-form strings providing more detailed explanations for any of the video accelerator features indicated in the 
        /// <see cref="AcceleratorCapabilities"/> array. Note, each entry of this array is related to the entry in the 
        /// <see cref="AcceleratorCapabilities"/> array that is located at the same index.
        /// </summary>
        public static object CapabilityDescriptions { get; protected set; }

        /// <summary>
        /// Short description of the object.
        /// </summary>
        public static object Caption { get; protected set; }

        /// <summary>
        /// Size of the system's color table. The device must have a color depth of no more than 8 bits per pixel; 
        /// otherwise, this property is not set.
        /// <code>Example: 256</code>
        /// </summary>
        public static object ColorTableEntries { get; protected set; }

        /*  ConfigManagerErrorCode:
            This device is working properly. (0)
                Device is working properly.

            This device is not configured correctly. (1)
                Device is not configured correctly.
            
            Windows cannot load the driver for this device. (2)
            
            The driver for this device might be corrupted, or your system may be running low on memory or other resources. (3)
                Driver for this device might be corrupted, or the system may be low on memory or other resources.
            
            This device is not working properly. One of its drivers or your registry might be corrupted. (4)
                Device is not working properly. One of its drivers or the registry might be corrupted.
            
            The driver for this device needs a resource that Windows cannot manage. (5)
                Driver for the device requires a resource that Windows cannot manage.
            
            The boot configuration for this device conflicts with other devices. (6)
                Boot configuration for the device conflicts with other devices.
            
            Cannot filter. (7)
            
            The driver loader for the device is missing. (8)
                Driver loader for the device is missing.
            
            This device is not working properly because the controlling firmware is reporting the resources for the device incorrectly. (9)
                Device is not working properly. The controlling firmware is incorrectly reporting the resources for the device.
            
            This device cannot start. (10)
                Device cannot start.
            
            This device failed. (11)
                Device failed.
            
            This device cannot find enough free resources that it can use. (12)
                Device cannot find enough free resources to use.
            
            Windows cannot verify this device's resources. (13)
                Windows cannot verify the device's resources.
            
            This device cannot work properly until you restart your computer. (14)
                Device cannot work properly until the computer is restarted.
            
            This device is not working properly because there is probably a re-enumeration problem. (15)
                Device is not working properly due to a possible re-enumeration problem.
            
            Windows cannot identify all the resources this device uses. (16)
                Windows cannot identify all of the resources that the device uses.
            
            This device is asking for an unknown resource type. (17)
                Device is requesting an unknown resource type.
            
            Reinstall the drivers for this device. (18)
                Device drivers must be reinstalled.
            
            Failure using the VxD loader. (19)
            
            Your registry might be corrupted. (20)
                Registry might be corrupted.
            
            System failure: Try changing the driver for this device. If that does not work, see your hardware documentation. Windows is removing this device. (21)
                System failure. If changing the device driver is ineffective, see the hardware documentation. Windows is removing the device.
            
            This device is disabled. (22)
                Device is disabled.
            
            System failure: Try changing the driver for this device. If that doesn't work, see your hardware documentation. (23)
                System failure. If changing the device driver is ineffective, see the hardware documentation.
            
            This device is not present, is not working properly, or does not have all its drivers installed. (24)
                Device is not present, not working properly, or does not have all of its drivers installed.
            
            Windows is still setting up this device. (25)
                Windows is still setting up the device.
            
            Windows is still setting up this device. (26)
                Windows is still setting up the device.
            
            This device does not have valid log configuration. (27)
                Device does not have valid log configuration.
            
            The drivers for this device are not installed. (28)
                Device drivers are not installed.
            
            This device is disabled because the firmware of the device did not give it the required resources. (29)
                Device is disabled. The device firmware did not provide the required resources.
            
            This device is using an Interrupt Request (IRQ) resource that another device is using. (30)
                Device is using an IRQ resource that another device is using.
            
            This device is not working properly because Windows cannot load the drivers required for this device. (31)
        */
        /// <summary>
        /// Win32 Configuration Manager error code.
        /// </summary>
        public static object ConfigManagerErrorCode { get; protected set; }

        /// <summary>
        /// If TRUE, the device is using a user-defined configuration.
        /// </summary>
        public static object ConfigManagerUserConfig { get; protected set; }

        /// <summary>
        /// Name of the first concrete class to appear in the inheritance chain used in the creation of an instance. When used 
        /// with the other key properties of the class, this property allows all instances of this class and its subclasses to be 
        /// uniquely identified.
        /// </summary>
        public static object CreationClassName { get; protected set; }

        /// <summary>
        /// Number of bits used to display each pixel.
        /// </summary>
        public static object CurrentBitsPerPixel { get; protected set; }

        /// <summary>
        /// Current number of horizontal pixels.
        /// </summary>
        public static object CurrentHorizontalResolution { get; protected set; }

        /// <summary>
        /// Number of colors supported at the current resolution.
        /// </summary>
        public static object CurrentNumberOfColors { get; protected set; }

        /// <summary>
        /// Number of columns for this video controller (if in character mode). Otherwise, enter 0 (zero).
        /// </summary>
        public static object CurrentNumberOfColumns { get; protected set; }

        /// <summary>
        /// Number of rows for this video controller (if in character mode). Otherwise, enter 0 (zero).
        /// </summary>
        public static object CurrentNumberOfRows { get; protected set; }

        /// <summary>
        /// Frequency at which the video controller refreshes the image for the monitor. 
        /// A value of 0 (zero) indicates the default rate is being used, while 0xFFFFFFFF indicates the optimal rate is being used.
        /// </summary>
        public static object CurrentRefreshRate { get; protected set; }

        /*  CurrentScanMode:
            Other (1)
            Unknown (2)
            Interlaced (3)
            Non Interlaced (4)
        */
        /// <summary>
        /// Current scan mode.
        /// </summary>
        public static object CurrentScanMode { get; protected set; }

        /// <summary>
        /// Current number of vertical pixels.
        /// </summary>
        public static object CurrentVerticalResolution { get; protected set; }

        /// <summary>
        /// Description of the object.
        /// </summary>
        public static object Description { get; protected set; }

        /// <summary>
        /// Identifier (unique to the computer system) for this video controller.
        /// </summary>
        public static object DeviceID { get; protected set; }

        /// <summary>
        /// Current number of device-specific pens. A value of 0xffff means that the device does not support pens.
        /// <code>Example: 3</code>
        /// </summary>
        public static object DeviceSpecificPens { get; protected set; }

        /*  DitherType:
            No dithering (1)
            Dithering with a coarse brush (2)
            Dithering with a fine brush (3)
            Line art dithering (4)
            Device does gray scaling (5)
        */
        /// <summary>
        /// Dither type of the video controller. The property can be one of the predefined values, or a driver-defined value 
        /// greater than or equal to 256. If line art dithering is chosen, the controller uses a dithering method that produces 
        /// well-defined borders between black, white, and gray scalings. Line art dithering is not suitable for images that 
        /// include continuous graduations in intensity and hue such as scanned photographs.
        /// </summary>
        public static object DitherType { get; protected set; }

        /// <summary>
        /// Last modification date and time of the currently installed video driver.
        /// </summary>
        public static object DriverDate { get; protected set; }

        /// <summary>
        /// Version number of the video driver.
        /// </summary>
        public static object DriverVersion { get; protected set; }

        /// <summary>
        /// If TRUE, the error reported in <see cref="LastErrorCode"/> property is now cleared.
        /// </summary>
        public static object ErrorCleared { get; protected set; }

        /// <summary>
        /// Free-form string supplying more information about the error recorded in <see cref="LastErrorCode"/> property, and 
        /// information on any corrective actions that may be taken.
        /// </summary>
        public static object ErrorDescription { get; protected set; }

        /*  ICMIntent:
            Saturation (1)
            Contrast (2)
            Exact Color (3)
        */
        /// <summary>
        /// Specific value of one of the three possible color-matching methods or intents that should be used by default. 
        /// This property is used primarily for non-ICM applications. ICM applications establish intents by using the ICM 
        /// functions. This property can be a predefined value or a driver defined value greater than or equal to 256. Color 
        /// matching based on saturation is the most appropriate choice for business graphs when dithering is not desired. 
        /// Color matching based on contrast is the most appropriate choice for scanned or photographic images when 
        /// dithering is desired. Color matching optimized to match the exact color requested is most appropriate for use 
        /// with business logos or other images when an exact color match is desired.
        /// </summary>
        public static object ICMIntent { get; protected set; }

        /*  ICMMethod:
            Disabled (1)
            Windows (2)
            Device Driver (3)
            Destination Device (4)
        */
        /// <summary>
        /// Method of handling ICM. For non-ICM applications, this property determines if ICM is enabled. For ICM 
        /// applications, the system examines this property to determine how to handle ICM support. This property can be a 
        /// predefined value or a driver-defined value greater than or equal to 256. The value determines which system 
        /// handles image color matching.
        /// </summary>
        public static object ICMMethod { get; protected set; }

        /// <summary>
        /// Path to the video adapter's .inf file.
        /// <code>
        /// Example: "C:\Windows\SYSTEM32\DRIVERS"
        /// </code>
        /// </summary>
        public static object InfFilename { get; protected set; }

        /// <summary>
        /// Section of the .inf file where the Windows video information resides.
        /// </summary>
        public static object InfSection { get; protected set; }

        /// <summary>
        /// Date and time the object was installed. This property does not need a value to indicate that the object is installed.
        /// </summary>
        public static object InstallDate { get; protected set; }

        /// <summary>
        /// Name of the installed display device driver.
        /// </summary>
        public static object InstalledDisplayDrivers { get; protected set; }

        /// <summary>
        /// Last error code reported by the logical device.
        /// </summary>
        public static object LastErrorCode { get; protected set; }

        /// <summary>
        /// Maximum amount of memory supported in bytes.
        /// </summary>
        public static object MaxMemorySupported { get; protected set; }

        /// <summary>
        /// Maximum number of directly addressable entities supportable by this controller. 
        /// A value of 0 (zero) should be used if the number is unknown.
        /// </summary>
        public static object MaxNumberControlled { get; protected set; }

        /// <summary>
        /// Maximum refresh rate of the video controller in hertz.
        /// </summary>
        public static object MaxRefreshRate { get; protected set; }

        /// <summary>
        /// Minimum refresh rate of the video controller in hertz.
        /// </summary>
        public static object MinRefreshRate { get; protected set; }

        /// <summary>
        /// If TRUE, gray scale is used to display images.
        /// </summary>
        public static object Monochrome { get; protected set; }

        /// <summary>
        /// Label by which the object is known. When subclassed, the property can be overridden to be a key property.
        /// </summary>
        public static object Name { get; protected set; }

        /// <summary>
        /// Current number of color planes. If this value is not applicable for the current video configuration, enter 0 (zero).
        /// </summary>
        public static object NumberOfColorPlanes { get; protected set; }

        /// <summary>
        /// Number of video pages supported given the current resolutions and available memory.
        /// </summary>
        public static object NumberOfVideoPages { get; protected set; }

        /// <summary>
        /// Windows Plug and Play device identifier of the logical device.
        /// <code>
        /// Example: "*PNP030b"
        /// </code>
        /// </summary>
        public static object PNPDeviceID { get; protected set; }

        /*  PowerManagementCapabilities:
            Unknown (0)
            Not Supported (1)
            Disabled (2)
            Enabled (3)
                The power management features are currently enabled but the exact feature set is unknown or the information is unavailable.

            Power Saving Modes Entered Automatically (4)
                The device can change its power state based on usage or other criteria.

            Power State Settable (5)
                The SetPowerState method is supported. This method is found on the parent CIM_LogicalDevice class and can be implemented. For more information, see Designing Managed Object Format (MOF) Classes.
            
            Power Cycling Supported (6)
                The SetPowerState method can be invoked with the PowerState parameter set to 5 (Power Cycle).

            Timed Power On Supported (7)
        */
        /// <summary>
        /// Array of the specific power-related capabilities of a logical device.
        /// </summary>
        public static object PowerManagementCapabilities { get; protected set; }

        /// <summary>
        /// If TRUE, the device can be power-managed (can be put into suspend mode, and so on). The property does not 
        /// indicate that power management features are currently enabled, only that the logical device is capable of power 
        /// management.
        /// </summary>
        public static object PowerManagementSupported { get; protected set; }

        /*  ProtocolSupported:
            Other (1)
            Unknown (2)
            EISA (3)
            ISA (4)
            PCI (5)
            ATA/ATAPI (6)
            Flexible Diskette (7)
            1496 (8)
            SCSI Parallel Interface (9)
            SCSI Fibre Channel Protocol (10)
            SCSI Serial Bus Protocol (11)
            SCSI Serial Bus Protocol-2 (1394) (12)
            SCSI Serial Storage Architecture (13)
            VESA (14)
            PCMCIA (15)
            Universal Serial Bus (16)
            Parallel Protocol (17)
            ESCON (18)
            Diagnostic (19)
            I2C (20)
            Power (21)
            HIPPI (22)
            MultiBus (23)
            VME (24)
            IPI (25)
            IEEE-488 (26)
            RS232 (27)
            IEEE 802.3 10BASE5 (28)
            IEEE 802.3 10BASE2 (29)
            IEEE 802.3 1BASE5 (30)
            IEEE 802.3 10BROAD36 (31)
            IEEE 802.3 100BASEVG (32)
            IEEE 802.5 Token-Ring (33)
            ANSI X3T9.5 FDDI (34)
            MCA (35)
            ESDI (36)
            IDE (37)
            CMD (38)
            ST506 (39)
            DSSI (40)
            QIC2 (41)
            Enhanced ATA/IDE (42)
            AGP (43)
            TWIRP (two-way infrared) (44)
            FIR (fast infrared) (45)
            SIR (serial infrared) (46)
            IrBus (47)
        */
        /// <summary>
        /// Protocol used by the controller to access "controlled" devices.
        /// </summary>
        public static object ProtocolSupported { get; protected set; }

        /// <summary>
        /// Number of reserved entries in the system palette. The operating system may reserve entries to support standard 
        /// colors for task bars and other desktop display items. This index is valid only if the device driver sets the 
        /// RC_PALETTE bit in the RASTERCAPS index, and is available only if the driver is compatible with 16-bit Windows. If 
        /// the system is not using a palette, ReservedSystemPaletteEntries is not set.
        /// <code>
        /// Example: 20
        /// </code>
        /// </summary>
        public static object ReservedSystemPaletteEntries { get; protected set; }

        /// <summary>
        /// Version number of the initialization data specification (upon which the structure is based).
        /// </summary>
        public static object SpecificationVersion { get; protected set; }

        /// <summary>
        /// Current status of the object. Various operational and nonoperational statuses can be defined.
        /// <code>
        /// Operational statuses include: "OK", "Degraded", and "Pred Fail"
        /// </code>
        /// (an element, such as a SMART-enabled hard disk drive, may 
        /// be functioning properly but predicting a failure in the near future). 
        /// <code>
        /// Nonoperational statuses include: "Error", "Starting", "Stopping", and "Service"
        /// </code>
        /// The latter, "Service", could apply during mirror-resilvering of a disk, reload of a user permissions list, or other 
        /// administrative work. Not all such work is online, yet the managed element is neither "OK" nor in one of the other states.
        /// </summary>
        public static object Status { get; protected set; }

        /*  StatusInfo:
            Other (1)
            Unknown (2)
            Enabled (3)
            Disabled (4)
            Not Applicable (5)
        */
        /// <summary>
        /// State of the logical device. If this property does not apply to the logical device, the value 5 (Not Applicable) 
        /// should be used.
        /// </summary>
        public static object StatusInfo { get; protected set; }

        /// <summary>
        /// Value of the scoping computer's <see cref="CreationClassName"/> property.
        /// </summary>
        public static object SystemCreationClassName { get; protected set; }

        /// <summary>
        /// Name of the scoping system.
        /// </summary>
        public static object SystemName { get; protected set; }

        /// <summary>
        /// Current number of color index entries in the system palette. This index is valid only if the device driver sets the 
        /// RC_PALETTE bit in the RASTERCAPS index, and is available only if the driver is compatible with 16-bit Windows. 
        /// If the system is not using a palette, SystemPaletteEntries is not set.
        /// <code>
        /// Example: 20
        /// </code>
        /// </summary>
        public static object SystemPaletteEntries { get; protected set; }

        /// <summary>
        /// Date and time this controller was last reset. This could mean the controller was powered down or reinitialized.
        /// </summary>
        public static object TimeOfLastReset { get; protected set; }

        /*  VideoArchitecture:
            Other (1)
            Unknown (2)
            CGA (3)
            EGA (4)
            VGA (5)
            SVGA (6)
            MDA (7)
            HGC (8)
            MCGA (9)
            8514A (10)
            XGA (11)
            Linear Frame Buffer (12)
            PC-98 (160)
        */
        /// <summary>
        /// Type of video architecture.
        /// </summary>
        public static object VideoArchitecture { get; protected set; }

        /*  VideoMemoryType:
            Other (1)
            Unknown (2)
            VRAM (3)
            DRAM (4)
            SRAM (5)
            WRAM (6)
            EDO RAM (7)
            Burst Synchronous DRAM (8)
            Pipelined Burst SRAM (9)
            CDRAM (10)
            3DRAM (11)
            SDRAM (12)
            SGRAM (13)
        */
        /// <summary>
        /// Type of video memory.
        /// </summary>
        public static object VideoMemoryType { get; protected set; }

        /// <summary>
        /// Current video mode.
        /// </summary>
        public static object VideoMode { get; protected set; }

        /// <summary>
        /// Current resolution, color, and scan mode settings of the video controller.
        /// <code>
        /// Example: "1024 x 768 x 256 colors"
        /// </code>
        /// </summary>
        public static object VideoModeDescription { get; protected set; }

        /// <summary>
        /// Free-form string describing the video processor.
        /// </summary>
        public static object VideoProcessor { get; protected set; }
    }
}
