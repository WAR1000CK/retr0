﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace Logic.Hardware.Model
{
    /*
       Source:
       https://docs.microsoft.com/en-us/windows/win32/cimwin32prov/win32-diskdrive
    */
    /// <summary>
    /// The <see cref="Win32_DiskDrive"/> WMI class represents a physical disk drive as seen by a computer running the Windows 
    /// operating system.
    /// </summary>
    public class Win32_DiskDriveModel : Win32_Helper
    {
        #region Private fields

        private static PerformanceCounter _read;
        private static PerformanceCounter _avgRead;
        private static PerformanceCounter _write;
        private static PerformanceCounter _avgWrite;
        private static PerformanceCounter _diskReadTime;
        private static PerformanceCounter _idleTime;
        private static PerformanceCounter _avgQueueLength;
        private static PerformanceCounter _diskTime;

        #endregion

        /// <summary>
        /// Gets or sets a value indicating whether [values initialized].
        /// </summary>
        public static bool ValuesInitialized { get; protected set; } = false;

        /// <summary>
        /// 
        /// </summary>
        protected const string CATEGORY = "PhysicalDisk";

        /// <summary>
        /// 
        /// </summary>
        protected static PerformanceCounterCategory Category;

        /// <summary>
        /// Gets or sets the drive infos.
        /// </summary>
        protected static List<DriveInfo> DriveInfos = new List<DriveInfo>();

        /// <summary>
        /// 
        /// </summary>
        public static string Id { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public static string Letter { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public static object SystemDrive => Path.GetPathRoot(Environment.SystemDirectory).Contains(Letter) ? "Ja" : "Nein";

        /// <summary>
        /// 
        /// </summary>
        public static object Name { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public static object DeviceId { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public static object Description { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public static object Manufacturer { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public static object MediaType { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public static object FirmwareRevision { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public static object Model { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public static object VolumeLabel { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public static object RootDirectory { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public static object DriveType { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public static object DriveFormat { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public static object IsReady { get; protected set; }

        private static long _size;
        public static object Size
        {
            get => BytesToString(_size);
            protected set { _size = Convert.ToInt64(value); }
        }

        /// <summary>
        /// 
        /// </summary>
        public static object TotalAvailableSize { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public static object TotalFreeSpace { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public static object AvailableFreeSpace { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public static object SerialNumber { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public static object Partitions { get; protected set; }


        /// <summary>
        /// 
        /// </summary>
        public static object DiskRead => BytesToString((long)_read.NextValue());

        /// <summary>
        /// 
        /// </summary>
        public static object AvgDiskRead => BytesToString((long)_avgRead.NextValue());

        /// <summary>
        /// 
        /// </summary>
        public static object DiskWrite => BytesToString((long)_write.NextValue());

        /// <summary>
        /// 
        /// </summary>
        public static object AvgDiskWrite => BytesToString((long)_avgWrite.NextValue());

        /// <summary>
        /// 
        /// </summary>
        public static object DiskReadTime => $"{_diskReadTime.NextValue()} %";

        /// <summary>
        /// 
        /// </summary>
        public static object IdleTime => $"{_idleTime.NextValue()} %";

        /// <summary>
        /// 
        /// </summary>
        public static object AvgDiskQueueLength => _avgQueueLength.NextValue();

        /// <summary>
        /// 
        /// </summary>
        public static object DiskTime => $"{_diskTime.NextValue()} %";

        /// <summary>
        /// Gets the Systemdrive.
        /// </summary>
        protected static string Pattern =>
            Path.GetPathRoot(Environment.GetFolderPath(Environment.SpecialFolder.System)).Insert(1, "\\").Remove(3, 1);

        /// <summary>
        /// 
        /// </summary>
        protected static string DriveID(string instance) => instance?.Substring(0, 1);

        /// <summary>
        /// 
        /// </summary>
        protected static string DriveLetter(string instance) => instance?.Substring(2, 2);

        /// <summary>
        /// 
        /// </summary>
        protected static void InitializePerformanceCounter(string id, string letter)
        {
            _read = new PerformanceCounter(CATEGORY, "Disk Read Bytes/sec", $"{id} {letter}", true);
            _write = new PerformanceCounter(CATEGORY, "Disk Write Bytes/sec", $"{id} {letter}", true);
            _avgRead = new PerformanceCounter(CATEGORY, "Avg. Disk sec/Read", $"{id} {letter}", true);
            _avgWrite = new PerformanceCounter(CATEGORY, "Avg. Disk sec/Write", $"{id} {letter}", true);
            _diskReadTime = new PerformanceCounter(CATEGORY, "% disk read time", $"{id} {letter}", true);
            _idleTime = new PerformanceCounter(CATEGORY, "% idle time", $"{id} {letter}", true);
            _avgQueueLength = new PerformanceCounter(CATEGORY, "Avg. Disk Queue Length", $"{id} {letter}", true);
            _diskTime = new PerformanceCounter(CATEGORY, "% Disk Time", $"{id} {letter}", true);
        }
    }
}
