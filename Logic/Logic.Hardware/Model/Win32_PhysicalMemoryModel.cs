﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Logic.Hardware.Model
{
    /*
       Source:
       https://docs.microsoft.com/en-us/windows/win32/cimwin32prov/win32-physicalmemory
    */
    /// <summary>
    /// The <see cref="Win32_PhysicalMemory"/> WMI class represents a physical memory device located on a computer system and 
    /// available to the operating system.
    /// </summary>
    public class Win32_PhysicalMemoryModel : Win32_Helper
    {
        #region Private fields

        private static uint _configuredClockSpeed;
        private static uint _configuredVoltage;
        private static ushort _formFactor;
        private static object _manufacturer;
        private static uint _maxVoltage;
        private static uint _minVoltage;
        private static object _serialNumber;

        private const string CATEGORY = "Memory";
        private static PerformanceCounter _memoryAvailable;
        private static PerformanceCounter _memoryCommitted;
        private static PerformanceCounter _memoryCommitLimit;
        private static PerformanceCounter _memoryCommitInUse;
        private static PerformanceCounter _memoryCachedFaults;
        private static PerformanceCounter _memoryCachedBytes;
        private static PerformanceCounter _memoryPollPaged;
        private static PerformanceCounter _memoryPollNonPaged;

        #endregion

        public static bool ValuesInitialized { get; protected set; } = false;


        /// <summary>
        /// SMBIOS - Type 17 - Attributes. Represents the RANK.
        /// </summary>
        public static object Attributes { get; protected set; }

        /// <summary>
        /// Physically labeled bank where the memory is located.
        /// <code>
        /// Examples: "Bank 0", "Bank A"
        /// </code>
        /// </summary>
        public static object BankLabel { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        internal static long CapacityInBytes { get; set; }

        /// <summary>
        /// Total capacity of the physical memory—in bytes.
        /// </summary>
        public static object Capacity { get => BytesToString(CapacityInBytes); }

        /// <summary>
        /// Short description of the object—a one-line string.
        /// </summary>
        public static object Caption { get; protected set; }

        /// <summary>
        /// The configured clock speed of the memory device, in megahertz (MHz), or 0, if the speed is unknown.
        /// </summary>
        public static object ConfiguredClockSpeed
        {
            get => _configuredClockSpeed == 0 ? "Unsupported" : $"{_configuredClockSpeed} Mhz";
            protected set { _configuredClockSpeed = Convert.ToUInt32(value); }
        }

        /// <summary>
        /// Configured voltage for this device, in millivolts, or 0, if the voltage is unknown.
        /// </summary>
        public static object ConfiguredVoltage
        {
            get => _configuredVoltage == 0 ? "Unsupported" : $"{_configuredVoltage.ToString().Insert(1, ".")} V";
            protected set { _configuredVoltage = Convert.ToUInt32(value); }
        }

        /// <summary>
        /// Name of the first concrete class that appears in the inheritance chain used in the creation of an instance. 
        /// When used with the other key properties of the class, the property allows all instances of this class and its 
        /// subclasses to be identified uniquely.
        /// </summary>
        public static object CreationClassName { get; protected set; }

        /// <summary>
        /// Data width of the physical memory—in bits. A data width of 0 (zero) and a total width of 8 (eight) indicates that 
        /// the memory is used solely to provide error correction bits.
        /// </summary>
        public static object DataWidth { get; protected set; }

        /// <summary>
        /// Description of an object.
        /// </summary>
        public static object Description { get; protected set; }

        /// <summary>
        /// Label of the socket or circuit board that holds the memory.
        /// <code>
        /// Example: "SIMM 3"
        /// </code>
        /// </summary>
        public static object DeviceLocator { get; protected set; }

        /*  FormFactor:
            (0) Unknown
            (1) Other
            (2) SIP
            (3) DIP
            (4) ZIP
            (5) SOJ
            (6) Proprietary
            (7) SIMM
            (8) DIMM
            (9) TSOP
            (10) PGA
            (11) RIMM
            (12) SODIMM
            (13) SRIMM
            (14) SMD
            (15) SSMP
            (16) QFP
            (17) TQFP
            (18) SOIC
            (19) LCC
            (20) PLCC
            (21) BGA
            (22) FPBGA
            (23) LGA
        */
        /// <summary>
        /// Implementation form factor for the chip.
        /// </summary>
        public static object FormFactor
        {
            get
            {
                var FormFactor = new Dictionary<ushort, string>
                {
                    { 0, "Unknown" },
                    { 1, "Other" },
                    { 2, "SIP" },
                    { 3, "DIP" },
                    { 4, "ZIP" },
                    { 5, "SOJ" },
                    { 6, "Proprietary" },
                    { 7, "SIMM" },
                    { 8, "DIMM" },
                    { 9, "TSOP" },
                    { 10, "PGA" },
                    { 11, "RIMM" },
                    { 12, "SODIMM" },
                    { 13, "SRIMM" },
                    { 14, "SMD" },
                    { 15, "SSMP" },
                    { 16, "QFP" },
                    { 17, "TQFP" },
                    { 18, "SOIC" },
                    { 19, "LCC" },
                    { 20, "PLCC" },
                    { 21, "BGA" },
                    { 22, "FPBGA" },
                    { 23, "LGA" }
                };
                return FormFactor[_formFactor];
            }
            protected set { _formFactor = Convert.ToUInt16(value); }
        }

        /// <summary>
        /// If TRUE, this physical media component can be replaced with a physically different but equivalent one while the 
        /// containing package has the power applied. For example, a fan component may be designed to be hot-swapped. 
        /// All components that can be hot-swapped are inherently removable and replaceable.
        /// </summary>
        public static object HotSwappable { get; protected set; }

        /// <summary>
        /// Date and time the object is installed. This property does not need a value to indicate that the object is installed.
        /// </summary>
        public static object InstallDate { get; protected set; }

        /// <summary>
        /// Unsigned 16-bit integer maximum number of consecutive rows of data that are accessed in a single interleaved transfer 
        /// from the memory device. If the value is 0 (zero), the memory is not interleaved.
        /// </summary>
        public static object InterleaveDataDepth { get; protected set; }

        /*  InterleavePosition:
            Noninterleaved (0)
            First position (1)
            Second position (2)
        */
        /// <summary>
        /// Position of the physical memory in an interleave. 
        /// <code>
        /// For example, in a 2:1 interleave, a value of "1" indicates that the memory is in the "even" position.
        /// </code>
        /// </summary>
        public static object InterleavePosition { get; protected set; }

        /// <summary>
        /// Name of the organization responsible for producing the physical element.
        /// </summary>
        public static object Manufacturer
        {
            get => _manufacturer ?? "Unsupported";
            protected set { _manufacturer = value; }
        }

        /// <summary>
        /// The maximum operating voltage for this device, in millivolts, or 0, if the voltage is unknown.
        /// </summary>
        public static object MaxVoltage
        {
            get => _maxVoltage == 0 ? "Unsupported" : $"{_maxVoltage.ToString().Insert(1, ".")} V";
            protected set { _maxVoltage = Convert.ToUInt32(value); }
        }

        /*  MemoryType:
            Unknown (0)
            Other (1)
            DRAM (2)
            Synchronous DRAM (3)
            Cache DRAM (4)
            EDO (5)
            EDRAM (6)
            VRAM (7)
            SRAM (8)
            RAM (9)
            ROM (10)
            Flash (11)
            EEPROM (12)
            FEPROM (13)
            EPROM (14)
            CDRAM (15)
            3DRAM (16)
            SDRAM (17)
            SGRAM (18)
            RDRAM (19)
            DDR (20)
            DDR2 (21)
                DDR2—May not be available; see note above.
            
            DDR2 FB-DIMM (22)
                DDR2—FB-DIMM,May not be available; see note above.
            
            24
                DDR3—May not be available; see note above.
            
            25
                FBD2
        */
        /// <summary>
        /// Type of physical memory. This is a CIM value that is mapped to the SMBIOS value. 
        /// The <see cref="SMBIOSMemoryType"/> property contains the raw SMBIOS memory type.
        /// </summary>
        public static object MemoryType { get; protected set; }

        /// <summary>
        /// The minimum operating voltage for this device, in millivolts, or 0, if the voltage is unknown.
        /// </summary>
        public static object MinVoltage
        {
            get => _minVoltage == 0 ? "Unsupported" : $"{_minVoltage.ToString().Insert(1, ".")} V";
            protected set { _minVoltage = Convert.ToUInt32(value); }
        }

        /// <summary>
        /// Name for the physical element.
        /// </summary>
        public static object Model { get; protected set; }

        /// <summary>
        /// Label for the object. When subclassed, the property can be overridden to be a key property.
        /// </summary>
        public static object Name { get; protected set; }

        /// <summary>
        /// Additional data, beyond asset tag information, that can be used to identify a physical element. One example is bar 
        /// code data associated with an element that also has an asset tag. If only bar code data is available and unique or 
        /// able to be used as an element key, this property is be NULL and the bar code data is used as the class key in the 
        /// tag property.
        /// </summary>
        public static object OtherIdentifyingInfo { get; protected set; }

        /// <summary>
        /// Part number assigned by the organization responsible for producing or manufacturing the physical element.
        /// </summary>
        public static object PartNumber { get; protected set; }

        /// <summary>
        /// Position of the physical memory in a row. 
        /// <code>
        /// For example, if it takes two 8-bit memory devices to form a 16-bit row, then a value of 2 (two) means that this memory 
        /// is the second device—0 (zero) is an invalid value for this property.
        /// </code>
        /// </summary>
        public static object PositionInRow { get; protected set; }

        /// <summary>
        /// If TRUE, the physical element is powered on.
        /// </summary>
        public static object PoweredOn { get; protected set; }

        /// <summary>
        /// If TRUE, a physical component is removable (if it is designed to be taken in and out of the physical container in 
        /// which it is normally found, without impairing the function of the overall packaging). A component can still be 
        /// removable if power must be "off" to perform the removal. If power can be "on" and the component removed, then the 
        /// element is removable and can be hot-swapped. 
        /// <code>
        /// For example, an upgradable processor chip is removable.
        /// </code>
        /// </summary>
        public static object Removable { get; protected set; }

        /// <summary>
        /// If TRUE, this physical media component can be replaced with a physically different one. 
        /// <code>
        /// For example, some computer 
        /// systems allow the main processor chip to be upgraded to one of a higher clock rating. In this case, the processor 
        /// is said to be replaceable. All removable components are inherently replaceable.
        /// </code>
        /// </summary>
        public static object Replaceable { get; protected set; }

        /// <summary>
        /// Manufacturer-allocated number to identify the physical element.
        /// </summary>
        public static object SerialNumber
        {
            get => _serialNumber ?? "Unsupported";
            protected set { _serialNumber = value; }
        }

        /// <summary>
        /// Stock keeping unit number for the physical element.
        /// </summary>
        public static object SKU { get; protected set; }

        /// <summary>
        /// The raw SMBIOS memory type. The value of the <see cref="MemoryType"/> property is a CIM value that is mapped to the 
        /// SMBIOS value.
        /// </summary>
        public static object SMBIOSMemoryType { get; protected set; }

        /// <summary>
        /// Speed of the physical memory—in nanoseconds.
        /// </summary>
        public static object Speed { get; protected set; }

        /*  Status:
            OK ("OK")
            Error ("Error")
            Degraded ("Degraded")
            Unknown ("Unknown")
            Pred Fail ("Pred Fail")
            Starting ("Starting")
            Stopping ("Stopping")
            Service ("Service")
            Stressed ("Stressed")
            NonRecover ("NonRecover")
            No Contact ("No Contact")
            Lost Comm ("Lost Comm")
        */
        /// <summary>
        /// Current status of the object. Various operational and nonoperational statuses can be defined. 
        /// <code>
        /// Operational statuses include: "OK", "Degraded", and "Pred Fail" 
        /// </code>
        /// (an element, such as a SMART-enabled hard disk drive, may be functioning properly but predicting a failure in 
        /// the near future). 
        /// <code>
        /// Nonoperational statuses include: "Error", "Starting", "Stopping", and "Service". 
        /// </code>
        /// The latter, "Service", could apply during mirror-resilvering of a disk, reload of a user permissions list, or other 
        /// administrative work. Not all such work is online, yet the managed element is neither "OK" nor in one of the other states.
        /// </summary>
        public static object Status { get; protected set; }

        /// <summary>
        /// Unique identifier for the physical memory device that is represented by an instance of <see cref="Win32_PhysicalMemory"/>. 
        /// This property is inherited from CIM_PhysicalElement.
        /// <code>
        /// Example: "Physical Memory 1"
        /// </code>
        /// </summary>
        public static object Tag { get; protected set; }

        /// <summary>
        /// Total width, in bits, of the physical memory, including check or error correction bits. If there are no error 
        /// correction bits, the value in this property should match what is specified for the <see cref="DataWidth"/> property.
        /// </summary>
        public static object TotalWidth { get; protected set; }

        /*  TypeDetail:
            Reserved (1)
            Other (2)
            Unknown (4)
            Fast-paged (8)
            Static column (16)
            Pseudo-static (32)
            RAMBUS (64)
            Synchronous (128)
            CMOS (256)
            EDO (512)
            Window DRAM (1024)
            Cache DRAM (2048)
            Non-volatile (4096)
        */
        /// <summary>
        /// Type of physical memory represented.
        /// </summary>
        public static object TypeDetail { get; protected set; }

        /// <summary>
        /// Version of the physical element.
        /// </summary>
        public new static object Version { get; protected set; }

        
        /// <summary>
        /// 
        /// </summary>
        internal static void InitializePerformanceCounter()
        {
            _memoryAvailable = new PerformanceCounter(CATEGORY, "Available Bytes", true);
            _memoryCommitted = new PerformanceCounter(CATEGORY, "Committed Bytes", null, true);
            _memoryCommitLimit = new PerformanceCounter(CATEGORY, "Commit Limit", null, true);
            _memoryCommitInUse = new PerformanceCounter(CATEGORY, "% Committed Bytes In Use", null, true);
            _memoryCachedFaults = new PerformanceCounter(CATEGORY, "Cache Faults/sec", null, true);
            _memoryCachedBytes = new PerformanceCounter(CATEGORY, "Cache Bytes", null, true);
            _memoryPollPaged = new PerformanceCounter(CATEGORY, "Pool Paged Bytes", null, true);
            _memoryPollNonPaged = new PerformanceCounter(CATEGORY, "Pool Nonpaged Bytes", null, true);
        }

        #region PerformanceCounter propertys

        /// <summary>
        /// 
        /// </summary>
        public static object CachedBytes => BytesToString((long)_memoryCachedBytes.NextValue());

        /// <summary>
        /// 
        /// </summary>
        public static object PoolPaged => BytesToString((long)_memoryPollPaged.NextValue());

        /// <summary>
        /// 
        /// </summary>
        public static object PoolNonpaged => BytesToString((long)_memoryPollNonPaged.NextValue());

        /// <summary>
        /// 
        /// </summary>
        public static object Used
        {
            get
            {
                var tmp = Convert.ToSingle(CapacityInBytes) - _memoryAvailable.NextValue();
                return BytesToString(Convert.ToInt64(tmp));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static object Available => BytesToString(Convert.ToInt64(_memoryAvailable.NextValue()));

        /// <summary>
        /// 
        /// </summary>
        public static object Committed => BytesToString(Convert.ToInt64(_memoryCommitted.NextValue()));

        /// <summary>
        /// 
        /// </summary>
        public static object CommitLimit => BytesToString(Convert.ToInt64(_memoryCommitLimit.NextValue()));

        /// <summary>
        /// 
        /// </summary>
        public static object CommitInUse => BytesToString(Convert.ToInt64(_memoryCommitInUse.NextValue()));

        /// <summary>
        /// 
        /// </summary>
        public static object CachedFaults
        {
            get
            {
                var tmp = Math.Round(_memoryCachedFaults.NextValue(), 1);
                return tmp.ToString().Trim();
            }
        }

        #endregion
    }
}
