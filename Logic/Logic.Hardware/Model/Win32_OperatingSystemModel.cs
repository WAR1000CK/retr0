﻿namespace Logic.Hardware.Model
{
    /*
       Source:
       https://docs.microsoft.com/en-us/windows/win32/cimwin32prov/win32-operatingsystem
    */
    /// <summary>
    /// The <see cref="Win32_OperatingSystem"/> WMI class represents a Windows-based operating system installed on a computer.
    /// </summary>
    public class Win32_OperatingSystemModel : Win32_Helper
    {
        public static bool ValuesInitialized { get; protected set; } = false;


        /// <summary>
        /// Name of the disk drive from which the Windows operating system starts.
        /// </summary>
        public static object BootDevice { get; protected set; }

        /// <summary>
        /// Build number of an operating system. It can be used for more precise version information than product release 
        /// version numbers.
        /// </summary>
        public static object BuildNumber { get; protected set; }

        /// <summary>
        /// Type of build used for an operating system.
        /// </summary>
        public static object BuildType { get; protected set; }

        /// <summary>
        /// Short description of the object—a one-line string. The string includes the operating system version.
        /// <code>
        /// For example, "Microsoft Windows 7 Enterprise"
        /// </code> 
        /// This property can be localized.
        /// </summary>
        public static object Caption { get; protected set; }

        /// <summary>
        /// Code page value an operating system uses. A code page contains a character table that an operating system uses 
        /// to translate strings for different languages. The American National Standards Institute (ANSI) lists values that 
        /// represent defined code pages. If an operating system does not use an ANSI code page, this member is set to 0 (zero). 
        /// The CodeSet string can use a maximum of six characters to define the code page value.
        /// </summary>
        public static object CodeSet { get; protected set; }

        /// <summary>
        /// Code for the country/region that an operating system uses. Values are based on international phone dialing 
        /// prefixes—also referred to as IBM country/region codes. This property can use a maximum of six characters to 
        /// define the country/region code value.
        /// </summary>
        public static object CountryCode { get; protected set; }

        /// <summary>
        /// Name of the first concrete class that appears in the inheritance chain used in the creation of an instance. 
        /// When used with other key properties of the class, this property allows all instances of this class and its subclasses 
        /// to be identified uniquely.
        /// </summary>
        public static object CreationClassName { get; protected set; }

        /// <summary>
        /// Creation class name of the scoping computer system.
        /// </summary>
        public static object CSCreationClassName { get; protected set; }

        /// <summary>
        /// NULL-terminated string that indicates the latest service pack installed on a computer. 
        /// If no service pack is installed, the string is NULL.
        /// </summary>
        public static object CSDVersion { get; protected set; }

        /// <summary>
        /// Name of the scoping computer system.
        /// </summary>
        public static object CSName { get; protected set; }

        /// <summary>
        /// Number, in minutes, an operating system is offset from Greenwich mean time (GMT). 
        /// The number is positive, negative, or zero.
        /// </summary>
        public static object CurrentTimeZone { get; protected set; }

        /// <summary>
        /// Data execution prevention is a hardware feature to prevent buffer overrun attacks by stopping the execution of 
        /// code on data-type memory pages. If True, then this feature is available. On 64-bit computers, the data execution 
        /// prevention feature is configured in the BCD store and the properties in <see cref="Win32_OperatingSystem"/> are set 
        /// accordingly.
        /// </summary>
        public static object DataExecutionPrevention_Available { get; protected set; }

        /// <summary>
        /// When the data execution prevention hardware feature is available, this property indicates that the feature is set 
        /// to work for 32-bit applications if True. On 64-bit computers, the data execution prevention feature is configured 
        /// in the Boot Configuration Data (BCD) store and the properties in <see cref="Win32_OperatingSystem"/> are set accordingly.
        /// </summary>
        public static object DataExecutionPrevention_32BitApplications { get; protected set; }

        /// <summary>
        /// When the data execution prevention hardware feature is available, this property indicates that the feature is set 
        /// to work for drivers if True. On 64-bit computers, the data execution prevention feature is configured in the BCD 
        /// store and the properties in <see cref="Win32_OperatingSystem"/> are set accordingly.
        /// </summary>
        public static object DataExecutionPrevention_Drivers { get; protected set; }

        /*  DataExecutionPrevention_SupportPolicy:
            Always Off (0)
                DEP is turned off for all 32-bit applications on the computer with no exceptions. 
                This setting is not available for the user interface.
            
            Always On (1)
                DEP is enabled for all 32-bit applications on the computer. This setting is not available for the user interface.
            
            Opt In (2)
                DEP is enabled for a limited number of binaries, the kernel, and all Windows-based services. 
                However, it is off by default for all 32-bit applications. A user or administrator must explicitly choose either the Always On or the Opt Out setting before DEP can be applied to 32-bit applications.
            
            Opt Out (3)
                DEP is enabled by default for all 32-bit applications. A user or administrator can explicitly remove support 
                for a 32-bit application by adding the application to an exceptions list.
        */
        /// <summary>
        /// Indicates which Data Execution Prevention (DEP) setting is applied. The DEP setting specifies the extent to which 
        /// DEP applies to 32-bit applications on the system. DEP is always applied to the Windows kernel.
        /// </summary>
        public static object DataExecutionPrevention_SupportPolicy { get; protected set; }

        /// <summary>
        /// Operating system is a checked (debug) build. If True, the debugging version is installed. Checked builds provide 
        /// error checking, argument verification, and system debugging code. Additional code in a checked binary 
        /// generates a kernel debugger error message and breaks into the debugger. This helps immediately determine the 
        /// cause and location of the error. Performance may be affected in a checked build due to the additional code that 
        /// is executed.
        /// </summary>
        public static object Debug { get; protected set; }

        /// <summary>
        /// Description of the Windows operating system. Some user interfaces for example, those that allow editing of this 
        /// description, limit its length to 48 characters.
        /// </summary>
        public static object Description { get; protected set; }

        /// <summary>
        /// If True, the operating system is distributed across several computer system nodes. 
        /// If so, these nodes should be grouped as a cluster.
        /// </summary>
        public static object Distributed { get; protected set; }

        /*  EncryptionLevel:
            40-bit (0)
            128-bit (1)
            n-bit (2)
        */
        /// <summary>
        /// Encryption level for secure transactions: 40-bit, 128-bit, or n-bit.
        /// </summary>
        public static object EncryptionLevel { get; protected set; }

        /*  ForegroundApplicationBoost:
            None (0)
                The system boosts the quantum length by 6.
            
            Minimum (1)
                The system boosts the quantum length by 12.
            
            Maximum (2)
                The system boosts the quantum length by 18.
        */
        /// <summary>
        /// Increase in priority is given to the foreground application. Application boost is implemented by giving an 
        /// application more execution time slices (quantum lengths).
        /// </summary>
        public static object ForegroundApplicationBoost { get; protected set; } = 2;

        /// <summary>
        /// Number, in kilobytes, of physical memory currently unused and available.
        /// </summary>
        public static object FreePhysicalMemory { get; protected set; }

        /// <summary>
        /// Number, in kilobytes, that can be mapped into the operating system paging files without causing any other 
        /// pages to be swapped out.
        /// </summary>
        public static object FreeSpaceInPagingFiles { get; protected set; }

        /// <summary>
        /// Number, in kilobytes, of virtual memory currently unused and available.
        /// </summary>
        public static object FreeVirtualMemory { get; protected set; }

        /// <summary>
        /// Date object was installed. This property does not require a value to indicate that the object is installed.
        /// </summary>
        public static object InstallDate { get; protected set; }

        /*  LargeSystemCache:
            Optimize for Applications (0)
                Optimize memory for applications.
            
            Optimize for System Performance (1)
                Optimize memory for system performance.
        */
        /// <summary>
        /// This property is obsolete and not supported.
        /// </summary>
        public static object LargeSystemCache { get; protected set; }

        /// <summary>
        /// Date and time the operating system was last restarted.
        /// </summary>
        public static object LastBootUpTime { get; protected set; }

        /// <summary>
        /// Operating system version of the local date and time-of-day.
        /// </summary>
        public static object LocalDateTime { get; protected set; }

        /// <summary>
        /// Language identifier used by the operating system. A language identifier is a standard international numeric 
        /// abbreviation for a country/region. Each language has a unique language identifier (LANGID), a 16-bit value that 
        /// consists of a primary language identifier and a secondary language identifier.
        /// </summary>
        public static object Locale { get; protected set; }

        /// <summary>
        /// Name of the operating system manufacturer.
        /// <code>
        /// For Windows-based systems, this value is "Microsoft Corporation"
        /// </code>
        /// </summary>
        public static object Manufacturer { get; protected set; }

        /// <summary>
        /// Maximum number of process contexts the operating system can support. The default value set by the provider is 
        /// 4294967295 (0xFFFFFFFF). If there is no fixed maximum, the value should be 0 (zero). On systems that have a 
        /// fixed maximum, this object can help diagnose failures that occur when the maximum is reached—if unknown, enter 
        /// 4294967295 (0xFFFFFFFF).
        /// </summary>
        public static object MaxNumberOfProcesses { get; protected set; }

        /// <summary>
        /// Maximum number, in kilobytes, of memory that can be allocated to a process. For operating systems with no 
        /// virtual memory, typically this value is equal to the total amount of physical memory minus the memory used by 
        /// the BIOS and the operating system. For some operating systems, this value may be infinity, in which case 0 (zero) 
        /// should be entered. In other cases, this value could be a constant, for example, 2G or 4G.
        /// </summary>
        public static object MaxProcessMemorySize { get; protected set; }

        /// <summary>
        /// Multilingual User Interface Pack (MUI Pack ) languages installed on the computer. 
        /// <code>
        /// For example, "en-us"
        /// </code> 
        /// MUI Pack languages are resource files that can be installed on the English version of the operating system. When an MUI 
        /// Pack is installed, you can can change the user interface language to one of 33 supported languages.
        /// </summary>
        public static object MUILanguages { get; protected set; }

        /// <summary>
        /// Operating system instance within a computer system.
        /// </summary>
        public static object Name { get; protected set; }

        /// <summary>
        /// Number of user licenses for the operating system. If unlimited, enter 0 (zero). If unknown, enter -1.
        /// </summary>
        public static object NumberOfLicensedUsers { get; protected set; }

        /// <summary>
        /// Number of process contexts currently loaded or running on the operating system.
        /// </summary>
        public static object NumberOfProcesses { get; protected set; }

        /// <summary>
        /// Number of user sessions for which the operating system is storing state information currently.
        /// </summary>
        public static object NumberOfUsers { get; protected set; }

        /*  OperatingSystemSKU:
            PRODUCT_UNDEFINED (0)
                Undefined
            
            PRODUCT_ULTIMATE (1)
                Ultimate Edition, e.g. Windows Vista Ultimate.
            
            PRODUCT_HOME_BASIC (2)
                Home Basic Edition
            
            PRODUCT_HOME_PREMIUM (3)
                Home Premium Edition
            
            PRODUCT_ENTERPRISE (4)
                Enterprise Edition
            
            PRODUCT_BUSINESS (6)
                Business Edition
            
            PRODUCT_STANDARD_SERVER (7)
                Windows Server Standard Edition (Desktop Experience installation)
            
            PRODUCT_DATACENTER_SERVER (8)
                Windows Server Datacenter Edition (Desktop Experience installation)
            
            PRODUCT_SMALLBUSINESS_SERVER (9)
                Small Business Server Edition
            
            PRODUCT_ENTERPRISE_SERVER (10)
                Enterprise Server Edition
            
            PRODUCT_STARTER (11)
                Starter Edition
            
            PRODUCT_DATACENTER_SERVER_CORE (12)
                Datacenter Server Core Edition
            
            PRODUCT_STANDARD_SERVER_CORE (13)
                Standard Server Core Edition
            
            PRODUCT_ENTERPRISE_SERVER_CORE (14)
                Enterprise Server Core Edition
            
            PRODUCT_WEB_SERVER (17)
                Web Server Edition
            
            PRODUCT_HOME_SERVER (19)
                Home Server Edition
            
            PRODUCT_STORAGE_EXPRESS_SERVER (20)
                Storage Express Server Edition
            
            PRODUCT_STORAGE_STANDARD_SERVER (21)
                Windows Storage Server Standard Edition (Desktop Experience installation)
            
            PRODUCT_STORAGE_WORKGROUP_SERVER (22)
                Windows Storage Server Workgroup Edition (Desktop Experience installation)
            
            PRODUCT_STORAGE_ENTERPRISE_SERVER (23)
                Storage Enterprise Server Edition
            
            PRODUCT_SERVER_FOR_SMALLBUSINESS (24)
                Server For Small Business Edition
            
            PRODUCT_SMALLBUSINESS_SERVER_PREMIUM (25)
                Small Business Server Premium Edition
            
            PRODUCT_ENTERPRISE_N (27)
                Windows Enterprise Edition
            
            PRODUCT_ULTIMATE_N (28)
                Windows Ultimate Edition
            
            PRODUCT_WEB_SERVER_CORE (29)
                Windows Server Web Server Edition (Server Core installation)
            
            PRODUCT_STANDARD_SERVER_V (36)
                Windows Server Standard Edition without Hyper-V
            
            PRODUCT_DATACENTER_SERVER_V (37)
                Windows Server Datacenter Edition without Hyper-V (full installation)
            
            PRODUCT_ENTERPRISE_SERVER_V (38)
                Windows Server Enterprise Edition without Hyper-V (full installation)
            
            PRODUCT_DATACENTER_SERVER_CORE_V (39)
                Windows Server Datacenter Edition without Hyper-V (Server Core installation)
            
            PRODUCT_STANDARD_SERVER_CORE_V (40)
                Windows Server Standard Edition without Hyper-V (Server Core installation)
            
            PRODUCT_ENTERPRISE_SERVER_CORE_V (41)
                Windows Server Enterprise Edition without Hyper-V (Server Core installation)
            
            PRODUCT_HYPERV (42)
                Microsoft Hyper-V Server
            
            PRODUCT_STORAGE_EXPRESS_SERVER_CORE (43)
                Storage Server Express Edition (Server Core installation)
            
            PRODUCT_STORAGE_STANDARD_SERVER_CORE (44)
                Storage Server Standard Edition (Server Core installation)
            
            PRODUCT_STORAGE_WORKGROUP_SERVER_CORE (45)
                Storage Server Workgroup Edition (Server Core installation)
            
            PRODUCT_STORAGE_ENTERPRISE_SERVER_CORE (46)
                Storage Server Enterprise Edition (Server Core installation)
            
            PRODUCT_SB_SOLUTION_SERVER (50)
                Windows Server Essentials (Desktop Experience installation)
            
            PRODUCT_SMALLBUSINESS_SERVER_PREMIUM_CORE (63)
                Small Business Server Premium (Server Core installation)
            
            PRODUCT_CLUSTER_SERVER_V (64)
                Windows Compute Cluster Server without Hyper-V
            
            PRODUCT_CORE_ARM (97)
                Windows RT
            
            PRODUCT_CORE (101)
                Windows Home
            
            PRODUCT_PROFESSIONAL_WMC (103)
                Windows Professional with Media Center
            
            PRODUCT_MOBILE_CORE (104)
                Windows Mobile
            
            PRODUCT_IOTUAP (123)
                Windows IoT (Internet of Things) Core
            
            PRODUCT_DATACENTER_NANO_SERVER (143)
                Windows Server Datacenter Edition (Nano Server installation)
            
            PRODUCT_STANDARD_NANO_SERVER (144)
                Windows Server Standard Edition (Nano Server installation)
            
            PRODUCT_DATACENTER_WS_SERVER_CORE (147)
                Windows Server Datacenter Edition (Server Core installation)
            
            PRODUCT_STANDARD_WS_SERVER_CORE (148)
                Windows Server Standard Edition (Server Core installation)
        */
        /// <summary>
        /// Stock Keeping Unit (SKU) number for the operating system. These values are the same as the PRODUCT_* 
        /// constants defined in WinNT.h that are used with the GetProductInfo function.
        /// </summary>
        public static object OperatingSystemSKU { get; protected set; }

        /// <summary>
        /// Company name for the registered user of the operating system.
        /// </summary>
        public static object Organization { get; protected set; }

        /// <summary>
        /// Architecture of the operating system, as opposed to the processor. This property can be localized.
        /// </summary>
        public static object OSArchitecture { get; protected set; }

        /*  OSLanguage:
            1 (0x1)
                Arabic
            
            4 (0x4)
                Chinese (Simplified)– China
            
            9 (0x9)
                English
            
            1025 (0x401)
                Arabic – Saudi Arabia
            
            1026 (0x402)
                Bulgarian
            
            1027 (0x403)
                Catalan
            
            1028 (0x404)
                Chinese (Traditional) – Taiwan
            
            1029 (0x405)
                Czech
            
            1030 (0x406)
                Danish
            
            1031 (0x407)
                German – Germany
            
            1032 (0x408)
                Greek
            
            1033 (0x409)
                English – United States
            
            1034 (0x40A)
                Spanish – Traditional Sort
            
            1035 (0x40B)
                Finnish
            
            1036 (0x40C)
                French – France
            
            1037 (0x40D)
                Hebrew
            
            1038 (0x40E)
                Hungarian
            
            1039 (0x40F)
                Icelandic
            
            1040 (0x410)
                Italian – Italy
            
            1041 (0x411)
                Japanese
            
            1042 (0x412)
                Korean
            
            1043 (0x413)
                Dutch – Netherlands
            
            1044 (0x414)
                Norwegian – Bokmal
            
            1045 (0x415)
                Polish
            
            1046 (0x416)
                Portuguese – Brazil
            
            1047 (0x417)
                Rhaeto-Romanic
            
            1048 (0x418)
                Romanian
            
            1049 (0x419)
                Russian
            
            1050 (0x41A)
                Croatian
            
            1051 (0x41B)
                Slovak
            
            1052 (0x41C)
                Albanian
            
            1053 (0x41D)
                Swedish
            
            1054 (0x41E)
                Thai
            
            1055 (0x41F)
                Turkish
            
            1056 (0x420)
                Urdu
            
            1057 (0x421)
                Indonesian
            
            1058 (0x422)
                Ukrainian
            
            1059 (0x423)
                Belarusian
            
            1060 (0x424)
            
                Slovenian
            
            1061 (0x425)
                Estonian
            
            1062 (0x426)
                Latvian
            
            1063 (0x427)
                Lithuanian
            
            1065 (0x429)
                Persian
            
            1066 (0x42A)
                Vietnamese
            
            1069 (0x42D)
                Basque (Basque)
            
            1070 (0x42E)
                Serbian
            
            1071 (0x42F)
                Macedonian (Macedonia (FYROM))
            
            1072 (0x430)
                Sutu
            
            1073 (0x431)
                Tsonga
            
            1074 (0x432)
                Tswana
            
            1076 (0x434)
                Xhosa
            
            1077 (0x435)
                Zulu
            
            1078 (0x436)
                Afrikaans
            
            1080 (0x438)
                Faeroese
            
            1081 (0x439)
                Hindi
            
            1082 (0x43A)
                Maltese
            
            1084 (0x43C)
                Scottish Gaelic (United Kingdom)
            
            1085 (0x43D)
                Yiddish
            
            1086 (0x43E)
                Malay – Malaysia
            
            2049 (0x801)
                Arabic – Iraq
            
            2052 (0x804)
                Chinese (Simplified) – PRC
            
            2055 (0x807)
                German – Switzerland
            
            2057 (0x809)
                English – United Kingdom
            
            2058 (0x80A)
                Spanish – Mexico
            
            2060 (0x80C)
                French – Belgium
            
            2064 (0x810)
                Italian – Switzerland
            
            2067 (0x813)
                Dutch – Belgium
            
            2068 (0x814)
                Norwegian – Nynorsk
            
            2070 (0x816)
                Portuguese – Portugal
            
            2072 (0x818)
                Romanian – Moldova
            
            2073 (0x819)
                Russian – Moldova
            
            2074 (0x81A)
                Serbian – Latin
            
            2077 (0x81D)
                Swedish – Finland
            
            3073 (0xC01)
                Arabic – Egypt
            
            3076 (0xC04)
                Chinese (Traditional) – Hong Kong SAR
            
            3079 (0xC07)
                German – Austria
            
            3081 (0xC09)
                English – Australia
            
            3082 (0xC0A)
                Spanish – International Sort
            
            3084 (0xC0C)
                French – Canada
            
            3098 (0xC1A)
                Serbian – Cyrillic
            
            4097 (0x1001)
                Arabic – Libya
            
            4100 (0x1004)
                Chinese (Simplified) – Singapore
            
            4103 (0x1007)
                German – Luxembourg
            
            4105 (0x1009)
                English – Canada
            
            4106 (0x100A)
                Spanish – Guatemala
            
            4108 (0x100C)
                French – Switzerland
            
            5121 (0x1401)
                Arabic – Algeria
            
            5127 (0x1407)
                German – Liechtenstein
            
            5129 (0x1409)
                English – New Zealand
            
            5130 (0x140A)
                Spanish – Costa Rica
            
            5132 (0x140C)
                French – Luxembourg
            
            6145 (0x1801)
                Arabic – Morocco
            
            6153 (0x1809)
                English – Ireland
            
            6154 (0x180A)
                Spanish – Panama
            
            7169 (0x1C01)
                Arabic – Tunisia
            
            7177 (0x1C09)
                English – South Africa
            
            7178 (0x1C0A)
                Spanish – Dominican Republic
            
            8193 (0x2001)
                Arabic – Oman
            
            8201 (0x2009)
                English – Jamaica
            
            8202 (0x200A)
                Spanish – Venezuela
            
            9217 (0x2401)
                Arabic – Yemen
            
            9226 (0x240A)
                Spanish – Colombia
            
            10241 (0x2801)
                Arabic – Syria
            
            10249 (0x2809)
                English – Belize
            
            10250 (0x280A)
                Spanish – Peru
            
            11265 (0x2C01)
                Arabic – Jordan
            
            11273 (0x2C09)
                English – Trinidad
            
            11274 (0x2C0A)
                Spanish – Argentina
            
            12289 (0x3001)
                Arabic – Lebanon
            
            12298 (0x300A)
                Spanish – Ecuador
            
            13313 (0x3401)
                Arabic – Kuwait
            
            13322 (0x340A)
                Spanish – Chile
            
            14337 (0x3801)
                Arabic – U.A.E.
            
            14346 (0x380A)
                Spanish – Uruguay
            
            15361 (0x3C01)
                Arabic – Bahrain
            
            15370 (0x3C0A)
                Spanish – Paraguay
            
            16385 (0x4001)
                Arabic – Qatar
            
            16394 (0x400A)
                Spanish – Bolivia
            
            17418 (0x440A)
                Spanish – El Salvador
            
            18442 (0x480A)
                Spanish – Honduras
            
            19466 (0x4C0A)
                Spanish – Nicaragua
            
            20490 (0x500A)
                Spanish – Puerto Rico
        */
        /// <summary>
        /// Language version of the operating system installed. The following list lists the possible values. 
        /// <code>
        /// Example: 0x0807 (German, Switzerland)
        /// </code>
        /// </summary>
        public static object OSLanguage { get; protected set; }

        /*  OSProductSuite:
            1 (0x1)
                Microsoft Small Business Server was once installed, but may have been upgraded to another version of Windows.
            
            2 (0x2)
                Windows Server 2008 Enterprise is installed.
            
            4 (0x4)
                Windows BackOffice components are installed.
            
            8 (0x8)
                Communication Server is installed.
            
            16 (0x10)
                Terminal Services is installed.
            
            32 (0x20)
                Microsoft Small Business Server is installed with the restrictive client license.
            
            64 (0x40)
                Windows Embedded is installed.
            
            128 (0x80)
                A Datacenter edition is installed.
            
            256 (0x100)
                Terminal Services is installed, but only one interactive session is supported.
            
            512 (0x200)
                Windows Home Edition is installed.
            
            1024 (0x400)
                Web Server Edition is installed.
            
            8192 (0x2000)
                Storage Server Edition is installed.
            
            16384 (0x4000)
                Compute Cluster Edition is installed.
        */
        /// <summary>
        /// Installed and licensed system product additions to the operating system. For example, the value of 146 (0x92) for 
        /// OSProductSuite indicates Enterprise, Terminal Services, and Data Center (bits one, four, and seven set). 
        /// The following list lists possible values.
        /// </summary>
        public static object OSProductSuite { get; protected set; }

        /*  OSType:
            Unknown (0)
            Other (1)
            MACOS (2)
                MACROS
            
            ATTUNIX (3)
            DGUX (4)
            DECNT (5)
            Digital Unix (6)
            OpenVMS (7)
            HPUX (8)
            AIX (9)
            MVS (10)
            OS400 (11)
            OS/2 (12)
            JavaVM (13)
            MSDOS (14)
            WIN3x (15)
            WIN95 (16)
            WIN98 (17)
            WINNT (18)
            WINCE (19)
            NCR3000 (20)
            NetWare (21)
            OSF (22)
            DC/OS (23)
            Reliant UNIX (24)
            SCO UnixWare (25)
            SCO OpenServer (26)
            Sequent (27)
            IRIX (28)
            Solaris (29)
                Solaris
            
            SunOS (30)
            U6000 (31)
            ASERIES (32)
            TandemNSK (33)
            TandemNT (34)
            BS2000 (35)
            LINUX (36)
            Lynx (37)
            XENIX (38)
            VM/ESA (39)
            Interactive UNIX (40)
            BSDUNIX (41)
            FreeBSD (42)
            NetBSD (43)
            GNU Hurd (44)
            OS9 (45)
            MACH Kernel (46)
            Inferno (47)
            QNX (48)
            EPOC (49)
            IxWorks (50)
            VxWorks (51)
            MiNT (52)
            BeOS (53)
            HP MPE (54)
            NextStep (55)
            PalmPilot (56)
            Rhapsody (57)
            Windows 2000 (58)
            Dedicated (59)
            OS/390 (60)
            VSE (61)
            TPF (62)
        */
        /// <summary>
        /// Type of operating system. The following list identifies the possible values.
        /// </summary>
        public static object OSType { get; protected set; }

        /// <summary>
        /// Additional description for the current operating system version.
        /// </summary>
        public static object OtherTypeDescription { get; protected set; }

        /// <summary>
        /// If True, the physical address extensions (PAE) are enabled by the operating system running on Intel processors. 
        /// PAE allows applications to address more than 4 GB of physical memory. When PAE is enabled, the operating 
        /// system uses three-level linear address translation rather than two-level. Providing more physical memory to an 
        /// application reduces the need to swap memory to the page file and increases performance. To enable, PAE, use 
        /// the "/PAE" switch in the Boot.ini file. For more information about the Physical Address Extension feature, 
        /// see <code>https://Go.Microsoft.Com/FWLink/p/?LinkID=45912</code>
        /// </summary>
        public static object PAEEnabled { get; protected set; }

        /// <summary>
        /// Not supported.
        /// </summary>
        public static object PlusProductID { get; protected set; }

        /// <summary>
        /// Not supported.
        /// </summary>
        public static object PlusVersionNumber { get; protected set; }

        /// <summary>
        /// Specifies whether the operating system booted from an external USB device. If true, the operating system has 
        /// detected it is booting on a supported locally connected storage device.
        /// </summary>
        public static object PortableOperatingSystem { get; protected set; }

        /// <summary>
        /// Specifies whether this is the primary operating system.
        /// </summary>
        public static object Primary { get; protected set; }

        /*  ProductType:
            Work Station (1)
            Domain Controller (2)
            Server (3)
        */
        /// <summary>
        /// Additional system information.
        /// </summary>
        public static object ProductType { get; protected set; }

        /// <summary>
        /// Name of the registered user of the operating system.
        /// <code>
        /// Example: "Ben Smith"
        /// </code>
        /// </summary>
        public static object RegisteredUser { get; protected set; }

        /// <summary>
        /// Operating system product serial identification number.
        /// <code>
        /// Example: "10497-OEM-0031416-71674"
        /// </code>
        /// </summary>
        public static object SerialNumber { get; protected set; }

        /// <summary>
        /// Major version number of the service pack installed on the computer system. 
        /// If no service pack has been installed, the value is 0 (zero).
        /// </summary>
        public static object ServicePackMajorVersion { get; protected set; }

        /// <summary>
        /// Minor version number of the service pack installed on the computer system. 
        /// If no service pack has been installed, the value is 0 (zero).
        /// </summary>
        public static object ServicePackMinorVersion { get; protected set; }

        /// <summary>
        /// Total number of kilobytes that can be stored in the operating system paging files—0 (zero) indicates that there 
        /// are no paging files. Be aware that this number does not represent the actual physical size of the paging file on disk.
        /// </summary>
        public static object SizeStoredInPagingFiles { get; protected set; }

        /*  Status:
            OK ("OK")
            Error ("Error")
            Degraded ("Degraded")
            Unknown ("Unknown")
            Pred Fail ("Pred Fail")
            Starting ("Starting")
            Stopping ("Stopping")
            Service ("Service")
            Stressed ("Stressed")
            NonRecover ("NonRecover")
            No Contact ("No Contact")
            Lost Comm ("Lost Comm")
        */
        /// <summary>
        /// Current status of the object. Various operational and nonoperational statuses can be defined. 
        /// <code>
        /// Operational statuses include: "OK", "Degraded", and "Pred Fail" 
        /// </code>
        /// (an element, such as a SMART-enabled hard disk drive may function properly, but predicts a failure in the near future).
        /// <code>
        /// Nonoperational statuses include: "Error", "Starting", "Stopping", and "Service"
        /// </code>
        /// The Service status applies to administrative work, such as mirror-resilvering of a disk, reload of a user permissions 
        /// list, or other administrative work. Not all such work is online, but the managed element is neither "OK" nor in one of 
        /// the other states.
        /// </summary>
        public static object Status { get; protected set; }

        /*  SuiteMask:
            1
                Small Business
            
            2
                Enterprise
            
            4
                BackOffice
            
            8
                Communications
            
            16
                Terminal Services
            
            32
                Small Business Restricted
            
            64
                Embedded Edition
            
            128
                Datacenter Edition
            
            256
                Single User
            
            512
                Home Edition
            
            1024
                Web Server Edition
        */
        /// <summary>
        /// <para>
        /// Qualifiers: BitMap ("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10"), BitValues ("Windows Server, Small Business 
        /// Edition", "Windows Server, Enterprise Edition", "Windows Server, Backoffice Edition", "Windows Server, 
        /// Communications Edition", "Microsoft Terminal Services", "Windows Server, Small Business Edition Restricted", 
        /// "Windows Embedded", "Windows Server, Datacenter Edition", "Single User", "Windows Home Edition", "Windows Server, 
        /// Web Edition")
        /// </para>
        /// Bit flags that identify the product suites available on the system.
        /// <code>
        /// For example, to specify both Personal and BackOffice, set SuiteMask to 4 | 512 or 516.
        /// </code>
        /// </summary>
        public static object SuiteMask { get; protected set; }

        /// <summary>
        /// Physical disk partition on which the operating system is installed.
        /// </summary>
        public static object SystemDevice { get; protected set; }

        /// <summary>
        /// System directory of the operating system.
        /// <code>
        /// Example: "C:\WINDOWS\SYSTEM32"
        /// </code>
        /// </summary>
        public static object SystemDirectory { get; protected set; }

        /// <summary>
        /// Letter of the disk drive on which the operating system resides.
        /// <code>
        /// Example: "C:"
        /// </code>
        /// </summary>
        public static object SystemDrive { get; protected set; }

        /// <summary>
        /// Total swap space in kilobytes. This value may be NULL (unspecified) if the swap space is not distinguished from 
        /// page files. However, some operating systems distinguish these concepts. 
        /// <code>
        /// For example, in UNIX, whole processes can be swapped out when the free page list falls and remains below a specified amount.
        /// </code>
        /// </summary>
        public static object TotalSwapSpaceSize { get; protected set; }

        /// <summary>
        /// Number, in kilobytes, of virtual memory. 
        /// <code>
        /// For example, this may be calculated by adding the amount of total RAM 
        /// to the amount of paging space, that is, adding the amount of memory in or aggregated by the computer system 
        /// to the property, <see cref="SizeStoredInPagingFiles"/>.
        /// </code>
        /// </summary>
        public static object TotalVirtualMemorySize { get; protected set; }

        /// <summary>
        /// Total amount, in kilobytes, of physical memory available to the operating system. This value does not necessarily 
        /// indicate the true amount of physical memory, but what is reported to the operating system as available to it.
        /// </summary>
        public static object TotalVisibleMemorySize { get; protected set; }

        /// <summary>
        /// Version number of the operating system.
        /// <code>
        /// Example: "4.0"
        /// </code>
        /// </summary>
        public new static object Version { get; protected set; }

        /// <summary>
        /// Windows directory of the operating system.
        /// <code>
        /// Example: "C:\WINDOWS"
        /// </code>
        /// </summary>
        public static object WindowsDirectory { get; protected set; }

        /*  QuantumLength:
            Unknown (0)
            One tick (1)
            Two ticks (2)
        */
        /// <summary>
        /// Not supported
        /// <para>
        /// The QuantumLength property defines the number of clock ticks per quantum. A quantum is a unit of execution 
        /// time that the scheduler is allowed to give to an application before switching to other applications. When a thread 
        /// runs one quantum, the kernel preempts it and moves it to the end of a queue for applications with equal priorities. 
        /// The actual length of a thread's quantum varies across different Windows platforms. For Windows NT/Windows 2000 only.
        /// </para>
        /// </summary>
        public static object QuantumLength { get; protected set; }

        /*  QuantumType:
            Unknown (0)
            Fixed (1)
            Variable (2)
        */
        /// <summary>
        /// Not supported
        /// <para>
        /// The QuantumType property specifies either fixed or variable length quantums. Windows defaults to variable 
        /// length quantums where the foreground application has a longer quantum than the background applications. 
        /// Windows Server defaults to fixed-length quantums. A quantum is a unit of execution time that the scheduler is 
        /// allowed to give to an application before switching to another application. When a thread runs one quantum, the 
        /// kernel preempts it and moves it to the end of a queue for applications with equal priorities. The actual length of a 
        /// thread's quantum varies across different Windows platforms.
        /// </para>
        /// </summary>
        public static object QuantumType { get; protected set; }
    }
}
