﻿namespace Logic.Hardware.Model
{
    /*
       Source:
       https://docs.microsoft.com/en-us/windows/win32/cimwin32prov/win32-computersystem
    */
    /// <summary>
    /// The <see cref="Win32_ComputerSystem"/> WMI class represents a computer system running Windows.
    /// </summary>
    public class Win32_ComputerSystemModel : Win32_Helper
    {
        public static bool ValuesInitialized { get; protected set; } = false;


        /*  AdminPasswordStatus:
            Disabled (0)
            Enabled (1)
            Not Implemented (2)
            Unknown (3)
        */
        /// <summary>
        /// System hardware security settings for administrator password status.
        /// </summary>
        public static object AdminPasswordStatus { get; protected set; }

        /// <summary>
        /// If True, the system manages the page file.
        /// </summary>
        public static object AutomaticManagedPagefile { get; protected set; }

        /// <summary>
        /// If True, the automatic reset boot option is enabled.
        /// </summary>
        public static object AutomaticResetBootOption { get; protected set; }

        /// <summary>
        /// If True, the automatic reset is enabled.
        /// </summary>
        public static object AutomaticResetCapability { get; protected set; }

        /*  BootOptionOnLimit:
            Reserved (0)
            Operating system (1)
            System utilities (2)
            Do not reboot (3)
        */
        /// <summary>
        /// Boot option limit is ON. Identifies the system action when the ResetLimit value is reached.
        /// </summary>
        public static object BootOptionOnLimit { get; protected set; }

        /*  BootOptionOnWatchDog:
            Reserved (0)
            Operating system (1)
            System utilities (2)
            Do not reboot (3)
        */
        /// <summary>
        /// Type of reboot action after the time on the watchdog timer is elapsed.
        /// </summary>
        public static object BootOptionOnWatchDog { get; protected set; }

        /// <summary>
        /// If True, indicates whether a boot ROM is supported.
        /// </summary>
        public static object BootROMSupported { get; protected set; }

        /*  BootupState:
            Normal boot ("Normal boot")
            Fail-safe boot ("Fail-safe boot")
            Fail-safe with network boot ("Fail-safe with network boot")
        */
        /// <summary>
        /// System is started. Fail-safe boot bypasses the user startup files also called SafeBoot.
        /// </summary>
        public static object BootupState { get; protected set; }

        /// <summary>
        /// Status and Additional Data fields that identify the boot status.
        /// </summary>
        public static object BootStatus { get; protected set; }

        /// <summary>
        /// Short description of the object a one-line string.
        /// </summary>
        public static object Caption { get; protected set; }

        /*  ChassisBootupState:
            Other (1)
            Unknown (2)
            Safe (3)
            Warning (4)
            Critical (5)
            Non-recoverable (6)
        */
        /// <summary>
        /// Boot up state of the chassis.
        /// </summary>
        public static object ChassisBootupState { get; protected set; }

        /// <summary>
        /// This value comes from the SKU Number member of the System Enclosure or Chassis structure in the SMBIOS information.
        /// </summary>
        public static object ChassisSKUNumber { get; protected set; }

        /// <summary>
        /// Name of the first concrete class in the inheritance chain of an instance. 
        /// You can use this property with other properties of the class to identify all instances of the class and its subclasses.
        /// </summary>
        public static object CreationClassName { get; protected set; }

        /// <summary>
        /// Amount of time the unitary computer system is offset from Coordinated Universal Time (UTC).
        /// </summary>
        public static object CurrentTimeZone { get; protected set; }

        /// <summary>
        /// If True, the daylight savings mode is ON.
        /// </summary>
        public static object DaylightInEffect { get; protected set; }

        /// <summary>
        /// Description of the object.
        /// </summary>
        public static object Description { get; protected set; }

        /// <summary>
        /// Name of local computer according to the domain name server (DNS).
        /// </summary>
        public static object DNSHostName { get; protected set; }

        /// <summary>
        /// Name of the domain to which a computer belongs.
        /// </summary>
        public static object Domain { get; protected set; }

        /*  DomainRole:
            Standalone Workstation (0)
            Member Workstation (1)
            Standalone Server (2)
            Member Server (3)
            Backup Domain Controller (4)
            Primary Domain Controller (5)
        */
        /// <summary>
        /// Role of a computer in an assigned domain workgroup. A domain workgroup is a collection of computers on the same network. 
        /// <code>
        /// For example, a DomainRole property may show that a computer is a member workstation.
        /// </code>
        /// </summary>
        public static object DomainRole { get; protected set; }

        /// <summary>
        /// Enables daylight savings time (DST) on a computer. A value of True indicates that the system time changes to an hour 
        /// ahead or behind when DST starts or ends. A value of False indicates that the system time does not change to an hour 
        /// ahead or behind when DST starts or ends. A value of NULL indicates that the DST status is unknown on a system.
        /// </summary>
        public static object EnableDaylightSavingsTime { get; protected set; }

        /*  FrontPanelResetStatus:
            Disabled (0)
            Enabled (1)
            Not Implemented (2)
            Unknown (3)
        */
        /// <summary>
        /// The following table lists the hardware security settings for the reset button on a computer.
        /// </summary>
        public static object FrontPanelResetStatus { get; protected set; }

        /// <summary>
        /// If True, a hypervisor is present.
        /// </summary>
        public static object HypervisorPresent { get; protected set; }

        /// <summary>
        /// If True, an infrared (IR) port exists on a computer system.
        /// </summary>
        public static object InfraredSupported { get; protected set; }

        /// <summary>
        /// Data required to find the initial load device or boot service to request that the operating system start up.
        /// </summary>
        public static object InitialLoadInfo { get; protected set; }

        /// <summary>
        /// Object is installed. An object does not need a value to indicate that it is installed.
        /// </summary>
        public static object InstallDate { get; protected set; }

        /*  KeyboardPasswordStatus:
            Disabled (0)
            Enabled (1)
            Not Implemented (2)
            Unknown (3)
        */
        /// <summary>
        /// System hardware security settings for Keyboard Password Status.
        /// </summary>
        public static object KeyboardPasswordStatus { get; protected set; }

        /// <summary>
        /// Array entry of the <see cref="InitialLoadInfo"/> property that contains the data to start the loaded operating system.
        /// </summary>
        public static object LastLoadInfo { get; protected set; }

        /// <summary>
        /// Name of a computer manufacturer.
        /// <code>
        /// Example: Adventure Works
        /// </code>
        /// </summary>
        public static object Manufacturer { get; protected set; }

        /// <summary>
        /// Product name that a manufacturer gives to a computer. This property must have a value.
        /// </summary>
        public static object Model { get; protected set; }

        /// <summary>
        /// Key of a CIM_System instance in an enterprise environment.
        /// </summary>
        public static object Name { get; protected set; }

        /*  NameFormat:
            IP ("IP")
            Dial ("Dial")
            HID ("HID")
            NWA ("NWA")
            HWA ("HWA")
            X25 ("X25")
            ISDN ("ISDN")
            IPX ("IPX")
            DCC ("DCC")
            ICD ("ICD")
            E.164 ("E.164")
            SNA ("SNA")
            OID/OSI ("OID/OSI")
            Other ("Other")
        */
        /// <summary>
        /// <para>
        /// Computer system Name value that is generated automatically. The CIM_ComputerSystem object and its derivatives are top-level 
        /// objects of the Common Information Model (CIM). They provide the scope for several components. Unique CIM_System keys are 
        /// required, but you can define a heuristic to create the CIM_ComputerSystem name that generates the same name, and is 
        /// independent from the discovery protocol. This prevents inventory and management problems when the same asset or entity is 
        /// discovered multiple times, but cannot be resolved to one object. Using a heuristic is recommended, but not required. 
        /// </para>
        /// <para>
        /// The heuristic is outlined in the CIM V2 Common Model specification, and assumes that the documented rules are used to 
        /// determine and assign a name.The NameFormat values list defines the order to assign a computer system name.Several rules 
        /// map to the same value. 
        /// </para>
        /// The CIM_ComputerSystem Name value that is calculated using the heuristic is the key value of the system.However, 
        /// use aliases to assign a different name for CIM_ComputerSystem, which can be more unique to your company.
        /// </summary>
        public static object NameFormat { get; protected set; }

        /// <summary>
        /// If True, the network Server Mode is enabled.
        /// </summary>
        public static object NetworkServerModeEnabled { get; protected set; }

        /// <summary>
        /// Number of logical processors available on the computer.
        /// <code>
        /// You can use <see cref="NumberOfLogicalProcessors"/> and <see cref="NumberOfProcessors"/> to determine if the computer 
        /// is hyperthreading.
        /// </code>
        /// </summary>
        public static object NumberOfLogicalProcessors { get; protected set; }

        /// <summary>
        /// Number of physical processors currently available on a system. This is the number of enabled processors for a system, 
        /// which does not include the disabled processors.
        /// <code>
        /// If a computer system has two physical processors each containing two 
        /// logical processors, then the value of <see cref="NumberOfProcessors"/> is 2 and <see cref="NumberOfLogicalProcessors"/> is 4. 
        /// The processors may be multicore or they may be hyperthreading processors.
        /// </code>
        /// </summary>
        public static object NumberOfProcessors { get; protected set; }

        /// <summary>
        /// List of data for a bitmap that the original equipment manufacturer (OEM) creates.
        /// </summary>
        public static object OEMLogoBitmap { get; protected set; }

        /// <summary>
        /// List of free-form strings that an OEM defines. For example, an OEM defines the part numbers for system reference 
        /// documents, manufacturer contact information, and so on.
        /// </summary>
        public static object OEMStringArray { get; protected set; }

        /// <summary>
        /// If True, the computer is part of a domain. If the value is NULL, the computer is not in a domain or the status is unknown. 
        /// If you remove the computer from a domain, the value becomes false.
        /// </summary>
        public static object PartOfDomain { get; protected set; }

        /// <summary>
        /// Time delay before a reboot is initiated in milliseconds. It is used after a system power cycle, local or remote system reset, 
        /// and automatic system reset. A value of 1 (minus one) indicates that the pause value is unknown.
        /// </summary>
        public static object PauseAfterReset { get; protected set; }

        /*  PCSystemType:
            Unspecified (0)
            Desktop (1)
            Mobile (2)
            Workstation (3)
            Enterprise Server (4)
            SOHO Server (5)
                Small Office and Home Office (SOHO) Server
            
            Appliance PC (6)
            Performance Server (7)
            Maximum (8)
        */
        /// <summary>
        /// Type of the computer in use, such as laptop, desktop, or Tablet.
        /// </summary>
        public static object PCSystemType { get; protected set; }

        /*  PCSystemTypeEx:
            Unspecified (0)
            Desktop (1)
            Mobile (2)
            Workstation (3)
            Enterprise Server (4)
            SOHO Server (5)
            Appliance PC (6)
            Performance Server (7)
            Slate (8)
            Maximum (9)
        */
        /// <summary>
        /// Type of the computer in use, such as laptop, desktop, or Tablet.
        /// </summary>
        public static object PCSystemTypeEx { get; protected set; }

        /*  PowerManagementCapabilities:
            Unknown (0)
            Not Supported (1)
            Disabled (2)
            Enabled (3)
                The power management features are currently enabled, but the exact feature set is unknown or the information is unavailable.
            
            Power Saving Modes Entered Automatically (4)
                The device can change its power state based on usage or other criteria.
            
            Power State Settable (5)
                The SetPowerState method is supported. This method is found on the parent CIM_LogicalDevice class and can be implemented. For more information, see Designing Managed Object Format (MOF) Classes.
            
            Power Cycling Supported (6)
                The SetPowerState method can be invoked with the PowerState parameter set to 5 (Power Cycle).
            
            Timed Power On Supported (7)
                Timed Power-On Supported
            
                The SetPowerState method can be invoked with the PowerState parameter set to 5 (Power Cycle) and Time set 
                to a specific date and time, or interval, for power-on.
        */
        /// <summary>
        /// Array of the specific power-related capabilities of a logical device.
        /// </summary>
        public static object PowerManagementCapabilities { get; protected set; }

        /// <summary>
        /// If True, device can be power-managed, for example, a device can be put into suspend mode, and so on. 
        /// This property does not indicate that power management features are enabled currently, but it does indicate 
        /// that the logical device is capable of power management.
        /// </summary>
        public static object PowerManagementSupported { get; protected set; }

        /*  PowerOnPasswordStatus:
            Disabled (0)
            Enabled (1)
            Not Implemented (2)
            Unknown (3)
        */
        /// <summary>
        /// System hardware security settings for Power-On Password Status.
        /// </summary>
        public static object PowerOnPasswordStatus { get; protected set; }

        /*  PowerState:
            Unknown (0)
            Full Power (1)
            Power Save - Low Power Mode (2)
            Power Save - Standby (3)
            Power Save - Unknown (4)
            Power Cycle (5)
            Power Off (6)
            Power Save - Warning (7)
            Power Save - Hibernate (8)
                Power save hibernate.
            
            Power Save - Soft Off (9)
                Power save soft off.
        */
        /// <summary>
        /// Current power state of a computer and its associated operating system. The power saving states have the following values: 
        /// Value 4 (Unknown) indicates that the system is known to be in a power save mode, but its exact status in this mode is 
        /// unknown; 2 (Low Power Mode) indicates that the system is in a power save state, but still functioning and may exhibit 
        /// degraded performance; 3 (Standby) indicates that the system is not functioning, but could be brought to full power quickly; 
        /// and 7 (Warning) indicates that the computer system is in a warning state and a power save mode.
        /// </summary>
        public static object PowerState { get; protected set; }

        /*  PowerSupplyState:
            Other (1)
            Unknown (2)
            Safe (3)
            Warning (4)
            Critical (5)
            Non-recoverable (6)
                Nonrecoverable
        */
        /// <summary>
        /// State of the power supply or supplies when last booted.
        /// </summary>
        public static object PowerSupplyState { get; protected set; }

        /// <summary>
        /// Contact information for the primary system owner, 
        /// <code>
        /// for example, phone number, email address, and so on.
        /// </code>
        /// </summary>
        public static object PrimaryOwnerContact { get; protected set; }

        /// <summary>
        /// Name of the primary system owner.
        /// </summary>
        public static object PrimaryOwnerName { get; protected set; }

        /*  ResetCapability:
            Other (1)
            Unknown (2)
            Disabled (3)
            Enabled (4)
            Not Implemented (5)
        */
        /// <summary>
        /// If enabled, the value is 4 and the unitary computer system can be reset using the power and reset buttons. If disabled, 
        /// the value is 3, and a reset is not allowed.
        /// </summary>
        public static object ResetCapability { get; protected set; }

        /// <summary>
        /// Number of automatic resets since the last reset. A value of 1 (minus one) indicates that the count is unknown.
        /// </summary>
        public static object ResetCount { get; protected set; }

        /// <summary>
        /// Number of consecutive times a system reset is attempted. A value of 1 (minus one) indicates that the limit is unknown.
        /// </summary>
        public static object ResetLimit { get; protected set; }

        /// <summary>
        /// List that specifies the roles of a system in the information technology environment.
        /// </summary>
        public static object Roles { get; protected set; }

        /*  Status:
            OK ("OK")
            Error ("Error")
            Degraded ("Degraded")
            Unknown ("Unknown")
            Pred Fail ("Pred Fail")
            Starting ("Starting")
            Stopping ("Stopping")
            Service ("Service")
            Stressed ("Stressed")
            NonRecover ("NonRecover")
            No Contact ("No Contact")
            Lost Comm ("Lost Comm")
        */
        /// <summary>
        /// Current status of an object. Various operational and nonoperational statuses can be defined.
        /// <code>
        /// Operational statuses include: OK, Degraded, and Pred Fail,
        /// </code>
        /// which is an element such as a SMART-enabled hard disk drive that may be functioning properly, but predicts a 
        /// failure in the near future. 
        /// <code>
        /// Nonoperational statuses include: Error, Starting, Stopping, and Service,
        /// </code>
        /// which can apply during mirror-resilvering of a disk, reloading a user permissions list, or other administrative work. 
        /// Not all status work is online, but the managed element is not OK or in one of the other states.
        /// </summary>
        public static object Status { get; protected set; }

        /// <summary>
        /// List of the support contact information for the Windows operating system.
        /// </summary>
        public static object SupportContactDescription { get; protected set; }

        /// <summary>
        /// The family to which a particular computer belongs. A family refers to a set of computers that are similar but 
        /// not identical from a hardware or software point of view.
        /// </summary>
        public static object SystemFamily { get; protected set; }

        /// <summary>
        /// Identifies a particular computer configuration for sale. It is sometimes also called a product ID or purchase order number.
        /// </summary>
        public static object SystemSKUNumber { get; protected set; }

        /// <summary>
        /// SystemStartupDelay is no longer available for use because Boot.ini is not used to configure system startup.
        /// Instead, use the BCD classes supplied by the Boot Configuration Data (BCD) WMI provider or the Bcdedit command.
        /// </summary>
        public static object SystemStartupDelay { get; protected set; }

        /// <summary>
        /// SystemStartupOptions is no longer available for use because Boot.ini is not used to configure system startup. 
        /// Instead, use the BCD classes supplied by the Boot Configuration Data (BCD) WMI provider or the Bcdedit command.
        /// </summary>
        public static object SystemStartupOptions { get; protected set; }

        /// <summary>
        /// SystemStartupSetting is no longer available for use because Boot.ini is not used to configure system startup. 
        /// Instead, use the BCD classes supplied by the Boot Configuration Data (BCD) WMI provider or the Bcdedit command.
        /// </summary>
        public static object SystemStartupSetting { get; protected set; }

        /*  SystemType:
            X86-based PC ("X86-based PC")
            MIPS-based PC ("MIPS-based PC")
            Alpha-based PC ("Alpha-based PC")
            Power PC ("Power PC")
            SH-x PC ("SH-x PC")
            StrongARM PC ("StrongARM PC")
            64-bit Intel PC ("64-bit Intel PC")
            x64-based PC ("x64-based PC")
            Unknown ("Unknown")
            X86-Nec98 PC ("X86-Nec98 PC")
        */
        /// <summary>
        /// System running on the Windows-based computer.
        /// </summary>
        public static object SystemType { get; protected set; }

        /*  ThermalState:
            Other (1)
            Unknown (2)
            Safe (3)
            Warning (4)
            Critical (5)
            Non-recoverable (6)
        */
        /// <summary>
        /// Thermal state of the system when last booted.
        /// </summary>
        public static object ThermalState { get; protected set; }

        /// <summary>
        /// Total size of physical memory. Be aware that, under some circumstances, this property may not return an accurate value 
        /// for the physical memory. For example, it is not accurate if the BIOS is using some of the physical memory. 
        /// For an accurate value, use the Capacity property in <see cref="Win32_PhysicalMemory"/> instead.
        /// <code>
        /// Example: 67108864
        /// </code>
        /// </summary>
        public static object TotalPhysicalMemory { get; protected set; }

        /// <summary>
        /// Name of a user that is logged on currently. This property must have a value. In a terminal services session, 
        /// UserName returns the name of the user that is logged on to the console not the user logged on during the terminal 
        /// service session.
        /// <code>
        /// Example: jeffsmith
        /// </code>
        /// </summary>
        public static object UserName { get; protected set; }

        /*  WakeUpType:
            Reserved (0)
            Other (1)
            Unknown (2)
            APM Timer (3)
            Modem Ring (4)
            LAN Remote (5)
            Power Switch (6)
            PCI PME# (7)
            AC Power Restored (8)
        */
        /// <summary>
        /// Event that causes the system to power up.
        /// </summary>
        public static object WakeUpType { get; protected set; }

        /// <summary>
        /// Name of the workgroup for this computer. If the value of the <see cref="PartOfDomain"/> property is False, then the name of the 
        /// workgroup is returned.
        /// </summary>
        public static object Workgroup { get; protected set; }
    }
}
