﻿using System;
using System.Diagnostics;

namespace Logic.Hardware.Model
{
    /*
       Source:
       https://docs.microsoft.com/en-us/windows/win32/cimwin32prov/win32-processor
    */
    /// <summary>
    /// The <see cref="Win32_Processor"/> WMI class represents a device that can interpret a sequence of instructions on a computer 
    /// running on a Windows operating system.
    /// </summary>
    public class Win32_ProcessorModel : Win32_Helper
    {
        #region Private fields

        private static PerformanceCounter _processorFrequency;
        private static PerformanceCounter _processorTime;
        private static PerformanceCounter _processorQueueLength;
        private static PerformanceCounter _privilegedTime;
        private static PerformanceCounter _interruptTime;
        private static PerformanceCounter _dpcTime;
        private static PerformanceCounter _processHandleCount;
        private static PerformanceCounter _processThreadCount;

        #endregion

        public static bool ValuesInitialized { get; protected set; } = false;


        /// <summary>
        /// On a 32-bit operating system, the value is 32 and on a 64-bit operating system it is 64.
        /// </summary>
        public static object AddressWidth { get; protected set; }

        /*  Architecture:
            x86 (0)
            MIPS (1)
            Alpha (2)
            PowerPC (3)
            ARM (5)
            ia64 (6)
                Itanium-based systems
            x64 (9)
        */
        /// <summary>
        /// Processor architecture used by the platform.
        /// </summary>
        public static object Architecture { get; protected set; }

        /// <summary>
        /// Represents the asset tag of this processor.
        /// </summary>
        public static object AssetTag { get; protected set; }

        /*  Availability:
            Other (1)
            Unknown (2)
            Running/Full Power (3)
                Running or Full Power
            
            Warning (4)
            In Test (5)
            Not Applicable (6)
            Power Off (7)
            Off Line (8)
            Off Duty (9)
            Degraded (10)
            Not Installed (11)
            Install Error (12)
            Power Save - Unknown (13)
                The device is known to be in a power save state, but its exact status is unknown.
            
            Power Save - Low Power Mode (14)
                The device is in a power save state, but is still functioning, and may exhibit decreased performance.
            
            Power Save - Standby (15)
                The device is not functioning, but can be brought to full power quickly.
            
            Power Cycle (16)
            Power Save - Warning (17)
                The device is in a warning state, though also in a power save state.
            
            Paused (18)
                The device is paused.
            
            Not Ready (19)
                The device is not ready.
            
            Not Configured (20)
                The device is not configured.
            
            Quiesced (21)
                The device is quiet.
        */
        /// <summary>
        /// Availability and status of the device.
        /// </summary>
        public static object Availability { get; protected set; }

        /// <summary>
        /// Short description of an object (a one-line string).
        /// </summary>
        public static object Caption { get; protected set; }

        /// <summary>
        /// Defines which functions the processor supports.
        /// </summary>
        public static object Characteristics { get; protected set; }

        /*  ConfigManagerErrorCode:
            This device is working properly. (0)
                Device is working properly.

            This device is not configured correctly. (1)
                Device is not configured correctly.
            
            Windows cannot load the driver for this device. (2)
            The driver for this device might be corrupted, or your system may be running low on memory or other resources. (3)
                Driver for this device might be corrupted or the system may be low on memory or other resources.
            
            This device is not working properly. One of its drivers or your registry might be corrupted. (4)
                Device is not working properly. One of its drivers or the registry might be corrupted.
            
            The driver for this device needs a resource that Windows cannot manage. (5)
                Driver for the device requires a resource that Windows cannot manage.
            
            The boot configuration for this device conflicts with other devices. (6)
                Boot configuration for the device conflicts with other devices.
            
            Cannot filter. (7)
            The driver loader for the device is missing. (8)
                Driver loader for the device is missing.
            
            This device is not working properly because the controlling firmware is reporting the resources for the device incorrectly. (9)
                Device is not working properly. The controlling firmware is incorrectly reporting the resources for the device.
            
            This device cannot start. (10)
                Device cannot start.
            
            This device failed. (11)
                Device failed.
            
            This device cannot find enough free resources that it can use. (12)
                Device cannot find enough free resources to use.
            
            Windows cannot verify this device's resources. (13)
                Windows cannot verify the device's resources.
            
            This device cannot work properly until you restart your computer. (14)
                Device cannot work properly until the computer is restarted.
            
            This device is not working properly because there is probably a re-enumeration problem. (15)
                Device is not working properly due to a possible re-enumeration problem.
            
            Windows cannot identify all the resources this device uses. (16)
                Windows cannot identify all of the resources that the device uses.
            
            This device is asking for an unknown resource type. (17)
                Device is requesting an unknown resource type.
            
            Reinstall the drivers for this device. (18)
                Device drivers must be reinstalled.
            
            Failure using the VxD loader. (19)
            Your registry might be corrupted. (20)
                Registry might be corrupted.
            
            System failure: Try changing the driver for this device. If that does not work, see your hardware documentation. Windows is removing this device. (21)
                System failure. If changing the device driver is ineffective, see the hardware documentation. Windows is removing the device.
            
            This device is disabled. (22)
                Device is disabled.
            
            System failure: Try changing the driver for this device. If that doesn't work, see your hardware documentation. (23)
                System failure. If changing the device driver is ineffective, see the hardware documentation.
            
            This device is not present, is not working properly, or does not have all its drivers installed. (24)
                Device is not present, not working properly, or does not have all of its drivers installed.
            
            Windows is still setting up this device. (25)
                Windows is still setting up the device.
            
            Windows is still setting up this device. (26)
                Windows is still setting up the device.
            
            This device does not have valid log configuration. (27)
                Device does not have valid log configuration.
            
            The drivers for this device are not installed. (28)
                Device drivers are not installed.
            
            This device is disabled because the firmware of the device did not give it the required resources. (29)
                Device is disabled. The device firmware did not provide the required resources.
            
            This device is using an Interrupt Request (IRQ) resource that another device is using. (30)
                Device is using an IRQ resource that another device is using.
            
            This device is not working properly because Windows cannot load the drivers required for this device. (31)
                Device is not working properly. Windows cannot load the required device drivers.
        */
        /// <summary>
        /// Windows API Configuration Manager error code.
        /// </summary>
        public static object ConfigManagerErrorCode { get; protected set; }

        /// <summary>
        /// If TRUE, the device is using a configuration that the user defines.
        /// </summary>
        public static object ConfigManagerUserConfig { get; protected set; }

        /*  CpuStatus:
            Unknown (0)
            CPU Enabled (1)
            CPU Disabled by User via BIOS Setup (2)
            CPU Disabled By BIOS (POST Error) (3)
            CPU is Idle (4)
            Reserved (5)
            Reserved (6)
            Other (7)
        */
        /// <summary>
        /// Current status of the processor. Status changes indicate processor usage, but not the physical condition of the processor.
        /// </summary>
        public static object CpuStatus { get; protected set; }

        /// <summary>
        /// Name of the first concrete class that appears in the inheritance chain used to create an instance. 
        /// When used with the other key properties of the class, the property allows all instances of this class 
        /// and its subclasses to be identified uniquely.
        /// </summary>
        public static object CreationClassName { get; protected set; }

        /// <summary>
        /// Current speed of the processor, in MHz.
        /// </summary>
        public static object CurrentClockSpeed { get; protected set; }

        /// <summary>
        /// Voltage of the processor. If the eighth bit is set, bits 0-6 contain the voltage multiplied by 10. 
        /// If the eighth bit is not set, then the bit setting in VoltageCaps represents the voltage value. 
        /// CurrentVoltage is only set when SMBIOS designates a voltage value.
        /// <code>
        /// Example: Value for a processor voltage of 1.8 volts is 0x12 (1.8 x 10).
        /// </code>
        /// </summary>
        public static object CurrentVoltage { get; protected set; }

        /// <summary>
        /// On a 32-bit processor, the value is 32 and on a 64-bit processor it is 64.
        /// </summary>
        public static object DataWidth { get; protected set; }

        /// <summary>
        /// Description of the object.
        /// </summary>
        public static object Description { get; protected set; }

        /// <summary>
        /// Unique identifier of a processor on the system.
        /// </summary>
        public static object DeviceID { get; protected set; }

        /// <summary>
        /// If TRUE, the error reported in <see cref="LastErrorCode"/> is clear.
        /// </summary>
        public static object ErrorCleared { get; protected set; }

        /// <summary>
        /// More information about the error recorded in <see cref="LastErrorCode"/>, and information about corrective actions 
        /// that can be taken.
        /// </summary>
        public static object ErrorDescription { get; protected set; }

        /// <summary>
        /// External clock frequency, in MHz. If the frequency is unknown, this property is set to NULL.
        /// </summary>
        public static object ExtClock { get; protected set; }

        /*  Family:
            Other (1)
            Unknown (2)
            8086 (3)
            80286 (4)
            80386 (5)
                Intel386™ Processor
            
            80486 (6)
                Intel486™ Processor
            
            8087 (7)
            80287 (8)
            80387 (9)
            80487 (10)
            Pentium(R) brand (11)
                Pentium Brand
            
            Pentium(R) Pro (12)
                Pentium Pro
            
            Pentium(R) II (13)
                Pentium II
            
            Pentium(R) processor with MMX(TM) technology (14)
                Pentium Processor with MMX™ Technology
            
            Celeron(TM) (15)
                Celeron™
            
            Pentium(R) II Xeon(TM) (16)
                Pentium II Xeon™
            
            Pentium(R) III (17)
                Pentium III
            
            M1 Family (18)
            M2 Family (19)
            K5 Family (24)
                AMD Duron™ Processor Family
            
            K6 Family (25)
                K5 Family
            
            K6-2 (26)
                K6 Family
            
            K6-3 (27)
                K6-2
            
            AMD Athlon(TM) Processor Family (28)
                K6-3
            
            AMD(R) Duron(TM) Processor (29)
                AMD Athlon™ Processor Family
            
            AMD29000 Family (30)
                AMD2900 Family
            
            K6-2+ (31)
            Power PC Family (32)
            Power PC 601 (33)
            Power PC 603 (34)
            Power PC 603+ (35)
            Power PC 604 (36)
            Power PC 620 (37)
            Power PC X704 (38)
            Power PC 750 (39)
            Alpha Family (48)
            Alpha 21064 (49)
            Alpha 21066 (50)
            Alpha 21164 (51)
            Alpha 21164PC (52)
            Alpha 21164a (53)
            Alpha 21264 (54)
            Alpha 21364 (55)
            MIPS Family (64)
            MIPS R4000 (65)
            MIPS R4200 (66)
            MIPS R4400 (67)
            MIPS R4600 (68)
            MIPS R10000 (69)
            SPARC Family (80)
            SuperSPARC (81)
            microSPARC II (82)
            microSPARC IIep (83)
            UltraSPARC (84)
            UltraSPARC II (85)
            UltraSPARC IIi (86)
            UltraSPARC III (87)
            UltraSPARC IIIi (88)
            68040 (96)
            68xxx Family (97)
            68000 (98)
            68010 (99)
            68020 (100)
            68030 (101)
            Hobbit Family (112)
            Crusoe(TM) TM5000 Family (120)
                Crusoe™ TM5000 Family
            
            Crusoe(TM) TM3000 Family (121)
                Crusoe™ TM3000 Family
            
            Efficeon(TM) TM8000 Family (122)
                Efficeon™ TM8000 Family
            
            Weitek (128)
            
            Itanium(TM) Processor (130)
                Itanium™ Processor
            
            AMD Athlon(TM) 64 Processor Family (131)
                AMD Athlon™ 64 Processor Family
            
            AMD Opteron(TM) Family (132)
                AMD Opteron™ Processor Family
            
            PA-RISC Family (144)
            PA-RISC 8500 (145)
            PA-RISC 8000 (146)
            PA-RISC 7300LC (147)
            PA-RISC 7200 (148)
            PA-RISC 7100LC (149)
            PA-RISC 7100 (150)
            V30 Family (160)
            
            Pentium(R) III Xeon(TM) (176)
                Pentium III Xeon™ Processor
            
            Pentium(R) III Processor with Intel(R) SpeedStep(TM) Technology (177)
                Pentium III Processor with Intel SpeedStep™ Technology
            
            Pentium(R) 4 (178)
                Pentium 4
            
            Intel(R) Xeon(TM) (179)
                Intel Xeon™
            
            AS400 Family (180)
            
            Intel(R) Xeon(TM) processor MP (181)
                Intel Xeon™ Processor MP
            
            AMD AthlonXP(TM) Family (182)
                AMD Athlon™ XP Family
            
            AMD AthlonMP(TM) Family (183)
                AMD Athlon™ MP Family
            
            Intel(R) Itanium(R) 2 (184)
                Intel Itanium 2
            
            Intel Pentium M Processor (185)
            K7 (190)
            Intel Core™ i7-2760QM (198)
            IBM390 Family (200)
            G4 (201)
            G5 (202)
            G6 (203)
            z/Architecture base (204)
            i860 (250)
            i960 (251)
            SH-3 (260)
            SH-4 (261)
            ARM (280)
            StrongARM (281)
            6x86 (300)
            MediaGX (301)
            MII (302)
            WinChip (320)
            DSP (350)
            Video Processor (500)
        */
        /// <summary>
        /// Processor family type.
        /// </summary>
        public static object Family { get; protected set; }

        /// <summary>
        /// Date and time the object is installed. This property does not require a value to indicate that the object is installed. 
        /// This property is inherited from CIM_ManagedSystemElement.
        /// </summary>
        public static object InstallDate { get; protected set; }

        private static object _l2CacheSize;
        /// <summary>
        /// Size of the Level 2 processor cache. A Level 2 cache is an external memory area that has a faster access time than 
        /// the main RAM memory.
        /// </summary>
        public static object L2CacheSize
        {
            get
            {
                double tmp = Convert.ToDouble(_l2CacheSize);
                return tmp < CONVERTVALUE ? $"{tmp} KB L2Cache" : $"{tmp / CONVERTVALUE} MB L2Cache";
            }
            protected set { _l2CacheSize = Convert.ToUInt32(value); }
        }

        /// <summary>
        /// Clock speed of the Level 2 processor cache. A Level 2 cache is an external memory area that has a faster access time than 
        /// the main RAM memory.
        /// </summary>
        public static object L2CacheSpeed { get; protected set; }

        private static object _l3CacheSize;
        /// <summary>
        /// Size of the Level 3 processor cache. A Level 3 cache is an external memory area that has a faster access time than 
        /// the main RAM memory.
        /// </summary>
        public static object L3CacheSize
        {
            get
            {
                double tmp = Convert.ToDouble(_l3CacheSize);
                return tmp < CONVERTVALUE ? $"{tmp} KB L3Cache" : $"{tmp / CONVERTVALUE} MB L3Cache";
            }
            protected set { _l3CacheSize = Convert.ToUInt32(value); }
        }

        /// <summary>
        /// Clockspeed of the Level 3 property cache. A Level 3 cache is an external memory area that has a faster access time than 
        /// the main RAM memory.
        /// </summary>
        public static object L3CacheSpeed { get; protected set; }

        /// <summary>
        /// Last error code reported by the logical device.
        /// </summary>
        public static object LastErrorCode { get; protected set; }

        /// <summary>
        /// Definition of the processor type. The value depends on the <see cref="Architecture"/> of the processor.
        /// </summary>
        public static object Level { get; protected set; }

        /// <summary>
        /// Load capacity of each processor, averaged to the last second. Processor loading refers to the total computing burden 
        /// for each processor at one time.
        /// </summary>
        public static object LoadPercentage { get; protected set; }

        /// <summary>
        /// Name of the processor manufacturer.
        /// </summary>
        public static object Manufacturer { get; protected set; }

        private static object _maxClockSpeed;
        /// <summary>
        /// Maximum speed of the processor, in MHz.
        /// </summary>
        public static object MaxClockSpeed
        {
            get
            {
                var final = Convert.ToDouble(_maxClockSpeed);
                return final < 0x3E8 ? $"{final} MHz" : $"{(float)Math.Round(final / 0x3E8, 1)} GHz";
            }
            protected set { _maxClockSpeed = Convert.ToUInt32(value); }
        }

        /// <summary>
        /// Label by which the object is known. When this property is a subclass, it can be overridden to be a key property.
        /// </summary>
        public static object Name { get; protected set; }

        /// <summary>
        /// Number of cores for the current instance of the processor. A core is a physical processor on the integrated circuit. 
        /// <code>
        /// For example, in a dual-core processor this property has a value of 2.
        /// </code>
        /// </summary>
        public static object NumberOfCores { get; protected set; }

        /// <summary>
        /// The number of enabled cores per processor socket.
        /// </summary>
        public static object NumberOfEnabledCore { get; protected set; }

        /// <summary>
        /// Number of logical processors for the current instance of the processor. For processors capable of hyperthreading, 
        /// this value includes only the processors which have hyperthreading enabled.
        /// </summary>
        public static object NumberOfLogicalProcessors { get; protected set; }

        /// <summary>
        /// Processor family type. Used when the Family property is set to 1, which means Other. This string should be set to NULL 
        /// when the <see cref="Family"/> property is a value that is not 1.
        /// </summary>
        public static object OtherFamilyDescription { get; protected set; }

        /// <summary>
        /// The part number of this processor as set by the manufacturer.
        /// </summary>
        public static object PartNumber { get; protected set; }

        /*  PNPDeviceID:
            Unknown (0)
            Not Supported (1)
            Disabled (2)
            Enabled (3)
                The power management features are currently enabled but the exact feature set is unknown or the information is unavailable.
            
            Power Saving Modes Entered Automatically (4)
                The device can change its power state based on usage or other criteria.
            
            Power State Settable (5)
                The SetPowerState method is supported. This method is found on the parent CIM_LogicalDevice class and can be implemented. For more information, see Designing Managed Object Format (MOF) Classes.
            
            Power Cycling Supported (6)
                The SetPowerState method can be invoked with the PowerState parameter set to 5 (Power Cycle).
            
            Timed Power On Supported (7)
                Timed Power-On Supported
        */
        /// <summary>
        /// Windows Plug and Play device identifier of the logical device.
        /// </summary>
        public static object PNPDeviceID { get; protected set; }

        /// <summary>
        /// Array of the specific power-related capabilities of a logical device.
        /// </summary>
        public static object PowerManagementCapabilities { get; protected set; }

        /// <summary>
        /// If TRUE, the power of the device can be managed, which means that it can be put into suspend mode, and so on. 
        /// The property does not indicate that power management features are enabled, but it does indicate that the logical device 
        /// power can be managed.
        /// </summary>
        public static object PowerManagementSupported { get; protected set; }

        /// <summary>
        /// Processor information that describes the processor features. For an x86 class CPU, the field format depends on the 
        /// processor support of the CPUID instruction. If the instruction is supported, the property contains 2 (two) DWORD 
        /// formatted values. The first is an offset of 08h-0Bh, which is the EAX value that a CPUID instruction returns with 
        /// input EAX set to 1. The second is an offset of 0Ch-0Fh, which is the EDX value that the instruction returns. 
        /// Only the first two bytes of the property are significant and contain the contents of the DX register at CPU reset—all 
        /// others are set to 0 (zero), and the contents are in DWORD format.
        /// </summary>
        public static object ProcessorId { get; protected set; }

        /*  ProcessorType:
            Other (1)
            Unknown (2)
            Central Processor (3)
            Math Processor (4)
            DSP Processor (5)
            Video Processor (6)
        */
        /// <summary>
        /// Primary function of the processor.
        /// </summary>
        public static object ProcessorType { get; protected set; }

        /// <summary>
        /// System revision level that depends on the <see cref="Architecture"/>. The system revision level contains the same values as the 
        /// <see cref="Version"/> property, but in a numerical format.
        /// </summary>
        public static object Revision { get; protected set; }

        /// <summary>
        /// Role of the processor.
        /// <code>
        /// Examples: Central Processor or Math Processor
        /// </code>
        /// </summary>
        public static object Role { get; protected set; }

        /// <summary>
        /// If True, the processor supports address translation extensions used for virtualization.
        /// </summary>
        public static object SecondLevelAddressTranslationExtensions { get; protected set; }

        /// <summary>
        /// The serial number of this processor This value is set by the manufacturer and normally not changeable.
        /// </summary>
        public static object SerialNumber { get; protected set; }

        /// <summary>
        /// Type of chip socket used on the circuit.
        /// <code>
        /// Example: J202
        /// </code>
        /// </summary>
        public static object SocketDesignation { get; protected set; }

        /*  Status:
            OK ("OK")
            Error ("Error")
            Degraded ("Degraded")
            Unknown ("Unknown")
            Pred Fail ("Pred Fail")
            Starting ("Starting")
            Stopping ("Stopping")
            Service ("Service")
            Stressed ("Stressed")
            NonRecover ("NonRecover")
            No Contact ("No Contact")
            Lost Comm ("Lost Comm")
        */
        /// <summary>
        /// Current status of an object. This property is inherited from CIM_ManagedSystemElement.
        /// </summary>
        public static object Status { get; protected set; }

        /*  StatusInfo:
            Other (1)
            Unknown (2)
            Enabled (3)
            Disabled (4)
            Not Applicable (5)
        */
        /// <summary>
        /// State of the logical device. If this property does not apply to the logical device, use the value 5, which means Not Applicable.
        /// </summary>
        public static object StatusInfo { get; protected set; }

        /// <summary>
        /// Revision level of the processor in the processor family.
        /// </summary>
        public static object Stepping { get; protected set; }

        /// <summary>
        /// Value of the <see cref="CreationClassName"/> property for the scoping computer.
        /// </summary>
        public static object SystemCreationClassName { get; protected set; }

        /// <summary>
        /// Name of the scoping system.
        /// </summary>
        public static object SystemName { get; protected set; }

        /// <summary>
        /// The number of threads per processor socket.
        /// </summary>
        public static object ThreadCount { get; protected set; }

        /// <summary>
        /// Globally unique identifier for the processor. This identifier may only be unique within a processor family.
        /// </summary>
        public static object UniqueId { get; protected set; }

        /*  UpgradeMethod:
            Other (1)
            Unknown (2)
            Daughter Board (3)
            ZIF Socket (4)
            Replacement/Piggy Back (5)
                Replacement or Piggy Back
            
            None (6)
            LIF Socket (7)
            Slot 1 (8)
            Slot 2 (9)
            370 Pin Socket (10)
            Slot A (11)
            Slot M (12)
            Socket 423 (13)
            Socket A (Socket 462) (14)
            Socket 478 (15)
            Socket 754 (16)
            Socket 940 (17)
            Socket 939 (18)
        */
        /// <summary>
        /// CPU socket information, including the method by which this processor can be upgraded, if upgrades are supported. 
        /// This property is an integer enumeration.
        /// </summary>
        public static object UpgradeMethod { get; protected set; }

        /// <summary>
        /// Processor revision number that depends on the <see cref="Architecture"/>.
        /// <code>
        /// Example: Model 2, Stepping 12
        /// </code>
        /// </summary>
        public new static object Version { get; protected set; }

        /// <summary>
        /// If True, the Firmware has enabled virtualization extensions.
        /// </summary>
        public static object VirtualizationFirmwareEnabled { get; protected set; }

        /// <summary>
        /// If True, the processor supports Intel or AMD Virtual Machine Monitor extensions.
        /// </summary>
        public static object VMMonitorModeExtensions { get; protected set; }

        /*  VoltageCaps:
            5 (1)
                5 volts

            3.3 (2)
                3.3 volts
            
            2.9 (4)
                2.9 volts
        */
        /// <summary>
        /// Voltage capabilities of the processor. Bits 0-3 of the field represent specific voltages that the processor socket 
        /// can accept. All other bits should be set to 0 (zero). The socket is configurable if multiple bits are set. 
        /// For more information about the actual voltage at which the processor is running, see <see cref="CurrentVoltage"/>. 
        /// If the property is NULL, then the voltage capabilities are unknown.
        /// </summary>
        public static object VoltageCaps { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public static object ProcessCount => Process.GetProcesses().Length;

        /// <summary>
        /// 
        /// </summary>
        public static object ProcessorFrequency
        {
            get
            {
                float tmp = (float)(Math.Round(_processorFrequency.NextValue(), 1));
                return tmp < 0x3E8 ? $"{tmp} MHz".Trim() : $"{(float)Math.Round(tmp / 0x3E8, 1)} GHz".Trim();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static object ProcessorTime
        {
            get
            {
                float tmp = (float)(Math.Round(_processorTime.NextValue(), 1));
                return $"{tmp} %".Trim();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static object PrivilegedTime
        {
            get
            {
                float tmp = (float)(Math.Round(_privilegedTime.NextValue(), 1));
                return $"{tmp} %".Trim();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static object InterruptTime
        {
            get
            {
                float tmp = (float)(Math.Round(_interruptTime.NextValue(), 1));
                return $"{tmp} %".Trim();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static object DpcTime
        {
            get
            {
                float tmp = (float)(Math.Round(_dpcTime.NextValue(), 1));
                return $"{tmp} %".Trim();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static object ProcessorQueueLength => $"{_processorQueueLength.NextValue()}";

        /// <summary>
        /// 
        /// </summary>
        public static object ProcessThreads => _processThreadCount.NextValue();

        /// <summary>
        /// 
        /// </summary>
        public static object ProcessHandles => _processHandleCount.NextValue();


        private const string CATEGORY = "Processor";
        /// <summary>
        /// Initializes the performance counter.
        /// </summary>
        protected static void InitializePerformanceCounter()
        {
            _processorFrequency = new PerformanceCounter("Processor Information", "Processor Frequency", "_Total", true);
            _processorTime = new PerformanceCounter(CATEGORY, "% Processor Time", "_Total", true);
            _processorQueueLength = new PerformanceCounter("System", "Processor Queue Length", null, true);
            _privilegedTime = new PerformanceCounter(CATEGORY, "% Privileged Time", "_Total", true);
            _interruptTime = new PerformanceCounter(CATEGORY, "% Interrupt Time", "_Total", true);
            _dpcTime = new PerformanceCounter(CATEGORY, "% DPC Time", "_Total", true);
            _processHandleCount = new PerformanceCounter("Process", "Handle Count", "_Total", true);
            _processThreadCount = new PerformanceCounter("Process", "Thread Count", "_Total", true);
        }
    }
}
