﻿using System;
using System.Collections.Generic;

namespace Logic.Hardware.Model
{
    /*
       Source:
       https://docs.microsoft.com/en-us/windows/win32/cimwin32prov/win32-battery
    */
    /// <summary>
    /// The <see cref="Win32_Battery"/> WMI class represents a battery connected to the computer system.
    /// </summary>
    public class Win32_BatteryModel : Win32_Helper
    {
        public static bool ValuesInitialized { get; protected set; } = false;


        /*  Availability:
            Other (1)
            Unknown (2)
            Running/Full Power (3)
                Running or Full Power
            
            Warning (4)
            In Test (5)
            Not Applicable (6)
            Power Off (7)
            Off Line (8)
            Off Duty (9)
            Degraded (10)
            Not Installed (11)
            Install Error (12)
            Power Save - Unknown (13)
                The device is known to be in a power save mode, but its exact status is unknown.
            
            Power Save - Low Power Mode (14)
                The device is in a power save state but still functioning, and may exhibit degraded performance.
            
            Power Save - Standby (15)
                The device is not functioning, but could be brought to full power quickly.
            
            Power Cycle (16)
            Power Save - Warning (17)
                The device is in a warning state, though also in a power save mode.
            
            Paused (18)
                The device is paused.
            
            Not Ready (19)
                The device is not ready.
            
            Not Configured (20)
                The device is not configured.
            
            Quiesced (21)
                The device is quiet.
        */
        /// <summary>
        /// Availability and status of the device.
        /// </summary>
        public static object Availability { get; protected set; }

        /// <summary>
        /// Time required to fully charge the battery. This property is not supported. 
        /// BatteryRechargeTime does not have a replacement property, and is now considered obsolete.
        /// </summary>
        public static object BatteryRechargeTime { get; protected set; }

        /*  BatteryStatus:
            Other (1)
                The battery is discharging.
            
            Unknown (2)
                The system has access to AC so no battery is being discharged. However, the battery is not necessarily charging.
            
            Fully Charged (3)
            Low (4)
            Critical (5)
            Charging (6)
            Charging and High (7)
            Charging and Low (8)
            Charging and Critical (9)
            Undefined (10)
            Partially Charged (11)
        */
        /// <summary>
        /// Status of the battery. The value 10 (Undefined) is not valid in the CIM schema because in DMI it represents 
        /// that no battery is installed. In this case, the object should not be instantiated.
        /// </summary>
        public static object BatteryStatus { get; protected set; }

        /// <summary>
        /// Short description of the object a one-line string.
        /// </summary>
        public static object Caption { get; protected set; }

        /*  Chemistry:
            Other (1)
            Unknown (2)
            Lead Acid (3)
            Nickel Cadmium (4)
            Nickel Metal Hydride (5)
            Lithium-ion (6)
            Zinc air (7)
            Lithium Polymer (8)
        */
        /// <summary>
        /// Enumeration that describes the battery's chemistry.
        /// </summary>
        public static object Chemistry { get; protected set; }

        /*  ConfigManagerErrorCode:
            This device is working properly. (0)
                Device is working properly.
            
            This device is not configured correctly. (1)
                Device is not configured correctly.
            
            Windows cannot load the driver for this device. (2)
            The driver for this device might be corrupted, or your system may be running low on memory or other resources. (3)
                Driver for this device might be corrupted, or the system may be low on memory or other resources.
            
            This device is not working properly. One of its drivers or your registry might be corrupted. (4)
                Device is not working properly. One of its drivers or the registry might be corrupted.
            
            The driver for this device needs a resource that Windows cannot manage. (5)
                Driver for the device requires a resource that Windows cannot manage.
            
            The boot configuration for this device conflicts with other devices. (6)
                Boot configuration for the device conflicts with other devices.
            
            Cannot filter. (7)
            The driver loader for the device is missing. (8)
                Driver loader for the device is missing.
            
            This device is not working properly because the controlling firmware is reporting the resources for the device incorrectly. (9)
                Device is not working properly. The controlling firmware is incorrectly reporting the resources for the device.
            
            This device cannot start. (10)
                Device cannot start.
            
            This device failed. (11)
                Device failed.
            
            This device cannot find enough free resources that it can use. (12)
                Device cannot find enough free resources to use.
            
            Windows cannot verify this device's resources. (13)
                Windows cannot verify the device's resources.
            
            This device cannot work properly until you restart your computer. (14)
                Device cannot work properly until the computer is restarted.
            
            This device is not working properly because there is probably a re-enumeration problem. (15)
                Device is not working properly due to a possible re-enumeration problem.
            
            Windows cannot identify all the resources this device uses. (16)
                Windows cannot identify all of the resources that the device uses.
            
            This device is asking for an unknown resource type. (17)
                Device is requesting an unknown resource type.
            
            Reinstall the drivers for this device. (18)
                Device drivers must be reinstalled.
            
            Failure using the VxD loader. (19)
            Your registry might be corrupted. (20)
                Registry might be corrupted.
            
            System failure: Try changing the driver for this device. If that does not work, see your hardware documentation. Windows is removing this device. (21)
                System failure. If changing the device driver is ineffective, see the hardware documentation. Windows is removing the device.
            
            This device is disabled. (22)
                Device is disabled.
            
            System failure: Try changing the driver for this device. If that doesn't work, see your hardware documentation. (23)
                System failure. If changing the device driver is ineffective, see the hardware documentation.
            
            This device is not present, is not working properly, or does not have all its drivers installed. (24)
                Device is not present, not working properly, or does not have all of its drivers installed.
            
            Windows is still setting up this device. (25)
                Windows is still setting up the device.
            
            Windows is still setting up this device. (26)
                Windows is still setting up the device.
            
            This device does not have valid log configuration. (27)
                Device does not have valid log configuration.
            
            The drivers for this device are not installed. (28)
                Device drivers are not installed.
            
            This device is disabled because the firmware of the device did not give it the required resources. (29)
                Device is disabled. The device firmware did not provide the required resources.
            
            This device is using an Interrupt Request (IRQ) resource that another device is using. (30)
                Device is using an IRQ resource that another device is using.
            
            This device is not working properly because Windows cannot load the drivers required for this device. (31)
                Device is not working properly. Windows cannot load the required device drivers.
        */
        /// <summary>
        /// Windows Configuration Manager error code.
        /// </summary>
        public static object ConfigManagerErrorCode { get; protected set; }

        /// <summary>
        /// If True, the device is using a user-defined configuration.
        /// </summary>
        public static object ConfigManagerUserConfig { get; protected set; }

        /// <summary>
        /// Name of the first concrete class that appears in the inheritance chain used in the creation of an instance. When 
        /// used with the other key properties of the class, the property allows all instances of this class and its subclasses to 
        /// be identified uniquely.
        /// </summary>
        public static object CreationClassName { get; protected set; }

        /// <summary>
        /// Description of the object.
        /// </summary>
        public static object Description { get; protected set; }

        /// <summary>
        /// Design capacity of the battery in milliwatt-hours. If the property is not supported, enter 0 (zero).
        /// </summary>
        public static object DesignCapacity { get; protected set; }

        /// <summary>
        /// Design voltage of the battery in millivolts. If the attribute is not supported, enter 0 (zero).
        /// </summary>
        public static object DesignVoltage { get; protected set; }

        /// <summary>
        /// Identifies the battery.
        /// <code>
        /// Example: "Internal Battery"
        /// </code>
        /// </summary>
        public static object DeviceID { get; protected set; }

        /// <summary>
        /// If True, the error reported in the <see cref="LastErrorCode"/> property is now cleared.
        /// </summary>
        public static object ErrorCleared { get; protected set; }

        /// <summary>
        /// Free-form string that supplies more information about the error recorded in <see cref="LastErrorCode"/> property, 
        /// and information about any corrective actions that may be taken.
        /// </summary>
        public static object ErrorDescription { get; protected set; }

        /// <summary>
        /// Estimate of the percentage of full charge remaining.
        /// </summary>
        public static object EstimatedChargeRemaining { get; protected set; }

        /// <summary>
        /// Estimate in minutes of the time to battery charge depletion under the present load conditions if the utility 
        /// power is off, or lost and remains off, or a laptop is disconnected from a power source.
        /// </summary>
        public static object EstimatedRunTime { get; protected set; }

        /// <summary>
        /// Amount of time it takes to completely drain the battery after it is fully charged. 
        /// This property is no longer used and is considered obsolete.
        /// </summary>
        public static object ExpectedBatteryLife { get; protected set; }

        /// <summary>
        /// Battery's expected lifetime in minutes, assuming that the battery is fully charged. 
        /// The property represents the total expected life of the battery, not its current remaining life, 
        /// which is indicated by the <see cref="EstimatedRunTime"/> property.
        /// </summary>
        public static object ExpectedLife { get; protected set; }

        /// <summary>
        /// Full charge capacity of the battery in milliwatt-hours. Comparison of the value to the <see cref="DesignCapacity"/> property 
        /// determines when the battery requires replacement. A battery's end of life is typically when the FullChargeCapacity 
        /// property falls below 80% of the <see cref="DesignCapacity"/> property. If the property is not supported, enter 0 (zero).
        /// </summary>
        public static object FullChargeCapacity { get; protected set; }

        /// <summary>
        /// Date and time the object was installed. This property does not need a value to indicate that the object is installed.
        /// </summary>
        public static object InstallDate { get; protected set; }

        /// <summary>
        /// Last error code reported by the logical device.
        /// </summary>
        public static object LastErrorCode { get; protected set; }

        /// <summary>
        /// Maximum time, in minutes, to fully charge the battery. The property represents the time to recharge a fully 
        /// depleted battery, not the current remaining charge time, which is indicated in the <see cref="TimeToFullCharge"/> property.
        /// </summary>
        public static object MaxRechargeTime { get; protected set; }

        /// <summary>
        /// Defines the label by which the object is known. When subclassed, the property can be overridden to be a key property.
        /// </summary>
        public static object Name { get; protected set; }

        /// <summary>
        /// Windows Plug and Play device identifier of the logical device.
        /// <code>
        /// Example: "*PNP030b"
        /// </code>
        /// </summary>
        public static object PNPDeviceID { get; protected set; }

        /*  PowerManagementCapabilities:
            Unknown (0)
            Not Supported (1)
            Disabled (2)
            Enabled (3)
                The power management features are currently enabled but the exact feature set is unknown or the information is unavailable.
            
            Power Saving Modes Entered Automatically (4)
                The device can change its power state based on usage or other criteria.
            
            Power State Settable (5)
                The SetPowerState method is supported. This method is found on the parent CIM_LogicalDevice class and can be implemented. For more information, see Designing Managed Object Format (MOF) Classes.
            
            Power Cycling Supported (6)
                The SetPowerState method can be invoked with the PowerState parameter set to 5 (Power Cycle).
            
            Timed Power On Supported (7)
                Timed Power-On Supported
        */
        /// <summary>
        /// Array of the specific power-related capabilities of a logical device.
        /// </summary>
        public static object PowerManagementCapabilities { get; protected set; }

        /// <summary>
        /// If True, the device can be power-managed (can be put into suspend mode, and so on). The property does not indicate 
        /// that power management features are currently enabled, only that the logical device is capable of power management.
        /// </summary>
        public static object PowerManagementSupported { get; protected set; }

        /// <summary>
        /// Data Specification version number supported by the battery. If the battery does not support this function, 
        /// the value should be left blank.
        /// </summary>
        public static object SmartBatteryVersion { get; protected set; }

        private static ushort _status;
        /*  Status:
            OK ("OK")
            Error ("Error")
            Degraded ("Degraded")
            Unknown ("Unknown")
            Pred Fail ("Pred Fail")
            Starting ("Starting")
            Stopping ("Stopping")
            Service ("Service")
            Stressed ("Stressed")
            NonRecover ("NonRecover")
            No Contact ("No Contact")
            Lost Comm ("Lost Comm")
        */
        /// <summary>
        /// Current status of the object. Various operational and nonoperational statuses can be defined.
        /// <code>
        /// Operational statuses include: "OK", "Degraded", and "Pred Fail"
        /// </code>
        /// (an element, such as a SMART-enabled hard disk drive, may be functioning properly but predicting a failure in the near future).
        /// <code>
        /// Nonoperational statuses include: "Error", "Starting", "Stopping", and "Service"
        /// </code>
        /// The latter, "Service", could apply during mirror-resilvering of a disk, reload of a user permissions list, 
        /// or other administrative work. Not all such work is online, yet the managed element is neither "OK" nor in one of the other states.
        /// </summary>
        public static string Status
        {
            get
            {
                var codes = new Dictionary<ushort, string>
                {
                    { 0, "Batterie nicht vorhanden!" },
                    { 1, "Die Batterie entlädt sich" },
                    { 2, "Das System hat Zugang zu Wechselstrom, so dass keine Batterie entladen wird." },
                    { 3, "Voll aufgeladen" },
                    { 4, "Niedrig" },
                    { 5, "Kritisch" },
                    { 6, "Aufladen" },
                    { 7, "Laden und Hoch" },
                    { 8, "Laden und Niedrig" },
                    { 9, "Nicht definiert" },
                    { 10, "Teilweise aufgeladen" }
                };
                return codes[_status];
            }
            protected set { _status = Convert.ToUInt16(value); }
        }

        /*  StatusInfo:
            Other (1)
            Unknown (2)
            Enabled (3)
            Disabled (4)
            Not Applicable (5)
        */
        /// <summary>
        /// State of the logical device. If this property does not apply to the logical device, the value 5 (Not Applicable) should be used.
        /// </summary>
        public static object StatusInfo { get; protected set; }

        /// <summary>
        /// Value of the scoping computer's <see cref="CreationClassName"/> property.
        /// </summary>
        public static object SystemCreationClassName { get; protected set; }

        /// <summary>
        /// Name of the scoping system.
        /// </summary>
        public static object SystemName { get; protected set; }

        /// <summary>
        /// Elapsed time in seconds since the computer system's UPS last switched to battery power, or the time since the system 
        /// or UPS was last restarted, whichever is less. If the battery is "on line", 0 (zero) is returned.
        /// </summary>
        public static object TimeOnBattery { get; protected set; }

        /// <summary>
        /// Remaining time to charge the battery fully in minutes at the current charging rate and usage.
        /// </summary>
        public static object TimeToFullCharge { get; protected set; }
    }
}