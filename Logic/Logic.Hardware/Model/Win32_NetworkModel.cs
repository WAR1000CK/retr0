﻿using System.Management;
using System.Net;
using System.Net.NetworkInformation;
using System.Text.RegularExpressions;

namespace Logic.Hardware.Model
{
    /*
       Source:
       https://docs.microsoft.com/en-us/windows/win32/cimwin32prov/win32-networkadapter
    */
    /// <summary>
    /// The <see cref="Win32_Network"/> class represents a network adapter of a computer running a Windows operating system.
    /// </summary>
    public class Win32_NetworkModel : Win32_NetworkCounter
    {
        /// <summary>
        /// Gets or sets a value indicating whether [values initialized].
        /// </summary>
        public static bool ValuesInitialized { get; protected set; } = false;

        public static ManagementObjectCollection ObjectCollection { get; set; }

        /// <summary>
        /// Gets or sets the ip properties.
        /// </summary>
        protected static IPInterfaceProperties IPProperties { get; set; }

        /// <summary>
        /// Gets or sets the IPv6 statistics.
        /// </summary>
        protected static IPInterfaceStatistics IPStatistics { get; set; }

        /// <summary>
        /// Gets or sets the IPv4 statistics.
        /// </summary>
        protected static IPv4InterfaceStatistics IPv4Statistics { get; set; }

        /// <summary>
        /// Gets the physical address.
        /// </summary>
        private static string _physicalAddress;
        public static string PhysicalAddress
        {
            get => _physicalAddress;
            protected set
            {
                var regex = "(.{2})(.{2})(.{2})(.{2})(.{2})(.{2})";
                var replace = "$1:$2:$3:$4:$5:$6";
                var newformat = Regex.Replace(value, regex, replace);

                _physicalAddress = newformat;
            }
        }

        /// <summary>
        /// Gets the length of the output queue.
        /// </summary>
        public static long OutputQueueLength => IPv4Statistics.OutputQueueLength;

        /// <summary>
        /// Gets the total download size.
        /// </summary>
        public static string TotalDownload => BytesToString(IPv4Statistics.BytesReceived);

        /// <summary>
        /// Gets the total upload size.
        /// </summary>
        public static string TotalUpload => BytesToString(IPv4Statistics.BytesSent);

        /// <summary>
        /// Gets the network interface description.
        /// </summary>
        public static string Description { get; protected set; }

        /// <summary>
        /// Gets the network interface identifier.
        /// </summary>
        public static string Id { get; protected set; }

        /// <summary>
        /// Gets a value indicating whether network interface is receive only.
        /// </summary>
        public static bool IsReceiveOnly { get; protected set; }

        /// <summary>
        /// Gets the network interface name.
        /// </summary>
        public static string Name { get; protected set; }

        /// <summary>
        /// Gets the type of the network interface.
        /// </summary>
        public static NetworkInterfaceType NetworkInterfaceType { get; protected set; }

        /// <summary>
        /// Gets the network interface operational status.
        /// </summary>
        public static OperationalStatus OperationalStatus { get; protected set; }

        /// <summary>
        /// Gets the network interface speed.
        /// </summary>
        public static string Speed { get; protected set; }

        /// <summary>
        /// Gets a value indicating whether network interface [supports multicast].
        /// </summary>
        public static bool SupportsMulticast { get; protected set; }

        /// <summary>
        /// Gets the network interface DNS suffix.
        /// </summary>
        public static string DnsSuffix { get; protected set; }

        /// <summary>
        /// Gets the network interface DNS addresse.
        /// </summary>
        public static IPAddress DnsAddresse { get; protected set; }

        /// <summary>
        /// Gets the gateway address ipv4.
        /// </summary>
        public static IPAddress GatewayAddressIpv4 { get; protected set; }

        /// <summary>
        /// Gets the gateway address ipv6.
        /// </summary>
        public static IPAddress GatewayAddressIpv6 { get; protected set; }


        // ----------------------------------------------------------------------
        // OPTIONAL FEATURES
        // ----------------------------------------------------------------------

        /// <summary>
        /// A maximum transmission unit (MTU) is the largest size packet or frame, specified in octets (eight-bit bytes), 
        /// that can be sent in a packet- or frame-based network such as the Internet.
        /// </summary>
        public static object ActiveMaximumTransmissionUnit { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public static object AdminLocked { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public static object ComponentID { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public static object ConnectorPresent { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public static object DeviceWakeUpEnable { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public static object DriverDate { get; protected set; } // TODO: Date converter

        /// <summary>
        /// 
        /// </summary>
        public static object DriverName { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public static object DriverVersionString { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public static object DriverProvider { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public static object ReceiveLinkSpeed { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public static object Virtual { get; protected set; }
    }
}
