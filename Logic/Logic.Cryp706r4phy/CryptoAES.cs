﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Logic.Cryp706r4phy
{
    public class CryptoAES
    {
        private readonly Aes aesProvider;

        /// <summary>
        /// Initializes a new instance of the <see cref="CryptoAES"/> class.
        /// </summary>
        /// <param name="initVector">The Initialization vector. The vector must be 16 characters long</param>
        /// <param name="keysize">The keysize. Recommended are 256bit.</param>
        /// <param name="key">The key. With a key size of 256bit, the key must be 32 characters long</param>
        public CryptoAES(string initVector, int keysize, string key)
        {
            if (initVector.Length != 16)
                throw new ArgumentException($"Invalid Initialization vector length!\nLength must be 16 chars long.\n\nCurrent length: {initVector.Length}");

            aesProvider = new AesCryptoServiceProvider
            {
                IV = Encoding.ASCII.GetBytes(initVector),
                Key = Encoding.ASCII.GetBytes(key),
                BlockSize = 128,
                KeySize = keysize,
                Mode = CipherMode.CBC,
                Padding = PaddingMode.PKCS7
            };
        }

        /// <summary>
        /// Encrypts the specified clear text.
        /// </summary>
        /// <param name="clearText">The clear text.</param>
        /// <returns></returns>
        public string Encrypt(string clearText)
        {
            if (string.IsNullOrEmpty(clearText))
                throw new ArgumentException("Bad input!", clearText);

            ICryptoTransform transform = aesProvider.CreateEncryptor();
            byte[] encryptedBytes = transform.TransformFinalBlock(Encoding.ASCII.GetBytes(Normalize(clearText)), 0, Normalize(clearText).Length);
            string baseString = Convert.ToBase64String(encryptedBytes);

            return baseString;
        }

        /// <summary>
        /// Decrypts the specified cipher text.
        /// </summary>
        /// <param name="cipherText">The cipher text.</param>
        /// <returns></returns>
        public string Decrypt(string cipherText)
        {
            if (string.IsNullOrEmpty(cipherText))
                throw new ArgumentException("Bad input!", cipherText);

            ICryptoTransform transform = aesProvider.CreateDecryptor();
            byte[] encodetBytes = Convert.FromBase64String(cipherText);
            byte[] decryptedBytes = transform.TransformFinalBlock(encodetBytes, 0, encodetBytes.Length);

            string baseString = ASCIIEncoding.ASCII.GetString(decryptedBytes);

            return baseString;
        }

        /// <summary>
        /// The Normalize() method is used to get a new string whose textual value is same as this string,
        /// but whose binary representation is in Unicode normalization form.
        /// </summary>
        /// <param name="clearText">The clear text.</param>
        /// <returns></returns>
        private string Normalize(string clearText)
        {
            clearText = clearText.Replace("Ä", "Ae");
            clearText = clearText.Replace("ä", "ae");
            clearText = clearText.Replace("Ö", "Oe");
            clearText = clearText.Replace("ö", "oe");
            clearText = clearText.Replace("Ü", "Ue");
            clearText = clearText.Replace("ü", "ue");
            clearText = clearText.Replace("ß", "ss");

            return clearText;
        }
    }
}
