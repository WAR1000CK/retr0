﻿using Logic.Cryp706r4phy.Enums;
using System;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;

namespace Logic.Cryp706r4phy
{
    /// <summary>
    /// The Secure Hash Algorithms are a family of cryptographic hash functions published by the
    /// National Institute of Standards and Technology (NIST).
    /// </summary>
    public class CryptoSHA
    {
        #region Propertys

        public static string Version { get => Assembly.GetExecutingAssembly().GetName().Version.ToString(); }

        #endregion

        /// <summary>
        /// Encrypts the specified clear text.
        /// </summary>
        /// <param name="supported">The supported hash.</param>
        /// <param name="clearText">The clear text.</param>
        /// <param name="salt">The salt.</param>
        /// <returns></returns>
        public static string Compute(Supported_SHA supported, string clearText, string salt)
        {
            if (clearText == default || salt == default)
                throw new ArgumentException("Bad input!", $"{clearText} {salt}");

            var builder = new StringBuilder();
            switch (supported)
            {
                case Supported_SHA.Sha1:
                    using (SHA1 hash = new SHA1Managed())
                    {
                        byte[] tmpHash = hash.ComputeHash(Encoding.ASCII.GetBytes(clearText + salt));
                        foreach (byte b in tmpHash)
                            builder.AppendFormat("{0:x2}", b);
                    }
                    return builder.ToString();

                case Supported_SHA.Sha256:
                    using (SHA256 hash = new SHA256Managed())
                    {
                        byte[] tmpHash = hash.ComputeHash(Encoding.ASCII.GetBytes(clearText + salt));
                        foreach (byte b in tmpHash)
                            builder.AppendFormat("{0:x2}", b);
                    }
                    return builder.ToString();

                case Supported_SHA.Sha384:
                    using (SHA384 hash = new SHA384Managed())
                    {
                        byte[] tmpHash = hash.ComputeHash(Encoding.ASCII.GetBytes(clearText + salt));
                        foreach (byte b in tmpHash)
                            builder.AppendFormat("{0:x2}", b);
                    }
                    return builder.ToString();

                case Supported_SHA.Sha512:
                    using (SHA512 hash = new SHA512Managed())
                    {
                        byte[] tmpHash = hash.ComputeHash(Encoding.ASCII.GetBytes(clearText + salt));
                        foreach (byte b in tmpHash)
                            builder.AppendFormat("{0:x2}", b);
                    }
                    return builder.ToString();

                default:
                    return "Not supported";
            }
        }
    }
}
