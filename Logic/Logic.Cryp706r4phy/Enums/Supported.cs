﻿namespace Logic.Cryp706r4phy.Enums
{
    /// <summary>
    /// Supported Sha-Algorithmus
    /// </summary>
    public enum Supported_SHA
    {
        /// <summary>
        /// The Sha1-160 bit,
        /// first published: 1995
        /// </summary>
        Sha1,

        /// <summary>
        /// The Sha-256 bit,
        /// first published: 2001
        /// </summary>
        Sha256,

        /// <summary>
        /// The Sha-384 bit,
        /// first published: 2001
        /// </summary>
        Sha384,

        /// <summary>
        /// The Sha-512 bit,
        /// first published: 2001
        /// </summary>
        Sha512
    }

    /// <summary>
    /// Supported MD-Algorithmus
    /// </summary>
    public enum Supported_MD
    {
        /// <summary>
        /// The MD5-128 bit,
        /// first published: 1992
        /// </summary>
        MD5
    }
}
