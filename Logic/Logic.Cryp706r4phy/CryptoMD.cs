﻿using Logic.Cryp706r4phy.Enums;
using System;
using System.Security.Cryptography;
using System.Text;

namespace Logic.Cryp706r4phy
{
    /// <summary>
    /// Message-Digest Algorithm 5 (MD5) is a widely used cryptographic hash function that generates a 128-bit hash value from any message.
    /// This allows, for example, easy checking of a download for correctness.
    /// </summary>
    public class CryptoMD
    {
        /// <summary>
        /// Encrypts the specified clear text.
        /// </summary>
        /// <param name="supported">The supported hash.</param>
        /// <param name="clearText">The clear text.</param>
        /// <param name="salt">The salt.</param>
        /// <returns></returns>
        public static string Compute(Supported_MD supported, string clearText, string salt)
        {
            if (clearText == default || salt == default)
                throw new ArgumentException("Bad input!", $"{clearText} {salt}");

            switch (supported)
            {
                case Supported_MD.MD5:
                    var builder = new StringBuilder();
                    using (MD5 hash = new MD5CryptoServiceProvider())
                    {
                        byte[] tmpHash = hash.ComputeHash(Encoding.ASCII.GetBytes(clearText + salt));
                        foreach (byte b in tmpHash)
                            builder.AppendFormat("{0:x2}", b);
                    }
                    return builder.ToString();

                default:
                    return "Not supported";
            }
        }
    }
}
