﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;

namespace Logic.Database
{
    public class Validate
    {
        /// <summary>
        /// Validates the username.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <returns></returns>
        protected static bool ValidateUsername(string username)
        {
            if (string.IsNullOrWhiteSpace(username))
                throw new ArgumentNullException(nameof(username));

            if (username.Length < 4)
                throw new Exception("Insufficient username length. Username must be at least 4 characters long");

            return true;
        }

        /// <summary>
        /// Validates the password.
        /// </summary>
        /// <param name="password">The password.</param>
        /// <param name="minPasswordLength">Minimum length of the password.</param>
        /// <param name="minNumOfSpecialCharacters">The minimum number of special characters.</param>
        /// <param name="minNumOfUpperCaseCharacters">The minimum number of upper case characters.</param>
        /// <param name="minNumOfLowerCaseCharacters">The minimum number of lower case characters.</param>
        /// <param name="minNumOfNumericCharacters">The minimum number of numeric characters.</param>
        /// <returns></returns>
        protected static bool ValidatePassword(string password, int minPasswordLength, int minNumOfSpecialCharacters, int minNumOfUpperCaseCharacters, int minNumOfLowerCaseCharacters, int minNumOfNumericCharacters)
        {
            if (string.IsNullOrWhiteSpace(password))
                throw new ArgumentNullException(nameof(password));

            int numberOfSpecialCharacters = 0;
            int numberOfLowerCaseCharacters = 0;
            int numberOfUpperCaseCharacters = 0;
            int numberOfNumericCharacters = 0;

            var regexSpecialCharacter = new Regex("[^A-Za-z0-9]");
            var regexLowerCaseCharacter = new Regex("[a-z]");
            var regexUpperCaseCharacter = new Regex("[A-Z]");
            var regexNumericCharacter = new Regex("[0-9]");

            foreach (var p in password)
            {
                if (regexSpecialCharacter.IsMatch(p.ToString()))
                    numberOfSpecialCharacters++;

                if (regexLowerCaseCharacter.IsMatch(p.ToString()))
                    numberOfLowerCaseCharacters++;

                if (regexUpperCaseCharacter.IsMatch(p.ToString()))
                    numberOfUpperCaseCharacters++;

                if (regexNumericCharacter.IsMatch(p.ToString()))
                    numberOfNumericCharacters++;
            }

            if (password.Length < minPasswordLength)
                throw new Exception($"Insufficient password length. Password must be at least {minPasswordLength} characters long");
            if (numberOfSpecialCharacters < minNumOfSpecialCharacters)
                throw new Exception($"Please insert at least {minNumOfSpecialCharacters} special character.");
            if (numberOfLowerCaseCharacters < minNumOfLowerCaseCharacters)
                throw new Exception($"Please insert at least {minNumOfLowerCaseCharacters} lowercase letters.");
            if (numberOfUpperCaseCharacters < minNumOfUpperCaseCharacters)
                throw new Exception($"Please insert at least {minNumOfUpperCaseCharacters} uppercase letters.");
            if (numberOfNumericCharacters < minNumOfNumericCharacters)
                throw new Exception($"Please insert at least {minNumOfNumericCharacters} numbers.");

            return (numberOfSpecialCharacters >= minNumOfSpecialCharacters)
                   && (numberOfLowerCaseCharacters >= minNumOfLowerCaseCharacters)
                   && (numberOfUpperCaseCharacters >= minNumOfUpperCaseCharacters)
                   && (numberOfNumericCharacters >= minNumOfNumericCharacters);
        }

        /// <summary>
        /// Validates the email.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <returns></returns>
        protected static bool ValidateEmail(string email)
        {
            if (string.IsNullOrWhiteSpace(email))
                throw new ArgumentNullException(nameof(email));

            try
            {
                // Normalize the domain
                email = Regex.Replace(email, @"(@)(.+)$", DomainMapper, RegexOptions.None, TimeSpan.FromMilliseconds(200));

                // Examines the domain part of the email and normalizes it.
                string DomainMapper(Match match)
                {
                    // Use IdnMapping class to convert Unicode domain names.
                    var idn = new IdnMapping();

                    // Pull out and process domain name (throws ArgumentException on invalid)
                    var domainName = idn.GetAscii(match.Groups[2].Value);

                    return match.Groups[1].Value + domainName;
                }
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
            try
            {
                #region Expression Description
                /*
                    Muster ^: Starten Sie den Vergleich am Beginn der Zeichenfolge.

                    Muster (?("): Ermittelt, ob es sich beim ersten Zeichen um ein Anführungszeichen handelt.
                    (?(") ist der Anfang eines Alternierungskonstrukts.
                    
                    Muster (?(")(".+?(?<!\\)"@): Wenn es sich beim ersten Zeichen um ein Anführungszeichen handelt, wird ein
                    öffnendes Anführungszeichen als Übereinstimmung verwendet, dem mindestens ein beliebiges Zeichen und ein schließendes
                    Anführungszeichen folgen. Dem schließenden Anführungszeichen darf kein umgekehrter Schrägstrich (\) vorangestellt sein.
                    (?<! ist der Anfang einer negativen Lookbehindassertion mit einer Breite von Null. Die Zeichenfolge muss mit einem @-Zeichen enden.
                    
                    Muster |(([0-9a-z]: Wenn es sich beim ersten Zeichen um kein Anführungszeichen handelt, wird ein beliebiges Buchstabenzeichen
                    von a bis z oder A bis Z (die Groß-/Kleinschreibung wird nicht beachtet) oder ein beliebiges numerisches Zeichen von 0 bis 9 als
                    Übereinstimmung verwendet.
                    
                    Muster (\.(?!\.)): Wenn es sich beim nächsten Zeichen um einen Punkt handelt, wird dieser als Übereinstimmung verwendet. 
                    Wenn es sich um keinen Punkt handelt, wird bis zum nächsten Zeichen weitergesucht und der Vergleich fortsetzt. (?!\.) ist eine 
                    negative Lookaheadassertion mit einer Breite von 0 (Null), die verhindert, dass im lokalen Teil einer E-Mail-Adresse zwei 
                    aufeinander folgende Punkte enthalten sind.
                    
                    Muster |[-!#\$%&'\*\+/=\?\^`\{\}\|~\w]: Wenn es sich beim nächsten Zeichen nicht um einen Punkt handelt, wird ein beliebiges 
                    Wortzeichen oder eines der folgenden Zeichen als Übereinstimmung verwendet: -!#$%&'*+/=?^`{}|~
                    
                    Muster ((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*: 0 oder mehr Vorkommen des Alternierungsmusters werden als Übereinstimmung 
                    verwendet (ein Punkt, dem ein anderes Zeichen als ein Punkt oder eines aus einer Reihe von Zeichen folgt).
                    
                    Muster @: Das @ Zeichen wird als Übereinstimmung verwendet.
                    
                    Muster (?<=[0-9a-z]): Die Suche wird fortgesetzt, wenn es sich bei dem Zeichen, das dem @ Zeichen vorausgeht, ist, um eines 
                    der Zeichen A bis Z, a bis z oder 0 bis 9 handelt. Dieses Muster definiert eine positive Lookbehindassertion mit einer Breite von 0 (null).
                    
                    Muster (?(\[): Überprüfen Sie, ob es sich bei dem Zeichen, das @ folgt, um eine eckige Klammer links handelt.
                    
                    Muster (\[(\d{1,3}\.){3}\d{1,3}\]): Wenn es sich um eine eckige Klammer links handelt, wird die eckige Klammer links als 
                    Übereinstimmung verwendet, der eine IP-Adresse (vier Gruppen aus einer bis drei Ziffern, jeweils durch einen Punkt getrennt) 
                    und eine eckige Klammer rechts folgen.
                    
                    Muster |(([0-9a-z][-0-9a-z]*[0-9a-z]*\.)+: Wenn es sich bei dem Zeichen, das @ folgt, um keine öffnende eckige Klammer handelt, 
                    wird ein alphanumerisches Zeichen mit einem Wert von A-Z, a-z oder 0-9 als Übereinstimmung verwendet, dem null oder mehr 
                    Vorkommen eines Bindestrichs und kein oder ein alphanumerisches Zeichen mit einem Wert von A-Z, a-z oder 0-9 sowie ein Punkt folgen. 
                    Dieses Muster kann ein- oder mehrmals wiederholt werden und der Domänenname der obersten Ebene muss darauf folgen.
                    
                    Muster [a-z0-9][\-a-z0-9]{0,22}[a-z0-9])): Der Domänenname der obersten Ebene muss mit einem alphanumerischen Zeichen (a-z, A-Z und 0-9) 
                    beginnen und enden. Er kann außerdem 0 bis 22 ASCII-Zeichen enthalten, die entweder alphanumerische Zeichen oder Bindestriche sind.
                    
                    Muster $: Beendet die Suche am Ende der Zeichenfolge.
                */
                #endregion
                return Regex.IsMatch(email,
                    @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                    @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-0-9a-z]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
                    RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
            }
            catch (Exception ex) { throw new Exception(ex.Message); }
        }

    }
}
