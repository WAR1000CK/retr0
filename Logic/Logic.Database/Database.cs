﻿using Logic.Cryp706r4phy;
using Logic.Cryp706r4phy.Enums;
using Logic.Debug.Enums;
using MySql.Data.MySqlClient;
using System;
using System.Reflection;

namespace Logic.Database
{
    public class Database : Validate
    {
        #region Propertys

        public static string Version { get => Assembly.GetExecutingAssembly().GetName().Version.ToString(); }

        #endregion

        #region Private fields

        private readonly string _db;
        private readonly string _table;
        private readonly MySqlConnection _connection;
        private readonly string _salt;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="Database"/> class.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <param name="table">The table.</param>
        /// <param name="connectionString">The mysql connection string.</param>
        /// <param name="salt">The salt for Sha algorithm.</param>
        public Database(string database, string table, string connectionString, string salt)
        {
            _db = database;
            _table = table;
            _connection = new MySqlConnection(connectionString);
            _salt = salt;

            #region Events

#if DEBUG
            // Handles the InfoMessage event of the MySqlConnection.
            _connection.InfoMessage += (s, e) =>
            {
                foreach (MySqlError error in e.errors)
                {
                    Debug.Message.Handle($"{error.Level} {error.Code} {error.Message}", ConsoleColor.Red, Logtype.Database);
                }
            };

            // Handles the StateChange event of the MySqlConnection.
            _connection.StateChange += (s, e) =>
            {
                Debug.Message.Handle($"{e.OriginalState} >> {e.CurrentState}", ConsoleColor.Yellow, Logtype.Database);
            };
#endif
            #endregion
        }

        /// <summary>
        /// Opens the mysql connection.
        /// </summary>
        /// <returns></returns>
        private bool OpenConnection()
        {
            try
            {
                if (_connection.State == System.Data.ConnectionState.Closed)
                {
                    _connection.Open();
                    return true;
                }
                else { throw new Exception("Connection is already open and must be closed first."); }
            }
            catch (MySqlException ex)
            {
                switch (ex.Number)
                {
                    case 0:
                        Debug.Message.Handle($"Cannot connect to server. Contact administrator.", ConsoleColor.Red, Logtype.Database);
                        break;
                    case 1042:
                        Debug.Message.Handle($"Unable to connect to any of the specified MySQL hosts.", ConsoleColor.Red, Logtype.Database);
                        break;
                    case 1045:
                        Debug.Message.Handle($"Invalid username/password, please try again", ConsoleColor.Red, Logtype.Database);
                        break;
                    default:
                        Debug.Message.Handle(ex.Message, ConsoleColor.Red, Logtype.Database);
                        break;
                }

                return false;
            }
        }

        /// <summary>
        /// Closes the mysql connection.
        /// </summary>
        /// <returns></returns>
        private void CloseConnection()
        {
            try
            {
                if (_connection.State == System.Data.ConnectionState.Open)
                    _connection.Close();
            }
            catch (MySqlException ex)
            { Debug.Message.Handle(ex.Message, ConsoleColor.Red, Logtype.Database); }
        }

        /// <summary>
        /// Sets the user data.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <param name="password">The password.</param>
        private void SetUserData(string username, string password)
        {
            if (!OpenConnection()) return;

            string query = $"SELECT Username, Password, Email, Pin, LastLogin, Registered from {_table} WHERE Username ='{username}' AND Password='{CryptoSHA.Compute(Supported_SHA.Sha512, password, _salt)}';";
            using (MySqlCommand cmd = new MySqlCommand(query, _connection))
            {
                //Execute query
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        UserData.Username = reader.GetValue(0).ToString();
                        UserData.Password = reader.GetValue(1).ToString();
                        UserData.Email = reader.GetValue(2).ToString();
                        UserData.Pin = reader.GetValue(3).ToString();
                        UserData.LastLogin = reader.GetValue(4).ToString();
                        UserData.Registered = reader.GetValue(5).ToString();
                    }
                }
            }

            //Close connection
            CloseConnection();
        }

        /// <summary>
        /// Login user.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <param name="password">The password.</param>
        /// <returns></returns>
        public bool LogIn(string username, string password)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(username))
                    throw new ArgumentNullException(nameof(username));
                if (string.IsNullOrWhiteSpace(password))
                    throw new ArgumentNullException(nameof(password));

                if (!OpenConnection()) return false;

                string query = $"SELECT Username, Password from {_table} WHERE Username ='{username}' AND Password='{CryptoSHA.Compute(Supported_SHA.Sha512, password, _salt)}';";
                using (MySqlCommand mySqlCommand = new MySqlCommand(query, _connection))
                {
                    //Execute query
                    using (var mySqlDataReader = mySqlCommand.ExecuteReader())
                    {
                        if (mySqlDataReader.Read())
                        {
                            //Close connection
                            CloseConnection();
                            // Set Userdata
                            SetUserData(username, password);
                            // Udate user table
                            UpdateLastLogin(username, password);

                            return true;
                        }
                        else { throw new Exception("Login failed: unknown user or incorrect password."); }
                    };
                };
            }
            catch (Exception ex)
            {
                //Close connection
                CloseConnection();
                Debug.Message.Handle(ex.Message, ConsoleColor.Red, Logtype.Database);
                return false;
            }
        }

        /// <summary>
        /// Updates the last login.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <param name="password">The password.</param>
        private void UpdateLastLogin(string username, string password)
        {
            if (!OpenConnection()) return;

            string query = $"UPDATE {_table} SET LastLogin ='{DateTime.Now.ToLocalTime().ToString()}' WHERE Username ='{username}' AND Password ='{CryptoSHA.Compute(Supported_SHA.Sha512, password, _salt)}';";
            using (MySqlCommand mySqlCommand2 = new MySqlCommand(query, _connection))
            {
                //Execute query
                mySqlCommand2.ExecuteNonQuery();
            }

            //Close connection
            CloseConnection();
        }

        /// <summary>
        /// Registrations the specified user.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <param name="password">The password.</param>
        /// <param name="email">The email.</param>
        /// <returns></returns>
        public bool Registration(string username, string password, string email)
        {
            try
            {
                if (!ValidateUsername(username)) return false;
                if (!ValidatePassword(password, 8, 1, 1, 1, 1)) return false;
                if (!ValidateEmail(email)) throw new Exception("Invalid e-mail address. Please enter a valid e-mail address");
                if (!OpenConnection()) return false;

                string query = $"INSERT INTO {_db}.{_table} (Username, Password, Email, Registered) VALUES ('{username}','{CryptoSHA.Compute(Supported_SHA.Sha512, password, _salt)}','{email}','{DateTime.Now.ToLocalTime()}');";
                using (MySqlCommand mySqlCommand = new MySqlCommand(query, _connection))
                {
                    //Execute query
                    mySqlCommand.ExecuteNonQuery();
                };

                //Close connection
                CloseConnection();
                return true;
            }
            catch (Exception ex)
            {
                //Close connection
                CloseConnection();
                Debug.Message.Handle(ex.Message, ConsoleColor.Red, Logtype.Database);
                return false;
            }
        }

        /// <summary>
        /// Check to see if the specified user exists.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <param name="password">The password.</param>
        /// <returns></returns>
        private bool ExistUser(string username, string password)
        {
            if (!OpenConnection()) return false;

            try
            {
                string query = $"SELECT Username, Password from {_table} WHERE Username ='{username}' AND Password='{CryptoSHA.Compute(Supported_SHA.Sha512, password, _salt)}';";
                using (MySqlCommand mySqlCommand = new MySqlCommand(query, _connection))
                {
                    //Execute query
                    using (var mySqlDataReader = mySqlCommand.ExecuteReader())
                    {
                        if (mySqlDataReader.Read())
                        {
                            //Close connection
                            CloseConnection();
                            return true;
                        }
                        else { throw new Exception("Unknown error occurred. Please contact the software developer."); }
                    };
                };
            }
            catch (Exception ex)
            {
                CloseConnection();
                Debug.Message.Handle(ex.Message, ConsoleColor.Red, Logtype.Database);
                return false;
            }
        }

        /// <summary>
        /// Changes the username.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <param name="password">The password.</param>
        /// <param name="newUsername">The new username.</param>
        /// <returns></returns>
        public bool ChangeUsername(string username, string password, string newUsername)
        {
            try
            {
                if (!ValidateUsername(newUsername)) return false;
                if (ExistUser(username, password))
                {
                    string query = $"UPDATE {_table} SET Username ='{newUsername}' WHERE Username ='{username}' AND Password ='{CryptoSHA.Compute(Supported_SHA.Sha512, password, _salt)}';";
                    using (MySqlCommand mySqlCommand = new MySqlCommand(query, _connection))
                    {
                        //Execute query
                        mySqlCommand.ExecuteNonQuery();
                    }

                    return true;
                }
                else return false;
            }
            catch (Exception ex)
            {
                Debug.Message.Handle(ex.Message, ConsoleColor.Red, Logtype.Database);
                return false;
            }
        }

        /// <summary>
        /// Changes the password.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <param name="password">The password.</param>
        /// <param name="newPassword">The new password.</param>
        /// <returns></returns>
        public bool ChangePassword(string username, string password, string newPassword)
        {
            try
            {
                if (!ValidatePassword(password, 8, 1, 1, 1, 1)) return false;
                if (ExistUser(username, password))
                {
                    string query = $"UPDATE {_table} SET Password ='{CryptoSHA.Compute(Supported_SHA.Sha512, newPassword, _salt)}' WHERE Username ='{username}' AND Password ='{CryptoSHA.Compute(Supported_SHA.Sha512, password, _salt)}';";
                    using (MySqlCommand mySqlCommand = new MySqlCommand(query, _connection))
                    {
                        //Execute query
                        mySqlCommand.ExecuteNonQuery();
                    }

                    return true;
                }
                else return false;
            }
            catch (Exception ex)
            {
                Debug.Message.Handle(ex.Message, ConsoleColor.Red, Logtype.Database);
                return false;
            }
        }

        /// <summary>
        /// Changes the email.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <param name="password">The password.</param>
        /// <param name="newEmail">The new email.</param>
        /// <returns></returns>
        public bool ChangeEmail(string username, string password, string newEmail)
        {
            try
            {
                if (ExistUser(username, password))
                {
                    if (!ValidateEmail(newEmail)) throw new Exception("Invalid e-mail address.Please enter a valid e-mail address");

                    string query = $"UPDATE {_table} SET Email ='{newEmail}' WHERE Username ='{username}' AND Password ='{CryptoSHA.Compute(Supported_SHA.Sha512, password, _salt)}';";
                    using (MySqlCommand mySqlCommand = new MySqlCommand(query, _connection))
                    {
                        //Execute query
                        mySqlCommand.ExecuteNonQuery();
                    }

                    return true;
                }
                else return false;
            }
            catch (Exception ex)
            {
                Debug.Message.Handle(ex.Message, ConsoleColor.Red, Logtype.Database);
                return false;
            }
        }
    }
}
