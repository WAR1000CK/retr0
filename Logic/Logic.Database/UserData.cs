﻿namespace Logic.Database
{
    public class UserData
    {
        public static string Username { get; set; }
        public static string Password { get; set; }
        public static string Pin { get; set; }
        public static string Email { get; set; }
        public static string LastLogin { get; set; }
        public static string Registered { get; set; }

        public static string GetUsername() { return $"Angemeldet als: {Username}"; }
        public static string GetPassword() { return $"Sha512: {Password}"; }
        public static string GetPin() { return $"Pin: {Pin}"; }
        public static string GetEmail() { return $"E-Mail: {Email}"; }
        public static string GetLastLogin() { return $"Letzter Login: {LastLogin}"; }
        public static string GetRegistered() { return $"Registriert am: {Registered}"; }

        public static string GetUserdata => $"{GetUsername()}\n" +
                                            $"{GetPassword()}\n" +
                                            $"{GetPin()}\n" +
                                            $"{GetEmail()}\n" +
                                            $"{GetLastLogin()}\n" +
                                            $"{GetRegistered()}";
    }
}
