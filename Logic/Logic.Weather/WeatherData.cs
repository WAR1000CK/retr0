﻿using System;
using System.Net;
using System.Xml;

namespace Logic.Weather
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="Logic.Weather.WeatherDataModel" />
    /// <seealso cref="System.IDisposable" />
    public class WeatherData : WeatherDataModel, IDisposable
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="WeatherData"/> class.
        /// </summary>
        /// <param name="location">The location.</param>
        /// <param name="apiKey">The API.</param>
        public WeatherData(string location, string apiKey) => Document = LoadDocument(Url(location, apiKey));

        /// <summary>
        /// Loads the xml document.
        /// </summary>
        /// <param name="url">The URL.</param>
        /// <returns></returns>
        private XmlDocument LoadDocument(Uri url)
        {
            XmlDocument doc = new XmlDocument();

            using (var client = new WebClient())
            {
                try
                {
                    // Get the response string from the URL.
                    string content = client.DownloadString(url);
                    // Load the response into an XML document.
                    doc.LoadXml(content);
                }
                catch (Exception ex)
                {
                    Debug.Message.Handle(ex.Message, ConsoleColor.Red, Debug.Enums.Logtype.Weather);
                }
            }

            return doc;
        }

        /// <summary>
        /// Get the specified URL location.
        /// </summary>
        /// <param name="location">The location.</param>
        /// <param name="api">The API.</param>
        /// <returns></returns>
        private Uri Url(string location, string api) => new Uri($"http://api.openweathermap.org/data/2.5/weather?q={location}&mode=xml&lang=de&units=metric&APPID={api}");

        #region IDisposable Support

        private bool disposedValue = false; // Dient zur Erkennung redundanter Aufrufe.

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: verwalteten Zustand (verwaltete Objekte) entsorgen.
                }

                // TODO: nicht verwaltete Ressourcen (nicht verwaltete Objekte) freigeben und Finalizer weiter unten überschreiben.
                // TODO: große Felder auf Null setzen.

                disposedValue = true;
            }
        }

        // TODO: Finalizer nur überschreiben, wenn Dispose(bool disposing) weiter oben Code für die Freigabe nicht verwalteter Ressourcen enthält.
        // ~WeatherData()
        // {
        //   // Ändern Sie diesen Code nicht. Fügen Sie Bereinigungscode in Dispose(bool disposing) weiter oben ein.
        //   Dispose(false);
        // }

        // Dieser Code wird hinzugefügt, um das Dispose-Muster richtig zu implementieren.
        public void Dispose()
        {
            // Ändern Sie diesen Code nicht. Fügen Sie Bereinigungscode in Dispose(bool disposing) weiter oben ein.
            Dispose(true);
            // TODO: Auskommentierung der folgenden Zeile aufheben, wenn der Finalizer weiter oben überschrieben wird.
            // GC.SuppressFinalize(this);
        }

        #endregion
    }
}
