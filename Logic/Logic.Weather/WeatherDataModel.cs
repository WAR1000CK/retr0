﻿using System;
using System.ComponentModel;
using System.Reflection;
using System.Text;
using System.Windows.Media.Imaging;
using System.Xml;

namespace Logic.Weather
{
    /// <summary>
    /// 
    /// </summary>
    public class WeatherDataModel
    {
        #region Public propertys

        /// <summary>
        /// Gets the version.
        /// </summary>
        public static string Version { get => Assembly.GetExecutingAssembly().GetName().Version.ToString(); }

        /// <summary>
        /// Gets or sets the XmlDocument.
        /// </summary>
        public XmlDocument Document { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public string City { get => Document.SelectSingleNode("//city").Attributes["name"].Value; }

        /// <summary>
        /// 
        /// </summary>
        public string Country { get => Document.SelectSingleNode("//country").FirstChild.InnerText; }

        /// <summary>
        /// 
        /// </summary>
        public string Clouds { get => DecodeFromUtf8(Document.SelectSingleNode("//clouds").Attributes["name"].Value); }

        /// <summary>
        /// 
        /// </summary>
        public string Weather { get => DecodeFromUtf8(Document.SelectSingleNode("//weather").Attributes["value"].Value); }

        /// <summary>
        /// Gets the <see cref="IconId"/> <see cref="BitmapImage"/> filename.
        /// </summary>
        public string IconId
        {
            get
            {
                string tmpId = Document.SelectSingleNode("//weather").Attributes["icon"].Value;
                switch (tmpId)
                {
                    // Clear sky
                    case "01d":
                    case "01n":
                        return "sunny.png";
                    // Few clouds
                    case "02d":
                    case "02n":
                        return "cloud.png";
                    // Scattered clouds
                    case "03d":
                    case "03n":
                        return "cloud.png";
                    // Broken clouds
                    case "04d":
                    case "04n":
                        return "cloud.png";
                    // Shower rain 
                    case "09d":
                    case "09n":
                        return "raining.png";
                    // Rain
                    case "10d":
                    case "10n":
                        return "raining.png";
                    // Thunderstorm
                    case "11d":
                    case "11n":
                        return "storm.png";
                    // Snow
                    case "13d":
                    case "13n":
                        return "snowing.png";
                    // Mist
                    case "50d":
                    case "50n":
                        return "raining.png";
                    // Id not found
                    default:
                        return "icons8_error_96px.png";
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Temperature { get => $"{Document.SelectSingleNode("//temperature").Attributes["value"].Value} °C"; }

        /// <summary>
        /// 
        /// </summary>
        public string FeelingTemperature { get => $"{Document.SelectSingleNode("//feels_like").Attributes["value"].Value} °C"; }
        
        /// <summary>
        /// 
        /// </summary>
        public string MinTemperature { get => $"{Document.SelectSingleNode("//temperature").Attributes["min"].Value} °C"; }

        /// <summary>
        /// 
        /// </summary>
        public string MaxTemperature { get => $"{Document.SelectSingleNode("//temperature").Attributes["max"].Value} °C"; }

        /// <summary>
        /// 
        /// </summary>
        public string Humidity { get => $"Luftfeuchtigkeit {Document.SelectSingleNode("//humidity").Attributes["value"].Value} {Document.SelectSingleNode("//humidity").Attributes["unit"].Value}"; }

        /// <summary>
        /// 
        /// </summary>
        public string Pressure { get => $"Luftdruck {Document.SelectSingleNode("//pressure").Attributes["value"].Value} {Document.SelectSingleNode("//pressure").Attributes["unit"].Value}"; }

        /// <summary>
        /// 
        /// </summary>
        public string WindSpeed { get => $"Windstärke {Document.SelectSingleNode("//speed").Attributes["value"].Value} m/s"; }

        /// <summary>
        /// Returns the <see cref="WindDirection"/> <see cref="BitmapImage"/> filename.
        /// </summary>
        public string WindDirection
        {
            get
            {
                switch (Document.SelectSingleNode("//direction").Attributes["code"].Value)
                {
                    case "N":
                        return "icons8_up_96px.png";
                    case "NNE":
                        return "icons8_up_right_96px.png";
                    case "E":
                        return "icons8_arrow_96px.png";
                    case "EES":
                        return "icons8_down_right_96px.png";
                    case "S":
                        return "icons8_down_arrow_96px.png";
                    case "SSW":
                        return "icons8_down_left_96px.png";
                    case "W":
                        return "icons8_arrow_pointing_left_96px.png";
                    case "WNW":
                        return "icons8_up_left_96px.png";
                    default:
                        return "icons8_error_96px.png";
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Visibility 
        {
            get
            {
                int value = Convert.ToInt32(Document.SelectSingleNode("//visibility").Attributes["value"].Value);
                value /= 1000;

                return $"Fernsicht {value} km"; 
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string LastUpdate
        {
            get
            {
                var value = Document.SelectSingleNode("//lastupdate").Attributes["value"].Value;

                var dtc = new DateTimeConverter();
                return $"Aktualisiert: {dtc.ConvertFromString(value)}";
            }
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Gets the <see cref="BitmapImage"/> from path.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <param name="filename">The filename.</param>
        public BitmapImage GetBitmapImage(string path, string filename)
        {
            BitmapImage src = new BitmapImage();
            src.BeginInit();
            src.UriSource = new Uri($"{path}/{filename}", UriKind.RelativeOrAbsolute);
            src.CacheOption = BitmapCacheOption.OnLoad;
            src.EndInit();

            return src;
        }

        #endregion

        #region Private methods

        /// <summary>
        /// Decodes from UTF8.
        /// </summary>
        /// <param name="utf8String">The UTF8 string.</param>
        /// <returns></returns>
        private string DecodeFromUtf8(string utf8String)
        {
            byte[] bytes = Encoding.GetEncoding(0).GetBytes(utf8String);
            var input = Encoding.UTF8.GetString(bytes);

            return input;
        }

        #endregion
    }
}
