﻿namespace Logic.Debug.Enums
{
    public enum Logtype
    {
        Info,
        Debug,
        Exception,
        Database,
        Weather,
        Hardware,
        App,
        Command
    }
}
