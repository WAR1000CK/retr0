﻿using Logic.Debug.Enums;
using System;
using System.Reflection;
using System.Runtime.InteropServices;

namespace Logic.Debug
{
    public static class Message
    {
        #region Propertys

        public static string Version { get => Assembly.GetExecutingAssembly().GetName().Version.ToString(); }

        #endregion

        #region Public fields

        public static readonly IntPtr PtrConsole;

        #endregion

        #region Unnecessary function

        static void Main(string[] args) { }

        #endregion

        [DllImport("kernel32.dll")]
        static extern IntPtr GetConsoleWindow();

        /// <summary>
        /// Initializes the <see cref="Message"/> class.
        /// </summary>
        static Message()
        {
            PtrConsole = GetConsoleWindow();

            // Make sure that the output type console is selected.
            if (PtrConsole != IntPtr.Zero)
            {
                Console.Title = $"{Assembly.GetExecutingAssembly().FullName}";
                Console.CursorVisible = false;
                Console.BackgroundColor = ConsoleColor.Black;
            }
        }

        public static void Handle(string message, ConsoleColor color, Logtype type)
        {
            // Make sure that the output type console is selected.
            if (PtrConsole != IntPtr.Zero)
            {
                Console.ForegroundColor = color;
                Console.Write($"[{type}]");
                Console.ResetColor();
                Console.ForegroundColor = ConsoleColor.Gray;
                Console.CursorLeft = 13;
                Console.Write($"{DateTime.UtcNow} >> {message}\n");
            }
        }
    }
}
