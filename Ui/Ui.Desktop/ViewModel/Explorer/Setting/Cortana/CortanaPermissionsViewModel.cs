﻿using Ui.Desktop.View.Setting.Cortana;

namespace Ui.Desktop.ViewModel.Setting.Cortana
{
    class CortanaPermissionsViewModel
    {
        #region Properties

        public CortanaView CortanaView { get; private set; }
        public CortanaPermissionsView CortanaPermissionsView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="CortanaPermissionsViewModel"/> class.
        /// </summary>
        /// <param name="cortanaView">The cortana view.</param>
        /// <param name="cortanaPermissionsView">The cortana permissions view.</param>
        public CortanaPermissionsViewModel(CortanaView cortanaView, CortanaPermissionsView cortanaPermissionsView)
        {
            CortanaView = cortanaView;
            CortanaPermissionsView = cortanaPermissionsView;
            cortanaView.CortanaGrid.Children.Clear();
            cortanaView.CortanaGrid.Children.Add(cortanaPermissionsView);
        }
    }
}
