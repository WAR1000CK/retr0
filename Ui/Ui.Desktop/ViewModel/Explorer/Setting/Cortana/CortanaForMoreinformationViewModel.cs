﻿using Ui.Desktop.View.Setting.Cortana;

namespace Ui.Desktop.ViewModel.Setting.Cortana
{
    class CortanaForMoreinformationViewModel
    {
        #region Properties

        public CortanaView CortanaView { get; private set; }
        public CortanaForMoreinformationView CortanaForMoreinformationView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="CortanaForMoreinformationViewModel"/> class.
        /// </summary>
        /// <param name="cortanaView">The cortana view.</param>
        /// <param name="cortanaForMoreinformationView">The cortana for moreinformation view.</param>
        public CortanaForMoreinformationViewModel(CortanaView cortanaView, CortanaForMoreinformationView cortanaForMoreinformationView)
        {
            CortanaView = cortanaView;
            CortanaForMoreinformationView = cortanaForMoreinformationView;
            cortanaView.CortanaGrid.Children.Clear();
            cortanaView.CortanaGrid.Children.Add(cortanaForMoreinformationView);
        }
    }
}
