﻿using Ui.Desktop.View.Setting.Cortana;

namespace Ui.Desktop.ViewModel.Setting.Cortana
{
    class CortanaTalkToCortanaViewModel
    {
        #region Properties

        public CortanaView CortanaView { get; private set; }
        public CortanaTalkToCortanaView CortanaTalkToCortanaView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="CortanaTalkToCortanaViewModel"/> class.
        /// </summary>
        /// <param name="cortanaView">The cortana view.</param>
        /// <param name="cortanaTalkToCortanaView">The cortana talk to cortana view.</param>
        public CortanaTalkToCortanaViewModel(CortanaView cortanaView, CortanaTalkToCortanaView cortanaTalkToCortanaView)
        {
            CortanaView = cortanaView;
            CortanaTalkToCortanaView = cortanaTalkToCortanaView;
            cortanaView.CortanaGrid.Children.Clear();
            cortanaView.CortanaGrid.Children.Add(cortanaTalkToCortanaView);
        }
    }
}
