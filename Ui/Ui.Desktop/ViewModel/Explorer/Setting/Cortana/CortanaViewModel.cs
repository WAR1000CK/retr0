﻿using Ui.Desktop.View.Setting.Cortana;

namespace Ui.Desktop.ViewModel.Setting.Cortana
{
    public class CortanaViewModel
    {
        #region Commands

        public RelayCommand CommandTalkToCortana { get; private set; }
        public RelayCommand CommandPermissions { get; private set; }
        public RelayCommand CommandForMoreInformations { get; private set; }
        public RelayCommand CommandCloseWindow { get; private set; }

        #endregion

        #region Private fields

        private readonly MainWindow _mainWindow;
        private static CortanaView _cortanaView;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="CortanaViewModel"/> class.
        /// </summary>
        /// <param name="mainWindow">The main window.</param>
        /// <param name="view">The view.</param>
        public CortanaViewModel(MainWindow mainWindow, CortanaView cortanaView)
        {
            _mainWindow = mainWindow;
            _cortanaView = cortanaView;
            mainWindow.MainGrid.Children.Add(cortanaView);

            #region Commands

            // Initialize commands and run when the button is pressed
            CommandTalkToCortana = new RelayCommand(TalkToCortana);
            CommandPermissions = new RelayCommand(Permissions);
            CommandForMoreInformations = new RelayCommand(ForMoreInformations);

            CommandCloseWindow = new RelayCommand(CloseWindow);

            #endregion

            TalkToCortana();
        }

        /// <summary>
        /// Gets the cortana view.
        /// </summary>
        /// <returns></returns>
        public static CortanaView GetCortanaView() => _cortanaView;

        #region Command Functions

        /// <summary>
        /// Creates an instance of <see cref="CortanaTalkToCortanaView"/>
        /// </summary>
        private void TalkToCortana() => new CortanaTalkToCortanaView(GetCortanaView());

        /// <summary>
        /// Creates an instance of <see cref="CortanaPermissionsView"/>
        /// </summary>
        private void Permissions() => new CortanaPermissionsView(GetCortanaView());

        /// <summary>
        /// Creates an instance of <see cref="CortanaForMoreinformationView"/>
        /// </summary>
        private void ForMoreInformations() => new CortanaForMoreinformationView(GetCortanaView());

        /// <summary>
        /// Removes the <see cref="View"/> from <see cref="Main.MainGrid"/>
        /// </summary>
        private void CloseWindow() => _mainWindow.MainGrid.Children.Remove(GetCortanaView());

        #endregion
    }
}
