﻿using Ui.Desktop.View.Setting.Sys;

namespace Ui.Desktop.ViewModel.Setting.Sys
{
    public class SystemMainsOperationViewModel : BaseViewModel
    {
        #region Properties

        public SystemView SystemView { get; private set; }
        public SystemMainsOperationView SystemMainsOperationView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="SystemMainsOperationViewModel"/> class.
        /// </summary>
        /// <param name="systemView">The system view.</param>
        /// <param name="systemMainsOperationView">The system mains operation view.</param>
        public SystemMainsOperationViewModel(SystemView systemView, SystemMainsOperationView systemMainsOperationView)
        {
            SystemView = systemView;
            SystemMainsOperationView = systemMainsOperationView;
            systemView.SystemGrid.Children.Clear();
            systemView.SystemGrid.Children.Add(systemMainsOperationView);
        }
    }
}
