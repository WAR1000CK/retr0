﻿using Ui.Desktop.View.Setting.Sys;

namespace Ui.Desktop.ViewModel.Setting.Sys
{
    public class SystemNotificationWizardViewModel : BaseViewModel
    {
        #region Properties

        public SystemView SystemView { get; private set; }
        public SystemNotificationWizardView SystemNotificationWizardView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="SystemNotificationWizardViewModel"/> class.
        /// </summary>
        /// <param name="systemView">The system view.</param>
        /// <param name="systemNotificationWizardView">The system notification wizard view.</param>
        public SystemNotificationWizardViewModel(SystemView systemView, SystemNotificationWizardView systemNotificationWizardView)
        {
            SystemView = systemView;
            SystemNotificationWizardView = systemNotificationWizardView;
            systemView.SystemGrid.Children.Clear();
            systemView.SystemGrid.Children.Add(systemNotificationWizardView);
        }
    }
}
