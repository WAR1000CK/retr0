﻿using Ui.Desktop.View.Setting.Sys;

namespace Ui.Desktop.ViewModel.Setting.Sys
{
    public class SystemDisplayViewModel : BaseViewModel
    {
        #region Properties

        public SystemView SystemView { get; private set; }
        public SystemDisplayView SystemDisplayView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="SystemDisplayViewModel"/> class.
        /// </summary>
        /// <param name="systemView">The system view.</param>
        /// <param name="systemDisplayView">The system display view.</param>
        public SystemDisplayViewModel(SystemView systemView, SystemDisplayView systemDisplayView)
        {
            SystemView = systemView;
            SystemDisplayView = systemDisplayView;
            systemView.SystemGrid.Children.Clear();
            systemView.SystemGrid.Children.Add(systemDisplayView);
        }
    }
}
