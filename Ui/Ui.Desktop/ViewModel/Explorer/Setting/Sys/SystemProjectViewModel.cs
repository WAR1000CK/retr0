﻿using Ui.Desktop.View.Setting.Sys;

namespace Ui.Desktop.ViewModel.Setting.Sys
{
    public class SystemProjectViewModel : BaseViewModel
    {
        #region Properties

        public SystemView SystemView { get; private set; }
        public SystemProjectView SystemProjectView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="SystemProjectViewModel"/> class.
        /// </summary>
        /// <param name="systemView">The system view.</param>
        /// <param name="systemProjectView">The system project view.</param>
        public SystemProjectViewModel(SystemView systemView, SystemProjectView systemProjectView)
        {
            SystemView = systemView;
            SystemProjectView = systemProjectView;
            systemView.SystemGrid.Children.Clear();
            systemView.SystemGrid.Children.Add(systemProjectView);
        }
    }
}
