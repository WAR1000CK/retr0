﻿using Ui.Desktop.View.Setting.Sys;

namespace Ui.Desktop.ViewModel.Setting.Sys
{
    public class SystemMemoryViewModel : BaseViewModel
    {
        #region Properties

        public SystemView SystemView { get; private set; }
        public SystemMemoryView SystemMemoryView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="SystemMemoryViewModel"/> class.
        /// </summary>
        /// <param name="systemView">The system view.</param>
        /// <param name="systemMemoryView">The system memory view.</param>
        public SystemMemoryViewModel(SystemView systemView, SystemMemoryView systemMemoryView)
        {
            SystemView = systemView;
            SystemMemoryView = systemMemoryView;
            systemView.SystemGrid.Children.Clear();
            systemView.SystemGrid.Children.Add(systemMemoryView);
        }
    }
}
