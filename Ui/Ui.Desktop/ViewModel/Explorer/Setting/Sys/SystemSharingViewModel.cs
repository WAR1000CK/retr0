﻿using Ui.Desktop.View.Setting.Sys;

namespace Ui.Desktop.ViewModel.Setting.Sys
{
    public class SystemSharingViewModel : BaseViewModel
    {
        #region Properties

        public SystemView SystemView { get; private set; }
        public SystemSharingView SystemSharingView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="SystemSharingViewModel"/> class.
        /// </summary>
        /// <param name="systemView">The system view.</param>
        /// <param name="systemSharingView">The system sharing view.</param>
        public SystemSharingViewModel(SystemView systemView, SystemSharingView systemSharingView)
        {
            SystemView = systemView;
            SystemSharingView = systemSharingView;
            systemView.SystemGrid.Children.Clear();
            systemView.SystemGrid.Children.Add(systemSharingView);
        }
    }
}
