﻿using Ui.Desktop.View.Setting.Sys;

namespace Ui.Desktop.ViewModel.Setting.Sys
{
    public class SystemMultitaskingViewModel : BaseViewModel
    {
        #region Properties

        public SystemView SystemView { get; private set; }
        public SystemMultitaskingView SystemMultitaskingView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="SystemMultitaskingViewModel"/> class.
        /// </summary>
        /// <param name="systemView">The system view.</param>
        /// <param name="systemMultitaskingView">The system multitasking view.</param>
        public SystemMultitaskingViewModel(SystemView systemView, SystemMultitaskingView systemMultitaskingView)
        {
            SystemView = systemView;
            SystemMultitaskingView = systemMultitaskingView;
            systemView.SystemGrid.Children.Clear();
            systemView.SystemGrid.Children.Add(systemMultitaskingView);
        }
    }
}
