﻿using Ui.Desktop.View.Setting.Sys;

namespace Ui.Desktop.ViewModel.Setting.Sys
{
    public class SystemRemoteDesktopViewModel : BaseViewModel
    {
        #region Properties

        public SystemView SystemView { get; private set; }
        public SystemRemoteDesktopView SystemRemoteDesktopView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="SystemRemoteDesktopViewModel"/> class.
        /// </summary>
        /// <param name="systemView">The system view.</param>
        /// <param name="systemRemoteDesktopView">The system remote desktop view.</param>
        public SystemRemoteDesktopViewModel(SystemView systemView, SystemRemoteDesktopView systemRemoteDesktopView)
        {
            SystemView = systemView;
            SystemRemoteDesktopView = systemRemoteDesktopView;
            systemView.SystemGrid.Children.Clear();
            systemView.SystemGrid.Children.Add(systemRemoteDesktopView);
        }
    }
}
