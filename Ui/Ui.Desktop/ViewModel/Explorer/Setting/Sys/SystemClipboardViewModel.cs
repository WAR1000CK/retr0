﻿using Ui.Desktop.View.Setting.Sys;

namespace Ui.Desktop.ViewModel.Setting.Sys
{
    public class SystemClipboardViewModel : BaseViewModel
    {
        #region Properties

        public SystemView SystemView { get; private set; }
        public SystemClipboardView SystemClipboardView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="SystemClipboardViewModel"/> class.
        /// </summary>
        /// <param name="systemView">The system view.</param>
        /// <param name="systemClipboardView">The system clipboard view.</param>
        public SystemClipboardViewModel(SystemView systemView, SystemClipboardView systemClipboardView)
        {
            SystemView = systemView;
            SystemClipboardView = systemClipboardView;
            systemView.SystemGrid.Children.Clear();
            systemView.SystemGrid.Children.Add(systemClipboardView);
        }
    }
}
