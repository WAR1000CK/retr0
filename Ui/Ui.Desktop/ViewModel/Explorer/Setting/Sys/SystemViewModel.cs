﻿using Ui.Desktop.View.Setting.Sys;

namespace Ui.Desktop.ViewModel.Setting.Sys
{
    public class SystemViewModel : BaseViewModel
    {
        #region Commands

        public RelayCommand CommandSystemDisplay { get; private set; }
        public RelayCommand CommandSystemSound { get; private set; }
        public RelayCommand CommandSystemNotification { get; private set; }
        public RelayCommand CommandSystemNotificationWizard { get; private set; }
        public RelayCommand CommandSystemMainsOperation { get; private set; }
        public RelayCommand CommandSystemMemory { get; private set; }
        public RelayCommand CommandSystemTabletModus { get; private set; }
        public RelayCommand CommandSystemMultitasking { get; private set; }
        public RelayCommand CommandSystemProject { get; private set; }
        public RelayCommand CommandSystemSharing { get; private set; }
        public RelayCommand CommandSystemClipboard { get; private set; }
        public RelayCommand CommandSystemRemote { get; private set; }
        public RelayCommand CommandSystemInfo { get; private set; }
        public RelayCommand CommandCloseWindow { get; private set; }

        #endregion

        #region Private fields

        private readonly MainWindow _mainWindow;
        private static SystemView _systemView;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="SystemViewModel"/> class.
        /// </summary>
        /// <param name="mainWindow">The main window.</param>
        /// <param name="view">The SystemView.</param>
        public SystemViewModel(MainWindow mainWindow, SystemView systemView)
        {
            _mainWindow = mainWindow;
            _systemView = systemView;
            mainWindow.MainGrid.Children.Add(systemView);

            #region Commands

            // Initialize commands and run when the button is pressed
            CommandSystemDisplay = new RelayCommand(SystemDisplay);
            CommandSystemSound = new RelayCommand(SystemSound);
            CommandSystemNotification = new RelayCommand(SystemNotification);
            CommandSystemNotificationWizard = new RelayCommand(SystemNotificationWizard);
            CommandSystemMainsOperation = new RelayCommand(SystemMainsOperation);
            CommandSystemMemory = new RelayCommand(SystemMemory);
            CommandSystemTabletModus = new RelayCommand(SystemTabletModus);
            CommandSystemMultitasking = new RelayCommand(SystemMultitasking);
            CommandSystemProject = new RelayCommand(SystemProject);
            CommandSystemSharing = new RelayCommand(SystemSharing);
            CommandSystemClipboard = new RelayCommand(SystemClipboard);
            CommandSystemRemote = new RelayCommand(SystemRemote);
            CommandSystemInfo = new RelayCommand(SystemInfo);
            CommandCloseWindow = new RelayCommand(CloseWindow);

            #endregion

            SystemDisplay();
        }

        /// <summary>
        /// Gets the system view.
        /// </summary>
        /// <returns></returns>
        public static SystemView GetSystemView() => _systemView;

        #region Command Functions

        /// <summary>
        /// Creates an instance of <see cref="SystemDisplayView"/>
        /// </summary>
        private void SystemDisplay() => new SystemDisplayView(GetSystemView());

        /// <summary>
        /// Creates an instance of <see cref="SystemSoundView"/>
        /// </summary>
        private void SystemSound() => new SystemSoundView(GetSystemView());

        /// <summary>
        /// Creates an instance of <see cref="SystemNotificationView"/>
        /// </summary>
        private void SystemNotification() => new SystemNotificationView(GetSystemView());

        /// <summary>
        /// Creates an instance of <see cref="SystemNotificationWizardView"/>
        /// </summary>
        private void SystemNotificationWizard() => new SystemNotificationWizardView(GetSystemView());

        /// <summary>
        /// Creates an instance of <see cref="SystemMainsOperationView"/>
        /// </summary>
        private void SystemMainsOperation() => new SystemMainsOperationView(GetSystemView());

        /// <summary>
        /// Creates an instance of <see cref="SystemMemoryView"/>
        /// </summary>
        private void SystemMemory() => new SystemMemoryView(GetSystemView());

        /// <summary>
        /// Creates an instance of <see cref="SystemTabletModeView"/>
        /// </summary>
        private void SystemTabletModus() => new SystemTabletModeView(GetSystemView());

        /// <summary>
        /// Creates an instance of <see cref="SystemMultitaskingView"/>
        /// </summary>
        private void SystemMultitasking() => new SystemMultitaskingView(GetSystemView());

        /// <summary>
        /// Creates an instance of <see cref="SystemProjectView"/>
        /// </summary>
        private void SystemProject() => new SystemProjectView(GetSystemView());

        /// <summary>
        /// Creates an instance of <see cref="SystemSharingView"/>
        /// </summary>
        private void SystemSharing() => new SystemSharingView(GetSystemView());

        /// <summary>
        /// Creates an instance of <see cref="SystemClipboardView"/>
        /// </summary>
        private void SystemClipboard() => new SystemClipboardView(GetSystemView());

        /// <summary>
        /// Creates an instance of <see cref="SystemRemoteDesktopView"/>
        /// </summary>
        private void SystemRemote() => new SystemRemoteDesktopView(GetSystemView());

        /// <summary>
        /// Creates an instance of <see cref="SystemInfoView"/>
        /// </summary>
        private void SystemInfo() => new SystemInfoView(GetSystemView());

        /// <summary>
        /// Removes the <see cref="_systemView"/> from <see cref="Main.MainGrid"/>
        /// </summary>
        private void CloseWindow() => _mainWindow.MainGrid.Children.Remove(GetSystemView());

        #endregion
    }
}
