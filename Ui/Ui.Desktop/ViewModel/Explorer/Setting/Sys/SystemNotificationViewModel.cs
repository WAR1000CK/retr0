﻿using Ui.Desktop.View.Setting.Sys;

namespace Ui.Desktop.ViewModel.Setting.Sys
{
    public class SystemNotificationViewModel : BaseViewModel
    {
        #region Properties

        public SystemView SystemView { get; private set; }
        public SystemNotificationView SystemNotificationView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="SystemNotificationViewModel"/> class.
        /// </summary>
        /// <param name="systemView">The system view.</param>
        /// <param name="systemNotificationView">The system notification view.</param>
        public SystemNotificationViewModel(SystemView systemView, SystemNotificationView systemNotificationView)
        {
            SystemView = systemView;
            SystemNotificationView = systemNotificationView;
            systemView.SystemGrid.Children.Clear();
            systemView.SystemGrid.Children.Add(systemNotificationView);
        }
    }
}
