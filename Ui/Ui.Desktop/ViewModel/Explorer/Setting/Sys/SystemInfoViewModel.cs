﻿using Ui.Desktop.View.Setting.Sys;

namespace Ui.Desktop.ViewModel.Setting.Sys
{
    public class SystemInfoViewModel : BaseViewModel
    {
        #region Properties

        public SystemView SystemView { get; private set; }
        public SystemInfoView SystemInfoView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="SystemInfoViewModel"/> class.
        /// </summary>
        /// <param name="systemView">The system view.</param>
        /// <param name="systemInfoView">The system information view.</param>
        public SystemInfoViewModel(SystemView systemView, SystemInfoView systemInfoView)
        {
            SystemView = systemView;
            SystemInfoView = systemInfoView;
            systemView.SystemGrid.Children.Clear();
            systemView.SystemGrid.Children.Add(systemInfoView);
        }
    }
}
