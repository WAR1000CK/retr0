﻿using Ui.Desktop.View.Setting.Sys;

namespace Ui.Desktop.ViewModel.Setting.Sys
{
    public class SystemTabletModeViewModel : BaseViewModel
    {
        #region Properties

        public SystemView SystemView { get; private set; }
        public SystemTabletModeView SystemTabletModeView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="SystemTabletModeViewModel"/> class.
        /// </summary>
        /// <param name="systemView">The system view.</param>
        /// <param name="systemTabletModeView">The system tablet mode view.</param>
        public SystemTabletModeViewModel(SystemView systemView, SystemTabletModeView systemTabletModeView)
        {
            SystemView = systemView;
            SystemTabletModeView = systemTabletModeView;
            systemView.SystemGrid.Children.Clear();
            systemView.SystemGrid.Children.Add(systemTabletModeView);
        }
    }
}
