﻿using Ui.Desktop.View.Setting.Sys;

namespace Ui.Desktop.ViewModel.Setting.Sys
{
    public class SystemSoundViewModel : BaseViewModel
    {
        #region Properties

        public SystemView SystemView { get; private set; }
        public SystemSoundView SystemSoundView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="SystemSoundViewModel"/> class.
        /// </summary>
        /// <param name="systemView">The system view.</param>
        /// <param name="systemSoundView">The system sound view.</param>
        public SystemSoundViewModel(SystemView systemView, SystemSoundView systemSoundView)
        {
            SystemView = systemView;
            SystemSoundView = systemSoundView;
            systemView.SystemGrid.Children.Clear();
            systemView.SystemGrid.Children.Add(systemSoundView);
        }
    }
}
