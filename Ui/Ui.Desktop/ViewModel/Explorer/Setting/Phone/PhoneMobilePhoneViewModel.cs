﻿using Ui.Desktop.View.Setting.Phone;

namespace Ui.Desktop.ViewModel.Setting.Phone
{
    class PhoneMobilePhoneViewModel
    {
        #region Properties

        public PhoneView PhoneView { get; private set; }
        public PhoneMobilePhoneView PhoneMobilePhoneView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="PhoneMobilePhoneViewModel"/> class.
        /// </summary>
        /// <param name="phoneView">The phone view.</param>
        /// <param name="phoneMobilePhoneView">The phone mobile phone view.</param>
        public PhoneMobilePhoneViewModel(PhoneView phoneView, PhoneMobilePhoneView phoneMobilePhoneView)
        {
            PhoneView = phoneView;
            PhoneMobilePhoneView = phoneMobilePhoneView;
            phoneView.PhoneGrid.Children.Clear();
            phoneView.PhoneGrid.Children.Add(phoneMobilePhoneView);
        }
    }
}
