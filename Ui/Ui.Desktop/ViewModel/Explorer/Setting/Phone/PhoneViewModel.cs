﻿using Ui.Desktop.View.Setting.Phone;

namespace Ui.Desktop.ViewModel.Setting.Phone
{
    public class PhoneViewModel
    {
        #region Commands

        public RelayCommand CommandPhone { get; private set; }
        public RelayCommand CommandCloseWindow { get; private set; }

        #endregion

        #region Private fields

        private readonly MainWindow _mainWindow;
        private static PhoneView _phoneView;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="PhoneViewModel"/> class.
        /// </summary>
        /// <param name="mainWindow">The main window.</param>
        /// <param name="view">The view.</param>
        public PhoneViewModel(MainWindow mainWindow, PhoneView phoneView)
        {
            _mainWindow = mainWindow;
            _phoneView = phoneView;
            mainWindow.MainGrid.Children.Add(phoneView);

            #region Commands

            // Initialize commands and run when the button is pressed
            CommandPhone = new RelayCommand(Phone);
            CommandCloseWindow = new RelayCommand(CloseWindow);

            #endregion

            Phone();
        }

        /// <summary>
        /// Gets the phone view.
        /// </summary>
        /// <returns></returns>
        public static PhoneView GetPhoneView() => _phoneView;

        #region Command Functions

        /// <summary>
        /// Creates an instance of <see cref="PhoneMobilePhoneView"/>
        /// </summary>
        private void Phone() => new PhoneMobilePhoneView(GetPhoneView());

        /// <summary>
        /// Removes the <see cref="View"/> from <see cref="Main.MainGrid"/>
        /// </summary>
        private void CloseWindow() => _mainWindow.MainGrid.Children.Remove(GetPhoneView());

        #endregion
    }
}
