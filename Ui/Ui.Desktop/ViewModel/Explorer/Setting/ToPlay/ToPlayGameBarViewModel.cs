﻿using Ui.Desktop.View.Setting.ToPlay;

namespace Ui.Desktop.ViewModel.Setting.ToPlay
{
    public class ToPlayGameBarViewModel
    {
        #region Properties

        public ToPlayView ToPlayView { get; private set; }
        public ToPlayGameBarView ToPlayGameBarView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="ToPlayGameBarViewModel"/> class.
        /// </summary>
        /// <param name="toPlayView">To play view.</param>
        /// <param name="toPlayGameBarView">To play game bar view.</param>
        public ToPlayGameBarViewModel(ToPlayView toPlayView, ToPlayGameBarView toPlayGameBarView)
        {
            ToPlayView = toPlayView;
            ToPlayGameBarView = toPlayGameBarView;
            toPlayView.ToPlayGrid.Children.Clear();
            toPlayView.ToPlayGrid.Children.Add(toPlayGameBarView);
        }
    }
}
