﻿using Ui.Desktop.View.Setting.ToPlay;

namespace Ui.Desktop.ViewModel.Setting.ToPlay
{
    class ToPlayXboxNetworkViewModel
    {
        #region Properties

        public ToPlayView ToPlayView { get; private set; }
        public ToPlayXboxNetworkView ToPlayXboxNetworkView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="ToPlayXboxNetworkViewModel"/> class.
        /// </summary>
        /// <param name="toPlayView">To play view.</param>
        /// <param name="toPlayXboxNetworkView">To play xbox network view.</param>
        public ToPlayXboxNetworkViewModel(ToPlayView toPlayView, ToPlayXboxNetworkView toPlayXboxNetworkView)
        {
            ToPlayView = toPlayView;
            ToPlayXboxNetworkView = toPlayXboxNetworkView;
            toPlayView.ToPlayGrid.Children.Clear();
            toPlayView.ToPlayGrid.Children.Add(toPlayXboxNetworkView);
        }
    }
}
