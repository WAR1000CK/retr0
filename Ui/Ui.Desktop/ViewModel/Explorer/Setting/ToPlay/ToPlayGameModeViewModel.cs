﻿using Ui.Desktop.View.Setting.ToPlay;

namespace Ui.Desktop.ViewModel.Setting.ToPlay
{
    class ToPlayGameModeViewModel
    {
        #region Properties

        public ToPlayView ToPlayView { get; private set; }
        public ToPlayGameModeView ToPlayGameModeView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="ToPlayGameModeViewModel"/> class.
        /// </summary>
        /// <param name="toPlayView">To play view.</param>
        /// <param name="toPlayGameModeView">To play game mode view.</param>
        public ToPlayGameModeViewModel(ToPlayView toPlayView, ToPlayGameModeView toPlayGameModeView)
        {
            ToPlayView = toPlayView;
            ToPlayGameModeView = toPlayGameModeView;
            toPlayView.ToPlayGrid.Children.Clear();
            toPlayView.ToPlayGrid.Children.Add(toPlayGameModeView);
        }
    }
}
