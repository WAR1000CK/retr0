﻿using Ui.Desktop.View.Setting.ToPlay;

namespace Ui.Desktop.ViewModel.Setting.ToPlay
{
    public class ToPlayViewModel
    {
        #region Commands

        public RelayCommand CommandGameBar { get; private set; }
        public RelayCommand CommandRecords { get; private set; }
        public RelayCommand CommandGameMode { get; private set; }
        public RelayCommand CommandXboxNetwork { get; private set; }
        public RelayCommand CommandCloseWindow { get; private set; }            // Close ToPlayView

        #endregion

        #region Private fields

        private readonly MainWindow _mainWindow;
        private static ToPlayView _toPlayView;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="ToPlayViewModel"/> class.
        /// </summary>
        /// <param name="mainWindow">The main window.</param>
        /// <param name="view">The view.</param>
        public ToPlayViewModel(MainWindow mainWindow, ToPlayView toPlayView)
        {
            _mainWindow = mainWindow;
            _toPlayView = toPlayView;
            mainWindow.MainGrid.Children.Add(toPlayView);

            #region Commands

            // Initialize commands and run when the button is pressed
            CommandGameBar = new RelayCommand(GameBar);
            CommandRecords = new RelayCommand(Records);
            CommandGameMode = new RelayCommand(GameMode);
            CommandXboxNetwork = new RelayCommand(XboxNetwork);
            CommandCloseWindow = new RelayCommand(CloseWindow);

            #endregion

            GameBar();
        }

        /// <summary>
        /// Gets to play view.
        /// </summary>
        /// <returns></returns>
        public static ToPlayView GetToPlayView() => _toPlayView;

        #region Command Functions

        /// <summary>
        /// Creates an instance of <see cref="ToPlayGameBarView"/>
        /// </summary>
        private void GameBar() => new ToPlayGameBarView(GetToPlayView());

        /// <summary>
        /// Creates an instance of <see cref="ToPlayRecordsView"/>
        /// </summary>
        private void Records() => new ToPlayRecordsView(GetToPlayView());

        /// <summary>
        /// Creates an instance of <see cref="ToPlayGameModeView"/>
        /// </summary>
        private void GameMode() => new ToPlayGameModeView(GetToPlayView());

        /// <summary>
        /// Creates an instance of <see cref="ToPlayXboxNetworkView"/>
        /// </summary>
        private void XboxNetwork() => new ToPlayXboxNetworkView(GetToPlayView());

        /// <summary>
        /// Removes the <see cref="View"/> from <see cref="Main.MainGrid"/>
        /// </summary>
        private void CloseWindow() => _mainWindow.MainGrid.Children.Remove(GetToPlayView());

        #endregion
    }
}
