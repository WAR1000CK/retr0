﻿using Ui.Desktop.View.Setting.ToPlay;

namespace Ui.Desktop.ViewModel.Setting.ToPlay
{
    class ToPlayRecordsViewModel
    {
        #region Properties

        public ToPlayView ToPlayView { get; private set; }
        public ToPlayRecordsView ToPlayRecordsView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="ToPlayRecordsViewModel"/> class.
        /// </summary>
        /// <param name="toPlayView">To play view.</param>
        /// <param name="toPlayRecordsView">To play records view.</param>
        public ToPlayRecordsViewModel(ToPlayView toPlayView, ToPlayRecordsView toPlayRecordsView)
        {
            ToPlayView = toPlayView;
            ToPlayRecordsView = toPlayRecordsView;
            toPlayView.ToPlayGrid.Children.Clear();
            toPlayView.ToPlayGrid.Children.Add(toPlayRecordsView);
        }
    }
}
