﻿using Ui.Desktop.View.Explorer.Setting.Update;
using Ui.Desktop.View.Setting;
using Ui.Desktop.View.Setting.Accounts;
using Ui.Desktop.View.Setting.Apps;
using Ui.Desktop.View.Setting.Cortana;
using Ui.Desktop.View.Setting.Devices;
using Ui.Desktop.View.Setting.FacilitatedOperation;
using Ui.Desktop.View.Setting.Network;
using Ui.Desktop.View.Setting.Personalize;
using Ui.Desktop.View.Setting.Phone;
using Ui.Desktop.View.Setting.Privacy;
using Ui.Desktop.View.Setting.Sys;
using Ui.Desktop.View.Setting.TimeDate;
using Ui.Desktop.View.Setting.ToPlay;
using Ui.Desktop.ViewModel.Explorer;

namespace Ui.Desktop.ViewModel.Setting
{
    public class OverviewViewModel : BaseViewModel
    {
        #region Commands

        public RelayCommand CommandCloseWindow { get; private set; }            // Close Overview
        public RelayCommand CommandSystem { get; private set; }                 // Open System settings
        public RelayCommand CommandDevices { get; private set; }                // Open Device settings
        public RelayCommand CommandPhone { get; private set; }                  // Open Phone settings
        public RelayCommand CommandNetwork { get; private set; }                // Open Network settings
        public RelayCommand CommandPersonalize { get; private set; }            // Open Personalize settings
        public RelayCommand CommandApps { get; private set; }                   // Open App settings
        public RelayCommand CommandAccounts { get; private set; }               // Open Account settings
        public RelayCommand CommandTime { get; private set; }                   // Open DateTime settings
        public RelayCommand CommandToPlay { get; private set; }                 // Open To play settings
        public RelayCommand CommandFacilitatedOperation { get; private set; }   // Open Facilitated Operation settings
        public RelayCommand CommandCortana { get; private set; }                // Open Cortana settings
        public RelayCommand CommandPrivacy { get; private set; }                // Open Privacy settings
        public RelayCommand CommandUpdate { get; private set; }                 // Open Update settings

        #endregion

        #region private fields

        private readonly MainWindow _mainWindow;
        private static OverviewView _overviewView;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="OverviewViewModel"/> class.
        /// </summary>
        /// <param name="mainWindow">The main window.</param>
        /// <param name="view">The OverviewView.</param>
        public OverviewViewModel(MainWindow mainWindow, OverviewView overviewView)
        {
            _mainWindow = mainWindow;
            _overviewView = overviewView;
            mainWindow.MainGrid.Children.Add(overviewView);

            #region Events

            // Reset lock delay
            overviewView.OverviewGrid.MouseMove += delegate { ExplorerViewModel.ResetAutolockDelay(); };

            #endregion

            #region Commands

            // Initialize commands and run when the button is pressed
            CommandSystem = new RelayCommand(System);
            CommandDevices = new RelayCommand(Devices);
            CommandPhone = new RelayCommand(Phone);
            CommandNetwork = new RelayCommand(Network);
            CommandPersonalize = new RelayCommand(Personalize);
            CommandApps = new RelayCommand(Apps);
            CommandAccounts = new RelayCommand(Accounts);
            CommandTime = new RelayCommand(Time);
            CommandToPlay = new RelayCommand(ToPlay);
            CommandFacilitatedOperation = new RelayCommand(FacilitatedOperation);
            CommandCortana = new RelayCommand(Cortana);
            CommandPrivacy = new RelayCommand(Privacy);
            CommandUpdate = new RelayCommand(Update);
            CommandCloseWindow = new RelayCommand(CloseWindow);

            #endregion
        }

        /// <summary>
        /// Gets the overviewview.
        /// </summary>
        /// <returns></returns>
        public static OverviewView GetOverviewView() => _overviewView;

        #region Command Functions

        /// <summary>
        /// Creates an instance of <see cref="SystemView"/>
        /// </summary>
        private void System() => new SystemView(_mainWindow);

        /// <summary>
        /// Creates an instance of <see cref="DevicesView"/>
        /// </summary>
        private void Devices() => new DevicesView(_mainWindow);

        /// <summary>
        /// Creates an instance of <see cref="PhoneView"/>
        /// </summary>
        private void Phone() => new PhoneView(_mainWindow);

        /// <summary>
        /// Creates an instance of <see cref="NetworkView"/>
        /// </summary>
        private void Network() => new NetworkView(_mainWindow);

        /// <summary>
        /// Creates an instance of <see cref="PersonalizeView"/>
        /// </summary>
        private void Personalize() => new PersonalizeView(_mainWindow);

        /// <summary>
        /// Creates an instance of <see cref="AppsView"/>
        /// </summary>
        private void Apps() => new AppsView(_mainWindow);

        /// <summary>
        /// Creates an instance of <see cref="AccountsView"/>
        /// </summary>
        private void Accounts() => new AccountsView(_mainWindow);

        /// <summary>
        /// Creates an instance of <see cref="TimeView"/>
        /// </summary>
        private void Time() => new TimeDateView(_mainWindow);

        /// <summary>
        /// Creates an instance of <see cref="ToPlayView"/>
        /// </summary>
        private void ToPlay() => new ToPlayView(_mainWindow);

        /// <summary>
        /// Creates an instance of <see cref="FacilitatedOperationView"/>
        /// </summary>
        private void FacilitatedOperation() => new FacilitatedOperationView(_mainWindow);

        /// <summary>
        /// Creates an instance of <see cref="CortanaView"/>
        /// </summary>
        private void Cortana() => new CortanaView(_mainWindow);

        /// <summary>
        /// Creates an instance of <see cref="PrivacyView"/>
        /// </summary>
        private void Privacy() => new PrivacyView(_mainWindow);

        /// <summary>
        /// Creates an instance of <see cref="UpdateView"/>
        /// </summary>
        private void Update() => new UpdateView(_mainWindow);

        /// <summary>
        /// Removes the <see cref="View"/> from <see cref="Main.MainGrid"/>
        /// </summary>
        private void CloseWindow() => _mainWindow.MainGrid.Children.Remove(GetOverviewView());

        #endregion

    }
}
