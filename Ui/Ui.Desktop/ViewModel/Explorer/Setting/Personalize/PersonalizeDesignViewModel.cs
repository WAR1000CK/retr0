﻿using Ui.Desktop.View.Setting.Personalize;

namespace Ui.Desktop.ViewModel.Setting.Personalize
{
    public class PersonalizeDesignViewModel
    {
        #region

        public PersonalizeView PersonalizeView { get; private set; }
        public PersonalizeDesignView PersonalizeDesignView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="PersonalizeDesignViewModel"/> class.
        /// </summary>
        /// <param name="personalizeView">The personalize view.</param>
        /// <param name="personalizeDesignView">The personalize design view.</param>
        public PersonalizeDesignViewModel(PersonalizeView personalizeView, PersonalizeDesignView personalizeDesignView)
        {
            PersonalizeView = personalizeView;
            PersonalizeDesignView = personalizeDesignView;
            personalizeView.PersonalizeGrid.Children.Clear();
            personalizeView.PersonalizeGrid.Children.Add(personalizeDesignView);
        }
    }
}
