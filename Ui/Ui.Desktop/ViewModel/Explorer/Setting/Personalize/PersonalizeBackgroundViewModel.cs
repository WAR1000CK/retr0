﻿using Ui.Desktop.View.Setting.Personalize;

namespace Ui.Desktop.ViewModel.Setting.Personalize
{
    public class PersonalizeBackgroundViewModel : BaseViewModel
    {
        #region Properties

        public PersonalizeView PersonalizeView { get; private set; }
        public PersonalizeBackgroundView PersonalizeBackgroundView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="PersonalizeBackgroundViewModel"/> class.
        /// </summary>
        /// <param name="personalizeView">The personalize view.</param>
        /// <param name="personalizeBackgroundView">The personalize background view.</param>
        public PersonalizeBackgroundViewModel(PersonalizeView personalizeView, PersonalizeBackgroundView personalizeBackgroundView)
        {
            PersonalizeView = personalizeView;
            PersonalizeBackgroundView = personalizeBackgroundView;
            personalizeView.PersonalizeGrid.Children.Clear();
            personalizeView.PersonalizeGrid.Children.Add(personalizeBackgroundView);
        }
    }
}
