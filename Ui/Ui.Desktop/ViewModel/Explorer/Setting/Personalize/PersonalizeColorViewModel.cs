﻿using Ui.Desktop.View.Setting.Personalize;

namespace Ui.Desktop.ViewModel.Setting.Personalize
{
    public class PersonalizeColorViewModel
    {
        #region

        public PersonalizeView PersonalizeView { get; private set; }
        public PersonalizeColorView PersonalizeColorView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="PersonalizeColorViewModel"/> class.
        /// </summary>
        /// <param name="personalizeView">The personalize view.</param>
        /// <param name="personalizeColorView">The personalize color view.</param>
        public PersonalizeColorViewModel(PersonalizeView personalizeView, PersonalizeColorView personalizeColorView)
        {
            PersonalizeView = personalizeView;
            PersonalizeColorView = personalizeColorView;
            personalizeView.PersonalizeGrid.Children.Clear();
            personalizeView.PersonalizeGrid.Children.Add(personalizeColorView);
        }
    }
}
