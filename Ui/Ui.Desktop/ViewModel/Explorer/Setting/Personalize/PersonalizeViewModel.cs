﻿using Ui.Desktop.View.Setting.Personalize;

namespace Ui.Desktop.ViewModel.Setting.Personalize
{
    public class PersonalizeViewModel
    {
        #region Commands

        public RelayCommand CommandPersonalizeBackground { get; private set; }
        public RelayCommand CommandPersonalizeColor { get; private set; }
        public RelayCommand CommandPersonalizeLockscreen { get; private set; }
        public RelayCommand CommandPersonalizeDesign { get; private set; }
        public RelayCommand CommandPersonalizeFont { get; private set; }
        public RelayCommand CommandPersonalizeStart { get; private set; }
        public RelayCommand CommandPersonalizeTaskbar { get; private set; }
        public RelayCommand CommandCloseWindow { get; private set; }

        #endregion

        #region Private fields

        private MainWindow _mainWindow;
        private static PersonalizeView _personalizeView;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="PersonalizeViewModel"/> class.
        /// </summary>
        /// <param name="mainWindow">The main window.</param>
        /// <param name="view">The PersonalizeView.</param>
        public PersonalizeViewModel(MainWindow mainWindow, PersonalizeView personalizeView)
        {
            _mainWindow = mainWindow;
            _personalizeView = personalizeView;
            mainWindow.MainGrid.Children.Add(personalizeView);

            #region Commands

            // Initialize commands and run when the button is pressed
            CommandPersonalizeBackground = new RelayCommand(PersonalizeBackground);
            CommandPersonalizeColor = new RelayCommand(PersonalizeColor);
            CommandPersonalizeLockscreen = new RelayCommand(PersonalizeLockscreen);
            CommandPersonalizeDesign = new RelayCommand(PersonalizeDesign);
            CommandPersonalizeFont = new RelayCommand(PersonalizeFont);
            CommandPersonalizeStart = new RelayCommand(PersonalizeStart);
            CommandPersonalizeTaskbar = new RelayCommand(PersonalizeTaskbar);
            CommandCloseWindow = new RelayCommand(CloseWindow);

            #endregion

            PersonalizeBackground();
        }

        /// <summary>
        /// Gets the personalize view.
        /// </summary>
        /// <returns></returns>
        public static PersonalizeView GetPersonalizeView() => _personalizeView;

        #region Command functions

        /// <summary>
        /// Creates an instance of <see cref="PersonalizeBackgroundView"/>
        /// </summary>
        private void PersonalizeBackground() => new PersonalizeBackgroundView(GetPersonalizeView());

        /// <summary>
        /// Creates an instance of <see cref="PersonalizeColorView"/>
        /// </summary>
        private void PersonalizeColor() => new PersonalizeColorView(GetPersonalizeView());

        /// <summary>
        /// Creates an instance of <see cref="PersonalizeLockscreenView"/>
        /// </summary>
        private void PersonalizeLockscreen() => new PersonalizeLockscreenView(GetPersonalizeView());

        /// <summary>
        /// Creates an instance of <see cref="PersonalizeDesignView"/>
        /// </summary>
        private void PersonalizeDesign() => new PersonalizeDesignView(GetPersonalizeView());

        /// <summary>
        /// Creates an instance of <see cref="PersonalizeFontView"/>
        /// </summary>
        private void PersonalizeFont() => new PersonalizeFontView(GetPersonalizeView());

        /// <summary>
        /// Creates an instance of <see cref="PersonalizeStartView"/>
        /// </summary>
        private void PersonalizeStart() => new PersonalizeStartView(GetPersonalizeView());

        /// <summary>
        /// Creates an instance of <see cref="PersonalizeTaskbarView"/>
        /// </summary>
        private void PersonalizeTaskbar() => new PersonalizeTaskbarView(GetPersonalizeView());

        /// <summary>
        /// Removes the <see cref="View"/> from <see cref="Main.MainGrid"/>
        /// </summary>
        private void CloseWindow() => _mainWindow.MainGrid.Children.Remove(GetPersonalizeView());

        #endregion
    }
}
