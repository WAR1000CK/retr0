﻿using Ui.Desktop.View.Setting.Personalize;

namespace Ui.Desktop.ViewModel.Setting.Personalize
{
    public class PersonalizeStartViewModel
    {
        #region

        public PersonalizeView PersonalizeView { get; private set; }
        public PersonalizeStartView PersonalizeStartView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="PersonalizeStartViewModel"/> class.
        /// </summary>
        /// <param name="personalizeView">The personalize view.</param>
        /// <param name="personalizeStartView">The personalize start view.</param>
        public PersonalizeStartViewModel(PersonalizeView personalizeView, PersonalizeStartView personalizeStartView)
        {
            PersonalizeView = personalizeView;
            PersonalizeStartView = personalizeStartView;
            personalizeView.PersonalizeGrid.Children.Clear();
            personalizeView.PersonalizeGrid.Children.Add(personalizeStartView);
        }
    }
}
