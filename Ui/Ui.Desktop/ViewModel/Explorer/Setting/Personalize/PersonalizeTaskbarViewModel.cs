﻿using Ui.Desktop.View.Setting.Personalize;

namespace Ui.Desktop.ViewModel.Setting.Personalize
{
    public class PersonalizeTaskbarViewModel
    {
        #region

        public PersonalizeView PersonalizeView { get; private set; }
        public PersonalizeTaskbarView PersonalizeTaskbarView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="PersonalizeTaskbarViewModel"/> class.
        /// </summary>
        /// <param name="personalizeView">The personalize view.</param>
        /// <param name="personalizeTaskbarView">The personalize taskbar view.</param>
        public PersonalizeTaskbarViewModel(PersonalizeView personalizeView, PersonalizeTaskbarView personalizeTaskbarView)
        {
            PersonalizeView = personalizeView;
            PersonalizeTaskbarView = personalizeTaskbarView;
            personalizeView.PersonalizeGrid.Children.Clear();
            personalizeView.PersonalizeGrid.Children.Add(personalizeTaskbarView);
        }
    }
}
