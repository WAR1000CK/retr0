﻿using Ui.Desktop.View.Setting.Personalize;

namespace Ui.Desktop.ViewModel.Setting.Personalize
{
    public class PersonalizeFontViewModel
    {
        #region

        public PersonalizeView PersonalizeView { get; private set; }
        public PersonalizeFontView PersonalizeFontView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="PersonalizeFontViewModel"/> class.
        /// </summary>
        /// <param name="personalizeView">The personalize view.</param>
        /// <param name="personalizeFontView">The personalize font view.</param>
        public PersonalizeFontViewModel(PersonalizeView personalizeView, PersonalizeFontView personalizeFontView)
        {
            PersonalizeView = personalizeView;
            PersonalizeFontView = personalizeFontView;
            personalizeView.PersonalizeGrid.Children.Clear();
            personalizeView.PersonalizeGrid.Children.Add(personalizeFontView);
        }
    }
}
