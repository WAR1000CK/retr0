﻿using Ui.Desktop.View.Setting.Personalize;

namespace Ui.Desktop.ViewModel.Setting.Personalize
{
    public class PersonalizeLockscreenViewModel
    {
        #region

        public PersonalizeView PersonalizeView { get; private set; }
        public PersonalizeLockscreenView PersonalizeLockscreenView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="PersonalizeLockscreenViewModel"/> class.
        /// </summary>
        /// <param name="personalizeView">The personalize view.</param>
        /// <param name="personalizeLockscreenView">The personalize lockscreen view.</param>
        public PersonalizeLockscreenViewModel(PersonalizeView personalizeView, PersonalizeLockscreenView personalizeLockscreenView)
        {
            PersonalizeView = personalizeView;
            PersonalizeLockscreenView = personalizeLockscreenView;
            personalizeView.PersonalizeGrid.Children.Clear();
            personalizeView.PersonalizeGrid.Children.Add(personalizeLockscreenView);
        }
    }
}
