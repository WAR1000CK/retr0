﻿using Ui.Desktop.View.Setting.TimeDate;

namespace Ui.Desktop.ViewModel.Setting.TimeDate
{
    class TimeDateSpeechRecognitionViewModel
    {
        #region Properties

        public TimeDateView TimeDateView { get; private set; }
        public TimeDateSpeechRecognitionView TimeDateSpeechRecognitionView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="TimeDateSpeechRecognitionViewModel"/> class.
        /// </summary>
        /// <param name="timeDateView">The time date view.</param>
        /// <param name="timeDateSpeechRecognitionView">The time date speech recognition view.</param>
        public TimeDateSpeechRecognitionViewModel(TimeDateView timeDateView, TimeDateSpeechRecognitionView timeDateSpeechRecognitionView)
        {
            TimeDateView = timeDateView;
            TimeDateSpeechRecognitionView = timeDateSpeechRecognitionView;
            timeDateView.TimeDateGrid.Children.Clear();
            timeDateView.TimeDateGrid.Children.Add(timeDateSpeechRecognitionView);
        }
    }
}
