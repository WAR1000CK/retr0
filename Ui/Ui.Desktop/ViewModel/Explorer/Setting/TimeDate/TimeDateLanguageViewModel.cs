﻿using Ui.Desktop.View.Setting.TimeDate;

namespace Ui.Desktop.ViewModel.Setting.TimeDate
{
    class TimeDateLanguageViewModel
    {
        #region Properties

        public TimeDateView TimeDateView { get; private set; }
        public TimeDateLanguageView TimeDateLanguageView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="TimeDateLanguageViewModel"/> class.
        /// </summary>
        /// <param name="timeDateView">The time date view.</param>
        /// <param name="timeDateLanguageView">The time date language view.</param>
        public TimeDateLanguageViewModel(TimeDateView timeDateView, TimeDateLanguageView timeDateLanguageView)
        {
            TimeDateView = timeDateView;
            TimeDateLanguageView = timeDateLanguageView;
            timeDateView.TimeDateGrid.Children.Clear();
            timeDateView.TimeDateGrid.Children.Add(timeDateLanguageView);
        }
    }
}
