﻿using Ui.Desktop.View.Setting.TimeDate;

namespace Ui.Desktop.ViewModel.Setting.TimeDate
{
    class TimeDateCurrentDateTimeViewModel
    {
        #region Properties

        public TimeDateView TimeDateView { get; private set; }
        public TimeDateCurrentDateTimeView TimeDateCurrentDateTimeView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="TimeDateCurrentDateTimeViewModel"/> class.
        /// </summary>
        /// <param name="timeDateView">The time date view.</param>
        /// <param name="timeDateCurrentDateTimeView">The time date current date time view.</param>
        public TimeDateCurrentDateTimeViewModel(TimeDateView timeDateView, TimeDateCurrentDateTimeView timeDateCurrentDateTimeView)
        {
            TimeDateView = timeDateView;
            TimeDateCurrentDateTimeView = timeDateCurrentDateTimeView;
            timeDateView.TimeDateGrid.Children.Clear();
            timeDateView.TimeDateGrid.Children.Add(timeDateCurrentDateTimeView);
        }
    }
}
