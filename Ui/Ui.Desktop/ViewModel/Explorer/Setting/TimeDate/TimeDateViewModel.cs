﻿using Ui.Desktop.View.Setting.TimeDate;

namespace Ui.Desktop.ViewModel.Setting.TimeDate
{
    public class TimeDateViewModel : BaseViewModel
    {
        #region Commands

        public RelayCommand CommandCurrentDateTime { get; private set; }
        public RelayCommand CommandRegion { get; private set; }
        public RelayCommand CommandLanguage { get; private set; }
        public RelayCommand CommandSpeechRecognition { get; private set; }
        public RelayCommand CommandCloseWindow { get; private set; }            // Close TimeDateView

        #endregion

        #region Private fields

        private readonly MainWindow _mainWindow;
        private static TimeDateView _timeDateView;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="TimeDateViewModel"/> class.
        /// </summary>
        /// <param name="mainWindow">The main window.</param>
        /// <param name="view">The view.</param>
        public TimeDateViewModel(MainWindow mainWindow, TimeDateView timeDateView)
        {
            _mainWindow = mainWindow;
            _timeDateView = timeDateView;
            mainWindow.MainGrid.Children.Add(timeDateView);

            #region Commands

            // Initialize commands and run when the button is pressed
            CommandCurrentDateTime = new RelayCommand(CurrentDateTime);
            CommandRegion = new RelayCommand(Region);
            CommandLanguage = new RelayCommand(Language);
            CommandSpeechRecognition = new RelayCommand(SpeechRecognition);
            CommandCloseWindow = new RelayCommand(CloseWindow);

            #endregion

            CurrentDateTime();
        }

        /// <summary>
        /// Gets the TimeDateView
        /// </summary>
        /// <returns></returns>
        public static TimeDateView GetTimeDateView() => _timeDateView;

        #region Command Functions

        /// <summary>
        /// Creates an instance of <see cref="TimeDateCurrentDateTimeView"/>
        /// </summary>
        private void CurrentDateTime() => new TimeDateCurrentDateTimeView(GetTimeDateView());

        /// <summary>
        /// Creates an instance of <see cref="TimeDateRegionView"/>
        /// </summary>
        private void Region() => new TimeDateRegionView(GetTimeDateView());

        /// <summary>
        /// Creates an instance of <see cref="TimeDateLanguageView"/>
        /// </summary>
        private void Language() => new TimeDateLanguageView(GetTimeDateView());

        /// <summary>
        /// Creates an instance of <see cref="TimeDateSpeechRecognitionView"/>
        /// </summary>
        private void SpeechRecognition() => new TimeDateSpeechRecognitionView(GetTimeDateView());

        /// <summary>
        /// Removes the <see cref="View"/> from <see cref="Main.MainGrid"/>
        /// </summary>
        private void CloseWindow() => _mainWindow.MainGrid.Children.Remove(GetTimeDateView());

        #endregion
    }
}
