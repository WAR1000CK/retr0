﻿using Ui.Desktop.View.Setting.TimeDate;

namespace Ui.Desktop.ViewModel.Setting.TimeDate
{
    class TimeDateRegionViewModel
    {
        #region Properties

        public TimeDateView TimeDateView { get; private set; }
        public TimeDateRegionView TimeDateRegionView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="TimeDateRegionViewModel"/> class.
        /// </summary>
        /// <param name="timeDateView">The time date view.</param>
        /// <param name="timeDateRegionView">The time date region view.</param>
        public TimeDateRegionViewModel(TimeDateView timeDateView, TimeDateRegionView timeDateRegionView)
        {
            TimeDateView = timeDateView;
            TimeDateRegionView = timeDateRegionView;
            timeDateView.TimeDateGrid.Children.Clear();
            timeDateView.TimeDateGrid.Children.Add(timeDateRegionView);
        }
    }
}
