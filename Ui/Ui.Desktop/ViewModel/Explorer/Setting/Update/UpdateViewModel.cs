﻿using Ui.Desktop.View.Explorer.Setting.Update;

namespace Ui.Desktop.ViewModel.Explorer.Setting.Update
{
    public class UpdateViewModel
    {
        #region Commands

        public RelayCommand CommandWindowsUpdate { get; private set; }
        public RelayCommand CommandTransmissionOptimization { get; private set; }
        public RelayCommand CommandWindowsSecurity { get; private set; }
        public RelayCommand CommandBackup { get; private set; }
        public RelayCommand CommandTroubleshooting { get; private set; }
        public RelayCommand CommandRestore { get; private set; }
        public RelayCommand CommandActivation { get; private set; }
        public RelayCommand CommandFindMyDevice { get; private set; }
        public RelayCommand CommandForDevelopers { get; private set; }
        public RelayCommand CommandWindowsInsiderProgram { get; private set; }
        public RelayCommand CommandCloseWindow { get; private set; }

        #endregion

        #region Private fields

        private readonly MainWindow _mainWindow;
        private static UpdateView _updateView;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="UpdateViewModel"/> class.
        /// </summary>
        /// <param name="mainWindow">The main window.</param>
        /// <param name="updateView">The view.</param>
        public UpdateViewModel(MainWindow mainWindow, UpdateView updateView)
        {
            _mainWindow = mainWindow;
            _updateView = updateView;
            mainWindow.MainGrid.Children.Add(updateView);

            #region Commands

            CommandWindowsUpdate = new RelayCommand(WindowsUpdate);
            CommandTransmissionOptimization = new RelayCommand(TransmissionOptimization);
            CommandWindowsSecurity = new RelayCommand(WindowsSecurity);
            CommandBackup = new RelayCommand(Backup);
            CommandTroubleshooting = new RelayCommand(Troubleshooting);
            CommandRestore = new RelayCommand(Restore);
            CommandActivation = new RelayCommand(Activation);
            CommandFindMyDevice = new RelayCommand(FindMyDevice);
            CommandForDevelopers = new RelayCommand(ForDevelopers);
            CommandWindowsInsiderProgram = new RelayCommand(WindowsInsiderProgram);
            CommandCloseWindow = new RelayCommand(CloseWindow);

            #endregion

            WindowsUpdate();
        }

        /// <summary>
        /// Gets to update view.
        /// </summary>
        /// <returns></returns>
        public static UpdateView GetUpdateView() => _updateView;

        #region Command functions

        /// <summary>
        /// Creates an instance of <see cref="UpdateWindowsUpdateView"/>
        /// </summary>
        private void WindowsUpdate() => new UpdateWindowsUpdateView(GetUpdateView());

        /// <summary>
        /// Creates an instance of <see cref="UpdateTransmissionOptimizationView"/>
        /// </summary>
        private void TransmissionOptimization() => new UpdateTransmissionOptimizationView(GetUpdateView());

        /// <summary>
        /// Creates an instance of <see cref="UpdateWindowsSecurityView"/>
        /// </summary>
        private void WindowsSecurity() => new UpdateWindowsSecurityView(GetUpdateView());

        /// <summary>
        /// Creates an instance of <see cref="UpdateBackupView"/>
        /// </summary>
        private void Backup() => new UpdateBackupView(GetUpdateView());

        /// <summary>
        /// Creates an instance of <see cref="UpdateTroubleshootingView"/>
        /// </summary>
        private void Troubleshooting() => new UpdateTroubleshootingView(GetUpdateView());

        /// <summary>
        /// Creates an instance of <see cref="UpdateRestoreView"/>
        /// </summary>
        private void Restore() => new UpdateRestoreView(GetUpdateView());

        /// <summary>
        /// Creates an instance of <see cref="UpdateActivationView"/>
        /// </summary>
        private void Activation() => new UpdateActivationView(GetUpdateView());

        /// <summary>
        /// Creates an instance of <see cref="UpdateFindMyDeviceView"/>
        /// </summary>
        private void FindMyDevice() => new UpdateFindMyDeviceView(GetUpdateView());

        /// <summary>
        /// Creates an instance of <see cref="UpdateForDevelopersView"/>
        /// </summary>
        private void ForDevelopers() => new UpdateForDevelopersView(GetUpdateView());

        /// <summary>
        /// Creates an instance of <see cref="UpdateWindowsInsiderProgramView"/>
        /// </summary>
        private void WindowsInsiderProgram() => new UpdateWindowsInsiderProgramView(GetUpdateView());

        /// <summary>
        /// Removes the <see cref="View"/> from <see cref="Main.MainGrid"/>
        /// </summary>
        private void CloseWindow() => _mainWindow.MainGrid.Children.Remove(GetUpdateView());

        #endregion
    }
}
