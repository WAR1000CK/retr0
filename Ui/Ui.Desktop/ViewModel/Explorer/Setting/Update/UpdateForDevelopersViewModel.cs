﻿using Ui.Desktop.View.Explorer.Setting.Update;

namespace Ui.Desktop.ViewModel.Explorer.Setting.Update
{
    public class UpdateForDevelopersViewModel
    {
        #region Properties

        public UpdateView UpdateView { get; private set; }
        public UpdateForDevelopersView UpdateForDevelopersView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="UpdateForDevelopersViewModel"/> class.
        /// </summary>
        /// <param name="updateView">The update view.</param>
        /// <param name="updateForDevelopersView">The update for developers view.</param>
        public UpdateForDevelopersViewModel(UpdateView updateView, UpdateForDevelopersView updateForDevelopersView)
        {
            UpdateView = updateView;
            UpdateForDevelopersView = updateForDevelopersView;
            updateView.UpdateGrid.Children.Clear();
            updateView.UpdateGrid.Children.Add(updateForDevelopersView);
        }
    }
}
