﻿using Ui.Desktop.View.Explorer.Setting.Update;

namespace Ui.Desktop.ViewModel.Explorer.Setting.Update
{
    class UpdateBackupViewModel
    {
        #region Properties

        public UpdateView UpdateView { get; private set; }
        public UpdateBackupView UpdateBackupView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="UpdateBackupViewModel"/> class.
        /// </summary>
        /// <param name="updateView">The update view.</param>
        /// <param name="updateBackupView">The update backup view.</param>
        public UpdateBackupViewModel(UpdateView updateView, UpdateBackupView updateBackupView)
        {
            UpdateView = updateView;
            UpdateBackupView = updateBackupView;
            updateView.UpdateGrid.Children.Clear();
            updateView.UpdateGrid.Children.Add(updateBackupView);
        }
    }
}
