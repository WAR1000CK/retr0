﻿using Ui.Desktop.View.Explorer.Setting.Update;

namespace Ui.Desktop.ViewModel.Explorer.Setting.Update
{
    public class UpdateRestoreViewModel
    {
        #region Properties

        public UpdateView UpdateView { get; private set; }
        public UpdateRestoreView UpdateRestoreView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="UpdateRestoreViewModel"/> class.
        /// </summary>
        /// <param name="updateView">The update view.</param>
        /// <param name="updateRestoreView">The update restore view.</param>
        public UpdateRestoreViewModel(UpdateView updateView, UpdateRestoreView updateRestoreView)
        {
            UpdateView = updateView;
            UpdateRestoreView = updateRestoreView;
            updateView.UpdateGrid.Children.Clear();
            updateView.UpdateGrid.Children.Add(updateRestoreView);
        }
    }
}
