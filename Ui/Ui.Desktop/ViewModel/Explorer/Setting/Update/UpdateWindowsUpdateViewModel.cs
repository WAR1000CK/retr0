﻿using Ui.Desktop.View.Explorer.Setting.Update;

namespace Ui.Desktop.ViewModel.Explorer.Setting.Update
{
    public class UpdateWindowsUpdateViewModel
    {
        #region Properties

        public UpdateView UpdateView { get; private set; }
        public UpdateWindowsUpdateView UpdateWindowsUpdateView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="UpdateWindowsUpdateViewModel"/> class.
        /// </summary>
        /// <param name="updateView">The update view.</param>
        /// <param name="updateWindowsUpdateView">The update windows update view.</param>
        public UpdateWindowsUpdateViewModel(UpdateView updateView, UpdateWindowsUpdateView updateWindowsUpdateView)
        {
            UpdateView = updateView;
            UpdateWindowsUpdateView = updateWindowsUpdateView;
            updateView.UpdateGrid.Children.Clear();
            updateView.UpdateGrid.Children.Add(updateWindowsUpdateView);
        }
    }
}
