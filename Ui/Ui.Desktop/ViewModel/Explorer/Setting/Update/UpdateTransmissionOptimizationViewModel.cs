﻿using Ui.Desktop.View.Explorer.Setting.Update;

namespace Ui.Desktop.ViewModel.Explorer.Setting.Update
{
    public class UpdateTransmissionOptimizationViewModel
    {
        #region Properties

        public UpdateView UpdateView { get; private set; }
        public UpdateTransmissionOptimizationView UpdateTransmissionOptimizationView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="UpdateTransmissionOptimizationViewModel"/> class.
        /// </summary>
        /// <param name="updateView">The update view.</param>
        /// <param name="updateTransmissionOptimizationView">The update transmission optimization view.</param>
        public UpdateTransmissionOptimizationViewModel(UpdateView updateView,
            UpdateTransmissionOptimizationView updateTransmissionOptimizationView)
        {
            UpdateView = updateView;
            UpdateTransmissionOptimizationView = updateTransmissionOptimizationView;
            updateView.UpdateGrid.Children.Clear();
            updateView.UpdateGrid.Children.Add(updateTransmissionOptimizationView);
        }
    }
}
