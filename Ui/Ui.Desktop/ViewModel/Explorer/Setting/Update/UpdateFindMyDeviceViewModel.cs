﻿using Ui.Desktop.View.Explorer.Setting.Update;

namespace Ui.Desktop.ViewModel.Explorer.Setting.Update
{
    public class UpdateFindMyDeviceViewModel
    {
        #region Properties

        public UpdateView UpdateView { get; private set; }
        public UpdateFindMyDeviceView UpdateFindMyDeviceView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="UpdateFindMyDeviceViewModel"/> class.
        /// </summary>
        /// <param name="updateView">The update view.</param>
        /// <param name="updateFindMyDeviceView">The update find my device view.</param>
        public UpdateFindMyDeviceViewModel(UpdateView updateView, UpdateFindMyDeviceView updateFindMyDeviceView)
        {
            UpdateView = updateView;
            UpdateFindMyDeviceView = updateFindMyDeviceView;
            updateView.UpdateGrid.Children.Clear();
            updateView.UpdateGrid.Children.Add(updateFindMyDeviceView);
        }
    }
}
