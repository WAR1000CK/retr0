﻿using Ui.Desktop.View.Explorer.Setting.Update;

namespace Ui.Desktop.ViewModel.Explorer.Setting.Update
{
    public class UpdateActivationViewModel
    {
        #region Properties

        public UpdateView UpdateView { get; private set; }
        public UpdateActivationView UpdateActivationView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="UpdateActivationViewModel"/> class.
        /// </summary>
        /// <param name="updateView">The update view.</param>
        /// <param name="updateActivationView">The update activation view.</param>
        public UpdateActivationViewModel(UpdateView updateView, UpdateActivationView updateActivationView)
        {
            UpdateView = updateView;
            UpdateActivationView = updateActivationView;
            updateView.UpdateGrid.Children.Clear();
            updateView.UpdateGrid.Children.Add(updateActivationView);
        }
    }
}
