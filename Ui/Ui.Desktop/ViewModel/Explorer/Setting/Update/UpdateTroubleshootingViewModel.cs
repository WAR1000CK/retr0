﻿using Ui.Desktop.View.Explorer.Setting.Update;

namespace Ui.Desktop.ViewModel.Explorer.Setting.Update
{
    public class UpdateTroubleshootingViewModel
    {
        #region Properties

        public UpdateView UpdateView { get; private set; }
        public UpdateTroubleshootingView UpdateTroubleshootingView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="UpdateTroubleshootingViewModel"/> class.
        /// </summary>
        /// <param name="updateView">The update view.</param>
        /// <param name="updateTroubleshootingView">The update troubleshooting view.</param>
        public UpdateTroubleshootingViewModel(UpdateView updateView, UpdateTroubleshootingView updateTroubleshootingView)
        {
            UpdateView = updateView;
            UpdateTroubleshootingView = updateTroubleshootingView;
            updateView.UpdateGrid.Children.Clear();
            updateView.UpdateGrid.Children.Add(updateTroubleshootingView);
        }
    }
}
