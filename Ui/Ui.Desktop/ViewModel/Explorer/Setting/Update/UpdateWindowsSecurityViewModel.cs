﻿using Ui.Desktop.View.Explorer.Setting.Update;

namespace Ui.Desktop.ViewModel.Explorer.Setting.Update
{
    public class UpdateWindowsSecurityViewModel
    {
        #region Properties

        public UpdateView UpdateView { get; private set; }
        public UpdateWindowsSecurityView UpdateWindowsSecurityView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="UpdateWindowsSecurityViewModel"/> class.
        /// </summary>
        /// <param name="updateView">The update view.</param>
        /// <param name="updateWindowsSecurityView">The update windows security view.</param>
        public UpdateWindowsSecurityViewModel(UpdateView updateView, UpdateWindowsSecurityView updateWindowsSecurityView)
        {
            UpdateView = updateView;
            UpdateWindowsSecurityView = updateWindowsSecurityView;
            updateView.UpdateGrid.Children.Clear();
            updateView.UpdateGrid.Children.Add(updateWindowsSecurityView);
        }
    }
}
