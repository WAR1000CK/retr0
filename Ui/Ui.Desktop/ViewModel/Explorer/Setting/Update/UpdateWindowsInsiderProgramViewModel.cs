﻿using Ui.Desktop.View.Explorer.Setting.Update;

namespace Ui.Desktop.ViewModel.Explorer.Setting.Update
{
    public class UpdateWindowsInsiderProgramViewModel
    {
        #region Properties

        public UpdateView UpdateView { get; private set; }
        public UpdateWindowsInsiderProgramView UpdateWindowsInsiderProgramView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="UpdateWindowsInsiderProgramViewModel"/> class.
        /// </summary>
        /// <param name="updateView">The update view.</param>
        /// <param name="updateWindowsInsiderProgramView">The update windows insider program view.</param>
        public UpdateWindowsInsiderProgramViewModel(UpdateView updateView, UpdateWindowsInsiderProgramView updateWindowsInsiderProgramView)
        {
            UpdateView = updateView;
            UpdateWindowsInsiderProgramView = updateWindowsInsiderProgramView;
            updateView.UpdateGrid.Children.Clear();
            updateView.UpdateGrid.Children.Add(updateWindowsInsiderProgramView);
        }
    }
}
