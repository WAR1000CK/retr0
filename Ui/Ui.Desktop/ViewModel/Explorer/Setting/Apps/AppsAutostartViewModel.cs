﻿using Ui.Desktop.View.Setting.Apps;

namespace Ui.Desktop.ViewModel.Setting.Apps
{
    class AppsAutostartViewModel
    {
        #region Properties

        public AppsView AppsView { get; private set; }
        public AppsAutostartView AppsAutostartView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="AppsAutostartViewModel"/> class.
        /// </summary>
        /// <param name="appsView">The apps view.</param>
        /// <param name="appsAutostartView">The apps autostart view.</param>
        public AppsAutostartViewModel(AppsView appsView, AppsAutostartView appsAutostartView)
        {
            AppsView = appsView;
            AppsAutostartView = appsAutostartView;
            appsView.AppsGrid.Children.Clear();
            appsView.AppsGrid.Children.Add(appsAutostartView);
        }
    }
}
