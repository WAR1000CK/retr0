﻿using Ui.Desktop.View.Setting.Apps;

namespace Ui.Desktop.ViewModel.Setting.Apps
{
    class AppsFeaturesViewModel
    {
        #region Properties

        public AppsView AppsView { get; private set; }
        public AppsFeaturesView AppsFeaturesView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="AppsFeaturesViewModel"/> class.
        /// </summary>
        /// <param name="appsView">The apps view.</param>
        /// <param name="appsFeaturesView">The apps features view.</param>
        public AppsFeaturesViewModel(AppsView appsView, AppsFeaturesView appsFeaturesView)
        {
            AppsView = appsView;
            AppsFeaturesView = appsFeaturesView;
            appsView.AppsGrid.Children.Clear();
            appsView.AppsGrid.Children.Add(appsFeaturesView);
        }
    }
}
