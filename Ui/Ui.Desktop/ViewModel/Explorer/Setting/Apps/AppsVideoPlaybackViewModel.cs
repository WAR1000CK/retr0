﻿using Ui.Desktop.View.Setting.Apps;

namespace Ui.Desktop.ViewModel.Setting.Apps
{
    class AppsVideoPlaybackViewModel
    {
        #region Properties

        public AppsView AppsView { get; private set; }
        public AppsVideoPlaybackView AppsVideoPlaybackView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="AppsVideoPlaybackViewModel"/> class.
        /// </summary>
        /// <param name="appsView">The apps view.</param>
        /// <param name="appsVideoPlaybackView">The apps video playback view.</param>
        public AppsVideoPlaybackViewModel(AppsView appsView, AppsVideoPlaybackView appsVideoPlaybackView)
        {
            AppsView = appsView;
            AppsVideoPlaybackView = appsVideoPlaybackView;
            appsView.AppsGrid.Children.Clear();
            appsView.AppsGrid.Children.Add(appsVideoPlaybackView);
        }
    }
}
