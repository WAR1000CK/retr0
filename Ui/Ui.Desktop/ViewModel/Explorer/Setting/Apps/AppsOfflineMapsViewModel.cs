﻿using Ui.Desktop.View.Setting.Apps;

namespace Ui.Desktop.ViewModel.Setting.Apps
{
    class AppsOfflineMapsViewModel
    {
        #region Properties

        public AppsView AppsView { get; private set; }
        public AppsOfflineMapsView AppsOfflineMapsView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="AppsOfflineMapsViewModel"/> class.
        /// </summary>
        /// <param name="appsView">The apps view.</param>
        /// <param name="appsOfflineMapsView">The apps offline maps view.</param>
        public AppsOfflineMapsViewModel(AppsView appsView, AppsOfflineMapsView appsOfflineMapsView)
        {
            AppsView = appsView;
            AppsOfflineMapsView = appsOfflineMapsView;
            appsView.AppsGrid.Children.Clear();
            appsView.AppsGrid.Children.Add(appsOfflineMapsView);
        }
    }
}
