﻿using Ui.Desktop.View.Setting.Apps;

namespace Ui.Desktop.ViewModel.Setting.Apps
{
    class AppsForWebsitesViewModel
    {
        #region Properties

        public AppsView AppsView { get; private set; }
        public AppsForWebsitesView AppsForWebsitesView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="AppsForWebsitesViewModel"/> class.
        /// </summary>
        /// <param name="appsView">The apps view.</param>
        /// <param name="appsForWebsitesView">The apps for websites view.</param>
        public AppsForWebsitesViewModel(AppsView appsView, AppsForWebsitesView appsForWebsitesView)
        {
            AppsView = appsView;
            AppsForWebsitesView = appsForWebsitesView;
            appsView.AppsGrid.Children.Clear();
            appsView.AppsGrid.Children.Add(appsForWebsitesView);
        }
    }
}
