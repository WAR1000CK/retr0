﻿using Ui.Desktop.View.Setting.Apps;

namespace Ui.Desktop.ViewModel.Setting.Apps
{
    class AppsStandardAppsViewModel
    {
        #region Properties

        public AppsView AppsView { get; private set; }
        public AppsStandardAppsView AppsStandardAppsView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="AppsStandardAppsViewModel"/> class.
        /// </summary>
        /// <param name="appsView">The apps view.</param>
        /// <param name="appsStandardAppsView">The apps standard apps view.</param>
        public AppsStandardAppsViewModel(AppsView appsView, AppsStandardAppsView appsStandardAppsView)
        {
            AppsView = appsView;
            AppsStandardAppsView = appsStandardAppsView;
            appsView.AppsGrid.Children.Clear();
            appsView.AppsGrid.Children.Add(appsStandardAppsView);
        }
    }
}
