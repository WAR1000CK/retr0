﻿using Ui.Desktop.View.Setting.Apps;

namespace Ui.Desktop.ViewModel.Setting.Apps
{
    public class AppsViewModel
    {
        #region Commands

        public RelayCommand CommandFeatures { get; private set; }
        public RelayCommand CommandStandardApps { get; private set; }
        public RelayCommand CommandOfflineMaps { get; private set; }
        public RelayCommand CommandAppsForWebsites { get; private set; }
        public RelayCommand CommandVideoPlayback { get; private set; }
        public RelayCommand CommandAutostart { get; private set; }
        public RelayCommand CommandCloseWindow { get; private set; }            // Close AppsView

        #endregion

        #region Private fields

        private readonly MainWindow _mainWindow;
        private static AppsView _appsView;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="AppsViewModel"/> class.
        /// </summary>
        /// <param name="mainWindow">The main window.</param>
        /// <param name="view">The view.</param>
        public AppsViewModel(MainWindow mainWindow, AppsView appsView)
        {
            _mainWindow = mainWindow;
            _appsView = appsView;
            mainWindow.MainGrid.Children.Add(appsView);

            #region Commands

            // Initialize commands and run when the button is pressed
            CommandFeatures = new RelayCommand(Features);
            CommandStandardApps = new RelayCommand(StandardApps);
            CommandOfflineMaps = new RelayCommand(OfflineMaps);
            CommandAppsForWebsites = new RelayCommand(AppsForWebsites);
            CommandVideoPlayback = new RelayCommand(VideoPlayback);
            CommandAutostart = new RelayCommand(Autostart);
            CommandCloseWindow = new RelayCommand(CloseWindow);

            #endregion

            Features();
        }

        /// <summary>
        /// Gets the apps view.
        /// </summary>
        /// <returns></returns>
        public static AppsView GetAppsView() => _appsView;

        #region Command Functions

        /// <summary>
        /// Creates an instance of <see cref="AppsFeaturesView"/>
        /// </summary>
        private void Features() => new AppsFeaturesView(GetAppsView());

        /// <summary>
        /// Creates an instance of <see cref="AppsStandardAppsView"/>
        /// </summary>
        private void StandardApps() => new AppsStandardAppsView(GetAppsView());

        /// <summary>
        /// Creates an instance of <see cref="AppsOfflineMapsView"/>
        /// </summary>
        private void OfflineMaps() => new AppsOfflineMapsView(GetAppsView());

        /// <summary>
        /// Creates an instance of <see cref="AppsForWebsitesView"/>
        /// </summary>
        private void AppsForWebsites() => new AppsForWebsitesView(GetAppsView());

        /// <summary>
        /// Creates an instance of <see cref="AppsVideoPlaybackView"/>
        /// </summary>
        private void VideoPlayback() => new AppsVideoPlaybackView(GetAppsView());

        /// <summary>
        /// Creates an instance of <see cref="AppsAutostartView"/>
        /// </summary>
        private void Autostart() => new AppsAutostartView(GetAppsView());

        /// <summary>
        /// Removes the <see cref="View"/> from <see cref="Main.MainGrid"/>
        /// </summary>
        private void CloseWindow() => _mainWindow.MainGrid.Children.Remove(GetAppsView());

        #endregion
    }
}
