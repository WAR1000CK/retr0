﻿using Ui.Desktop.View.Setting.Devices;

namespace Ui.Desktop.ViewModel.Setting.Devices
{
    public class DevicesViewModel
    {
        #region Commands

        public RelayCommand CommandBluetooth { get; private set; }
        public RelayCommand CommandPrinter { get; private set; }
        public RelayCommand CommandMouse { get; private set; }
        public RelayCommand CommandInput { get; private set; }
        public RelayCommand CommandPen { get; private set; }
        public RelayCommand CommandAutomaticPlayback { get; private set; }
        public RelayCommand CommandUSB { get; private set; }
        public RelayCommand CommandCloseWindow { get; private set; }            // Close DevicesView

        #endregion

        #region Private fields

        private readonly MainWindow _mainWindow;
        private static DevicesView _devicesView;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="DevicesViewModel"/> class.
        /// </summary>
        /// <param name="mainWindow">The main window.</param>
        /// <param name="view">The view.</param>
        public DevicesViewModel(MainWindow mainWindow, DevicesView devicesView)
        {
            _mainWindow = mainWindow;
            _devicesView = devicesView;
            mainWindow.MainGrid.Children.Add(devicesView);

            #region Commands

            // Initialize commands and run when the button is pressed
            CommandBluetooth = new RelayCommand(Bluetooth);
            CommandPrinter = new RelayCommand(Printer);
            CommandMouse = new RelayCommand(Mouse);
            CommandInput = new RelayCommand(Input);
            CommandPen = new RelayCommand(Pen);
            CommandAutomaticPlayback = new RelayCommand(AutomaticPlayback);
            CommandUSB = new RelayCommand(USB);
            CommandCloseWindow = new RelayCommand(CloseWindow);

            #endregion

            Bluetooth();
        }

        /// <summary>
        /// Gets the devices view.
        /// </summary>
        /// <returns></returns>
        public static DevicesView GetDevicesView() => _devicesView;

        #region Command Functions

        /// <summary>
        /// Creates an instance of <see cref="DevicesBluetoothView"/>
        /// </summary>
        private void Bluetooth() => new DevicesBluetoothView(GetDevicesView());

        /// <summary>
        /// Creates an instance of <see cref="DevicesPrinterView"/>
        /// </summary>
        private void Printer() => new DevicesPrinterView(GetDevicesView());

        /// <summary>
        /// Creates an instance of <see cref="DevicesMouseView"/>
        /// </summary>
        private void Mouse() => new DevicesMouseView(GetDevicesView());

        /// <summary>
        /// Creates an instance of <see cref="DevicesInputView"/>
        /// </summary>
        private void Input() => new DevicesInputView(GetDevicesView());

        /// <summary>
        /// Creates an instance of <see cref="DevicesPenView"/>
        /// </summary>
        private void Pen() => new DevicesPenView(GetDevicesView());

        /// <summary>
        /// Creates an instance of <see cref="DevicesAutomaticPlaybackView"/>
        /// </summary>
        private void AutomaticPlayback() => new DevicesAutomaticPlaybackView(GetDevicesView());

        /// <summary>
        /// Creates an instance of <see cref="DevicesUSBView"/>
        /// </summary>
        private void USB() => new DevicesUSBView(GetDevicesView());

        /// <summary>
        /// Removes the <see cref="View"/> from <see cref="Main.MainGrid"/>
        /// </summary>
        private void CloseWindow() => _mainWindow.MainGrid.Children.Remove(GetDevicesView());

        #endregion
    }
}
