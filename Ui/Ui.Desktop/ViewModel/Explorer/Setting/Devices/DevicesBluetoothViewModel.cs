﻿using Ui.Desktop.View.Setting.Devices;

namespace Ui.Desktop.ViewModel.Setting.Devices
{
    class DevicesBluetoothViewModel
    {
        #region Properties

        public DevicesView DevicesView { get; private set; }
        public DevicesBluetoothView DevicesBluetoothView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="DevicesBluetoothViewModel"/> class.
        /// </summary>
        /// <param name="devicesView">The devices view.</param>
        /// <param name="devicesBluetoothView">The devices bluetooth view.</param>
        public DevicesBluetoothViewModel(DevicesView devicesView, DevicesBluetoothView devicesBluetoothView)
        {
            DevicesView = devicesView;
            DevicesBluetoothView = devicesBluetoothView;
            devicesView.DevicesGrid.Children.Clear();
            devicesView.DevicesGrid.Children.Add(devicesBluetoothView);
        }
    }
}
