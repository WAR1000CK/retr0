﻿using Ui.Desktop.View.Setting.Devices;

namespace Ui.Desktop.ViewModel.Setting.Devices
{
    class DevicesPenViewModel
    {
        #region Properties

        public DevicesView DevicesView { get; private set; }
        public DevicesPenView DevicesPenView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="DevicesPenViewModel"/> class.
        /// </summary>
        /// <param name="devicesView">The devices view.</param>
        /// <param name="devicesPenView">The devices pen view.</param>
        public DevicesPenViewModel(DevicesView devicesView, DevicesPenView devicesPenView)
        {
            DevicesView = devicesView;
            DevicesPenView = devicesPenView;
            devicesView.DevicesGrid.Children.Clear();
            devicesView.DevicesGrid.Children.Add(devicesPenView);
        }
    }
}
