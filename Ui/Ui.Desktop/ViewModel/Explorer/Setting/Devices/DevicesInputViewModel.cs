﻿using Ui.Desktop.View.Setting.Devices;

namespace Ui.Desktop.ViewModel.Setting.Devices
{
    class DevicesInputViewModel
    {
        #region Properties

        public DevicesView DevicesView { get; private set; }
        public DevicesInputView DevicesInputView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="DevicesInputViewModel"/> class.
        /// </summary>
        /// <param name="devicesView">The devices view.</param>
        /// <param name="devicesInputView">The devices input view.</param>
        public DevicesInputViewModel(DevicesView devicesView, DevicesInputView devicesInputView)
        {
            DevicesView = devicesView;
            DevicesInputView = devicesInputView;
            devicesView.DevicesGrid.Children.Clear();
            devicesView.DevicesGrid.Children.Add(devicesInputView);
        }
    }
}
