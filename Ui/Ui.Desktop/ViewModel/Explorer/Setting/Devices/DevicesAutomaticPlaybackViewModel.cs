﻿using Ui.Desktop.View.Setting.Devices;

namespace Ui.Desktop.ViewModel.Setting.Devices
{
    class DevicesAutomaticPlaybackViewModel
    {
        #region Properties

        public DevicesView DevicesView { get; private set; }
        public DevicesAutomaticPlaybackView DevicesAutomaticPlaybackView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="DevicesAutomaticPlaybackViewModel"/> class.
        /// </summary>
        /// <param name="devicesView">The devices view.</param>
        /// <param name="devicesAutomaticPlaybackView">The devices automatic playback view.</param>
        public DevicesAutomaticPlaybackViewModel(DevicesView devicesView, DevicesAutomaticPlaybackView devicesAutomaticPlaybackView)
        {
            DevicesView = devicesView;
            DevicesAutomaticPlaybackView = devicesAutomaticPlaybackView;
            devicesView.DevicesGrid.Children.Clear();
            devicesView.DevicesGrid.Children.Add(devicesAutomaticPlaybackView);
        }
    }
}
