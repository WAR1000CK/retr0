﻿using Ui.Desktop.View.Setting.Devices;

namespace Ui.Desktop.ViewModel.Setting.Devices
{
    class DevicesPrinterViewModel
    {
        #region Properties

        public DevicesView DevicesView { get; private set; }
        public DevicesPrinterView DevicesPrinterView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="DevicesPrinterViewModel"/> class.
        /// </summary>
        /// <param name="devicesView">The devices view.</param>
        /// <param name="devicesPrinterView">The devices printer view.</param>
        public DevicesPrinterViewModel(DevicesView devicesView, DevicesPrinterView devicesPrinterView)
        {
            DevicesView = devicesView;
            DevicesPrinterView = devicesPrinterView;
            devicesView.DevicesGrid.Children.Clear();
            devicesView.DevicesGrid.Children.Add(devicesPrinterView);
        }
    }
}
