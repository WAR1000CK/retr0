﻿using Ui.Desktop.View.Setting.Devices;

namespace Ui.Desktop.ViewModel.Setting.Devices
{
    class DevicesMouseViewModel
    {
        #region Properties

        public DevicesView DevicesView { get; private set; }
        public DevicesMouseView DevicesMouseView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="DevicesMouseViewModel"/> class.
        /// </summary>
        /// <param name="devicesView">The devices view.</param>
        /// <param name="devicesMouseView">The devices mouse view.</param>
        public DevicesMouseViewModel(DevicesView devicesView, DevicesMouseView devicesMouseView)
        {
            DevicesView = devicesView;
            DevicesMouseView = devicesMouseView;
            devicesView.DevicesGrid.Children.Clear();
            devicesView.DevicesGrid.Children.Add(devicesMouseView);
        }
    }
}
