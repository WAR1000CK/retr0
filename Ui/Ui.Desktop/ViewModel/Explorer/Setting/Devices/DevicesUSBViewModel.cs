﻿using Ui.Desktop.View.Setting.Devices;

namespace Ui.Desktop.ViewModel.Setting.Devices
{
    class DevicesUSBViewModel
    {
        #region Properties

        public DevicesView DevicesView { get; private set; }
        public DevicesUSBView DevicesUSBView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="DevicesUSBViewModel"/> class.
        /// </summary>
        /// <param name="devicesView">The devices view.</param>
        /// <param name="devicesUSBView">The devices usb view.</param>
        public DevicesUSBViewModel(DevicesView devicesView, DevicesUSBView devicesUSBView)
        {
            DevicesView = devicesView;
            DevicesUSBView = devicesUSBView;
            devicesView.DevicesGrid.Children.Clear();
            devicesView.DevicesGrid.Children.Add(devicesUSBView);
        }
    }
}
