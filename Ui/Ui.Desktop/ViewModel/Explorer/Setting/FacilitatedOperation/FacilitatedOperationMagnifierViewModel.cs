﻿using Ui.Desktop.View.Setting.FacilitatedOperation;

namespace Ui.Desktop.ViewModel.Explorer.Setting.FacilitatedOperation
{
    public class FacilitatedOperationMagnifierViewModel
    {
        #region Properties

        public static FacilitatedOperationMagnifierView FacilitatedOperationMagnifierView { private get; set; }

        #endregion

        public FacilitatedOperationMagnifierViewModel(FacilitatedOperationView facilitatedOperationView,
            FacilitatedOperationMagnifierView facilitatedOperationMagnifierView)
        {
            FacilitatedOperationMagnifierView = facilitatedOperationMagnifierView;
            facilitatedOperationView.FacilitatedOperationGrid.Children.Clear();
            facilitatedOperationView.FacilitatedOperationGrid.Children.Add(facilitatedOperationMagnifierView);
        }

        /// <summary>
        /// Gets the facilitated operation magnifier view.
        /// </summary>
        /// <returns></returns>
        public static FacilitatedOperationMagnifierView GetFacilitatedOperationMagnifierView() => FacilitatedOperationMagnifierView;
    }
}
