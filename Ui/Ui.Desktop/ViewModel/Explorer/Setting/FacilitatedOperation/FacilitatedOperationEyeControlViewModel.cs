﻿using Ui.Desktop.View.Setting.FacilitatedOperation;

namespace Ui.Desktop.ViewModel.Explorer.Setting.FacilitatedOperation
{
    public class FacilitatedOperationEyeControlViewModel
    {
        #region Properties

        public static FacilitatedOperationEyeControlView FacilitatedOperationEyeControlView { private get; set; }

        #endregion

        public FacilitatedOperationEyeControlViewModel(FacilitatedOperationView facilitatedOperationView,
            FacilitatedOperationEyeControlView facilitatedOperationEyeControlView)
        {
            FacilitatedOperationEyeControlView = facilitatedOperationEyeControlView;
            facilitatedOperationView.FacilitatedOperationGrid.Children.Clear();
            facilitatedOperationView.FacilitatedOperationGrid.Children.Add(facilitatedOperationEyeControlView);
        }

        /// <summary>
        /// Gets the facilitated operation eye control view.
        /// </summary>
        /// <returns></returns>
        public static FacilitatedOperationEyeControlView GetFacilitatedOperationEyeControlView() => FacilitatedOperationEyeControlView;
    }
}
