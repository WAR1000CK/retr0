﻿using Ui.Desktop.View.Setting.FacilitatedOperation;

namespace Ui.Desktop.ViewModel.Setting.FacilitatedOperation
{
    public class FacilitatedOperationDisplayViewModel
    {
        #region Properties

        public static FacilitatedOperationDisplayView FacilitatedOperationDisplayView { private get; set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="FacilitatedOperationDisplayViewModel"/> class.
        /// </summary>
        /// <param name="facilitatedOperationView">The facilitated operation view.</param>
        /// <param name="facilitatedOperationDisplayView">The facilitated operation display view.</param>
        public FacilitatedOperationDisplayViewModel(FacilitatedOperationView facilitatedOperationView,
            FacilitatedOperationDisplayView facilitatedOperationDisplayView)
        {
            FacilitatedOperationDisplayView = facilitatedOperationDisplayView;
            facilitatedOperationView.FacilitatedOperationGrid.Children.Clear();
            facilitatedOperationView.FacilitatedOperationGrid.Children.Add(facilitatedOperationDisplayView);
        }

        /// <summary>
        /// Gets the facilitated operation display view.
        /// </summary>
        /// <returns></returns>
        public static FacilitatedOperationDisplayView GetFacilitatedOperationDisplayView() => FacilitatedOperationDisplayView;
    }
}
