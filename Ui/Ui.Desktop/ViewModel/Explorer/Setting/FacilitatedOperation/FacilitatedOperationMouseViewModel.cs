﻿using Ui.Desktop.View.Setting.FacilitatedOperation;

namespace Ui.Desktop.ViewModel.Explorer.Setting.FacilitatedOperation
{
    public class FacilitatedOperationMouseViewModel
    {
        #region Properties

        public static FacilitatedOperationMouseView FacilitatedOperationMouseView { private get; set; }

        #endregion

        public FacilitatedOperationMouseViewModel(FacilitatedOperationView facilitatedOperationView,
            FacilitatedOperationMouseView facilitatedOperationMouseView)
        {
            FacilitatedOperationMouseView = facilitatedOperationMouseView;
            facilitatedOperationView.FacilitatedOperationGrid.Children.Clear();
            facilitatedOperationView.FacilitatedOperationGrid.Children.Add(facilitatedOperationMouseView);
        }

        /// <summary>
        /// Gets the facilitated operation mouse view.
        /// </summary>
        /// <returns></returns>
        public static FacilitatedOperationMouseView GetFacilitatedOperationMouseView() => FacilitatedOperationMouseView;
    }
}
