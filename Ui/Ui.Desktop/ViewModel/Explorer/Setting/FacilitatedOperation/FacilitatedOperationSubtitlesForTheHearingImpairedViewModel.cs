﻿using Ui.Desktop.View.Setting.FacilitatedOperation;

namespace Ui.Desktop.ViewModel.Explorer.Setting.FacilitatedOperation
{
    public class FacilitatedOperationSubtitlesForTheHearingImpairedViewModel
    {
        #region Properties

        public static FacilitatedOperationSubtitlesForTheHearingImpairedView FacilitatedOperationSubtitlesForTheHearingImpairedView { private get; set; }

        #endregion

        public FacilitatedOperationSubtitlesForTheHearingImpairedViewModel(FacilitatedOperationView facilitatedOperationView,
            FacilitatedOperationSubtitlesForTheHearingImpairedView facilitatedOperationSubtitlesForTheHearingImpairedView)
        {
            FacilitatedOperationSubtitlesForTheHearingImpairedView = facilitatedOperationSubtitlesForTheHearingImpairedView;
            facilitatedOperationView.FacilitatedOperationGrid.Children.Clear();
            facilitatedOperationView.FacilitatedOperationGrid.Children.Add(facilitatedOperationSubtitlesForTheHearingImpairedView);
        }

        /// <summary>
        /// Gets the facilitated operation subtitles for the hearing impaired view.
        /// </summary>
        /// <returns></returns>
        public static FacilitatedOperationSubtitlesForTheHearingImpairedView GetFacilitatedOperationSubtitlesForTheHearingImpairedView() => FacilitatedOperationSubtitlesForTheHearingImpairedView;
    }
}
