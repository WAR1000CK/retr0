﻿using Ui.Desktop.View.Setting.FacilitatedOperation;

namespace Ui.Desktop.ViewModel.Explorer.Setting.FacilitatedOperation
{
    public class FacilitatedOperationAudioViewModel
    {
        #region Properties

        public static FacilitatedOperationAudioView FacilitatedOperationAudioView { private get; set; }

        #endregion

        public FacilitatedOperationAudioViewModel(FacilitatedOperationView facilitatedOperationView,
            FacilitatedOperationAudioView facilitatedOperationAudioView)
        {
            FacilitatedOperationAudioView = facilitatedOperationAudioView;
            facilitatedOperationView.FacilitatedOperationGrid.Children.Clear();
            facilitatedOperationView.FacilitatedOperationGrid.Children.Add(facilitatedOperationAudioView);
        }

        /// <summary>
        /// Gets the facilitated operation audio view.
        /// </summary>
        /// <returns></returns>
        public static FacilitatedOperationAudioView GetFacilitatedOperationAudioView() => FacilitatedOperationAudioView;
    }
}
