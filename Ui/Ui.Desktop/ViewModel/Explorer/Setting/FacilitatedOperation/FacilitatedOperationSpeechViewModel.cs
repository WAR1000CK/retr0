﻿using Ui.Desktop.View.Setting.FacilitatedOperation;

namespace Ui.Desktop.ViewModel.Explorer.Setting.FacilitatedOperation
{
    public class FacilitatedOperationSpeechViewModel
    {
        #region Properties

        public static FacilitatedOperationSpeechView FacilitatedOperationSpeechView { private get; set; }

        #endregion

        public FacilitatedOperationSpeechViewModel(FacilitatedOperationView facilitatedOperationView,
            FacilitatedOperationSpeechView facilitatedOperationSpeechView)
        {
            FacilitatedOperationSpeechView = facilitatedOperationSpeechView;
            facilitatedOperationView.FacilitatedOperationGrid.Children.Clear();
            facilitatedOperationView.FacilitatedOperationGrid.Children.Add(facilitatedOperationSpeechView);
        }

        /// <summary>
        /// Gets the facilitated operation speech view.
        /// </summary>
        /// <returns></returns>
        public static FacilitatedOperationSpeechView GetFacilitatedOperationSpeechView() => FacilitatedOperationSpeechView;
    }
}
