﻿using Ui.Desktop.View.Setting.FacilitatedOperation;

namespace Ui.Desktop.ViewModel.Explorer.Setting.FacilitatedOperation
{
    public class FacilitatedOperationCursorsAndPointersViewModel
    {
        #region Properties

        public static FacilitatedOperationCursorsAndPointersView FacilitatedOperationCursorsAndPointersView { private get; set; }

        #endregion

        public FacilitatedOperationCursorsAndPointersViewModel(FacilitatedOperationView facilitatedOperationView,
            FacilitatedOperationCursorsAndPointersView facilitatedOperationCursorsAndPointersView)
        {
            FacilitatedOperationCursorsAndPointersView = facilitatedOperationCursorsAndPointersView;
            facilitatedOperationView.FacilitatedOperationGrid.Children.Clear();
            facilitatedOperationView.FacilitatedOperationGrid.Children.Add(facilitatedOperationCursorsAndPointersView);
        }

        /// <summary>
        /// Gets the facilitated operation cursors and pointers view.
        /// </summary>
        /// <returns></returns>
        public static FacilitatedOperationCursorsAndPointersView GetFacilitatedOperationCursorsAndPointersView() => FacilitatedOperationCursorsAndPointersView;
    }
}
