﻿using Ui.Desktop.View.Setting.FacilitatedOperation;

namespace Ui.Desktop.ViewModel.Explorer.Setting.FacilitatedOperation
{
    public class FacilitatedOperationHighContrastViewModel
    {
        #region Properties

        public static FacilitatedOperationHighContrastView FacilitatedOperationHighContrastView { private get; set; }

        #endregion

        public FacilitatedOperationHighContrastViewModel(FacilitatedOperationView facilitatedOperationView,
            FacilitatedOperationHighContrastView facilitatedOperationHighContrastView)
        {
            FacilitatedOperationHighContrastView = facilitatedOperationHighContrastView;
            facilitatedOperationView.FacilitatedOperationGrid.Children.Clear();
            facilitatedOperationView.FacilitatedOperationGrid.Children.Add(facilitatedOperationHighContrastView);
        }

        /// <summary>
        /// Gets the facilitated operation high contrast view.
        /// </summary>
        /// <returns></returns>
        public static FacilitatedOperationHighContrastView GetFacilitatedOperationHighContrastView() => FacilitatedOperationHighContrastView;
    }
}
