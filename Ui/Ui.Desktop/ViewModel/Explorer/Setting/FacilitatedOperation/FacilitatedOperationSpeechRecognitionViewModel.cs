﻿using Ui.Desktop.View.Setting.FacilitatedOperation;

namespace Ui.Desktop.ViewModel.Explorer.Setting.FacilitatedOperation
{
    public class FacilitatedOperationSpeechRecognitionViewModel
    {
        #region Properties

        public static FacilitatedOperationSpeechRecognitionView FacilitatedOperationSpeechRecognitionView { private get; set; }

        #endregion

        public FacilitatedOperationSpeechRecognitionViewModel(FacilitatedOperationView facilitatedOperationView,
            FacilitatedOperationSpeechRecognitionView facilitatedOperationSpeechRecognitionView)
        {
            FacilitatedOperationSpeechRecognitionView = facilitatedOperationSpeechRecognitionView;
            facilitatedOperationView.FacilitatedOperationGrid.Children.Clear();
            facilitatedOperationView.FacilitatedOperationGrid.Children.Add(facilitatedOperationSpeechRecognitionView);
        }

        /// <summary>
        /// Gets the facilitated operation speech recognition view.
        /// </summary>
        /// <returns></returns>
        public static FacilitatedOperationSpeechRecognitionView GetFacilitatedOperationSpeechRecognitionView() => FacilitatedOperationSpeechRecognitionView;
    }
}
