﻿using Ui.Desktop.View.Setting.FacilitatedOperation;

namespace Ui.Desktop.ViewModel.Explorer.Setting.FacilitatedOperation
{
    public class FacilitatedOperationFilterViewModel
    {
        #region Properties

        public static FacilitatedOperationFilterView FacilitatedOperationFilterView { private get; set; }

        #endregion

        public FacilitatedOperationFilterViewModel(FacilitatedOperationView facilitatedOperationView,
            FacilitatedOperationFilterView facilitatedOperationFilterView)
        {
            FacilitatedOperationFilterView = facilitatedOperationFilterView;
            facilitatedOperationView.FacilitatedOperationGrid.Children.Clear();
            facilitatedOperationView.FacilitatedOperationGrid.Children.Add(facilitatedOperationFilterView);
        }

        /// <summary>
        /// Gets the facilitated operation filter view.
        /// </summary>
        /// <returns></returns>
        public static FacilitatedOperationFilterView GetFacilitatedOperationFilterView() => FacilitatedOperationFilterView;
    }
}
