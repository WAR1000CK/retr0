﻿using Ui.Desktop.View.Setting.FacilitatedOperation;

namespace Ui.Desktop.ViewModel.Explorer.Setting.FacilitatedOperation
{
    public class FacilitatedOperationKeyboardViewModel
    {
        #region Properties

        public static FacilitatedOperationKeyboardView FacilitatedOperationKeyboardView { private get; set; }

        #endregion

        public FacilitatedOperationKeyboardViewModel(FacilitatedOperationView facilitatedOperationView,
            FacilitatedOperationKeyboardView facilitatedOperationKeyboardView)
        {
            FacilitatedOperationKeyboardView = facilitatedOperationKeyboardView;
            facilitatedOperationView.FacilitatedOperationGrid.Children.Clear();
            facilitatedOperationView.FacilitatedOperationGrid.Children.Add(facilitatedOperationKeyboardView);
        }

        /// <summary>
        /// Gets the facilitated operation keyboard view.
        /// </summary>
        /// <returns></returns>
        public static FacilitatedOperationKeyboardView GetFacilitatedOperationKeyboardView() => FacilitatedOperationKeyboardView;
    }
}
