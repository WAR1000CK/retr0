﻿using Ui.Desktop.View.Setting.FacilitatedOperation;

namespace Ui.Desktop.ViewModel.Setting.FacilitatedOperation
{
    public class FacilitatedOperationViewModel
    {
        #region Commands

        public RelayCommand CommandDisplay { get; private set; }
        public RelayCommand CommandCursorAndPointer { get; private set; }
        public RelayCommand CommandMagnifier { get; private set; }
        public RelayCommand CommandFilter { get; private set; }
        public RelayCommand CommandHighContrast { get; private set; }
        public RelayCommand CommandSpeech { get; private set; }
        public RelayCommand CommandAudio { get; private set; }
        public RelayCommand CommandSubtitlesForTheHearingImpaired { get; private set; }
        public RelayCommand CommandSpeechRecognition { get; private set; }
        public RelayCommand CommandKeyboard { get; private set; }
        public RelayCommand CommandMouse { get; private set; }
        public RelayCommand CommandEyeControl { get; private set; }
        public RelayCommand CommandCloseWindow { get; private set; }

        #endregion

        #region Private fields

        private readonly MainWindow _mainWindow;
        private static FacilitatedOperationView _facilitatedOperationView;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="FacilitatedOperationViewModel"/> class.
        /// </summary>
        /// <param name="mainWindow">The main window.</param>
        /// <param name="view">The view.</param>
        public FacilitatedOperationViewModel(MainWindow mainWindow, FacilitatedOperationView facilitatedOperationView)
        {
            _mainWindow = mainWindow;
            _facilitatedOperationView = facilitatedOperationView;
            mainWindow.MainGrid.Children.Add(facilitatedOperationView);

            #region Commands

            // Initialize commands and run when the button is pressed
            CommandDisplay = new RelayCommand(Display);
            CommandCursorAndPointer = new RelayCommand(CursorAndPointer);
            CommandMagnifier = new RelayCommand(Magnifier);
            CommandFilter = new RelayCommand(Filter);
            CommandHighContrast = new RelayCommand(HighContrast);
            CommandSpeech = new RelayCommand(Speech);
            CommandAudio = new RelayCommand(Audio);
            CommandSubtitlesForTheHearingImpaired = new RelayCommand(SubtitlesForTheHearingImpaired);
            CommandSpeechRecognition = new RelayCommand(SpeechRecognition);
            CommandKeyboard = new RelayCommand(Keyboard);
            CommandMouse = new RelayCommand(Mouse);
            CommandEyeControl = new RelayCommand(EyeControl);
            CommandCloseWindow = new RelayCommand(CloseWindow);

            #endregion

            Display();
        }

        /// <summary>
        /// Gets the facilitated operationview.
        /// </summary>
        /// <returns></returns>
        public static FacilitatedOperationView GetFacilitatedOperationView() => _facilitatedOperationView;

        #region Command Functions

        /// <summary>
        /// Creates an instance of <see cref="FacilitatedOperationDisplayView"/>
        /// </summary>
        private void Display() => new FacilitatedOperationDisplayView(GetFacilitatedOperationView());

        /// <summary>
        /// Creates an instance of <see cref="FacilitatedOperationCursorsAndPointersView"/>
        /// </summary>
        private void CursorAndPointer() => new FacilitatedOperationCursorsAndPointersView(GetFacilitatedOperationView());

        /// <summary>
        /// Creates an instance of <see cref="FacilitatedOperationMagnifierView"/>
        /// </summary>
        private void Magnifier() => new FacilitatedOperationMagnifierView(GetFacilitatedOperationView());

        /// <summary>
        /// Creates an instance of <see cref="FacilitatedOperationFilterView"/>
        /// </summary>
        private void Filter() => new FacilitatedOperationFilterView(GetFacilitatedOperationView());

        /// <summary>
        /// Creates an instance of <see cref="FacilitatedOperationHighContrastView"/>
        /// </summary>
        private void HighContrast() => new FacilitatedOperationHighContrastView(GetFacilitatedOperationView());

        /// <summary>
        /// Creates an instance of <see cref="FacilitatedOperationSpeechView"/>
        /// </summary>
        private void Speech() => new FacilitatedOperationSpeechView(GetFacilitatedOperationView());

        /// <summary>
        /// Creates an instance of <see cref="FacilitatedOperationAudioView"/>
        /// </summary>
        private void Audio() => new FacilitatedOperationAudioView(GetFacilitatedOperationView());

        /// <summary>
        /// Creates an instance of <see cref="FacilitatedOperationSubtitlesForTheHearingImpairedView"/>
        /// </summary>
        private void SubtitlesForTheHearingImpaired() => new FacilitatedOperationSubtitlesForTheHearingImpairedView(GetFacilitatedOperationView());

        /// <summary>
        /// Creates an instance of <see cref="FacilitatedOperationSpeechRecognitionView"/>
        /// </summary>
        private void SpeechRecognition() => new FacilitatedOperationSpeechRecognitionView(GetFacilitatedOperationView());

        /// <summary>
        /// Creates an instance of <see cref="FacilitatedOperationKeyboardView"/>
        /// </summary>
        private void Keyboard() => new FacilitatedOperationKeyboardView(GetFacilitatedOperationView());

        /// <summary>
        /// Creates an instance of <see cref="FacilitatedOperationMouseView"/>
        /// </summary>
        private void Mouse() => new FacilitatedOperationMouseView(GetFacilitatedOperationView());

        /// <summary>
        /// Creates an instance of <see cref="FacilitatedOperationEyeControlView"/>
        /// </summary>
        private void EyeControl() => new FacilitatedOperationEyeControlView(GetFacilitatedOperationView());

        /// <summary>
        /// Removes the <see cref="View"/> from <see cref="mainWindow.MainGrid"/>
        /// </summary>
        private void CloseWindow() => _mainWindow.MainGrid.Children.Remove(GetFacilitatedOperationView());

        #endregion
    }
}
