﻿using Ui.Desktop.View.Setting.Privacy;

namespace Ui.Desktop.ViewModel.Setting.Privacy
{
    class PrivacyVideosViewModel
    {
        #region Properties

        public PrivacyView PrivacyView { get; private set; }
        public PrivacyVideosView PrivacyVideosView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="PrivacyVideosViewModel"/> class.
        /// </summary>
        /// <param name="privacyView">The privacy view.</param>
        /// <param name="privacyVideosView">The privacy videos view.</param>
        public PrivacyVideosViewModel(PrivacyView privacyView, PrivacyVideosView privacyVideosView)
        {
            PrivacyView = privacyView;
            PrivacyVideosView = privacyVideosView;
            privacyView.PrivacyGrid.Children.Clear();
            privacyView.PrivacyGrid.Children.Add(privacyVideosView);
        }
    }
}
