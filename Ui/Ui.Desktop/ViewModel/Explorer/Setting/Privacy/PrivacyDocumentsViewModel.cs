﻿using Ui.Desktop.View.Setting.Privacy;

namespace Ui.Desktop.ViewModel.Setting.Privacy
{
    class PrivacyDocumentsViewModel
    {
        #region Properties

        public PrivacyView PrivacyView { get; private set; }
        public PrivacyDocumentsView PrivacyDocumentsView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="PrivacyDocumentsViewModel"/> class.
        /// </summary>
        /// <param name="privacyView">The privacy view.</param>
        /// <param name="privacyDocumentsView">The privacy documents view.</param>
        public PrivacyDocumentsViewModel(PrivacyView privacyView,
            PrivacyDocumentsView privacyDocumentsView)
        {
            PrivacyView = privacyView;
            PrivacyDocumentsView = privacyDocumentsView;
            privacyView.PrivacyGrid.Children.Clear();
            privacyView.PrivacyGrid.Children.Add(privacyDocumentsView);
        }
    }
}
