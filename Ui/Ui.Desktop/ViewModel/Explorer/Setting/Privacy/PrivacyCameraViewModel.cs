﻿using Ui.Desktop.View.Setting.Privacy;

namespace Ui.Desktop.ViewModel.Setting.Privacy
{
    class PrivacyCameraViewModel
    {
        #region Properties

        public PrivacyView PrivacyView { get; private set; }
        public PrivacyCameraView PrivacyCameraView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="PrivacyCameraViewModel"/> class.
        /// </summary>
        /// <param name="privacyView">The privacy view.</param>
        /// <param name="privacyCameraView">The privacy camera view.</param>
        public PrivacyCameraViewModel(PrivacyView privacyView, PrivacyCameraView privacyCameraView)
        {
            PrivacyView = privacyView;
            PrivacyCameraView = privacyCameraView;
            privacyView.PrivacyGrid.Children.Clear();
            privacyView.PrivacyGrid.Children.Add(privacyCameraView);
        }
    }
}
