﻿using Ui.Desktop.View.Setting.Privacy;

namespace Ui.Desktop.ViewModel.Setting.Privacy
{
    class PrivacyPhoneCallsViewModel
    {
        #region Properties

        public PrivacyView PrivacyView { get; private set; }
        public PrivacyPhoneCallsView PrivacyPhoneCallsView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="PrivacyPhoneCallsViewModel"/> class.
        /// </summary>
        /// <param name="privacyView">The privacy view.</param>
        /// <param name="privacyPhoneCallsView">The privacy phone calls view.</param>
        public PrivacyPhoneCallsViewModel(PrivacyView privacyView, PrivacyPhoneCallsView privacyPhoneCallsView)
        {
            PrivacyView = privacyView;
            PrivacyPhoneCallsView = privacyPhoneCallsView;
            privacyView.PrivacyGrid.Children.Clear();
            privacyView.PrivacyGrid.Children.Add(privacyPhoneCallsView);
        }
    }
}
