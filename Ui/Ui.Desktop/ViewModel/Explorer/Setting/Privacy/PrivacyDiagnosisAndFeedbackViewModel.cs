﻿using Ui.Desktop.View.Setting.Privacy;

namespace Ui.Desktop.ViewModel.Setting.Privacy
{
    class PrivacyDiagnosisAndFeedbackViewModel
    {
        #region Properties

        public PrivacyView PrivacyView { get; private set; }
        public PrivacyDiagnosisAndFeedbackView PrivacyDiagnosisAndFeedbackView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="PrivacyDiagnosisAndFeedbackViewModel"/> class.
        /// </summary>
        /// <param name="privacyView">The privacy view.</param>
        /// <param name="privacyDiagnosisAndFeedbackView">The privacy diagnosis and feedback view.</param>
        public PrivacyDiagnosisAndFeedbackViewModel(PrivacyView privacyView,
            PrivacyDiagnosisAndFeedbackView privacyDiagnosisAndFeedbackView)
        {
            PrivacyView = privacyView;
            PrivacyDiagnosisAndFeedbackView = privacyDiagnosisAndFeedbackView;
            privacyView.PrivacyGrid.Children.Clear();
            privacyView.PrivacyGrid.Children.Add(privacyDiagnosisAndFeedbackView);
        }
    }
}
