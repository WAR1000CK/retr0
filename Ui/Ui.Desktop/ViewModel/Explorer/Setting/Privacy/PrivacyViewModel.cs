﻿using Ui.Desktop.View.Setting.Privacy;

namespace Ui.Desktop.ViewModel.Setting.Privacy
{
    public class PrivacyViewModel
    {
        #region Commands

        public RelayCommand CommandGeneral { get; private set; }
        public RelayCommand CommandSpeechRecognition { get; private set; }
        public RelayCommand CommandInkAndInputAdjustment { get; private set; }
        public RelayCommand CommandDiagnosisAndFeedback { get; private set; }
        public RelayCommand CommandActivityHistory { get; private set; }
        public RelayCommand CommandPosition { get; private set; }
        public RelayCommand CommandCamera { get; private set; }
        public RelayCommand CommandVoiceActivation { get; private set; }
        public RelayCommand CommandNotifications { get; private set; }
        public RelayCommand CommandAccount { get; private set; }
        public RelayCommand CommandContacts { get; private set; }
        public RelayCommand CommandCalendar { get; private set; }
        public RelayCommand CommandPhoneCalls { get; private set; }
        public RelayCommand CommandCall { get; private set; }
        public RelayCommand CommandEmail { get; private set; }
        public RelayCommand CommandTasks { get; private set; }
        public RelayCommand CommandMessaging { get; private set; }
        public RelayCommand CommandWireless { get; private set; }
        public RelayCommand CommandOtherDevices { get; private set; }
        public RelayCommand CommandBackgroundApps { get; private set; }
        public RelayCommand CommandAppDiagnostics { get; private set; }
        public RelayCommand CommandAutomaticFileDownloads { get; private set; }
        public RelayCommand CommandDocuments { get; private set; }
        public RelayCommand CommandPictures { get; private set; }
        public RelayCommand CommandVideos { get; private set; }
        public RelayCommand CommandFilesystem { get; private set; }
        public RelayCommand CommandCloseWindow { get; private set; }

        #endregion

        #region Private fields

        private readonly MainWindow _mainWindow;
        private static PrivacyView _privacyView;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="PrivacyViewModel"/> class.
        /// </summary>
        /// <param name="mainWindow">The main window.</param>
        /// <param name="view">The view.</param>
        public PrivacyViewModel(MainWindow mainWindow, PrivacyView privacyView)
        {
            _mainWindow = mainWindow;
            _privacyView = privacyView;
            mainWindow.MainGrid.Children.Add(privacyView);

            #region Commands

            // Initialize commands and run when the button is pressed
            CommandGeneral = new RelayCommand(General);
            CommandSpeechRecognition = new RelayCommand(SpeechRecognition);
            CommandInkAndInputAdjustment = new RelayCommand(InkAndInputAdjustment);
            CommandDiagnosisAndFeedback = new RelayCommand(DiagnosisAndFeedback);
            CommandActivityHistory = new RelayCommand(ActivityHistory);
            CommandPosition = new RelayCommand(Position);
            CommandCamera = new RelayCommand(Camera);
            CommandVoiceActivation = new RelayCommand(VoiceActivation);
            CommandNotifications = new RelayCommand(Notifications);
            CommandAccount = new RelayCommand(Account);
            CommandContacts = new RelayCommand(Contacts);
            CommandCalendar = new RelayCommand(Calendar);
            CommandPhoneCalls = new RelayCommand(PhoneCalls);
            CommandCall = new RelayCommand(Call);
            CommandEmail = new RelayCommand(Email);
            CommandTasks = new RelayCommand(Tasks);
            CommandMessaging = new RelayCommand(Messaging);
            CommandWireless = new RelayCommand(Wireless);
            CommandOtherDevices = new RelayCommand(OtherDevices);
            CommandBackgroundApps = new RelayCommand(BackgroundApps);
            CommandAppDiagnostics = new RelayCommand(AppDiagnostics);
            CommandAutomaticFileDownloads = new RelayCommand(AutomaticFileDownloads);
            CommandDocuments = new RelayCommand(Documents);
            CommandPictures = new RelayCommand(Pictures);
            CommandVideos = new RelayCommand(Videos);
            CommandFilesystem = new RelayCommand(Filesystem);
            CommandCloseWindow = new RelayCommand(CloseWindow);

            #endregion

            General();
        }

        /// <summary>
        /// Gets the privacy view.
        /// </summary>
        /// <returns></returns>
        public static PrivacyView GetPrivacyView() => _privacyView;

        #region Command Functions

        /// <summary>
        /// Creates an instance of <see cref="PrivacyGeneralView"/>
        /// </summary>
        private void General() => new PrivacyGeneralView(GetPrivacyView());

        /// <summary>
        /// Creates an instance of <see cref="PrivacySpeechRecognitionView"/>
        /// </summary>
        private void SpeechRecognition() => new PrivacySpeechRecognitionView(GetPrivacyView());

        /// <summary>
        /// Creates an instance of <see cref="PrivacyInkAndInputAdjustmentView"/>
        /// </summary>
        private void InkAndInputAdjustment() => new PrivacyInkAndInputAdjustmentView(GetPrivacyView());

        /// <summary>
        /// Creates an instance of <see cref="PrivacyDiagnosisAndFeedbackView"/>
        /// </summary>
        private void DiagnosisAndFeedback() => new PrivacyDiagnosisAndFeedbackView(GetPrivacyView());

        /// <summary>
        /// Creates an instance of <see cref="PrivacyActivityHistoryView"/>
        /// </summary>
        private void ActivityHistory() => new PrivacyActivityHistoryView(GetPrivacyView());

        /// <summary>
        /// Creates an instance of <see cref="PrivacyPositionView"/>
        /// </summary>
        private void Position() => new PrivacyPositionView(GetPrivacyView());

        /// <summary>
        /// Creates an instance of <see cref="PrivacyCameraView"/>
        /// </summary>
        private void Camera() => new PrivacyCameraView(GetPrivacyView());

        /// <summary>
        /// Creates an instance of <see cref="PrivacyVoiceActivationView"/>
        /// </summary>
        private void VoiceActivation() => new PrivacyVoiceActivationView(GetPrivacyView());

        /// <summary>
        /// Creates an instance of <see cref="PrivacyNotificationsView"/>
        /// </summary>
        private void Notifications() => new PrivacyNotificationsView(GetPrivacyView());

        /// <summary>
        /// Creates an instance of <see cref="PrivacyAccountView"/>
        /// </summary>
        private void Account() => new PrivacyAccountView(GetPrivacyView());

        /// <summary>
        /// Creates an instance of <see cref="PrivacyContactsView"/>
        /// </summary>
        private void Contacts() => new PrivacyContactsView(GetPrivacyView());

        /// <summary>
        /// Creates an instance of <see cref="PrivacyCalendarView"/>
        /// </summary>
        private void Calendar() => new PrivacyCalendarView(GetPrivacyView());

        /// <summary>
        /// Creates an instance of <see cref="PrivacyPhoneCallsView"/>
        /// </summary>
        private void PhoneCalls() => new PrivacyPhoneCallsView(GetPrivacyView());

        /// <summary>
        /// Creates an instance of <see cref="PrivacyCallView"/>
        /// </summary>
        private void Call() => new PrivacyCallView(GetPrivacyView());

        /// <summary>
        /// Creates an instance of <see cref="PrivacyEmailView"/>
        /// </summary>
        private void Email() => new PrivacyEmailView(GetPrivacyView());

        /// <summary>
        /// Creates an instance of <see cref="PrivacyTasksView"/>
        /// </summary>
        private void Tasks() => new PrivacyTasksView(GetPrivacyView());

        /// <summary>
        /// Creates an instance of <see cref="PrivacyMessagingView"/>
        /// </summary>
        private void Messaging() => new PrivacyMessagingView(GetPrivacyView());

        /// <summary>
        /// Creates an instance of <see cref="PrivacyWirelessView"/>
        /// </summary>
        private void Wireless() => new PrivacyWirelessView(GetPrivacyView());

        /// <summary>
        /// Creates an instance of <see cref="PrivacyOtherDevicesView"/>
        /// </summary>
        private void OtherDevices() => new PrivacyOtherDevicesView(GetPrivacyView());

        /// <summary>
        /// Creates an instance of <see cref="PrivacyBackgroundAppsView"/>
        /// </summary>
        private void BackgroundApps() => new PrivacyBackgroundAppsView(GetPrivacyView());

        /// <summary>
        /// Creates an instance of <see cref="PrivacyAppDiagnosticsView"/>
        /// </summary>
        private void AppDiagnostics() => new PrivacyAppDiagnosticsView(GetPrivacyView());

        /// <summary>
        /// Creates an instance of <see cref="PrivacyAutomaticFileDownloadsView"/>
        /// </summary>
        private void AutomaticFileDownloads() => new PrivacyAutomaticFileDownloadsView(GetPrivacyView());

        /// <summary>
        /// Creates an instance of <see cref="PrivacyDocumentsView"/>
        /// </summary>
        private void Documents() => new PrivacyDocumentsView(GetPrivacyView());

        /// <summary>
        /// Creates an instance of <see cref="PrivacyPicturesView"/>
        /// </summary>
        private void Pictures() => new PrivacyPicturesView(GetPrivacyView());

        /// <summary>
        /// Creates an instance of <see cref="PrivacyVideosView"/>
        /// </summary>
        private void Videos() => new PrivacyVideosView(GetPrivacyView());

        /// <summary>
        /// Creates an instance of <see cref="PrivacyFilesystemView"/>
        /// </summary>
        private void Filesystem() => new PrivacyFilesystemView(GetPrivacyView());

        /// <summary>
        /// Removes the <see cref="View"/> from <see cref="Main.MainGrid"/>
        /// </summary>
        private void CloseWindow() => _mainWindow.MainGrid.Children.Remove(GetPrivacyView());

        #endregion
    }
}
