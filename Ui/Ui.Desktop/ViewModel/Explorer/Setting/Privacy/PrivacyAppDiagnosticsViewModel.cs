﻿using Ui.Desktop.View.Setting.Privacy;

namespace Ui.Desktop.ViewModel.Setting.Privacy
{
    class PrivacyAppDiagnosticsViewModel
    {
        #region Properties

        public PrivacyView PrivacyView { get; private set; }
        public PrivacyAppDiagnosticsView PrivacyAppDiagnosticsView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="PrivacyAppDiagnosticsViewModel"/> class.
        /// </summary>
        /// <param name="privacyView">The privacy view.</param>
        /// <param name="privacyAppDiagnosticsView">The privacy application diagnostics view.</param>
        public PrivacyAppDiagnosticsViewModel(PrivacyView privacyView,
            PrivacyAppDiagnosticsView privacyAppDiagnosticsView)
        {
            PrivacyView = privacyView;
            PrivacyAppDiagnosticsView = privacyAppDiagnosticsView;
            privacyView.PrivacyGrid.Children.Clear();
            privacyView.PrivacyGrid.Children.Add(privacyAppDiagnosticsView);
        }
    }
}
