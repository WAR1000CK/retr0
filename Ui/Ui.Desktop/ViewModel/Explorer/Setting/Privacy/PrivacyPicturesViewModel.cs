﻿using Ui.Desktop.View.Setting.Privacy;

namespace Ui.Desktop.ViewModel.Setting.Privacy
{
    class PrivacyPicturesViewModel
    {
        #region Properties

        public PrivacyView PrivacyView { get; private set; }
        public PrivacyPicturesView PrivacyPicturesView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="PrivacyPicturesViewModel"/> class.
        /// </summary>
        /// <param name="privacyView">The privacy view.</param>
        /// <param name="privacyPicturesView">The privacy pictures view.</param>
        public PrivacyPicturesViewModel(PrivacyView privacyView, PrivacyPicturesView privacyPicturesView)
        {
            PrivacyView = privacyView;
            PrivacyPicturesView = privacyPicturesView;
            privacyView.PrivacyGrid.Children.Clear();
            privacyView.PrivacyGrid.Children.Add(privacyPicturesView);
        }
    }
}
