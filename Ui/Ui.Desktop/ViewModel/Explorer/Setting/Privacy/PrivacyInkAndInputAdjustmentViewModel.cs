﻿using Ui.Desktop.View.Setting.Privacy;

namespace Ui.Desktop.ViewModel.Setting.Privacy
{
    class PrivacyInkAndInputAdjustmentViewModel
    {
        #region Properties

        public PrivacyView PrivacyView { get; private set; }
        public PrivacyInkAndInputAdjustmentView PrivacyInkAndInputAdjustmentView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="PrivacyInkAndInputAdjustmentViewModel"/> class.
        /// </summary>
        /// <param name="privacyView">The privacy view.</param>
        /// <param name="privacyInkAndInputAdjustmentView">The privacy ink and input adjustment view.</param>
        public PrivacyInkAndInputAdjustmentViewModel(PrivacyView privacyView,
            PrivacyInkAndInputAdjustmentView privacyInkAndInputAdjustmentView)
        {
            PrivacyView = privacyView;
            PrivacyInkAndInputAdjustmentView = privacyInkAndInputAdjustmentView;
            privacyView.PrivacyGrid.Children.Clear();
            privacyView.PrivacyGrid.Children.Add(privacyInkAndInputAdjustmentView);
        }
    }
}
