﻿using Ui.Desktop.View.Setting.Privacy;

namespace Ui.Desktop.ViewModel.Setting.Privacy
{
    class PrivacyCalendarViewModel
    {
        #region Properties

        public PrivacyView PrivacyView { get; private set; }
        public PrivacyCalendarView PrivacyCalendarView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="PrivacyCalendarViewModel"/> class.
        /// </summary>
        /// <param name="privacyView">The privacy view.</param>
        /// <param name="privacyCalendarView">The privacy calendar view.</param>
        public PrivacyCalendarViewModel(PrivacyView privacyView,
            PrivacyCalendarView privacyCalendarView)
        {
            PrivacyView = privacyView;
            PrivacyCalendarView = privacyCalendarView;
            privacyView.PrivacyGrid.Children.Clear();
            privacyView.PrivacyGrid.Children.Add(privacyCalendarView);
        }
    }
}
