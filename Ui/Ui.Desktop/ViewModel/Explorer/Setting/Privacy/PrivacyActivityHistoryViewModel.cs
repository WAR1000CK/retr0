﻿using Ui.Desktop.View.Setting.Privacy;

namespace Ui.Desktop.ViewModel.Setting.Privacy
{
    class PrivacyActivityHistoryViewModel
    {
        #region Properties

        public PrivacyView PrivacyView { get; private set; }
        public PrivacyActivityHistoryView PrivacyActivityHistoryView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="PrivacyActivityHistoryViewModel"/> class.
        /// </summary>
        /// <param name="privacyView">The privacy view.</param>
        /// <param name="privacyActivityHistoryView">The privacy activity history view.</param>
        public PrivacyActivityHistoryViewModel(PrivacyView privacyView,
            PrivacyActivityHistoryView privacyActivityHistoryView)
        {
            PrivacyView = privacyView;
            PrivacyActivityHistoryView = privacyActivityHistoryView;
            privacyView.PrivacyGrid.Children.Clear();
            privacyView.PrivacyGrid.Children.Add(privacyActivityHistoryView);
        }
    }
}
