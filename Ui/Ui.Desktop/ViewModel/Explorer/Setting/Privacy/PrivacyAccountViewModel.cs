﻿using Ui.Desktop.View.Setting.Privacy;

namespace Ui.Desktop.ViewModel.Setting.Privacy
{
    class PrivacyAccountViewModel
    {
        #region Properties

        public PrivacyView PrivacyView { get; private set; }
        public PrivacyAccountView PrivacyAccountView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="PrivacyAccountViewModel"/> class.
        /// </summary>
        /// <param name="privacyView">The privacy view.</param>
        /// <param name="privacyAccountView">The privacy account view.</param>
        public PrivacyAccountViewModel(PrivacyView privacyView,
            PrivacyAccountView privacyAccountView)
        {
            PrivacyView = privacyView;
            PrivacyAccountView = privacyAccountView;
            privacyView.PrivacyGrid.Children.Clear();
            privacyView.PrivacyGrid.Children.Add(privacyAccountView);
        }
    }
}
