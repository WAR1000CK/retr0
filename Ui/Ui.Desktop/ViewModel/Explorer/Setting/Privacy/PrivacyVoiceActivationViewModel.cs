﻿using Ui.Desktop.View.Setting.Privacy;

namespace Ui.Desktop.ViewModel.Setting.Privacy
{
    class PrivacyVoiceActivationViewModel
    {
        #region Properties

        public PrivacyView PrivacyView { get; private set; }
        public PrivacyVoiceActivationView PrivacyVoiceActivationView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="PrivacyVoiceActivationViewModel"/> class.
        /// </summary>
        /// <param name="privacyView">The privacy view.</param>
        /// <param name="privacyVoiceActivationView">The privacy voice activation view.</param>
        public PrivacyVoiceActivationViewModel(PrivacyView privacyView, PrivacyVoiceActivationView privacyVoiceActivationView)
        {
            PrivacyView = privacyView;
            PrivacyVoiceActivationView = privacyVoiceActivationView;
            privacyView.PrivacyGrid.Children.Clear();
            privacyView.PrivacyGrid.Children.Add(privacyVoiceActivationView);
        }
    }
}
