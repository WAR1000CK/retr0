﻿using Ui.Desktop.View.Setting.Privacy;

namespace Ui.Desktop.ViewModel.Setting.Privacy
{
    class PrivacyAutomaticFileDownloadsViewModel
    {
        #region Properties

        public PrivacyView PrivacyView { get; private set; }
        public PrivacyAutomaticFileDownloadsView PrivacyAutomaticFileDownloadsView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="PrivacyAutomaticFileDownloadsViewModel"/> class.
        /// </summary>
        /// <param name="privacyView">The privacy view.</param>
        /// <param name="privacyAutomaticFileDownloadsView">The privacy automatic file downloads view.</param>
        public PrivacyAutomaticFileDownloadsViewModel(PrivacyView privacyView,
            PrivacyAutomaticFileDownloadsView privacyAutomaticFileDownloadsView)
        {
            PrivacyView = privacyView;
            PrivacyAutomaticFileDownloadsView = privacyAutomaticFileDownloadsView;
            privacyView.PrivacyGrid.Children.Clear();
            privacyView.PrivacyGrid.Children.Add(privacyAutomaticFileDownloadsView);
        }
    }
}
