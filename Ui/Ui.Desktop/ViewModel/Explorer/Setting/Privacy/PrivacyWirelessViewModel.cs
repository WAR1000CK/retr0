﻿using Ui.Desktop.View.Setting.Privacy;

namespace Ui.Desktop.ViewModel.Setting.Privacy
{
    class PrivacyWirelessViewModel
    {
        #region Properties

        public PrivacyView PrivacyView { get; private set; }
        public PrivacyWirelessView PrivacyWirelessView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="PrivacyWirelessViewModel"/> class.
        /// </summary>
        /// <param name="privacyView">The privacy view.</param>
        /// <param name="privacyWirelessView">The privacy wireless view.</param>
        public PrivacyWirelessViewModel(PrivacyView privacyView, PrivacyWirelessView privacyWirelessView)
        {
            PrivacyView = privacyView;
            PrivacyWirelessView = privacyWirelessView;
            privacyView.PrivacyGrid.Children.Clear();
            privacyView.PrivacyGrid.Children.Add(privacyWirelessView);
        }
    }
}
