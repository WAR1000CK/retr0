﻿using Ui.Desktop.View.Setting.Privacy;

namespace Ui.Desktop.ViewModel.Setting.Privacy
{
    class PrivacyContactsViewModel
    {
        #region Properties

        public PrivacyView PrivacyView { get; private set; }
        public PrivacyContactsView PrivacyContactsView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="PrivacyContactsViewModel"/> class.
        /// </summary>
        /// <param name="privacyView">The privacy view.</param>
        /// <param name="privacyContactsView">The privacy contacts view.</param>
        public PrivacyContactsViewModel(PrivacyView privacyView, PrivacyContactsView privacyContactsView)
        {
            PrivacyView = privacyView;
            PrivacyContactsView = privacyContactsView;
            privacyView.PrivacyGrid.Children.Clear();
            privacyView.PrivacyGrid.Children.Add(privacyContactsView);
        }
    }
}
