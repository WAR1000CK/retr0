﻿using Ui.Desktop.View.Setting.Privacy;

namespace Ui.Desktop.ViewModel.Setting.Privacy
{
    class PrivacyPositionViewModel
    {
        #region Properties

        public PrivacyView PrivacyView { get; private set; }
        public PrivacyPositionView PrivacyPositionView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="PrivacyPositionViewModel"/> class.
        /// </summary>
        /// <param name="privacyView">The privacy view.</param>
        /// <param name="privacyPositionView">The privacy position view.</param>
        public PrivacyPositionViewModel(PrivacyView privacyView, PrivacyPositionView privacyPositionView)
        {
            PrivacyView = privacyView;
            PrivacyPositionView = privacyPositionView;
            privacyView.PrivacyGrid.Children.Clear();
            privacyView.PrivacyGrid.Children.Add(privacyPositionView);
        }
    }
}
