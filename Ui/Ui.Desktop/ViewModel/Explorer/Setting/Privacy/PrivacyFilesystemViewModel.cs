﻿using Ui.Desktop.View.Setting.Privacy;

namespace Ui.Desktop.ViewModel.Setting.Privacy
{
    class PrivacyFilesystemViewModel
    {
        #region Properties

        public PrivacyView PrivacyView { get; private set; }
        public PrivacyFilesystemView PrivacyFilesystemView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="PrivacyFilesystemViewModel"/> class.
        /// </summary>
        /// <param name="privacyView">The privacy view.</param>
        /// <param name="privacyFilesystemView">The privacy filesystem view.</param>
        public PrivacyFilesystemViewModel(PrivacyView privacyView, PrivacyFilesystemView privacyFilesystemView)
        {
            PrivacyView = privacyView;
            PrivacyFilesystemView = privacyFilesystemView;
            privacyView.PrivacyGrid.Children.Clear();
            privacyView.PrivacyGrid.Children.Add(privacyFilesystemView);
        }
    }
}
