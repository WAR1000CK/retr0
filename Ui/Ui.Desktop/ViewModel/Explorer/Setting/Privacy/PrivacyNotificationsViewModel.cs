﻿using Ui.Desktop.View.Setting.Privacy;

namespace Ui.Desktop.ViewModel.Setting.Privacy
{
    class PrivacyNotificationsViewModel
    {
        #region Properties

        public PrivacyView PrivacyView { get; private set; }
        public PrivacyNotificationsView PrivacyNotificationsView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="PrivacyNotificationsViewModel"/> class.
        /// </summary>
        /// <param name="privacyView">The privacy view.</param>
        /// <param name="privacyNotificationsView">The privacy notifications view.</param>
        public PrivacyNotificationsViewModel(PrivacyView privacyView, PrivacyNotificationsView privacyNotificationsView)
        {
            PrivacyView = privacyView;
            PrivacyNotificationsView = privacyNotificationsView;
            privacyView.PrivacyGrid.Children.Clear();
            privacyView.PrivacyGrid.Children.Add(privacyNotificationsView);
        }
    }
}
