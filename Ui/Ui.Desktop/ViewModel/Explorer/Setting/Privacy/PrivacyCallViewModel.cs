﻿using Ui.Desktop.View.Setting.Privacy;

namespace Ui.Desktop.ViewModel.Setting.Privacy
{
    class PrivacyCallViewModel
    {
        #region Properties

        public PrivacyView PrivacyView { get; private set; }
        public PrivacyCallView PrivacyCallView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="PrivacyCallViewModel"/> class.
        /// </summary>
        /// <param name="privacyView">The privacy view.</param>
        /// <param name="privacyCallView">The privacy call view.</param>
        public PrivacyCallViewModel(PrivacyView privacyView, PrivacyCallView privacyCallView)
        {
            PrivacyView = privacyView;
            PrivacyCallView = privacyCallView;
            privacyView.PrivacyGrid.Children.Clear();
            privacyView.PrivacyGrid.Children.Add(privacyCallView);
        }
    }
}
