﻿using Ui.Desktop.View.Setting.Privacy;

namespace Ui.Desktop.ViewModel.Setting.Privacy
{
    class PrivacyOtherDevicesViewModel
    {
        #region Properties

        public PrivacyView PrivacyView { get; private set; }
        public PrivacyOtherDevicesView PrivacyOtherDevicesView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="PrivacyOtherDevicesViewModel"/> class.
        /// </summary>
        /// <param name="privacyView">The privacy view.</param>
        /// <param name="privacyOtherDevicesView">The privacy other devices view.</param>
        public PrivacyOtherDevicesViewModel(PrivacyView privacyView, PrivacyOtherDevicesView privacyOtherDevicesView)
        {
            PrivacyView = privacyView;
            PrivacyOtherDevicesView = privacyOtherDevicesView;
            privacyView.PrivacyGrid.Children.Clear();
            privacyView.PrivacyGrid.Children.Add(privacyOtherDevicesView);
        }
    }
}
