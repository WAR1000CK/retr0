﻿using Ui.Desktop.View.Setting.Privacy;

namespace Ui.Desktop.ViewModel.Setting.Privacy
{
    class PrivacyTasksViewModel
    {
        #region Properties

        public PrivacyView PrivacyView { get; private set; }
        public PrivacyTasksView PrivacyTasksView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="PrivacyTasksViewModel"/> class.
        /// </summary>
        /// <param name="privacyView">The privacy view.</param>
        /// <param name="privacyTasksView">The privacy tasks view.</param>
        public PrivacyTasksViewModel(PrivacyView privacyView, PrivacyTasksView privacyTasksView)
        {
            PrivacyView = privacyView;
            PrivacyTasksView = privacyTasksView;
            privacyView.PrivacyGrid.Children.Clear();
            privacyView.PrivacyGrid.Children.Add(privacyTasksView);
        }
    }
}
