﻿using Ui.Desktop.View.Setting.Privacy;

namespace Ui.Desktop.ViewModel.Setting.Privacy
{
    class PrivacySpeechRecognitionViewModel
    {
        #region Properties

        public PrivacyView PrivacyView { get; private set; }
        public PrivacySpeechRecognitionView PrivacySpeechRecognitionView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="PrivacySpeechRecognitionViewModel"/> class.
        /// </summary>
        /// <param name="privacyView">The privacy view.</param>
        /// <param name="privacySpeechRecognitionView">The privacy speech recognition view.</param>
        public PrivacySpeechRecognitionViewModel(PrivacyView privacyView, PrivacySpeechRecognitionView privacySpeechRecognitionView)
        {
            PrivacyView = privacyView;
            PrivacySpeechRecognitionView = privacySpeechRecognitionView;
            privacyView.PrivacyGrid.Children.Clear();
            privacyView.PrivacyGrid.Children.Add(privacySpeechRecognitionView);
        }
    }
}
