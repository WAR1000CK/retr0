﻿using Ui.Desktop.View.Setting.Privacy;

namespace Ui.Desktop.ViewModel.Setting.Privacy
{
    class PrivacyMessagingViewModel
    {
        #region Properties

        public PrivacyView PrivacyView { get; private set; }
        public PrivacyMessagingView PrivacyMessagingView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="PrivacyMessagingViewModel"/> class.
        /// </summary>
        /// <param name="privacyView">The privacy view.</param>
        /// <param name="privacyMessagingView">The privacy messaging view.</param>
        public PrivacyMessagingViewModel(PrivacyView privacyView, PrivacyMessagingView privacyMessagingView)
        {
            PrivacyView = privacyView;
            PrivacyMessagingView = privacyMessagingView;
            privacyView.PrivacyGrid.Children.Clear();
            privacyView.PrivacyGrid.Children.Add(privacyMessagingView);
        }
    }
}
