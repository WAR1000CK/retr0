﻿using Ui.Desktop.View.Setting.Privacy;

namespace Ui.Desktop.ViewModel.Setting.Privacy
{
    class PrivacyEmailViewModel
    {
        #region Properties

        public PrivacyView PrivacyView { get; private set; }
        public PrivacyEmailView PrivacyEmailView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="PrivacyEmailViewModel"/> class.
        /// </summary>
        /// <param name="privacyView">The privacy view.</param>
        /// <param name="privacyEmailView">The privacy email view.</param>
        public PrivacyEmailViewModel(PrivacyView privacyView, PrivacyEmailView privacyEmailView)
        {
            PrivacyView = privacyView;
            PrivacyEmailView = privacyEmailView;
            privacyView.PrivacyGrid.Children.Clear();
            privacyView.PrivacyGrid.Children.Add(privacyEmailView);
        }
    }
}
