﻿using Ui.Desktop.View.Setting.Privacy;

namespace Ui.Desktop.ViewModel.Setting.Privacy
{
    class PrivacyGeneralViewModel
    {
        #region Properties

        public PrivacyView PrivacyView { get; private set; }
        public PrivacyGeneralView PrivacyGeneralView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="PrivacyGeneralViewModel"/> class.
        /// </summary>
        /// <param name="privacyView">The privacy view.</param>
        /// <param name="privacyGeneralView">The privacy general view.</param>
        public PrivacyGeneralViewModel(PrivacyView privacyView, PrivacyGeneralView privacyGeneralView)
        {
            PrivacyView = privacyView;
            PrivacyGeneralView = privacyGeneralView;
            privacyView.PrivacyGrid.Children.Clear();
            privacyView.PrivacyGrid.Children.Add(privacyGeneralView);
        }
    }
}
