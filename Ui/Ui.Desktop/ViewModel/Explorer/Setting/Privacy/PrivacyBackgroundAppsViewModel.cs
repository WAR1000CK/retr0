﻿using Ui.Desktop.View.Setting.Privacy;

namespace Ui.Desktop.ViewModel.Setting.Privacy
{
    class PrivacyBackgroundAppsViewModel
    {
        #region Properties

        public PrivacyView PrivacyView { get; private set; }
        public PrivacyBackgroundAppsView PrivacyBackgroundAppsView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="PrivacyBackgroundAppsViewModel"/> class.
        /// </summary>
        /// <param name="privacyView">The privacy view.</param>
        /// <param name="privacyBackgroundAppsView">The privacy background apps view.</param>
        public PrivacyBackgroundAppsViewModel(PrivacyView privacyView,
            PrivacyBackgroundAppsView privacyBackgroundAppsView)
        {
            PrivacyView = privacyView;
            PrivacyBackgroundAppsView = privacyBackgroundAppsView;
            privacyView.PrivacyGrid.Children.Clear();
            privacyView.PrivacyGrid.Children.Add(privacyBackgroundAppsView);
        }
    }
}
