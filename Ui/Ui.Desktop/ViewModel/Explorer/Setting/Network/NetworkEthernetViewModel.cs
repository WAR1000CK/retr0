﻿using Ui.Desktop.View.Setting.Network;

namespace Ui.Desktop.ViewModel.Setting.Network
{
    class NetworkEthernetViewModel
    {
        #region Properties

        public NetworkView NetworkView { get; private set; }
        public NetworkEthernetView NetworkEthernetView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="NetworkEthernetViewModel"/> class.
        /// </summary>
        /// <param name="networkView">The network view.</param>
        /// <param name="networkEthernetView">The network ethernet view.</param>
        public NetworkEthernetViewModel(NetworkView networkView, NetworkEthernetView networkEthernetView)
        {
            NetworkView = networkView;
            NetworkEthernetView = networkEthernetView;
            networkView.NetworkGrid.Children.Clear();
            networkView.NetworkGrid.Children.Add(networkEthernetView);
        }
    }
}
