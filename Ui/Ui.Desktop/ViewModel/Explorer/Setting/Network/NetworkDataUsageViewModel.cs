﻿using Ui.Desktop.View.Setting.Network;

namespace Ui.Desktop.ViewModel.Setting.Network
{
    class NetworkDataUsageViewModel
    {
        #region Properties

        public NetworkView NetworkView { get; private set; }
        public NetworkDataUsageView NetworkDataUsageView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="NetworkDataUsageViewModel"/> class.
        /// </summary>
        /// <param name="networkView">The network view.</param>
        /// <param name="networkDataUsageView">The network data usage view.</param>
        public NetworkDataUsageViewModel(NetworkView networkView, NetworkDataUsageView networkDataUsageView)
        {
            NetworkView = networkView;
            NetworkDataUsageView = networkDataUsageView;
            networkView.NetworkGrid.Children.Clear();
            networkView.NetworkGrid.Children.Add(networkDataUsageView);
        }
    }
}
