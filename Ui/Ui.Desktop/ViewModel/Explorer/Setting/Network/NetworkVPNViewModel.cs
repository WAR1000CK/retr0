﻿using Ui.Desktop.View.Setting.Network;

namespace Ui.Desktop.ViewModel.Setting.Network
{
    class NetworkVPNViewModel
    {
        #region Properties

        public NetworkView NetworkView { get; private set; }
        public NetworkVPNView NetworkVPNView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="NetworkVPNViewModel"/> class.
        /// </summary>
        /// <param name="networkView">The network view.</param>
        /// <param name="networkVPNView">The network VPN view.</param>
        public NetworkVPNViewModel(NetworkView networkView, NetworkVPNView networkVPNView)
        {
            NetworkView = networkView;
            NetworkVPNView = networkVPNView;
            networkView.NetworkGrid.Children.Clear();
            networkView.NetworkGrid.Children.Add(networkVPNView);
        }
    }
}
