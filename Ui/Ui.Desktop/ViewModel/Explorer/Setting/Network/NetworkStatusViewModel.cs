﻿using Ui.Desktop.View.Setting.Network;

namespace Ui.Desktop.ViewModel.Setting.Network
{
    class NetworkStatusViewModel
    {
        #region Properties

        public NetworkView NetworkView { get; private set; }
        public NetworkStatusView NetworkStatusView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="NetworkStatusViewModel"/> class.
        /// </summary>
        /// <param name="networkView">The network view.</param>
        /// <param name="networkStatusView">The network status view.</param>
        public NetworkStatusViewModel(NetworkView networkView, NetworkStatusView networkStatusView)
        {
            NetworkView = networkView;
            NetworkStatusView = networkStatusView;
            networkView.NetworkGrid.Children.Clear();
            networkView.NetworkGrid.Children.Add(networkStatusView);
        }
    }
}
