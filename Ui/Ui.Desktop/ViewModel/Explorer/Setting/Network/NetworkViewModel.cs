﻿using Ui.Desktop.View.Setting.Network;

namespace Ui.Desktop.ViewModel.Setting.Network
{
    public class NetworkViewModel
    {
        #region Commands

        public RelayCommand CommandStatus { get; private set; }
        public RelayCommand CommandEthernet { get; private set; }
        public RelayCommand CommandDial { get; private set; }
        public RelayCommand CommandVPN { get; private set; }
        public RelayCommand CommandDataUsage { get; private set; }
        public RelayCommand CommandProxy { get; private set; }
        public RelayCommand CommandCloseWindow { get; private set; }

        #endregion

        #region Private fields

        private MainWindow _mainWindow;
        private static NetworkView _networkView;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="NetworkViewModel"/> class.
        /// </summary>
        /// <param name="mainWindow">The main window.</param>
        /// <param name="view">The view.</param>
        public NetworkViewModel(MainWindow mainWindow, NetworkView networkView)
        {
            _mainWindow = mainWindow;
            _networkView = networkView;
            mainWindow.MainGrid.Children.Add(networkView);

            #region Commands

            // Initialize commands and run when the button is pressed
            CommandStatus = new RelayCommand(Status);
            CommandEthernet = new RelayCommand(Ethernet);
            CommandDial = new RelayCommand(Dial);
            CommandVPN = new RelayCommand(VPN);
            CommandDataUsage = new RelayCommand(DataUsage);
            CommandProxy = new RelayCommand(Proxy);
            CommandCloseWindow = new RelayCommand(CloseWindow);

            #endregion

            Status();
        }

        /// <summary>
        /// Gets the network view.
        /// </summary>
        /// <returns></returns>
        public static NetworkView GetNetworkView() => _networkView;

        #region Command Functions

        /// <summary>
        /// Creates an instance of <see cref="NetworkStatusView"/>
        /// </summary>
        private void Status() => new NetworkStatusView(GetNetworkView());

        /// <summary>
        /// Creates an instance of <see cref="NetworkEthernetView"/>
        /// </summary>
        private void Ethernet() => new NetworkEthernetView(GetNetworkView());

        /// <summary>
        /// Creates an instance of <see cref="NetworkDialView"/>
        /// </summary>
        private void Dial() => new NetworkDialView(GetNetworkView());

        /// <summary>
        /// Creates an instance of <see cref="NetworkVPNView"/>
        /// </summary>
        private void VPN() => new NetworkVPNView(GetNetworkView());

        /// <summary>
        /// Creates an instance of <see cref="NetworkDataUsageView"/>
        /// </summary>
        private void DataUsage() => new NetworkDataUsageView(GetNetworkView());

        /// <summary>
        /// Creates an instance of <see cref="NetworkProxyView"/>
        /// </summary>
        private void Proxy() => new NetworkProxyView(GetNetworkView());

        /// <summary>
        /// Removes the <see cref="View"/> from <see cref="Main.MainGrid"/>
        /// </summary>
        private void CloseWindow() => _mainWindow.MainGrid.Children.Remove(GetNetworkView());

        #endregion
    }
}
