﻿using Ui.Desktop.View.Setting.Network;

namespace Ui.Desktop.ViewModel.Setting.Network
{
    class NetworkProxyViewModel
    {
        #region Properties

        public NetworkView NetworkView { get; private set; }
        public NetworkProxyView NetworkProxyView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="NetworkProxyViewModel"/> class.
        /// </summary>
        /// <param name="networkView">The network view.</param>
        /// <param name="networkProxyView">The network proxy view.</param>
        public NetworkProxyViewModel(NetworkView networkView, NetworkProxyView networkProxyView)
        {
            NetworkView = networkView;
            NetworkProxyView = networkProxyView;
            networkView.NetworkGrid.Children.Clear();
            networkView.NetworkGrid.Children.Add(networkProxyView);
        }
    }
}
