﻿using Ui.Desktop.View.Setting.Network;

namespace Ui.Desktop.ViewModel.Setting.Network
{
    class NetworkDialViewModel
    {
        #region Properties

        public NetworkView NetworkView { get; private set; }
        public NetworkDialView NetworkDialView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="NetworkDialViewModel"/> class.
        /// </summary>
        /// <param name="networkView">The network view.</param>
        /// <param name="networkDialView">The network dial view.</param>
        public NetworkDialViewModel(NetworkView networkView, NetworkDialView networkDialView)
        {
            NetworkView = networkView;
            NetworkDialView = networkDialView;
            networkView.NetworkGrid.Children.Clear();
            networkView.NetworkGrid.Children.Add(networkDialView);
        }
    }
}
