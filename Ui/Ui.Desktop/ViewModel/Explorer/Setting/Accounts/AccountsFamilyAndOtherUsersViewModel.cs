﻿using Ui.Desktop.View.Setting.Accounts;

namespace Ui.Desktop.ViewModel.Setting.Accounts
{
    class AccountsFamilyAndOtherUsersViewModel
    {
        #region Properties

        public AccountsView AccountsView { get; private set; }
        public AccountsFamilyAndOtherUsersView AccountsFamilyAndOtherUsersView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="AccountsFamilyAndOtherUsersViewModel"/> class.
        /// </summary>
        /// <param name="accountsView">The accounts view.</param>
        /// <param name="accountsFamilyAndOtherUsersView">The accounts family and other users view.</param>
        public AccountsFamilyAndOtherUsersViewModel(AccountsView accountsView,
            AccountsFamilyAndOtherUsersView accountsFamilyAndOtherUsersView)
        {
            AccountsView = accountsView;
            AccountsFamilyAndOtherUsersView = accountsFamilyAndOtherUsersView;
            accountsView.AccountsGrid.Children.Clear();
            accountsView.AccountsGrid.Children.Add(accountsFamilyAndOtherUsersView);
        }
    }
}
