﻿using Ui.Desktop.View.Setting.Accounts;

namespace Ui.Desktop.ViewModel.Setting.Accounts
{
    public class AccountsViewModel
    {
        #region Commands

        public RelayCommand CommandYourInfo { get; private set; }
        public RelayCommand CommandEmailAndAccounts { get; private set; }
        public RelayCommand CommandSignupOption { get; private set; }
        public RelayCommand CommandAccessWorkOrSchoolAccounts { get; private set; }
        public RelayCommand CommandFamilyAndOtherUsers { get; private set; }
        public RelayCommand CommandSynchronizeSettings { get; private set; }
        public RelayCommand CommandCloseWindow { get; private set; }

        #endregion

        #region Private fields

        private readonly MainWindow _mainWindow;
        private static AccountsView _accountsView;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="AccountsViewModel"/> class.
        /// </summary>
        /// <param name="mainWindow"></param>
        /// <param name="view"></param>
        public AccountsViewModel(MainWindow mainWindow, AccountsView accountsView)
        {
            _mainWindow = mainWindow;
            _accountsView = accountsView;
            mainWindow.MainGrid.Children.Add(accountsView);

            #region Commands

            // Initialize commands and run when the button is pressed
            CommandYourInfo = new RelayCommand(YourInfo);
            CommandEmailAndAccounts = new RelayCommand(EmailAndAccounts);
            CommandSignupOption = new RelayCommand(SignupOption);
            CommandAccessWorkOrSchoolAccounts = new RelayCommand(AccessWorkOrSchoolAccounts);
            CommandFamilyAndOtherUsers = new RelayCommand(FamilyAndOtherUsers);
            CommandSynchronizeSettings = new RelayCommand(SynchronizeSettings);
            CommandCloseWindow = new RelayCommand(CloseWindow);

            #endregion

            YourInfo();
        }

        /// <summary>
        /// Gets the AccountsView.
        /// </summary>
        /// <returns></returns>
        public static AccountsView GetAccountsView() => _accountsView;

        #region Command Functions

        /// <summary>
        /// Creates an instance of <see cref="AccountsYourInfoView"/>
        /// </summary>
        private void YourInfo() => new AccountsYourInfoView(GetAccountsView());

        /// <summary>
        /// Creates an instance of <see cref="AccountsEmailAndAccountsView"/>
        /// </summary>
        private void EmailAndAccounts() => new AccountsEmailAndAccountsView(GetAccountsView());

        /// <summary>
        /// Creates an instance of <see cref="AccountsSignupOptionView"/>
        /// </summary>
        private void SignupOption() => new AccountsSignupOptionView(GetAccountsView());

        /// <summary>
        /// Creates an instance of <see cref="AccountsAccessWorkOrSchoolAccountsView"/>
        /// </summary>
        private void AccessWorkOrSchoolAccounts() => new AccountsAccessWorkOrSchoolAccountsView(GetAccountsView());

        /// <summary>
        /// Creates an instance of <see cref="AccountsFamilyAndOtherUsersView"/>
        /// </summary>
        private void FamilyAndOtherUsers() => new AccountsFamilyAndOtherUsersView(GetAccountsView());

        /// <summary>
        /// Creates an instance of <see cref="AccountsSynchronizeSettingsView"/>
        /// </summary>
        private void SynchronizeSettings() => new AccountsSynchronizeSettingsView(GetAccountsView());

        /// <summary>
        /// Removes the <see cref="View"/> from <see cref="Main.MainGrid"/>
        /// </summary>
        private void CloseWindow() => _mainWindow.MainGrid.Children.Remove(GetAccountsView());

        #endregion
    }
}
