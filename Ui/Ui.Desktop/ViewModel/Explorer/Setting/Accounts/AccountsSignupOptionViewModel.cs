﻿using Ui.Desktop.View.Setting.Accounts;

namespace Ui.Desktop.ViewModel.Setting.Accounts
{
    class AccountsSignupOptionViewModel
    {
        #region Properties

        public AccountsView AccountsView { get; private set; }
        public AccountsSignupOptionView AccountsSignupOptionView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="AccountsSignupOptionViewModel"/> class.
        /// </summary>
        /// <param name="accountsView">The accounts view.</param>
        /// <param name="accountsSignupOptionView">The accounts signup option view.</param>
        public AccountsSignupOptionViewModel(AccountsView accountsView,
            AccountsSignupOptionView accountsSignupOptionView)
        {
            AccountsView = accountsView;
            AccountsSignupOptionView = accountsSignupOptionView;
            accountsView.AccountsGrid.Children.Clear();
            accountsView.AccountsGrid.Children.Add(accountsSignupOptionView);
        }
    }
}
