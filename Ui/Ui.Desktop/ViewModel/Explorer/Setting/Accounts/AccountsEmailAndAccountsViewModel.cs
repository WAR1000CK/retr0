﻿using Ui.Desktop.View.Setting.Accounts;

namespace Ui.Desktop.ViewModel.Setting.Accounts
{
    class AccountsEmailAndAccountsViewModel
    {
        #region Properties

        public AccountsView AccountsView { get; private set; }
        public AccountsEmailAndAccountsView AccountsEmailAndAccountsView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="AccountsEmailAndAccountsViewModel"/> class.
        /// </summary>
        /// <param name="accountsView">The accounts view.</param>
        /// <param name="accountsEmailAndAccountsView">The accounts email and accounts view.</param>
        public AccountsEmailAndAccountsViewModel(AccountsView accountsView,
            AccountsEmailAndAccountsView accountsEmailAndAccountsView)
        {
            AccountsView = accountsView;
            AccountsEmailAndAccountsView = accountsEmailAndAccountsView;
            accountsView.AccountsGrid.Children.Clear();
            accountsView.AccountsGrid.Children.Add(accountsEmailAndAccountsView);
        }
    }
}
