﻿using Ui.Desktop.View.Setting.Accounts;

namespace Ui.Desktop.ViewModel.Setting.Accounts
{
    public class AccountsAccessWorkOrSchoolAccountsViewModel
    {
        #region Properties

        public AccountsView AccountsView { get; private set; }
        public AccountsAccessWorkOrSchoolAccountsView AccountsAccessWorkOrSchoolAccountsView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="AccountsAccessWorkOrSchoolAccountsViewModel"/> class.
        /// </summary>
        /// <param name="accountsView">The accounts view.</param>
        /// <param name="accountsAccessWorkOrSchoolAccountsView">The accounts access work or school accounts view.</param>
        public AccountsAccessWorkOrSchoolAccountsViewModel(AccountsView accountsView,
            AccountsAccessWorkOrSchoolAccountsView accountsAccessWorkOrSchoolAccountsView)
        {
            AccountsView = accountsView;
            AccountsAccessWorkOrSchoolAccountsView = accountsAccessWorkOrSchoolAccountsView;
            accountsView.AccountsGrid.Children.Clear();
            accountsView.AccountsGrid.Children.Add(accountsAccessWorkOrSchoolAccountsView);
        }
    }
}
