﻿using Ui.Desktop.View.Setting.Accounts;

namespace Ui.Desktop.ViewModel.Setting.Accounts
{
    class AccountsYourInfoViewModel
    {
        #region Properties

        public AccountsView AccountsView { get; private set; }
        public AccountsYourInfoView AccountsYourInfoView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="AccountsYourInfoViewModel"/> class.
        /// </summary>
        /// <param name="accountsView">The accounts view.</param>
        /// <param name="accountsYourInfoView">The accounts your information view.</param>
        public AccountsYourInfoViewModel(AccountsView accountsView,
            AccountsYourInfoView accountsYourInfoView)
        {
            AccountsView = accountsView;
            AccountsYourInfoView = accountsYourInfoView;
            accountsView.AccountsGrid.Children.Clear();
            accountsView.AccountsGrid.Children.Add(accountsYourInfoView);
        }


    }
}
