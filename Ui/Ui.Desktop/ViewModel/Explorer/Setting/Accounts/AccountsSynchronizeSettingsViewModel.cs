﻿using Ui.Desktop.View.Setting.Accounts;

namespace Ui.Desktop.ViewModel.Setting.Accounts
{
    class AccountsSynchronizeSettingsViewModel
    {
        #region Properties

        public AccountsView AccountsView { get; private set; }
        public AccountsSynchronizeSettingsView AccountsSynchronizeSettingsView { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="AccountsSynchronizeSettingsViewModel"/> class.
        /// </summary>
        /// <param name="accountsView">The accounts view.</param>
        /// <param name="accountsSynchronizeSettingsView">The accounts synchronize settings view.</param>
        public AccountsSynchronizeSettingsViewModel(AccountsView accountsView,
            AccountsSynchronizeSettingsView accountsSynchronizeSettingsView)
        {
            AccountsView = accountsView;
            AccountsSynchronizeSettingsView = accountsSynchronizeSettingsView;
            accountsView.AccountsGrid.Children.Clear();
            accountsView.AccountsGrid.Children.Add(accountsSynchronizeSettingsView);
        }
    }
}
