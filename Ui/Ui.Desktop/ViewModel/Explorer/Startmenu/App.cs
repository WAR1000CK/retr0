﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Ui.Desktop.ViewModel.Explorer.Infocenter.Message;

namespace Ui.Desktop.ViewModel.Explorer.Startmenu
{
    /// <summary>
    /// App propertys
    /// </summary>
    public class App
    {
        #region Propertys

        public object DisplayName { get; private set; }
        public object DisplayVersion { get; private set; }
        public object Publisher { get; private set; }
        public object Comments { get; private set; }
        public object Contact { get; private set; }
        public object EstimatedSize { get; private set; }
        public object InstallSource { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="App"/> class.
        /// </summary>
        /// <param name="displayName">The display name.</param>
        /// <param name="displayVersion">The display version.</param>
        /// <param name="publisher">The publisher.</param>
        /// <param name="comments">The comments.</param>
        /// <param name="contact">The contact.</param>
        /// <param name="estimatedSize">Size of the estimated.</param>
        /// <param name="installSource">The install source.</param>
        public App(object displayName, object displayVersion, object publisher, object comments, object contact, object estimatedSize, object installSource)
        {
            DisplayName = displayName;
            DisplayVersion = displayVersion;
            Publisher = publisher;
            Comments = comments;
            Contact = contact;
            EstimatedSize = estimatedSize;
            InstallSource = installSource;
        }

        /// <summary>
        /// List of all app propertys.
        /// </summary>
        /// <returns></returns>
        public static List<App> Propertys()
        {
            var list = new List<App>();

            // search in: CurrentUser
            using (RegistryKey rk = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall"))
            {
                foreach (string skName in rk.GetSubKeyNames())
                {
                    using (RegistryKey sk = rk.OpenSubKey(skName))
                    {
                        try
                        {
                            var displayName = sk.GetValue("DisplayName");
                            var displayVersion = sk.GetValue("DisplayVersion");
                            var publisher = sk.GetValue("Publisher");
                            var comments = sk.GetValue("Comments");
                            var contact = sk.GetValue("Contact");
                            var estimatedSize = sk.GetValue("EstimatedSize");
                            var installSource = sk.GetValue("InstallLocation");

                            if (displayName != null)
                                list.Add(new App(displayName, displayVersion, publisher, comments, contact, estimatedSize, installSource));
                        }
                        catch (Exception ex) { InfocenterMessageViewModel.Add(IconTyp.Error, "Error", ex.Message); }
                    }
                }
            }

            // search in: LocalMachine_32
            using (RegistryKey rk = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall"))
            {
                foreach (string skName in rk.GetSubKeyNames())
                {
                    using (RegistryKey sk = rk.OpenSubKey(skName))
                    {
                        try
                        {
                            var displayName = sk.GetValue("DisplayName");
                            var displayVersion = sk.GetValue("DisplayVersion");
                            var publisher = sk.GetValue("Publisher");
                            var comments = sk.GetValue("Comments");
                            var contact = sk.GetValue("Contact");
                            var estimatedSize = sk.GetValue("EstimatedSize");
                            var installSource = sk.GetValue("InstallLocation");

                            if (displayName != null)
                                list.Add(new App(displayName, displayVersion, publisher, comments, contact, estimatedSize, installSource));
                        }
                        catch (Exception ex) { InfocenterMessageViewModel.Add(IconTyp.Error, "Error", ex.Message); }
                    }
                }
            }

            return list.OrderBy(y => y.DisplayName).ToList();
        }

        public static void GetInstalled(MainWindow mainWindow)
        {
            int index = 0;
            foreach (var item in Propertys())
            {
                var AppButtonTooltip = new ToolTip
                {
                    Style = mainWindow.FindResource("AppTooltip") as Style,
                    Content = $"{item.DisplayName}\nVersion: {item.DisplayVersion}\nPath: {item.InstallSource}\nSize: {SystemHelper.ToFileSize(Convert.ToUInt64(item.EstimatedSize))}"
                };
                var AppButton = new Button
                {
                    Style = StartmenuViewModel.GetStartmenuView().FindResource("DefaultAppButton") as Style,
                    Content = item.DisplayName,
                    ToolTip = AppButtonTooltip,
                    Tag = index
                };
                AppButton.Click += (s, e) =>
                {
                    try
                    {
                        string path = Propertys()[(int)((Button)s).Tag].InstallSource.ToString();
                        string file = Propertys()[(int)((Button)s).Tag].DisplayName.ToString();
                        Process.Start($"{path}{file}.exe");

                        MessageBox.Show($"{path}{file}", "Application", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show($"{ex.Message}", $"{item.DisplayName}", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                };

                StartmenuViewModel.GetStartmenuView().AppStackpanel.Children.Add(AppButton);

                index++;
            }
        }

        public static string GetCount => $"Installed Apps: {Propertys().Count}";
    }
}
