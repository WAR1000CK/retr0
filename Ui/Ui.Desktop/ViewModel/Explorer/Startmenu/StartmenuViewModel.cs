﻿using Logic.Database;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Ui.Desktop.Resource.Audio;
using Ui.Desktop.View.Bootup;
using Ui.Desktop.View.Explorer.Lockscreen;
using Ui.Desktop.View.Explorer.Shutdown;
using Ui.Desktop.View.Explorer.Startmenu;
using Ui.Desktop.View.Setting;
using Ui.Desktop.View.Setting.Accounts;
using Ui.Desktop.View.SignIn;
using static Ui.Desktop.ViewModel.Explorer.ExplorerViewModel;

namespace Ui.Desktop.ViewModel.Explorer.Startmenu
{
    public class StartmenuViewModel : BaseViewModel
    {
        #region Commands

        public RelayCommand CommandExpand { get; private set; }
        public RelayCommand CommandApps { get; private set; }
        public RelayCommand CommandUser { get; private set; }
        public RelayCommand CommandDocuments { get; private set; }
        public RelayCommand CommandPictures { get; private set; }
        public RelayCommand CommandSsettings { get; private set; }
        public RelayCommand CommandShutdown { get; private set; }

        #endregion

        #region Propertys

        public string Userdata => $"{UserData.GetUsername()}\n" +
                                  $"{UserData.GetPassword()}\n" +
                                  $"{UserData.GetPin()}\n" +
                                  $"{UserData.GetEmail()}\n" +
                                  $"{UserData.GetLastLogin()}\n" +
                                  $"{UserData.GetRegistered()}";

        public string AppCount { get => App.GetCount; }

        #endregion

        #region Events

        public event AudioHelper.Play OnPlay;

        #endregion

        #region Public fields

        public bool StartmenuIsVisible = false;

        #endregion

        #region Private fields

        private readonly MainWindow _mainWindow;
        private static StartmenuView _startmenuView;
        private bool _isExpand = false;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="StartmenuViewModel"/> class.
        /// </summary>
        /// <param name="mainWindow">The main window.</param>
        /// <param name="startmenu">The StartmenuView.</param>
        public StartmenuViewModel(MainWindow mainWindow, StartmenuView startmenu)
        {
            _mainWindow = mainWindow;
            _startmenuView = startmenu;

            #region Events

            // Handles the Loaded event of the StartmenuView control.
            startmenu.Loaded += delegate
            {
                #region Initialize contextmenu

                InitUserContextMenu();
                InitShutdownContextMenu();

                #endregion

                #region Add installed apps to startmenu

                App.GetInstalled(mainWindow);

                #endregion
            };

            // Handles the Loaded event of the StartmenuView control.
            // Reset lock windows delay
            startmenu.GridStartmenu.MouseMove += delegate { ResetAutolockDelay(); };

            // Handles the OnPlay event of the StartmenuView control.
            OnPlay += (s) => { AudioHelper.Player(s); };

            #endregion

            #region Initialize commands

            CommandExpand = new RelayCommand(Expand);
            CommandApps = new RelayCommand(Apps);
            CommandUser = new RelayCommand(User);
            CommandDocuments = new RelayCommand(Documents);
            CommandPictures = new RelayCommand(Pictures);
            CommandSsettings = new RelayCommand(Settings);
            CommandShutdown = new RelayCommand(Shutdown);

            #endregion
        }

        /// <summary>
        /// Gets the startmenuview.
        /// </summary>
        /// <returns></returns>
        public static StartmenuView GetStartmenuView() => _startmenuView;

        /// <summary>
        /// Handles the startmenu.
        /// </summary>
        /// <param name="resourceKey">The resource.</param>
        public void HandleStartmenu(string resourceKey)
        {
            switch (resourceKey)
            {
                case "ShowStartmenu":
                    // Add StartmenuView to MainWindow.MainGrid
                    _mainWindow.MainGrid.Children.Add(GetStartmenuView());
                    // Scroll stackpanel to top
                    GetStartmenuView().AppScroll.ScrollToTop();
                    // Start animation
                    StoryboardHelper.Animation(GetStartmenuView(), resourceKey, GetStartmenuView().GridStartmenu);
                    StartmenuIsVisible = true;
                    break;

                case "HideStartmenu":
                    _mainWindow.MainGrid.Children.Remove(GetStartmenuView());
                    StartmenuIsVisible = false;
                    break;
            }
        }

        #region Command Functions

        private void Expand()
        {
            if (!_isExpand)
            {
                StoryboardHelper.Animation(GetStartmenuView(), "ShowTiles", GetStartmenuView().GridStartmenu);
                _isExpand = true;
            }
            else
            {
                StoryboardHelper.Animation(GetStartmenuView(), "HideTiles", GetStartmenuView().GridStartmenu);
                _isExpand = false;
            }
        }

        private void Apps()
        {

        }

        private void User() => GetStartmenuView().BtnUser.ContextMenu.IsOpen = true;

        private void Documents()
        {
            ExplorerViewModel.DestroyElements();
        }

        private void Pictures()
        {
            ExplorerViewModel.DestroyElements();
        }

        private void Settings()
        {
            ExplorerViewModel.DestroyElements();
            new OverviewView(_mainWindow);
        }

        private void Shutdown() => GetStartmenuView().BtnShutdown.ContextMenu.IsOpen = true;


        private void InitUserContextMenu()
        {
            var contextMenu = new ContextMenu
            { Style = _startmenuView.FindResource("DefaultContextMenu") as Style };

            MenuItem menuItem;
            
            var header = new List<string>()
            {
                "Kontoeinstellungen ändern",
                "Sperren",
                "Abmelden"
            };
            foreach (var h in header)
            {
                menuItem = new MenuItem
                {
                    Header = h,
                    FontSize = 14,
                    Foreground = Brushes.White
                    
                };
                menuItem.Click += (s, e) =>
                {
                    var tmp = s as MenuItem;
                    switch (tmp.Header)
                    {
                        case "Kontoeinstellungen ändern":
                            new OverviewView(_mainWindow);
                            new AccountsView(_mainWindow);
                            break;
                        case "Sperren":
                            DestroyExplorerView();
                            new LockscreenView(_mainWindow);
                            break;
                        case "Abmelden":
                            new SigninView(_mainWindow);
                            OnPlay?.Invoke(AudioHelper.SoundTyp.Logoff);
                            break;
                    }
                    DestroyElements();
                };
                contextMenu.Items.Add(menuItem);
            }
            GetStartmenuView().BtnUser.ContextMenu = contextMenu;
        }

        private void InitShutdownContextMenu()
        {
            var contextMenu = new ContextMenu
            { Style = _startmenuView.FindResource("DefaultContextMenu") as Style };

            MenuItem menuItem;

            var header = new List<string>()
            {
                "Herunterfahren",
                "Neu starten"
            };
            foreach (var h in header)
            {
                menuItem = new MenuItem
                {
                    Header = h,
                    FontSize = 14,
                    Foreground = Brushes.White
                };
                menuItem.Click += (s, e) =>
                {
                    var tmp = s as MenuItem;
                    switch (tmp.Header)
                    {
                        case "Herunterfahren":
                            new ShutdownView(_mainWindow);
                            break;
                        case "Neu starten":
                            _mainWindow.MainGrid.Children.Clear();
                            new BootupView(_mainWindow);
                            break;
                    }
                    DestroyElements();
                };
                contextMenu.Items.Add(menuItem);
            }
            GetStartmenuView().BtnShutdown.ContextMenu = contextMenu;
        }

        #endregion

    }
}
