﻿namespace Ui.Desktop.ViewModel.Explorer.Infocenter.Message
{
    public enum IconTyp
    {
        Info,
        Attention,
        Error
    }
}
