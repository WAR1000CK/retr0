﻿using System.Windows;
using Ui.Desktop.View.Explorer.Infocenter;
using Ui.Desktop.View.Explorer.Infocenter.Message;

namespace Ui.Desktop.ViewModel.Explorer.Infocenter.Message
{
    public class InfocenterMessageViewModel
    {
        #region Propertys

        public static string ImgSource { get; private set; }
        public static string InfoTitle { get; private set; }
        public static string InfoMessage { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="InfocenterMessageViewModel"/> class.
        /// </summary>
        /// <param name="infocenter">The infocenter.</param>
        /// <param name="infocenterMessageView">The infocenter message view.</param>
        public InfocenterMessageViewModel(InfocenterView infocenter, InfocenterMessageView infocenterMessageView)
        {
            infocenter?.Messages.Children.Add(infocenterMessageView);

            #region Events

            // Handles the MouseDown event of the InfocenterMessageView control.
            infocenterMessageView.MouseDown += (s, e) =>
            {
                MessageBox.Show(e.Source.ToString());
            };

            #endregion
        }

        /// <summary>
        /// Adds the <see cref="InfocenterMessageView"/> to <see cref="InfocenterView"/>.
        /// </summary>
        /// <param name="imageTyp">The image typ.</param>
        /// <param name="title">The title.</param>
        /// <param name="message">The message.</param>
        /// <returns></returns>
        public static InfocenterMessageView Add(IconTyp iconTyp, string title, string message)
        {
            switch (iconTyp)
            {
                case IconTyp.Info:
                    ImgSource = @"/Ui.Desktop;component/Resource\Image\Icons\Info.png";
                    break;
                case IconTyp.Attention:
                    ImgSource = @"/Ui.Desktop;component/Resource\Image\Icons\Attention.png";
                    break;
                case IconTyp.Error:
                    ImgSource = @"/Ui.Desktop;component/Resource\Image\Icons\Error.png";
                    break;
            }

            InfoTitle = title;
            InfoMessage = message;

            return new InfocenterMessageView(InfocenterViewModel.GetInfocenterView());
        }
    }
}
