﻿using Ui.Desktop.View.Explorer.Infocenter;

namespace Ui.Desktop.ViewModel.Explorer.Infocenter
{
    public class InfocenterViewModel
    {
        #region Public fields

        public bool InfocenterIsVisible = false;

        #endregion

        #region Private fields

        private readonly MainWindow _mainWindow;
        private static InfocenterView _infocenterView;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="InfocenterViewModel"/> class.
        /// </summary>
        /// <param name="mainWindow">The main window.</param>
        /// <param name="infocenter">The InfocenterView.</param>
        public InfocenterViewModel(MainWindow mainWindow, InfocenterView infocenter)
        {
            _mainWindow = mainWindow;
            _infocenterView = infocenter;

            #region Events

            // Reset lock delay
            infocenter.GridInfocenter.MouseMove += delegate { ExplorerViewModel.ResetAutolockDelay(); };

            #endregion
        }

        /// <summary>
        /// Gets the infocenterview.
        /// </summary>
        /// <returns></returns>
        public static InfocenterView GetInfocenterView() => _infocenterView;

        /// <summary>
        /// Handles the <see cref="InfocenterView"/> visibility.
        /// </summary>
        /// <param name="resourceKey">The resource.</param>
        public void HandleInfocenter(string resourceKey)
        {
            switch (resourceKey)
            {
                case "ShowInfocenter":
                    // Add InfoCenterView to MainWindow.MainGrid
                    _mainWindow.MainGrid.Children.Add(GetInfocenterView());
                    // Start animation
                    StoryboardHelper.Animation(GetInfocenterView(), resourceKey, GetInfocenterView().GridInfocenter);
                    InfocenterIsVisible = true;
                    break;

                case "HideInfocenter":
                    _mainWindow.MainGrid.Children.Remove(GetInfocenterView());
                    InfocenterIsVisible = false;
                    break;
            }
        }
    }
}
