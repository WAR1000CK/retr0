﻿using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Threading;
using Ui.Desktop.Extension;
using Ui.Desktop.View.Explorer;
using Ui.Desktop.View.Explorer.Infocenter;
using Ui.Desktop.View.Explorer.Lockscreen;
using Ui.Desktop.View.Explorer.Startmenu;
using Ui.Desktop.ViewModel.Bootup;
using Ui.Desktop.ViewModel.Explorer.Lockscreen;
using Ui.Desktop.ViewModel.Explorer.Shutdown;
using Ui.Desktop.ViewModel.Note;
using Ui.Desktop.ViewModel.Registration;
using Ui.Desktop.ViewModel.SignIn;

namespace Ui.Desktop.ViewModel.Explorer
{
    public class ExplorerViewModel : BaseViewModel
    {
        #region Commands

        public RelayCommand CommandStartmenu { get; private set; }
        public RelayCommand CommandInfocenter { get; private set; }
        public RelayCommand CommandAppFileExplorer { get; private set; }
        public RelayCommand CommandAppInternetExplorer { get; private set; }

        #endregion

        #region Propertys

        private string time;
        public string Time
        {
            get { return time; }
            set
            {
                if (time == value)
                    return;

                time = value;
                OnPropertyChanged();
            }
        }

        public string Date { get; set; }

        #endregion

        #region Private fields

        private static MainWindow _staticMainWindow;
        private readonly MainWindow _mainWindow;
        private static ExplorerView _explorerView;
        private static TimeSpan _autolockDelay = new TimeSpan(0, 5, 0);

        #endregion

        #region Public fields

        public static DispatcherTimer AutolockTimer;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="ExplorerViewModel"/> class.
        /// </summary>
        /// <param name="mainWindow">The main window.</param>
        /// <param name="explorerView">The ExplorerView.</param>
        public ExplorerViewModel(MainWindow mainWindow, ExplorerView explorerView)
        {
            _mainWindow = mainWindow;
            _staticMainWindow = mainWindow;
            _explorerView = explorerView;
            mainWindow.MainGrid.Children.Add(explorerView);

            #region Events

            // Handles the Load event of the ExplorerView control.
            explorerView.Loaded += delegate
            {
                new InfocenterView(mainWindow); // Adds the infocenter to ExplorerView
                new StartmenuView(mainWindow);  // Adds the startmenu to ExplorerView

                // Show ExplorerView.PanelTaskbar, change opacity
                GetExplorerView().PanelTaskbar.HandleOpacity(new TimeSpan(0, 0, 1), TimeSpan.Zero);

                // Start AutoLock timer after restart
                if (AutolockTimer != null)
                {
                    if (AutolockTimer.IsEnabled == false)
                        AutolockTimer.Start();
                }
            };

            // Handles the MouseDown event of the ExplorerView control.
            explorerView.ImageExplorer.MouseDown += delegate { DestroyElements(); };
            // Handles the MouseMove event of the ExplorerView control. Reset _autolockDelay
            explorerView.ImageExplorer.MouseMove += delegate { ResetAutolockDelay(); };
            explorerView.PanelTaskbar.MouseMove += delegate { ResetAutolockDelay(); };

            #endregion

            // Show the time at the bottom right of the taskbar
            AutolockTimer = new DispatcherTimer(new TimeSpan(0, 0, 1), DispatcherPriority.Normal, delegate
            {
                Time = DateTime.Now.ToShortTimeString();
                Date = DateTime.Now.ToLongDateString();
                CheckAutolockDelay();
            }, Application.Current.Dispatcher);

            #region Initialize commands

            CommandStartmenu = new RelayCommand(OpenStartmenu);       // Initialize startmenu command and run when the button is pressed
            CommandInfocenter = new RelayCommand(OpenInfocenter);     // Initialize infocenter command and run when the button is pressed
            CommandAppFileExplorer = new RelayCommand(StartFileExplorer);
            CommandAppInternetExplorer = new RelayCommand(StartInternetExplorer);

            #endregion
        }

        /// <summary>
        /// Gets the <see cref="ExplorerView"/>.
        /// </summary>
        /// <returns></returns>
        public static ExplorerView GetExplorerView() => _explorerView;

        /// <summary>
        /// Destroys the <see cref="ExplorerView"/>.
        /// </summary>
        public static void DestroyExplorerView() => _staticMainWindow.MainGrid.Children.Remove(_explorerView);

        /// <summary>
        /// Removes the <see cref="StartmenuView"/>, <see cref="InfocenterView"/> from <see cref="ExplorerView"/>
        /// </summary>
        public static void DestroyElements()
        {
            StartmenuView.GetStartmenuViewModel().HandleStartmenu("HideStartmenu");
            InfocenterView.GetInfocenterViewModel().HandleInfocenter("HideInfocenter");
        }

        #region Autolock

        /// <summary>
        /// Subtract the <see cref="_autolockDelay"/> by 1sec.
        /// </summary>
        public void CheckAutolockDelay()
        {
            // Check if the time has run out
            if (_autolockDelay == TimeSpan.Zero)
            {
                // Lock windows
                new LockscreenView(_mainWindow);
                // Reset the timer
                ResetAutolockDelay();
            }

            // Subtract delay
            _autolockDelay = _autolockDelay.Subtract(new TimeSpan(0, 0, 1));
            // DEBUG
            Logic.Debug.Message.Handle($"Lock windows in: {_autolockDelay.Duration()}", ConsoleColor.Gray, Logic.Debug.Enums.Logtype.Debug);
        }

        /// <summary>
        /// Resets the <see cref="_autolockDelay"/> to 5min.
        /// </summary>
        public static void ResetAutolockDelay() => _autolockDelay = new TimeSpan(0, 5, 0);

        #endregion

        #region Command Functions

        /// <summary>
        ///  Displays the start menu on the explorer when the command is running
        /// </summary>
        private void OpenStartmenu()
        {
            if (!StartmenuView.GetStartmenuViewModel().StartmenuIsVisible)
                StartmenuView.GetStartmenuViewModel().HandleStartmenu("ShowStartmenu");
            else StartmenuView.GetStartmenuViewModel().HandleStartmenu("HideStartmenu");
        }

        /// <summary>
        /// Displays the infocenter on the explorer when the command is running
        /// </summary>
        private void OpenInfocenter()
        {
            if (!InfocenterView.GetInfocenterViewModel().InfocenterIsVisible)
                InfocenterView.GetInfocenterViewModel().HandleInfocenter("ShowInfocenter");
            else InfocenterView.GetInfocenterViewModel().HandleInfocenter("HideInfocenter");
        }

        private void StartFileExplorer()
        {
            ProcessStartInfo processStartInfo = new ProcessStartInfo()
            {
                FileName = @"C:\Users\Nutte\OneDrive\Dokumente\Visual Studio 2019\Project\2019\C#\Retr0\Ui\Ui.FileManager\bin\Debug\Ui.FileManager.exe",
                WindowStyle = ProcessWindowStyle.Normal
            };

            Process.Start(processStartInfo);
        }

        private void StartInternetExplorer()
        {

        }

        #endregion

    }
}
