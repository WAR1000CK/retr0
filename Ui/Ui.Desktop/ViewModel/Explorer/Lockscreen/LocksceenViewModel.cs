﻿using Logic.Database;
using Logic.Weather;
using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using Ui.Desktop.Extension;
using Ui.Desktop.Resource.Audio;
using Ui.Desktop.View.Explorer.Lockscreen;

namespace Ui.Desktop.ViewModel.Explorer.Lockscreen
{
    public class LocksceenViewModel : BaseViewModel
    {
        #region Commands

        public RelayCommand CommandShutdown { get; private set; }

        #endregion

        #region Static propertys

        public static string WeatherFromLocation { get; set; } = "Berlin, DE";

        #endregion

        #region Propertys

        #region Weather

        private string location;
        public string Location
        {
            get => location;
            set
            {
                if (location == value)
                    return;

                location = value;
                OnPropertyChanged();
            }
        }

        private string clouds;
        public string Clouds
        {
            get => clouds;
            set
            {
                if (clouds == value)
                    return;

                clouds = value;
                OnPropertyChanged();
            }
        }

        private BitmapImage weatherIconId;
        public BitmapImage WeatherIcon
        {
            get => weatherIconId;
            set
            {
                if (weatherIconId == value)
                    return;

                weatherIconId = value;
                OnPropertyChanged();
            }
        }

        private string weather;
        public string Weather
        {
            get => weather;
            set
            {
                if (weather == value)
                    return;

                weather = value;
                OnPropertyChanged();
            }
        }

        private string temperature;
        public string Temperature
        {
            get => temperature;
            set
            {
                if (temperature == value)
                    return;

                temperature = value;
                OnPropertyChanged();
            }
        }

        private string humidity;
        public string Humidity
        {
            get => humidity;
            set
            {
                if (humidity == value)
                    return;

                humidity = value;
                OnPropertyChanged();
            }
        }

        private string windSpeed;
        public string WindSpeed
        {
            get => windSpeed;
            set
            {
                if (windSpeed == value)
                    return;

                windSpeed = value;
                OnPropertyChanged();
            }
        }

        private BitmapImage windDirectionIcon;
        public BitmapImage WindDirectionIcon
        {
            get => windDirectionIcon;
            set
            {
                if (windDirectionIcon == value)
                    return;

                windDirectionIcon = value;
                OnPropertyChanged();
            }
        }

        private string visible;
        public string Visible
        {
            get => visible;
            set
            {
                if (visible == value)
                    return;

                visible = value;
                OnPropertyChanged();
            }
        }

        private string lastUpdate;
        public string LastUpdate
        {
            get => lastUpdate;
            set
            {
                if (lastUpdate == value)
                    return;

                lastUpdate = value;
                OnPropertyChanged();
            }
        }

        #endregion

        private string time;
        public string Time
        {
            get => time;
            set
            {
                if (time == value)
                    return;

                time = value;
                OnPropertyChanged();
            }
        }

        private string date;
        public string Date
        {
            get => date;
            set
            {
                if (date == value)
                    return;

                date = value;
                OnPropertyChanged();
            }
        }

        public bool IsVisible { get; private set; } = false;

        public string Username { get => UserData.Username; }
        public string RegisteredAs { get => UserData.Email.ToLower(); }

        #endregion

        #region Events

        public event AudioHelper.Play OnPlay;

        #endregion

        #region Private fields

        private readonly MainWindow _mainWindow;
        private static LockscreenView _lockscreenView;
        private bool _isMouseClicked = false;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="LocksceenViewModel"/> class.
        /// </summary>
        /// <param name="mainWindow">The main window.</param>
        /// <param name="lockscreen">The LockscreenView.</param>
        public LocksceenViewModel(MainWindow mainWindow, LockscreenView lockscreen)
        {
            _mainWindow = mainWindow;
            _lockscreenView = lockscreen;
            mainWindow.MainGrid.Children.Add(lockscreen);

            if (ExplorerViewModel.AutolockTimer != null)
            {
                if (ExplorerViewModel.AutolockTimer.IsEnabled)
                    ExplorerViewModel.AutolockTimer.Stop();
            }

            #region Events

            // Handles the Loaded event of the LockscreenView control.
            lockscreen.Loaded += delegate
            {
                UpdateWeatherData();

                // Show Weather panel
                GetLockscreenView().WeatherPanel.HandleOpacity(new TimeSpan(0, 0, 5), TimeSpan.Zero);

                // Show TimeDate panel
                Time = DateTime.Now.ToShortTimeString();
                Date = DateTime.Now.ToLongDateString();
                GetLockscreenView().TimeDatePanel.HandleOpacity(new TimeSpan(0, 0, 1), TimeSpan.Zero);

                // Make sure the LockscreenView is in the foreground
                mainWindow.InvokeUIElement(() => lockscreen.Loginfield.Focus(), DispatcherPriority.Render);
            };

            // Handles the KeyDown event of the LockscreenView control.
            lockscreen.KeyDown += (s, e) =>
            {
                if (e.Key != Key.Enter) return;

                // Check logindata
                if (mainWindow.Database.LogIn(UserData.Username, lockscreen.TxtPassword.Text))
                {
                    // Add ExplorerView
                    mainWindow.MainGrid.Children.Add(ExplorerViewModel.GetExplorerView());

                    // Destroy LockscreenView
                    mainWindow.MainGrid.Children.Remove(lockscreen);
                    // Event call
                    OnPlay?.Invoke(AudioHelper.SoundTyp.Unlock);
                }
                else
                {
                    lockscreen.TxtPassword.Text = "";
                    // Event call
                    OnPlay?.Invoke(AudioHelper.SoundTyp.Error);
                }
            };

            // Handles the MouseClick event of the LockscreenView control.
            lockscreen.MouseDown += (s, e) =>
            {
                if (_isMouseClicked) return;

                // Blur BackgroundImage
                lockscreen.ImgLockscreen.HandleBlur(20, new TimeSpan(0, 0, 3), TimeSpan.Zero);

                // Show loginfield
                IsVisible = true;
                OnPropertyChanged(nameof(IsVisible));

                // Show Loginfield Animation
                lockscreen.Loginfield.HandleOpacity(new TimeSpan(0, 0, 1), TimeSpan.Zero);

                // Show shutdown button
                lockscreen.BtnShutdown.Show();
                lockscreen.BtnShutdown.HandleOpacity(new TimeSpan(0, 0, 3), TimeSpan.Zero);

                // Focus TextBox
                lockscreen.TxtPassword.Focus();

                _isMouseClicked = true;
            };

            // Handles the OnPlay event of the LockscreenView control.
            OnPlay += (s) => { AudioHelper.Player(s); };

            #endregion

            #region Initialize commands

            CommandShutdown = new RelayCommand(Shutdown);

            #endregion

            // Update DateTime
            var updateDateTime = new DispatcherTimer(new TimeSpan(0, 0, 1), DispatcherPriority.Normal, delegate
            {
                Time = DateTime.Now.ToShortTimeString();                             // Binding to label and set current time
                Date = DateTime.Now.ToLongDateString();                             // Binding to label and set current date
            }, Application.Current.Dispatcher);

            // Update Weather
            var updateWeather = new DispatcherTimer(new TimeSpan(0, 10, 0), DispatcherPriority.Normal, delegate
            {
                UpdateWeatherData();
            }, Application.Current.Dispatcher);
        }

        /// <summary>
        /// Gets the lockscreenview.
        /// </summary>
        /// <returns></returns>
        public static LockscreenView GetLockscreenView() => _lockscreenView;

        #region Command Functions

        /// <summary>
        /// Exits the application.
        /// </summary>
        private void Shutdown() => Environment.Exit(0);

        #endregion

        private void UpdateWeatherData()
        {
            try
            {
                const string path = @"pack://application:,,,/Resource/Image/Weather";

                using (WeatherData weather = new WeatherData(WeatherFromLocation, _mainWindow.WeatherApiKey))
                {
                    Location = $"{weather.City}, {weather.Country}";
                    Clouds = weather.Clouds;
                    WeatherIcon = weather.GetBitmapImage(path, weather.IconId);
                    Weather = weather.Weather;
                    Temperature = weather.Temperature;
                    Humidity = weather.Humidity;
                    WindSpeed = weather.WindSpeed;
                    WindDirectionIcon = weather.GetBitmapImage(path, weather.WindDirection);
                    Visible = weather.Visibility;
                    LastUpdate = weather.LastUpdate;

                    if (Clouds.Equals(Weather))
                    {
                        _lockscreenView.DPWeather.Height = 0;
                        _lockscreenView.DPWeather.HandleOpacity(0, new TimeSpan(0, 0, 3), TimeSpan.Zero);
                    }
                    else
                    {
                        _lockscreenView.DPWeather.Height = 30;
                        _lockscreenView.DPWeather.HandleOpacity(1, new TimeSpan(0, 0, 3), TimeSpan.Zero);
                    }
                }
            }
            catch (Exception) { GetLockscreenView().WeatherPanel.Hide(); }
        }
    }
}
