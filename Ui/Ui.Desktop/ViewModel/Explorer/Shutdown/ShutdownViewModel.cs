﻿using System;
using System.Threading;
using System.Windows;
using System.Windows.Threading;
using Ui.Desktop.Extension;
using Ui.Desktop.Resource.Audio;
using Ui.Desktop.View.Explorer.Shutdown;
using Ui.Desktop.ViewModel.Explorer.Infocenter.Message;

namespace Ui.Desktop.ViewModel.Explorer.Shutdown
{
    public class ShutdownViewModel : BaseViewModel
    {
        #region Commands

        public RelayCommand CommandShutdownYes { get; private set; }
        public RelayCommand CommandShutdownNo { get; private set; }

        #endregion

        #region Propertys

        private string automaticallyShutdown;
        public string AutomaticallyShutdown
        {
            get => automaticallyShutdown;
            private set
            {
                if (automaticallyShutdown == value)
                    return;

                automaticallyShutdown = value;
                OnPropertyChanged();
            }
        }

        #endregion

        #region Events

        public event AudioHelper.Play OnPlay;

        #endregion

        #region Private fields

        private readonly MainWindow _mainWindow;
        private static ShutdownView _shutdownView;
        private readonly DispatcherTimer _autoShutdownTimer;
        private TimeSpan _autoShutdownDelay = new TimeSpan(0, 5, 0);

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="ShutdownViewModel"/> class.
        /// </summary>
        /// <param name="mainWindow">The main window.</param>
        /// <param name="shutdown">The ShutdownView.</param>
        public ShutdownViewModel(MainWindow mainWindow, ShutdownView shutdown)
        {
            _mainWindow = mainWindow;
            _shutdownView = shutdown;
            mainWindow.MainGrid.Children.Add(shutdown);

            #region Events

            shutdown.GridShutdown.Loaded += delegate
            {
                if (ExplorerViewModel.AutolockTimer != null)
                {
                    if (ExplorerViewModel.AutolockTimer.IsEnabled)
                        ExplorerViewModel.AutolockTimer.Stop();
                }
            };

            // Reset lock delay
            shutdown.GridShutdown.MouseMove += delegate { ExplorerViewModel.ResetAutolockDelay(); };

            // Handles the OnPlay event of the ShutdownView control.
            OnPlay += (s) => { AudioHelper.Player(s); };

            #endregion

            #region Initialize commands

            CommandShutdownYes = new RelayCommand(ShutdownYes);
            CommandShutdownNo = new RelayCommand(ShutdownNo);

            #endregion

            #region Extensions

            // Hides the taskbar
            ExplorerViewModel.GetExplorerView().PanelTaskbar.HandleOpacity(0, new TimeSpan(0, 0, 1), TimeSpan.Zero);
            // Enable blur effect
            ExplorerViewModel.GetExplorerView().ImageExplorer.HandleBlur(10, new TimeSpan(0, 0, 3), TimeSpan.Zero);
            // Enable the opacity
            ExplorerViewModel.GetExplorerView().ImageExplorer.HandleOpacity(0.2, new TimeSpan(0, 0, 1), TimeSpan.Zero);

            #endregion

            // When the countdown is down, the program ends automatically.
            _autoShutdownTimer = new DispatcherTimer(new TimeSpan(0, 0, 1), DispatcherPriority.Normal, delegate
            {
                AutomaticallyShutdown = HandleAutoShutdown();
            }, Application.Current.Dispatcher);
        }

        /// <summary>
        /// Gets the shutdownview.
        /// </summary>
        /// <returns></returns>
        public static ShutdownView GetShutdownView() => _shutdownView;

        private string HandleAutoShutdown()
        {
            if (_autoShutdownDelay == TimeSpan.Zero) { ShutdownYes(); }
            _autoShutdownDelay = _autoShutdownDelay.Subtract(new TimeSpan(0, 0, 1));

            return $"Shutdown in {_autoShutdownDelay.Duration()}";
        }

        #region Command Functions

        /// <summary>
        /// Exits the application.
        /// </summary>
        private void ShutdownYes()
        {
            // Event call
            OnPlay?.Invoke(AudioHelper.SoundTyp.Shutdown);
            Thread.Sleep(800);
            Environment.Exit(0);
        }

        /// <summary>
        /// Cancels the shutdown.
        /// </summary>
        private void ShutdownNo()
        {
            // Removes the ShutdownView from Main.MainGrid
            _mainWindow.MainGrid.Children.Remove(GetShutdownView());
            // Stops the DispatcherTimer countdownTimer
            _autoShutdownTimer.Stop();

            if (ExplorerViewModel.AutolockTimer != null)
            {
                if (ExplorerViewModel.AutolockTimer.IsEnabled == false)
                    ExplorerViewModel.AutolockTimer.Start();
            }

            #region Extensions

            // Show the taskbar
            ExplorerViewModel.GetExplorerView().PanelTaskbar.HandleOpacity(new TimeSpan(0, 0, 1), TimeSpan.Zero);
            // Disable blur effect
            ExplorerViewModel.GetExplorerView().ImageExplorer.HandleBlur();
            // Disable the opacity
            ExplorerViewModel.GetExplorerView().ImageExplorer.HandleOpacity(new TimeSpan(0, 0, 1), TimeSpan.Zero);

            #endregion

            InfocenterMessageViewModel.Add(IconTyp.Info, "Info", "Messagesystem still in development!");
        }

        #endregion
    }
}
