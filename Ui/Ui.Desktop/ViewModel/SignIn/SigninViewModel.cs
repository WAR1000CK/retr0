﻿using System;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using Ui.Desktop.Extension;
using Ui.Desktop.Model.SignIn;
using Ui.Desktop.Resource.Audio;
using Ui.Desktop.View.Explorer;
using Ui.Desktop.View.SignIn;
using Ui.Desktop.ViewModel.Explorer;

namespace Ui.Desktop.ViewModel.SignIn
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="Ui.Desktop.Model.SignIn.SigninModel" />
    public class SigninViewModel : SigninModel
    {
        #region Public Propertys

        public string Username { get; set; }

        #endregion

        #region Private fields

        private static SigninView _signinView;

        #endregion

        #region Events

        public event AudioHelper.Play OnPlay;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="SigninViewModel"/> class.
        /// </summary>
        /// <param name="mainWindow">The main window.</param>
        /// <param name="signinView">The SigninView.</param>
        public SigninViewModel(MainWindow mainWindow, SigninView signinView)
        {
            _signinView = signinView;
            mainWindow.MainGrid.Children.Clear();
            mainWindow.MainGrid.Children.Add(signinView);

            // Handles the OnPlay event of the SigninView control.
            OnPlay += (s) => { AudioHelper.Player(s); };

            #region Events

            // Handles the Loaded event of the TxtPassword control.
            signinView.Loaded += delegate
            {
                if (ExplorerViewModel.AutolockTimer != null)
                {
                    if (ExplorerViewModel.AutolockTimer.IsEnabled)
                        ExplorerViewModel.AutolockTimer.Stop();
                }

                // Make sure the SigninView.TxtUsername is in the foreground
                mainWindow.InvokeUIElement(() => signinView.TxtUsername.Focus(), DispatcherPriority.Render);

                // Show SignIn field
                signinView.SignInPanel.HandleOpacity(new TimeSpan(0, 0, 1), TimeSpan.Zero);
            };

            // Handles the KeyDown event of the TxtPassword control.
            signinView.TxtPassword.KeyDown += (s, e) =>
            {
                // Check if the enter key has been pressed
                if (e.Key == Key.Enter)
                {
                    // Login with the specified username and password
                    if (mainWindow.Database.LogIn(Username, Password))
                    {
                        // Removes all child elements from the Main.MainGrid
                        mainWindow.MainGrid.Children.Clear();

                        // Initializes a new instance of ExplorerView
                        new ExplorerView(mainWindow);

                        // Event call
                        OnPlay?.Invoke(AudioHelper.SoundTyp.Logon);
                    }
                    else
                    {
                        signinView.TxtUsername.Focus();
                        signinView.TxtUsername.Text = "";
                        signinView.TxtPassword.Password = "";
                        // Event call
                        OnPlay?.Invoke(AudioHelper.SoundTyp.Error);
                    }
                }
            };

            // Handles the PasswordChanged event of the TxtPassword control.
            signinView.TxtPassword.PasswordChanged += (s, e) =>
            {
                Password = ((PasswordBox)s).Password;

                if (signinView.TxtPassword.Password.Length == 0)
                    signinView.PreviewPassword.Text = "Password";
                else
                    signinView.PreviewPassword.Text = "";
            };

            #endregion
        }

        /// <summary>
        /// Gets the signin view.
        /// </summary>
        /// <returns></returns>
        public static SigninView GetSigninView() => _signinView;
    }
}
