﻿using Logic.Cryp706r4phy;
using Logic.Database;
using Logic.Hardware;
using Logic.Hardware.Model;
using System.Collections.Generic;
using System.Reflection;

namespace Ui.Desktop.ViewModel.Bootup
{
    /// <summary>
    /// 
    /// </summary>
    public class BootupViewModelHelper
    {
        #region Propertys

        public static List<string> BootList => GetBootList();
        public static int BootListCount => GetBootList().Count;

        #endregion

        private static List<string> GetBootList()
        {
            var tmp = new List<string>()
            {
                "██████╗ ███████╗████████╗██████╗  ██████╗      ██████╗ ███████╗",
                "██╔══██╗██╔════╝╚══██╔══╝██╔══██╗██╔═████╗    ██╔═══██╗██╔════╝",
                "██████╔╝█████╗     ██║   ██████╔╝██║██╔██║    ██║   ██║███████╗",
                "██╔══██╗██╔══╝     ██║   ██╔══██╗████╔╝██║    ██║   ██║╚════██║",
                "██║  ██║███████╗   ██║   ██║  ██║╚██████╔╝    ╚██████╔╝███████║",
                "╚═╝  ╚═╝╚══════╝   ╚═╝   ╚═╝  ╚═╝ ╚═════╝      ╚═════╝ ╚══════╝\n\n",
                $"Running Windows Kernel {Assembly.GetExecutingAssembly().GetName().Version}\n",
                "INIT Hardware...",
                
                // Processor
                $"\t\tProcessor:  {Win32_ProcessorModel.Name} <{Win32_ProcessorModel.L2CacheSize}, {Win32_ProcessorModel.L3CacheSize}>",
                $"\t\t            Frequency: {Win32_ProcessorModel.MaxClockSpeed}",
                $"\t\t            Cores: {Win32_ProcessorModel.NumberOfCores}, LogicalCores {Win32_ProcessorModel.NumberOfLogicalProcessors}",
                $"\t\t            Socket: {Win32_ProcessorModel.SocketDesignation}\n",
                
                // Memory
                $"\t\tMemory:     Serialnumber: {Win32_PhysicalMemoryModel.SerialNumber}",
                $"\t\t            {Win32_PhysicalMemoryModel.Manufacturer} {Win32_PhysicalMemoryModel.ConfiguredClockSpeed} {Win32_PhysicalMemoryModel.FormFactor}",
                $"\t\t            Voltage: {Win32_PhysicalMemoryModel.MinVoltage}",
                $"\t\t            Installed: {Win32_PhysicalMemoryModel.Capacity}, Free: {Win32_PhysicalMemoryModel.Available}\n",
                
                // Harddrive
                $"\t\tHarddrive:  Serialnumber: {Win32_DiskDriveModel.SerialNumber}",
                $"\t\t            ID: {Win32_DiskDriveModel.Id} => {Win32_DiskDriveModel.Model}",
                $"\t\t            {Win32_DiskDriveModel.Name} {Win32_DiskDriveModel.DriveType} {Win32_DiskDriveModel.DriveFormat}",
                $"\t\t            Size: {Win32_DiskDriveModel.Size}, Total space available: {Win32_DiskDriveModel.TotalAvailableSize}, Space free: {Win32_DiskDriveModel.TotalFreeSpace}\n",

                // Graphiccard
                $"\t\tDisplay:    {Win32_VideoControllerModel.DeviceID} => {Win32_VideoControllerModel.VideoProcessor}",
                $"\t\t            {Win32_VideoControllerModel.Name}, AdapterRAM: {Win32_VideoControllerModel.AdapterRAM}, MemoryType: {Win32_VideoControllerModel.VideoMemoryType}",
                $"\t\t            DACType: {Win32_VideoControllerModel.AdapterDACType}",
                $"\t\t            {Win32_VideoControllerModel.VideoArchitecture}",
                $"\t\t            Driver => {Win32_VideoControllerModel.DriverVersion}\n",
                
                // Network
                $"\t\tNetwork:    {Win32_NetworkModel.Id} => OperationalStatus {Win32_NetworkModel.OperationalStatus}",
                $"\t\t            {Win32_NetworkModel.Description}, Max throughput: {Win32_NetworkModel.Speed}",
                $"\t\t            {Win32_NetworkModel.DnsAddresse}, {Win32_NetworkModel.DnsSuffix}",
                $"\t\t            {Win32_NetworkModel.GatewayAddressIpv4}, {Win32_NetworkModel.GatewayAddressIpv6}",
                $"\t\t            Media-Access-Control-Adresse (MAC): {Win32_NetworkModel.PhysicalAddress}",
                $"\t\t            ReceiveOnly => {Win32_NetworkModel.IsReceiveOnly}\n",

                "INIT Librarys...",
                $"\t\tDatabaseAPI\t\t{Database.Version}",
                $"\t\tCryptography\t\t{CryptoSHA.Version}",
                $"\t\tHardwareAPI\t\t{Win32_Helper.Version}",
                $"\t\tDebuggerAPI\t\t{Logic.Debug.Message.Version}",
                $"\t\tWeatherAPI\t\t{Logic.Weather.WeatherDataModel.Version}\n\n",
                $"Configuring for Windows Kernel {Assembly.GetExecutingAssembly().GetName().Version}",
                "Windows is starting... please wait a moment."
            };

            return tmp;
        }
    }
}
