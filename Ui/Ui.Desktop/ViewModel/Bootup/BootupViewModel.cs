﻿using Logic.Debug.Enums;
using Logic.Hardware;
using Logic.Hardware.Model;
using System;
using System.ComponentModel;
using System.Threading;
using System.Windows.Threading;
using Ui.Desktop.View.Bootup;
using Ui.Desktop.View.Note;
using Ui.Desktop.ViewModel.Explorer;

namespace Ui.Desktop.ViewModel.Bootup
{
    /// <summary>
    /// 
    /// </summary>
    /// <seealso cref="Ui.Desktop.BaseViewModel" />
    public class BootupViewModel : BaseViewModel
    {
        #region Propertys

        private string text;
        public string Text
        {
            get => text;
            set
            {
                if (text == value)
                    return;

                text = value;
                OnPropertyChanged();
            }
        }

        private int progressbarValue;
        public int ProgressbarValue
        {
            get => progressbarValue;
            set
            {
                if (progressbarValue == value)
                    return;

                progressbarValue = value;
                OnPropertyChanged();
            }
        }

        #endregion

        #region Private fields

        private static MainWindow _mainWindow;
        private static BootupView _bootupView;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="BootupViewModel"/> class.
        /// </summary>
        /// <param name="mainWindow">The main window.</param>
        /// <param name="bootupView">The BootupView.</param>
        public BootupViewModel(MainWindow mainWindow, BootupView bootupView)
        {
            _mainWindow = mainWindow;
            _bootupView = bootupView;
            mainWindow.MainGrid.Children.Add(bootupView);

            #region Events

            // Handles the Loaded event of the BootupView control.
            bootupView.Loaded += delegate
            {
                // Make sure that the output type console is selected.
                if (Logic.Debug.Message.PtrConsole != IntPtr.Zero)
                    // Delete all previous messages
                    Console.Clear();

                // Stop AutoLock timer after restart
                if (ExplorerViewModel.AutolockTimer != null)
                {
                    if (ExplorerViewModel.AutolockTimer.IsEnabled)
                        ExplorerViewModel.AutolockTimer.Stop();
                }

                #region Initialize Hardware

                Win32_Battery.GetData();
                Win32_ComputerSystem.GetData();
                Win32_DiskDrive.GetData();
                Win32_Motherboard.GetData();
                Win32_Network.GetData();
                Win32_OperatingSystem.GetData();
                Win32_PhysicalMemory.GetData();
                Win32_Processor.GetData();
                Win32_VideoController.GetData();

                #endregion

                using (var worker = new BackgroundWorker())
                {
                    worker.WorkerSupportsCancellation = true;
                    worker.WorkerReportsProgress = true;

                    // Handles the DoWork event of the Loader control.
                    worker.DoWork += delegate
                    {
                        // Wait until all data is loaded.
                        while
                        (!Win32_BatteryModel.ValuesInitialized || !Win32_ComputerSystemModel.ValuesInitialized
                        || !Win32_DiskDriveModel.ValuesInitialized || !Win32_NetworkModel.ValuesInitialized
                        || !Win32_OperatingSystemModel.ValuesInitialized || !Win32_PhysicalMemoryModel.ValuesInitialized
                        || !Win32_ProcessorModel.ValuesInitialized || !Win32_VideoControllerModel.ValuesInitialized)
                        { Thread.Sleep(500); /* Reduce Cpu Utilization */ }

                        int iteration = 0;
                        foreach (var n in BootupViewModelHelper.BootList)
                        {
                            // Output the text
                            Text += $"{n}\n";

                            // Calculate repetitions in percent
                            iteration += 1;
                            int result = (int)Math.Round((double)(100 * iteration) / BootupViewModelHelper.BootListCount);

                            // Transfer the result to the Progressbar
                            ProgressbarValue = result;
                            // Transfer the result to the background thread
                            worker.ReportProgress(result);
                            // Invokes the ScrollViewer
                            mainWindow.InvokeUIElement(() => bootupView.BootScrollbar.ScrollToBottom(), DispatcherPriority.Normal);

                            // Sleep 30ms
                            Thread.Sleep(30);
                        }

                        // After completing the task wait for a short time so that the window does not close immediately.
                        Thread.Sleep(3000);
                    };

                    // Handles the ProgressChanged event of the Loader control.
                    worker.ProgressChanged += (s, e) =>
                    { Logic.Debug.Message.Handle($"Initialize HardwareAPI {e.ProgressPercentage} %", ConsoleColor.Yellow, Logtype.Hardware); };

                    // Handles the Completed event of the Loader control.
                    worker.RunWorkerCompleted += delegate { new NoteView(mainWindow); };

                    // Handles the RunWorkerAsync event of the Loader control.
                    worker.RunWorkerAsync();
                }
            };

            #endregion
        }

        /// <summary>
        /// Gets the <see cref="BootupView"/>.
        /// </summary>
        public static BootupView GetBootupView() => _bootupView;
    }
}
