﻿using Logic.Debug.Enums;
using System;
using Ui.Desktop.Extension;
using Ui.Desktop.View.Registration;
using Ui.Desktop.View.SignIn;
using Ui.Desktop.ViewModel.Explorer;

namespace Ui.Desktop.ViewModel.Registration
{
    public class RegistrationViewModel : BaseViewModel
    {
        #region Propertys

        public RelayCommand CommandRegistration { get; private set; }
        public RelayCommand CommandSignIn { get; private set; }

        public string Username { get; set; }
        public string Password1 { get; set; }
        public string Password2 { get; set; }
        public string Email { get; set; }

        #endregion

        #region Private fields

        private readonly MainWindow _mainWindow;
        private static RegistrationView _registrationView;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="RegistrationViewModel"/> class.
        /// </summary>
        /// <param name="mainWindow">The main window.</param>
        /// <param name="registrationView">The RegistrationView.</param>
        public RegistrationViewModel(MainWindow mainWindow, RegistrationView registrationView)
        {
            _mainWindow = mainWindow;
            _registrationView = registrationView;
            mainWindow.MainGrid.Children.Clear();
            mainWindow.MainGrid.Children.Add(registrationView);

            #region Events

            // Handles the Loaded event of the RegistrationView control.
            registrationView.Loaded += delegate
            {
                if (ExplorerViewModel.AutolockTimer != null)
                {
                    if (ExplorerViewModel.AutolockTimer.IsEnabled)
                        ExplorerViewModel.AutolockTimer.Stop();
                }

                // Show the Registration field
                registrationView.RegistrationPanel.HandleOpacity(new TimeSpan(0, 0, 1), TimeSpan.Zero);
            };

            #endregion

            #region Commands

            CommandSignIn = new RelayCommand(SignIn);
            CommandRegistration = new RelayCommand(Registration);

            #endregion
        }

        /// <summary>
        /// Gets the registration view.
        /// </summary>
        public static RegistrationView GetRegistrationView() => _registrationView;

        #region Command Functions

        /// <summary>
        /// Creates an instance of <see cref="SigninView"/>
        /// </summary>
        private void SignIn() => new SigninView(_mainWindow);

        /// <summary>
        /// Creates the user.
        /// </summary>
        private void Registration()
        {
            // Check if the specified passwords match
            if (Password1 == Password2)
            {
                // Registrations the specified user.
                if (_mainWindow.Database.Registration(Username, Password1, Email))
                    // After successful registration, go to the login page
                    SignIn();
            }
            else
                Logic.Debug.Message.Handle("Password do not match!", ConsoleColor.Red, Logtype.Exception);
        }

        #endregion
    }
}
