﻿using Ui.Desktop.View.Note;
using Ui.Desktop.View.Setup;
using Ui.Desktop.ViewModel.Explorer;

namespace Ui.Desktop.ViewModel.Note
{
    public class NoteViewModel : BaseViewModel
    {
        #region Propertys

        public RelayCommand CommandSetup { get; private set; }

        #endregion

        #region Private fields

        private readonly MainWindow _mainWindow;
        private static NoteView _noteView;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="NoteViewModel"/> class.
        /// </summary>
        /// <param name="mainWindow">The main window.</param>
        /// <param name="noteView">The NoteView.</param>
        public NoteViewModel(MainWindow mainWindow, NoteView noteView)
        {
            _mainWindow = mainWindow;
            _noteView = noteView;
            mainWindow.MainGrid.Children.Clear();
            mainWindow.MainGrid.Children.Add(noteView);

            noteView.Loaded += delegate
            {
                if (ExplorerViewModel.AutolockTimer != null)
                {
                    if (ExplorerViewModel.AutolockTimer.IsEnabled)
                        ExplorerViewModel.AutolockTimer.Stop();
                }
            };

            #region Commands

            CommandSetup = new RelayCommand(Setup);

            #endregion
        }

        /// <summary>
        /// Gets the note view.
        /// </summary>
        public static NoteView GetNoteView() => _noteView;

        #region Command Functions

        /// <summary>
        /// Creates an instance of <see cref="SetupView"/>
        /// </summary>
        private void Setup() => new SetupView(_mainWindow);

        #endregion
    }
}
