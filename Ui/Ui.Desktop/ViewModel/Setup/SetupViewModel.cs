﻿using Ui.Desktop.View.Registration;
using Ui.Desktop.View.Setup;
using Ui.Desktop.ViewModel.Explorer;

namespace Ui.Desktop.ViewModel
{
    public class SetupViewModel : BaseViewModel
    {
        #region Propertys

        public RelayCommand CommandRegistration { get; private set; }

        #endregion

        #region Private fields

        private readonly MainWindow _mainWindow;
        private static SetupView _setupView;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="SetupViewModel"/> class.
        /// </summary>
        /// <param name="mainWindow">The main window.</param>
        /// <param name="setup">The SetupView.</param>
        public SetupViewModel(MainWindow mainWindow, SetupView setup)
        {
            _mainWindow = mainWindow;
            _setupView = setup;
            mainWindow.MainGrid.Children.Clear();
            mainWindow.MainGrid.Children.Add(setup);

            setup.Loaded += delegate
            {
                if (ExplorerViewModel.AutolockTimer != null)
                {
                    if (ExplorerViewModel.AutolockTimer.IsEnabled)
                        ExplorerViewModel.AutolockTimer.Stop();
                }
            };

            #region Commands

            CommandRegistration = new RelayCommand(Registration);

            #endregion
        }

        /// <summary>
        /// Gets the setup view.
        /// </summary>
        public static SetupView GetSetupView() => _setupView;

        #region Command Functions

        /// <summary>
        /// Creates an instance of <see cref="RegistrationView"/>
        /// </summary>
        private void Registration() => new RegistrationView(_mainWindow);

        #endregion
    }
}
