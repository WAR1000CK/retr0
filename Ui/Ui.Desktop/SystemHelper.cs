﻿using System;
using System.Collections.Generic;

namespace Ui.Desktop
{
    public class SystemHelper
    {
        private const ushort CONVERT_VALUE = 1024;

        /// <summary>
        /// Byteses to readable value.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <returns></returns>
        public static string ToFileSize(ulong source)
        {
            List<string> suffixes = new List<string> { " B", " KB", " MB", " GB", " TB", " PB" };

            for (int i = 0; i < suffixes.Count; i++)
            {
                ulong temp = source / (ulong)Math.Pow(CONVERT_VALUE, i + 1);

                if (temp == 0)
                    return (source / (ulong)Math.Pow(CONVERT_VALUE, i)) + suffixes[i];
            }

            return source.ToString();
        }
    }
}
