﻿using System.Windows;
using System.Windows.Media.Animation;

namespace Ui.Desktop
{
    public class StoryboardHelper
    {
        /// <summary>
        /// Animations the specified FrameworkElement.
        /// </summary>
        /// <param name="resource">The resource path.</param>
        /// <param name="resourceKey">The resource key.</param>
        /// <param name="element">The framework element.</param>
        public static void Animation(FrameworkElement resource, string resourceKey, FrameworkElement element)
        {
            var storyboard = resource.FindResource(resourceKey) as Storyboard;
            storyboard?.Begin(element);
        }
    }
}
