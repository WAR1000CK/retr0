﻿using System.Windows;

namespace Ui.Desktop.Extension
{
    public static class VisibilityExt
    {
        /// <summary>
        /// Shows the specified UIElement.
        /// </summary>
        /// <param name="uIElement">The u i element.</param>
        public static void Show(this UIElement uIElement) => uIElement.Visibility = Visibility.Visible;

        /// <summary>
        /// Hides the specified UIElement.
        /// </summary>
        /// <param name="uIElement">The u i element.</param>
        public static void Hide(this UIElement uIElement) => uIElement.Visibility = Visibility.Collapsed;
    }
}
