﻿using System;
using System.Windows;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;

namespace Ui.Desktop.Extension
{
    public static class BlurExt
    {
        /// <summary>
        /// Disable the blur.
        /// </summary>
        /// <param name="uIElement">The u i element.</param>
        public static void HandleBlur(this UIElement uIElement) => uIElement.Effect = null;

        /// <summary>
        /// Handle the blur.
        /// </summary>
        /// <param name="uIElement">The UIElement.</param>
        /// <param name="toRadius">To radius.</param>
        /// <param name="duaration">The duaration.</param>
        /// <param name="beginTime">The begin time.</param>
        public static void HandleBlur(this UIElement uIElement, double toRadius, TimeSpan duaration, TimeSpan beginTime)
        {
            var blur = new BlurEffect
            {
                KernelType = KernelType.Gaussian,
                RenderingBias = RenderingBias.Quality
            };
            uIElement.Effect = blur;

            var animation = new DoubleAnimation(0, toRadius, duaration) { BeginTime = beginTime };

            blur.BeginAnimation(BlurEffect.RadiusProperty, animation);
        }
    }
}
