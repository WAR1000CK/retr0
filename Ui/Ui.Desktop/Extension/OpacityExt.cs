﻿using System;
using System.Windows;
using System.Windows.Media.Animation;

namespace Ui.Desktop.Extension
{
    public static class OpacityExt
    {
        /// <summary>
        /// Handle the opacity from one to specific value.
        /// </summary>
        /// <param name="uIElement">The ui element.</param>
        /// <param name="value">The value.</param>
        /// <param name="duration">The duration.</param>
        /// <param name="begin">The begin.</param>
        public static void HandleOpacity(this UIElement uIElement, double value, TimeSpan duration, TimeSpan begin)
        {
            DoubleAnimation animation = new DoubleAnimation(1, value, duration) { BeginTime = begin };
            uIElement.BeginAnimation(UIElement.OpacityProperty, animation);
        }

        /// <summary>
        /// Handles the opacity from zero to one.
        /// </summary>
        /// <param name="uIElement">The ui element.</param>
        /// <param name="duration">The duration.</param>
        /// <param name="begin">The begin.</param>
        public static void HandleOpacity(this UIElement uIElement, TimeSpan duration, TimeSpan begin)
        {
            DoubleAnimation animation = new DoubleAnimation(1, duration) { BeginTime = begin };
            uIElement.BeginAnimation(UIElement.OpacityProperty, animation);
        }
    }
}
