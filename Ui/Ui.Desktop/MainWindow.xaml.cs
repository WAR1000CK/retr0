﻿using Logic.Database;
using System;
using System.Configuration;
using System.Reflection;
using System.Windows;
using System.Windows.Threading;
using Ui.Desktop.View.Bootup;

namespace Ui.Desktop
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Propertys

        public string Version { get => Assembly.GetExecutingAssembly().GetName().FullName; }
        public Database Database
        {
            get => new Database(ConfigurationManager.AppSettings["Database"],
                                ConfigurationManager.AppSettings["Table"],
                                ConfigurationManager.ConnectionStrings["MySqlConnection"].ConnectionString,
                                ConfigurationManager.AppSettings["Salt"]);
        }
        public string WeatherApiKey { get => ConfigurationManager.AppSettings["WeatherAPI"]; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
            DataContext = this;

            // Handles the Loaded event of the MainWindow control.
            this.Loaded += delegate
            {
                // Initializes a new instance of BootupView
                new BootupView(this);
            };
        }

        /// <summary>
        /// Invokes the specified UIElement.
        /// <para>
        /// The calling thread cannot access this object because the object is owned by another thread.
        /// </para>
        /// </summary>
        /// <param name="action">The action.</param>
        /// <param name="priority">The priority.</param>
        public void InvokeUIElement(Action action, DispatcherPriority priority) => Dispatcher.Invoke(action, priority);
    }
}
