﻿using System.Media;

namespace Ui.Desktop.Resource.Audio
{
    public class AudioHelper
    {
        public enum SoundTyp
        {
            Logon,
            Logoff,
            Unlock,
            Notify,
            Error,
            Shutdown
        }

        #region Delegates

        public delegate void Play(SoundTyp typ);

        #endregion

        /// <summary>
        /// Plays the specified *.wav file.
        /// </summary>
        /// <param name="path">wav file path.</param>
        public static void Player(SoundTyp typ)
        {
            using (var sp = new SoundPlayer())
            {
                switch (typ)
                {
                    case SoundTyp.Logon:
                        sp.SoundLocation = @"Resource\Audio\Sfx\Logon.wav";
                        break;
                    case SoundTyp.Logoff:
                        sp.SoundLocation = @"Resource\Audio\Sfx\Logoff.wav";
                        break;
                    case SoundTyp.Unlock:
                        sp.SoundLocation = @"Resource\Audio\Sfx\Unlock.wav";
                        break;
                    case SoundTyp.Notify:
                        sp.SoundLocation = @"Resource\Audio\Sfx\Notify.wav";
                        break;
                    case SoundTyp.Error:
                        sp.SoundLocation = @"Resource\Audio\Sfx\Error.wav";
                        break;
                    case SoundTyp.Shutdown:
                        sp.SoundLocation = @"Resource\Audio\Sfx\Shutdown.wav";
                        break;
                }
                sp.Play();
            }
        }
    }
}
