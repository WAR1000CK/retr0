﻿using Logic.Debug.Enums;
using System;
using System.Windows.Input;

namespace Ui.Desktop
{
    public class RelayCommand : ICommand
    {
        #region Events

        /// <summary>
        /// Tritt ein, wenn Änderungen auftreten, die sich auf die Ausführung des Befehls auswirken.
        /// </summary>
        public event EventHandler CanExecuteChanged;

        #endregion

        #region Action

        private readonly Action action;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="RelayCommand"/> class.
        /// </summary>
        /// <param name="action">The methode.</param>
        public RelayCommand(Action action)
        {
            this.action = action;
            Logic.Debug.Message.Handle($"{action.Target} -> {action.Method}", ConsoleColor.DarkGray, Logtype.Command);
        }

        /// <summary>
        /// Definiert die Methode, die bestimmt, ob der Befehl im aktuellen Zustand ausgeführt werden kann.
        /// </summary>
        /// <param name="parameter">Vom Befehl verwendete Daten.  Wenn der Befehl keine Datenübergabe erfordert, kann das Objekt auf <see langword="null" /> festgelegt werden.</param>
        /// <returns>
        ///   <see langword="true" />, wenn der Befehl ausgeführt werden kann, andernfalls <see langword="false" />.
        /// </returns>
        public bool CanExecute(object parameter) => true;

        /// <summary>
        /// Definiert die Methode, die aufgerufen wird, wenn der Befehl aufgerufen wird.
        /// </summary>
        /// <param name="parameter">Vom Befehl verwendete Daten.  Wenn der Befehl keine Datenübergabe erfordert, kann das Objekt auf <see langword="null" /> festgelegt werden.</param>
        public void Execute(object parameter) => action();
    }
}
