﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Explorer.Infocenter;

namespace Ui.Desktop.View.Explorer.Infocenter
{
    /// <summary>
    /// Interaktionslogik für InfocenterView.xaml
    /// </summary>
    public partial class InfocenterView : UserControl
    {
        #region Private fields

        private static InfocenterViewModel infocenterViewModel;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="InfocenterView"/> class.
        /// </summary>
        /// <param name="mainWindow">The main window.</param>
        public InfocenterView(MainWindow mainWindow)
        {
            InitializeComponent();
            infocenterViewModel = new InfocenterViewModel(mainWindow, this);
            DataContext = infocenterViewModel;
        }

        /// <summary>
        /// Gets the infocenter viewmodel.
        /// </summary>
        /// <returns></returns>
        public static InfocenterViewModel GetInfocenterViewModel() => infocenterViewModel;
    }
}
