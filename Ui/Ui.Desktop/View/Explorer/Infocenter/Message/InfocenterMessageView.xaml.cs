﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Explorer.Infocenter.Message;

namespace Ui.Desktop.View.Explorer.Infocenter.Message
{
    /// <summary>
    /// Interaktionslogik für InfocenterMessageView.xaml
    /// </summary>
    public partial class InfocenterMessageView : UserControl
    {
        public InfocenterMessageView(InfocenterView infocenterView)
        {
            InitializeComponent();
            DataContext = new InfocenterMessageViewModel(infocenterView, this);
        }
    }
}
