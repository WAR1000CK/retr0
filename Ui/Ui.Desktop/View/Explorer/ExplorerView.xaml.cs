﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Explorer;

namespace Ui.Desktop.View.Explorer
{
    /// <summary>
    /// Interaktionslogik für ExplorerView.xaml
    /// </summary>
    public partial class ExplorerView : UserControl
    {
        #region Private fields

        private static ExplorerViewModel _explorerViewModel;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="ExplorerView"/> class.
        /// </summary>
        /// <param name="mainWindow">The main window.</param>
        public ExplorerView(MainWindow mainWindow)
        {
            InitializeComponent();
            _explorerViewModel = new ExplorerViewModel(mainWindow, this);
            DataContext = _explorerViewModel;
        }

        /// <summary>
        /// Gets the explorer viewmodel.
        /// </summary>
        /// <returns></returns>
        public static ExplorerViewModel GetExplorerViewModel() => _explorerViewModel;

    }
}
