﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Explorer.Shutdown;

namespace Ui.Desktop.View.Explorer.Shutdown
{
    /// <summary>
    /// Interaktionslogik für ShutdownView.xaml
    /// </summary>
    public partial class ShutdownView : UserControl
    {
        #region Private fields

        private static ShutdownViewModel shutdownViewModel;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="ShutdownView"/> class.
        /// </summary>
        /// <param name="mainWindow">The main window.</param>
        public ShutdownView(MainWindow mainWindow)
        {
            InitializeComponent();
            shutdownViewModel = new ShutdownViewModel(mainWindow, this);
            DataContext = shutdownViewModel;
        }

        /// <summary>
        /// Gets the shutdown viewmodel.
        /// </summary>
        /// <returns></returns>
        public static ShutdownViewModel GetShutdownViewModel() => shutdownViewModel;
    }
}
