﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Explorer.Startmenu;

namespace Ui.Desktop.View.Explorer.Startmenu
{
    public partial class StartmenuView : UserControl
    {
        #region Private fields

        private static StartmenuViewModel startmenuViewModel;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="StartmenuView"/> class.
        /// </summary>
        /// <param name="mainWindow">The main window.</param>
        public StartmenuView(MainWindow mainWindow)
        {
            InitializeComponent();
            startmenuViewModel = new StartmenuViewModel(mainWindow, this);
            DataContext = startmenuViewModel;
        }

        /// <summary>
        /// Gets the startmenu viewmodel.
        /// </summary>
        /// <returns></returns>
        public static StartmenuViewModel GetStartmenuViewModel() => startmenuViewModel;
    }
}
