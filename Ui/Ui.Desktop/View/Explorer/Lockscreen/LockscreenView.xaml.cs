﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Explorer.Lockscreen;

namespace Ui.Desktop.View.Explorer.Lockscreen
{
    /// <summary>
    /// Interaktionslogik für LockscreenView.xaml
    /// </summary>
    public partial class LockscreenView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LockscreenView"/> class.
        /// </summary>
        /// <param name="mainWindow">The main window.</param>
        public LockscreenView(MainWindow mainWindow)
        {
            InitializeComponent();
            DataContext = new LocksceenViewModel(mainWindow, this);
        }
    }
}
