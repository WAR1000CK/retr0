﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.ToPlay;

namespace Ui.Desktop.View.Setting.ToPlay
{
    /// <summary>
    /// Interaktionslogik für ToPlayRecordsView.xaml
    /// </summary>
    public partial class ToPlayRecordsView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ToPlayRecordsView"/> class.
        /// </summary>
        /// <param name="toPlayView">To play view.</param>
        public ToPlayRecordsView(ToPlayView toPlayView)
        {
            InitializeComponent();
            DataContext = new ToPlayRecordsViewModel(toPlayView, this);
        }
    }
}
