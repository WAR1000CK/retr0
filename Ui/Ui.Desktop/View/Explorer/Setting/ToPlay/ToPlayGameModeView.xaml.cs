﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.ToPlay;

namespace Ui.Desktop.View.Setting.ToPlay
{
    /// <summary>
    /// Interaktionslogik für ToPlayGameModeView.xaml
    /// </summary>
    public partial class ToPlayGameModeView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ToPlayGameModeView"/> class.
        /// </summary>
        /// <param name="toPlayView">To play view.</param>
        public ToPlayGameModeView(ToPlayView toPlayView)
        {
            InitializeComponent();
            DataContext = new ToPlayGameModeViewModel(toPlayView, this);
        }
    }
}
