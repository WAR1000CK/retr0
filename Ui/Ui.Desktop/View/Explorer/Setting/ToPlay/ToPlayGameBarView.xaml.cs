﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.ToPlay;

namespace Ui.Desktop.View.Setting.ToPlay
{
    /// <summary>
    /// Interaktionslogik für ToPlayGameBarView.xaml
    /// </summary>
    public partial class ToPlayGameBarView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ToPlayGameBarView"/> class.
        /// </summary>
        /// <param name="toPlayView">To play view.</param>
        public ToPlayGameBarView(ToPlayView toPlayView)
        {
            InitializeComponent();
            DataContext = new ToPlayGameBarViewModel(toPlayView, this);
        }
    }
}
