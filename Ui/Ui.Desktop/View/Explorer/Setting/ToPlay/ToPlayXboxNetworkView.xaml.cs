﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.ToPlay;

namespace Ui.Desktop.View.Setting.ToPlay
{
    /// <summary>
    /// Interaktionslogik für ToPlayXboxNetworkView.xaml
    /// </summary>
    public partial class ToPlayXboxNetworkView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ToPlayXboxNetworkView"/> class.
        /// </summary>
        /// <param name="toPlayView">To play view.</param>
        public ToPlayXboxNetworkView(ToPlayView toPlayView)
        {
            InitializeComponent();
            DataContext = new ToPlayXboxNetworkViewModel(toPlayView, this);
        }
    }
}
