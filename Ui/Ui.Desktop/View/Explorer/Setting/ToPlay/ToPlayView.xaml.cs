﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.ToPlay;

namespace Ui.Desktop.View.Setting.ToPlay
{
    /// <summary>
    /// Interaktionslogik für ToPlayView.xaml
    /// </summary>
    public partial class ToPlayView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ToPlayView"/> class.
        /// </summary>
        /// <param name="mainWindow">The main window.</param>
        public ToPlayView(MainWindow mainWindow)
        {
            InitializeComponent();
            DataContext = new ToPlayViewModel(mainWindow, this);
        }
    }
}
