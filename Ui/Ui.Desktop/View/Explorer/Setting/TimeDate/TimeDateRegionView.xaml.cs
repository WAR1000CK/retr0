﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.TimeDate;

namespace Ui.Desktop.View.Setting.TimeDate
{
    /// <summary>
    /// Interaktionslogik für TimeDateRegionView.xaml
    /// </summary>
    public partial class TimeDateRegionView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TimeDateRegionView"/> class.
        /// </summary>
        /// <param name="timeDateView">The time date view.</param>
        public TimeDateRegionView(TimeDateView timeDateView)
        {
            InitializeComponent();
            DataContext = new TimeDateRegionViewModel(timeDateView, this);
        }
    }
}
