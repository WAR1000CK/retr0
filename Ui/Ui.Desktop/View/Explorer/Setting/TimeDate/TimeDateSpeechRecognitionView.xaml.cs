﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.TimeDate;

namespace Ui.Desktop.View.Setting.TimeDate
{
    /// <summary>
    /// Interaktionslogik für TimeDateSpeechRecognitionView.xaml
    /// </summary>
    public partial class TimeDateSpeechRecognitionView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TimeDateSpeechRecognitionView"/> class.
        /// </summary>
        /// <param name="timeDateView">The time date view.</param>
        public TimeDateSpeechRecognitionView(TimeDateView timeDateView)
        {
            InitializeComponent();
            DataContext = new TimeDateSpeechRecognitionViewModel(timeDateView, this);
        }
    }
}
