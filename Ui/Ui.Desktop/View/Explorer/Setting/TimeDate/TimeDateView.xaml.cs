﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.TimeDate;

namespace Ui.Desktop.View.Setting.TimeDate
{
    /// <summary>
    /// Interaktionslogik für TimeDateView.xaml
    /// </summary>
    public partial class TimeDateView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TimeDateView"/> class.
        /// </summary>
        /// <param name="mainWindow">The main window.</param>
        public TimeDateView(MainWindow mainWindow)
        {
            InitializeComponent();
            DataContext = new TimeDateViewModel(mainWindow, this);
        }
    }
}
