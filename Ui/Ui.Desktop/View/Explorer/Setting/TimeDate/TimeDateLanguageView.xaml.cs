﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.TimeDate;

namespace Ui.Desktop.View.Setting.TimeDate
{
    /// <summary>
    /// Interaktionslogik für TimeDateLanguageView.xaml
    /// </summary>
    public partial class TimeDateLanguageView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TimeDateLanguageView"/> class.
        /// </summary>
        /// <param name="timeDateView">The time date view.</param>
        public TimeDateLanguageView(TimeDateView timeDateView)
        {
            InitializeComponent();
            DataContext = new TimeDateLanguageViewModel(timeDateView, this);
        }
    }
}
