﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.TimeDate;

namespace Ui.Desktop.View.Setting.TimeDate
{
    /// <summary>
    /// Interaktionslogik für TimeDateCurrentDateTimeView.xaml
    /// </summary>
    public partial class TimeDateCurrentDateTimeView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TimeDateCurrentDateTimeView"/> class.
        /// </summary>
        /// <param name="timeDateView">The time date view.</param>
        public TimeDateCurrentDateTimeView(TimeDateView timeDateView)
        {
            InitializeComponent();
            DataContext = new TimeDateCurrentDateTimeViewModel(timeDateView, this);
        }
    }
}
