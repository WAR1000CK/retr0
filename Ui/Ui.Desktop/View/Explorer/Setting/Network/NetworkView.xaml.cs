﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Network;

namespace Ui.Desktop.View.Setting.Network
{
    /// <summary>
    /// Interaktionslogik für NetworkView.xaml
    /// </summary>
    public partial class NetworkView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NetworkView"/> class.
        /// </summary>
        /// <param name="mainWindow">The main window.</param>
        public NetworkView(MainWindow mainWindow)
        {
            InitializeComponent();
            DataContext = new NetworkViewModel(mainWindow, this);
        }
    }
}
