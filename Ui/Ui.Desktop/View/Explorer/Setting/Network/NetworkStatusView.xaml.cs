﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Network;

namespace Ui.Desktop.View.Setting.Network
{
    /// <summary>
    /// Interaktionslogik für NetworkStatusView.xaml
    /// </summary>
    public partial class NetworkStatusView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NetworkStatusView"/> class.
        /// </summary>
        /// <param name="networkView">The network view.</param>
        public NetworkStatusView(NetworkView networkView)
        {
            InitializeComponent();
            DataContext = new NetworkStatusViewModel(networkView, this);
        }
    }
}
