﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Network;

namespace Ui.Desktop.View.Setting.Network
{
    /// <summary>
    /// Interaktionslogik für NetworkVPNView.xaml
    /// </summary>
    public partial class NetworkVPNView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NetworkVPNView"/> class.
        /// </summary>
        /// <param name="networkView">The network view.</param>
        public NetworkVPNView(NetworkView networkView)
        {
            InitializeComponent();
            DataContext = new NetworkVPNViewModel(networkView, this);
        }
    }
}
