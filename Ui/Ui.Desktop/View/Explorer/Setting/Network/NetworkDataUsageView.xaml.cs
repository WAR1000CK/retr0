﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Network;

namespace Ui.Desktop.View.Setting.Network
{
    /// <summary>
    /// Interaktionslogik für NetworkDataUsageView.xaml
    /// </summary>
    public partial class NetworkDataUsageView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NetworkDataUsageView"/> class.
        /// </summary>
        /// <param name="networkView">The network view.</param>
        public NetworkDataUsageView(NetworkView networkView)
        {
            InitializeComponent();
            DataContext = new NetworkDataUsageViewModel(networkView, this);
        }
    }
}
