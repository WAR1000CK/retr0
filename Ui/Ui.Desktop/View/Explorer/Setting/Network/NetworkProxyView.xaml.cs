﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Network;

namespace Ui.Desktop.View.Setting.Network
{
    /// <summary>
    /// Interaktionslogik für NetworkProxyView.xaml
    /// </summary>
    public partial class NetworkProxyView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NetworkProxyView"/> class.
        /// </summary>
        /// <param name="networkView">The network view.</param>
        public NetworkProxyView(NetworkView networkView)
        {
            InitializeComponent();
            DataContext = new NetworkProxyViewModel(networkView, this);
        }
    }
}
