﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Network;

namespace Ui.Desktop.View.Setting.Network
{
    /// <summary>
    /// Interaktionslogik für NetworkDialView.xaml
    /// </summary>
    public partial class NetworkDialView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NetworkDialView"/> class.
        /// </summary>
        /// <param name="networkView">The network view.</param>
        public NetworkDialView(NetworkView networkView)
        {
            InitializeComponent();
            DataContext = new NetworkDialViewModel(networkView, this);
        }
    }
}
