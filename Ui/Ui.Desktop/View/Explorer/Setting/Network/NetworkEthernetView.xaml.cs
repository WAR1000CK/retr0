﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Network;

namespace Ui.Desktop.View.Setting.Network
{
    /// <summary>
    /// Interaktionslogik für NetworkEthernetView.xaml
    /// </summary>
    public partial class NetworkEthernetView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NetworkEthernetView"/> class.
        /// </summary>
        /// <param name="networkView">The network view.</param>
        public NetworkEthernetView(NetworkView networkView)
        {
            InitializeComponent();
            DataContext = new NetworkEthernetViewModel(networkView, this);
        }
    }
}
