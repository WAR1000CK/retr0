﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting;

namespace Ui.Desktop.View.Setting
{
    /// <summary>
    /// Interaktionslogik für OverviewView.xaml
    /// </summary>
    public partial class OverviewView : UserControl
    {
        #region Private fields

        private static OverviewViewModel overviewViewModel;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="OverviewView"/> class.
        /// </summary>
        /// <param name="mainWindow">The main window.</param>
        public OverviewView(MainWindow mainWindow)
        {
            InitializeComponent();
            overviewViewModel = new OverviewViewModel(mainWindow, this);
            DataContext = overviewViewModel;
        }

        /// <summary>
        /// Gets the overview viewmodel.
        /// </summary>
        /// <returns></returns>
        public static OverviewViewModel GetOverviewViewModel() => overviewViewModel;
    }
}
