﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Phone;

namespace Ui.Desktop.View.Setting.Phone
{
    /// <summary>
    /// Interaktionslogik für PhoneView.xaml
    /// </summary>
    public partial class PhoneView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PhoneView"/> class.
        /// </summary>
        /// <param name="mainWindow">The main window.</param>
        public PhoneView(MainWindow mainWindow)
        {
            InitializeComponent();
            DataContext = new PhoneViewModel(mainWindow, this);
        }
    }
}
