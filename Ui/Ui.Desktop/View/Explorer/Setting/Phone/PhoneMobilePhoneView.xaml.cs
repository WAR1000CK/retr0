﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Phone;

namespace Ui.Desktop.View.Setting.Phone
{
    /// <summary>
    /// Interaktionslogik für PhoneMobilePhoneView.xaml
    /// </summary>
    public partial class PhoneMobilePhoneView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PhoneMobilePhoneView"/> class.
        /// </summary>
        /// <param name="phoneView">The phone view.</param>
        public PhoneMobilePhoneView(PhoneView phoneView)
        {
            InitializeComponent();
            DataContext = new PhoneMobilePhoneViewModel(phoneView, this);
        }
    }
}
