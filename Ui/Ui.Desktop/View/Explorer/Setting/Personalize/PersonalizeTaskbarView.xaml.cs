﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Personalize;

namespace Ui.Desktop.View.Setting.Personalize
{
    /// <summary>
    /// Interaktionslogik für PersonalizeTaskbarView.xaml
    /// </summary>
    public partial class PersonalizeTaskbarView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PersonalizeTaskbarView"/> class.
        /// </summary>
        /// <param name="personalizeView">The personalize view.</param>
        public PersonalizeTaskbarView(PersonalizeView personalizeView)
        {
            InitializeComponent();
            DataContext = new PersonalizeTaskbarViewModel(personalizeView, this);
        }
    }
}
