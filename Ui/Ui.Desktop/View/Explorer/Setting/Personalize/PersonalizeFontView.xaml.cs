﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Personalize;

namespace Ui.Desktop.View.Setting.Personalize
{
    /// <summary>
    /// Interaktionslogik für PersonalizeFontView.xaml
    /// </summary>
    public partial class PersonalizeFontView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PersonalizeFontView"/> class.
        /// </summary>
        /// <param name="personalizeView">The personalize view.</param>
        public PersonalizeFontView(PersonalizeView personalizeView)
        {
            InitializeComponent();
            DataContext = new PersonalizeFontViewModel(personalizeView, this);
        }
    }
}
