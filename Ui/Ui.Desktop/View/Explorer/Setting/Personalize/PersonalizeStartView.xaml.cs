﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Personalize;

namespace Ui.Desktop.View.Setting.Personalize
{
    /// <summary>
    /// Interaktionslogik für PersonalizeStartView.xaml
    /// </summary>
    public partial class PersonalizeStartView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PersonalizeStartView"/> class.
        /// </summary>
        /// <param name="personalizeView">The personalize view.</param>
        public PersonalizeStartView(PersonalizeView personalizeView)
        {
            InitializeComponent();
            DataContext = new PersonalizeStartViewModel(personalizeView, this);
        }
    }
}
