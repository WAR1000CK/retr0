﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Personalize;

namespace Ui.Desktop.View.Setting.Personalize
{
    /// <summary>
    /// Interaktionslogik für PersonalizeView.xaml
    /// </summary>
    public partial class PersonalizeView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PersonalizeView"/> class.
        /// </summary>
        /// <param name="mainWindow">The main window.</param>
        public PersonalizeView(MainWindow mainWindow)
        {
            InitializeComponent();
            DataContext = new PersonalizeViewModel(mainWindow, this);
        }
    }
}
