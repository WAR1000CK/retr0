﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Personalize;

namespace Ui.Desktop.View.Setting.Personalize
{
    /// <summary>
    /// Interaktionslogik für PersonalizeLockscreenView.xaml
    /// </summary>
    public partial class PersonalizeLockscreenView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PersonalizeLockscreenView"/> class.
        /// </summary>
        /// <param name="personalizeView">The personalize view.</param>
        public PersonalizeLockscreenView(PersonalizeView personalizeView)
        {
            InitializeComponent();
            DataContext = new PersonalizeLockscreenViewModel(personalizeView, this);
        }
    }
}
