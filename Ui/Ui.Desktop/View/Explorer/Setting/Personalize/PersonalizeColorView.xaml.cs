﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Personalize;

namespace Ui.Desktop.View.Setting.Personalize
{
    /// <summary>
    /// Interaktionslogik für PersonalizeColorView.xaml
    /// </summary>
    public partial class PersonalizeColorView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PersonalizeColorView"/> class.
        /// </summary>
        /// <param name="personalizeView">The personalize view.</param>
        public PersonalizeColorView(PersonalizeView personalizeView)
        {
            InitializeComponent();
            DataContext = new PersonalizeColorViewModel(personalizeView, this);
        }
    }
}
