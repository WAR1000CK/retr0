﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Personalize;

namespace Ui.Desktop.View.Setting.Personalize
{
    /// <summary>
    /// Interaktionslogik für PersonalizeDesignView.xaml
    /// </summary>
    public partial class PersonalizeDesignView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PersonalizeDesignView"/> class.
        /// </summary>
        /// <param name="personalizeView">The personalize view.</param>
        public PersonalizeDesignView(PersonalizeView personalizeView)
        {
            InitializeComponent();
            DataContext = new PersonalizeDesignViewModel(personalizeView, this);
        }
    }
}
