﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Personalize;

namespace Ui.Desktop.View.Setting.Personalize
{
    /// <summary>
    /// Interaktionslogik für PersonalizeBackgroundView.xaml
    /// </summary>
    public partial class PersonalizeBackgroundView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PersonalizeBackgroundView"/> class.
        /// </summary>
        /// <param name="personalizeView">The personalize view.</param>
        public PersonalizeBackgroundView(PersonalizeView personalizeView)
        {
            InitializeComponent();
            DataContext = new PersonalizeBackgroundViewModel(personalizeView, this);
        }
    }
}
