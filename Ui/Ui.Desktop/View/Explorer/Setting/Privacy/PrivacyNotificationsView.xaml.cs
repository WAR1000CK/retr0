﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Privacy;

namespace Ui.Desktop.View.Setting.Privacy
{
    /// <summary>
    /// Interaktionslogik für PrivacyNotificationsView.xaml
    /// </summary>
    public partial class PrivacyNotificationsView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PrivacyNotificationsView"/> class.
        /// </summary>
        /// <param name="privacyView">The privacy view.</param>
        public PrivacyNotificationsView(PrivacyView privacyView)
        {
            InitializeComponent();
            DataContext = new PrivacyNotificationsViewModel(privacyView, this);
        }
    }
}
