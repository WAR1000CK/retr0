﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Privacy;

namespace Ui.Desktop.View.Setting.Privacy
{
    /// <summary>
    /// Interaktionslogik für PrivacyWirelessView.xaml
    /// </summary>
    public partial class PrivacyWirelessView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PrivacyWirelessView"/> class.
        /// </summary>
        /// <param name="privacyView">The privacy view.</param>
        public PrivacyWirelessView(PrivacyView privacyView)
        {
            InitializeComponent();
            DataContext = new PrivacyWirelessViewModel(privacyView, this);
        }
    }
}
