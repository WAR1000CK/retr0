﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Privacy;

namespace Ui.Desktop.View.Setting.Privacy
{
    /// <summary>
    /// Interaktionslogik für PrivacySpeechRecognitionView.xaml
    /// </summary>
    public partial class PrivacySpeechRecognitionView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PrivacySpeechRecognitionView"/> class.
        /// </summary>
        /// <param name="privacyView">The privacy view.</param>
        public PrivacySpeechRecognitionView(PrivacyView privacyView)
        {
            InitializeComponent();
            DataContext = new PrivacySpeechRecognitionViewModel(privacyView, this);
        }
    }
}
