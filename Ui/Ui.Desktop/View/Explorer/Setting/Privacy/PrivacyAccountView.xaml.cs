﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Privacy;

namespace Ui.Desktop.View.Setting.Privacy
{
    /// <summary>
    /// Interaktionslogik für PrivacyAccountView.xaml
    /// </summary>
    public partial class PrivacyAccountView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PrivacyAccountView"/> class.
        /// </summary>
        /// <param name="privacyView">The privacy view.</param>
        public PrivacyAccountView(PrivacyView privacyView)
        {
            InitializeComponent();
            DataContext = new PrivacyAccountViewModel(privacyView, this);
        }
    }
}
