﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Privacy;

namespace Ui.Desktop.View.Setting.Privacy
{
    /// <summary>
    /// Interaktionslogik für PrivacyAutomaticFileDownloadsView.xaml
    /// </summary>
    public partial class PrivacyAutomaticFileDownloadsView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PrivacyAutomaticFileDownloadsView"/> class.
        /// </summary>
        /// <param name="privacyView">The privacy view.</param>
        public PrivacyAutomaticFileDownloadsView(PrivacyView privacyView)
        {
            InitializeComponent();
            DataContext = new PrivacyAutomaticFileDownloadsViewModel(privacyView, this);
        }
    }
}
