﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Privacy;

namespace Ui.Desktop.View.Setting.Privacy
{
    /// <summary>
    /// Interaktionslogik für PrivacyGeneralView.xaml
    /// </summary>
    public partial class PrivacyGeneralView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PrivacyGeneralView"/> class.
        /// </summary>
        /// <param name="privacyView">The privacy view.</param>
        public PrivacyGeneralView(PrivacyView privacyView)
        {
            InitializeComponent();
            DataContext = new PrivacyGeneralViewModel(privacyView, this);
        }
    }
}
