﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Privacy;

namespace Ui.Desktop.View.Setting.Privacy
{
    /// <summary>
    /// Interaktionslogik für PrivacyPositionView.xaml
    /// </summary>
    public partial class PrivacyPositionView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PrivacyPositionView"/> class.
        /// </summary>
        /// <param name="privacyView">The privacy view.</param>
        public PrivacyPositionView(PrivacyView privacyView)
        {
            InitializeComponent();
            DataContext = new PrivacyPositionViewModel(privacyView, this);
        }
    }
}
