﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Privacy;

namespace Ui.Desktop.View.Setting.Privacy
{
    /// <summary>
    /// Interaktionslogik für PrivacyCalendarView.xaml
    /// </summary>
    public partial class PrivacyCalendarView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PrivacyCalendarView"/> class.
        /// </summary>
        /// <param name="privacyView">The privacy view.</param>
        public PrivacyCalendarView(PrivacyView privacyView)
        {
            InitializeComponent();
            DataContext = new PrivacyCalendarViewModel(privacyView, this);
        }
    }
}
