﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Privacy;

namespace Ui.Desktop.View.Setting.Privacy
{
    /// <summary>
    /// Interaktionslogik für PrivacyBackgroundAppsView.xaml
    /// </summary>
    public partial class PrivacyBackgroundAppsView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PrivacyBackgroundAppsView"/> class.
        /// </summary>
        /// <param name="privacyView">The privacy view.</param>
        public PrivacyBackgroundAppsView(PrivacyView privacyView)
        {
            InitializeComponent();
            DataContext = new PrivacyBackgroundAppsViewModel(privacyView, this);
        }
    }
}
