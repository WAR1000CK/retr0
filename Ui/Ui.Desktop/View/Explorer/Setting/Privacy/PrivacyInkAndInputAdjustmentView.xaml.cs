﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Privacy;

namespace Ui.Desktop.View.Setting.Privacy
{
    /// <summary>
    /// Interaktionslogik für PrivacyInkAndInputAdjustmentView.xaml
    /// </summary>
    public partial class PrivacyInkAndInputAdjustmentView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PrivacyInkAndInputAdjustmentView"/> class.
        /// </summary>
        /// <param name="privacyView">The privacy view.</param>
        public PrivacyInkAndInputAdjustmentView(PrivacyView privacyView)
        {
            InitializeComponent();
            DataContext = new PrivacyInkAndInputAdjustmentViewModel(privacyView, this);
        }
    }
}
