﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Privacy;

namespace Ui.Desktop.View.Setting.Privacy
{
    /// <summary>
    /// Interaktionslogik für PrivacyActivityHistoryView.xaml
    /// </summary>
    public partial class PrivacyActivityHistoryView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PrivacyActivityHistoryView"/> class.
        /// </summary>
        /// <param name="privacyView">The privacy view.</param>
        public PrivacyActivityHistoryView(PrivacyView privacyView)
        {
            InitializeComponent();
            DataContext = new PrivacyActivityHistoryViewModel(privacyView, this);
        }
    }
}
