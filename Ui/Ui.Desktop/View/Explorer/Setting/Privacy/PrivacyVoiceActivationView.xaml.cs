﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Privacy;

namespace Ui.Desktop.View.Setting.Privacy
{
    /// <summary>
    /// Interaktionslogik für PrivacyVoiceActivationView.xaml
    /// </summary>
    public partial class PrivacyVoiceActivationView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PrivacyVoiceActivationView"/> class.
        /// </summary>
        /// <param name="privacyView">The privacy view.</param>
        public PrivacyVoiceActivationView(PrivacyView privacyView)
        {
            InitializeComponent();
            DataContext = new PrivacyVoiceActivationViewModel(privacyView, this);
        }
    }
}
