﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Privacy;

namespace Ui.Desktop.View.Setting.Privacy
{
    /// <summary>
    /// Interaktionslogik für PrivacyAppDiagnosticsView.xaml
    /// </summary>
    public partial class PrivacyAppDiagnosticsView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PrivacyAppDiagnosticsView"/> class.
        /// </summary>
        /// <param name="privacyView">The privacy view.</param>
        public PrivacyAppDiagnosticsView(PrivacyView privacyView)
        {
            InitializeComponent();
            DataContext = new PrivacyAppDiagnosticsViewModel(privacyView, this);
        }
    }
}
