﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Privacy;

namespace Ui.Desktop.View.Setting.Privacy
{
    /// <summary>
    /// Interaktionslogik für PrivacyCallView.xaml
    /// </summary>
    public partial class PrivacyCallView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PrivacyCallView"/> class.
        /// </summary>
        /// <param name="privacyView">The privacy view.</param>
        public PrivacyCallView(PrivacyView privacyView)
        {
            InitializeComponent();
            DataContext = new PrivacyCallViewModel(privacyView, this);
        }
    }
}
