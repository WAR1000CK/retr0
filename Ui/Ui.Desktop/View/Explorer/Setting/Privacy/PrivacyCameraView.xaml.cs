﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Privacy;

namespace Ui.Desktop.View.Setting.Privacy
{
    /// <summary>
    /// Interaktionslogik für PrivacyCameraView.xaml
    /// </summary>
    public partial class PrivacyCameraView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PrivacyCameraView"/> class.
        /// </summary>
        /// <param name="privacyView">The privacy view.</param>
        public PrivacyCameraView(PrivacyView privacyView)
        {
            InitializeComponent();
            DataContext = new PrivacyCameraViewModel(privacyView, this);
        }
    }
}
