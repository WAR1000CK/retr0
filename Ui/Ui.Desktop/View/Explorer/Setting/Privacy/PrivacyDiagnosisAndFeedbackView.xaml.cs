﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Privacy;

namespace Ui.Desktop.View.Setting.Privacy
{
    /// <summary>
    /// Interaktionslogik für PrivacyDiagnosisAndFeedbackView.xaml
    /// </summary>
    public partial class PrivacyDiagnosisAndFeedbackView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PrivacyDiagnosisAndFeedbackView"/> class.
        /// </summary>
        /// <param name="privacyView">The privacy view.</param>
        public PrivacyDiagnosisAndFeedbackView(PrivacyView privacyView)
        {
            InitializeComponent();
            DataContext = new PrivacyDiagnosisAndFeedbackViewModel(privacyView, this);
        }
    }
}
