﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Privacy;

namespace Ui.Desktop.View.Setting.Privacy
{
    /// <summary>
    /// Interaktionslogik für PrivacyOtherDevicesView.xaml
    /// </summary>
    public partial class PrivacyOtherDevicesView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PrivacyOtherDevicesView"/> class.
        /// </summary>
        /// <param name="privacyView">The privacy view.</param>
        public PrivacyOtherDevicesView(PrivacyView privacyView)
        {
            InitializeComponent();
            DataContext = new PrivacyOtherDevicesViewModel(privacyView, this);
        }
    }
}
