﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Privacy;

namespace Ui.Desktop.View.Setting.Privacy
{
    /// <summary>
    /// Interaktionslogik für PrivacyPhoneCallsView.xaml
    /// </summary>
    public partial class PrivacyPhoneCallsView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PrivacyPhoneCallsView"/> class.
        /// </summary>
        /// <param name="privacyView">The privacy view.</param>
        public PrivacyPhoneCallsView(PrivacyView privacyView)
        {
            InitializeComponent();
            DataContext = new PrivacyPhoneCallsViewModel(privacyView, this);
        }
    }
}
