﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Privacy;

namespace Ui.Desktop.View.Setting.Privacy
{
    /// <summary>
    /// Interaktionslogik für PrivacyFilesystemView.xaml
    /// </summary>
    public partial class PrivacyFilesystemView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PrivacyFilesystemView"/> class.
        /// </summary>
        /// <param name="privacyView">The privacy view.</param>
        public PrivacyFilesystemView(PrivacyView privacyView)
        {
            InitializeComponent();
            DataContext = new PrivacyFilesystemViewModel(privacyView, this);
        }
    }
}
