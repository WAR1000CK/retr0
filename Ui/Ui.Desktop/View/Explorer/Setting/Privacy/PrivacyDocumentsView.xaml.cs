﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Privacy;

namespace Ui.Desktop.View.Setting.Privacy
{
    /// <summary>
    /// Interaktionslogik für PrivacyDocumentsView.xaml
    /// </summary>
    public partial class PrivacyDocumentsView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PrivacyDocumentsView"/> class.
        /// </summary>
        /// <param name="privacyView">The privacy view.</param>
        public PrivacyDocumentsView(PrivacyView privacyView)
        {
            InitializeComponent();
            DataContext = new PrivacyDocumentsViewModel(privacyView, this);
        }
    }
}
