﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Privacy;

namespace Ui.Desktop.View.Setting.Privacy
{
    /// <summary>
    /// Interaktionslogik für PrivacyContactsView.xaml
    /// </summary>
    public partial class PrivacyContactsView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PrivacyContactsView"/> class.
        /// </summary>
        /// <param name="privacyView">The privacy view.</param>
        public PrivacyContactsView(PrivacyView privacyView)
        {
            InitializeComponent();
            DataContext = new PrivacyContactsViewModel(privacyView, this);
        }
    }
}
