﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Privacy;

namespace Ui.Desktop.View.Setting.Privacy
{
    /// <summary>
    /// Interaktionslogik für PrivacyEmailView.xaml
    /// </summary>
    public partial class PrivacyEmailView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PrivacyEmailView"/> class.
        /// </summary>
        /// <param name="privacyView">The privacy view.</param>
        public PrivacyEmailView(PrivacyView privacyView)
        {
            InitializeComponent();
            DataContext = new PrivacyEmailViewModel(privacyView, this);
        }
    }
}
