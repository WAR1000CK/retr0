﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Privacy;

namespace Ui.Desktop.View.Setting.Privacy
{
    /// <summary>
    /// Interaktionslogik für PrivacyPicturesView.xaml
    /// </summary>
    public partial class PrivacyPicturesView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PrivacyPicturesView"/> class.
        /// </summary>
        /// <param name="privacyView">The privacy view.</param>
        public PrivacyPicturesView(PrivacyView privacyView)
        {
            InitializeComponent();
            DataContext = new PrivacyPicturesViewModel(privacyView, this);
        }
    }
}
