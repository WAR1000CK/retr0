﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Privacy;

namespace Ui.Desktop.View.Setting.Privacy
{
    /// <summary>
    /// Interaktionslogik für PrivacyMessagingView.xaml
    /// </summary>
    public partial class PrivacyMessagingView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PrivacyMessagingView"/> class.
        /// </summary>
        /// <param name="privacyView">The privacy view.</param>
        public PrivacyMessagingView(PrivacyView privacyView)
        {
            InitializeComponent();
            DataContext = new PrivacyMessagingViewModel(privacyView, this);
        }
    }
}
