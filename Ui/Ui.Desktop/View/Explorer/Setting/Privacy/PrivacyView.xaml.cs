﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Privacy;

namespace Ui.Desktop.View.Setting.Privacy
{
    /// <summary>
    /// Interaktionslogik für PrivacyView.xaml
    /// </summary>
    public partial class PrivacyView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PrivacyView"/> class.
        /// </summary>
        /// <param name="mainWindow">The main window.</param>
        public PrivacyView(MainWindow mainWindow)
        {
            InitializeComponent();
            DataContext = new PrivacyViewModel(mainWindow, this);
        }
    }
}
