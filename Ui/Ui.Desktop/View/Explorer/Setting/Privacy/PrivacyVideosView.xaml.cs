﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Privacy;

namespace Ui.Desktop.View.Setting.Privacy
{
    /// <summary>
    /// Interaktionslogik für PrivacyVideosView.xaml
    /// </summary>
    public partial class PrivacyVideosView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PrivacyVideosView"/> class.
        /// </summary>
        /// <param name="privacyView">The privacy view.</param>
        public PrivacyVideosView(PrivacyView privacyView)
        {
            InitializeComponent();
            DataContext = new PrivacyVideosViewModel(privacyView, this);
        }
    }
}
