﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Privacy;

namespace Ui.Desktop.View.Setting.Privacy
{
    /// <summary>
    /// Interaktionslogik für PrivacyTasksView.xaml
    /// </summary>
    public partial class PrivacyTasksView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PrivacyTasksView"/> class.
        /// </summary>
        /// <param name="privacyView">The privacy view.</param>
        public PrivacyTasksView(PrivacyView privacyView)
        {
            InitializeComponent();
            DataContext = new PrivacyTasksViewModel(privacyView, this);
        }
    }
}
