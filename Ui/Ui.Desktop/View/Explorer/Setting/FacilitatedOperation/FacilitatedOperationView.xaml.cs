﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.FacilitatedOperation;

namespace Ui.Desktop.View.Setting.FacilitatedOperation
{
    /// <summary>
    /// Interaktionslogik für FacilitatedOperationView.xaml
    /// </summary>
    public partial class FacilitatedOperationView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FacilitatedOperationView"/> class.
        /// </summary>
        /// <param name="mainWindow">The main window.</param>
        public FacilitatedOperationView(MainWindow mainWindow)
        {
            InitializeComponent();
            DataContext = new FacilitatedOperationViewModel(mainWindow, this);
        }
    }
}
