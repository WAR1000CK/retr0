﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.FacilitatedOperation;

namespace Ui.Desktop.View.Setting.FacilitatedOperation
{
    /// <summary>
    /// Interaktionslogik für FacilitatedOperationDisplayView.xaml
    /// </summary>
    public partial class FacilitatedOperationDisplayView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FacilitatedOperationDisplayView"/> class.
        /// </summary>
        /// <param name="facilitatedOperationView">The facilitated operation view.</param>
        public FacilitatedOperationDisplayView(FacilitatedOperationView facilitatedOperationView)
        {
            InitializeComponent();
            DataContext = new FacilitatedOperationDisplayViewModel(facilitatedOperationView, this);
        }
    }
}
