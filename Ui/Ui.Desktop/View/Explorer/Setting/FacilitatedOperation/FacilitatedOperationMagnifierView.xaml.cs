﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Explorer.Setting.FacilitatedOperation;

namespace Ui.Desktop.View.Setting.FacilitatedOperation
{
    /// <summary>
    /// Interaktionslogik für FacilitatedOperationMagnifierView.xaml
    /// </summary>
    public partial class FacilitatedOperationMagnifierView : UserControl
    {
        public FacilitatedOperationMagnifierView(FacilitatedOperationView facilitatedOperationView)
        {
            InitializeComponent();
            DataContext = new FacilitatedOperationMagnifierViewModel(facilitatedOperationView, this);
        }
    }
}
