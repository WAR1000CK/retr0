﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Explorer.Setting.FacilitatedOperation;

namespace Ui.Desktop.View.Setting.FacilitatedOperation
{
    /// <summary>
    /// Interaktionslogik für FacilitatedOperationSubtitlesForTheHearingImpairedView.xaml
    /// </summary>
    public partial class FacilitatedOperationSubtitlesForTheHearingImpairedView : UserControl
    {
        public FacilitatedOperationSubtitlesForTheHearingImpairedView(FacilitatedOperationView facilitatedOperationView)
        {
            InitializeComponent();
            DataContext = new FacilitatedOperationSubtitlesForTheHearingImpairedViewModel(facilitatedOperationView, this);
        }
    }
}
