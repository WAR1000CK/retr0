﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Explorer.Setting.FacilitatedOperation;

namespace Ui.Desktop.View.Setting.FacilitatedOperation
{
    /// <summary>
    /// Interaktionslogik für FacilitatedOperationAudioView.xaml
    /// </summary>
    public partial class FacilitatedOperationAudioView : UserControl
    {
        public FacilitatedOperationAudioView(FacilitatedOperationView facilitatedOperationView)
        {
            InitializeComponent();
            DataContext = new FacilitatedOperationAudioViewModel(facilitatedOperationView, this);
        }
    }
}
