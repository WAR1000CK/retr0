﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Explorer.Setting.FacilitatedOperation;

namespace Ui.Desktop.View.Setting.FacilitatedOperation
{
    /// <summary>
    /// Interaktionslogik für FacilitatedOperationMouseView.xaml
    /// </summary>
    public partial class FacilitatedOperationMouseView : UserControl
    {
        public FacilitatedOperationMouseView(FacilitatedOperationView facilitatedOperationView)
        {
            InitializeComponent();
            DataContext = new FacilitatedOperationMouseViewModel(facilitatedOperationView, this);
        }
    }
}
