﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Explorer.Setting.FacilitatedOperation;

namespace Ui.Desktop.View.Setting.FacilitatedOperation
{
    /// <summary>
    /// Interaktionslogik für FacilitatedOperationSpeechView.xaml
    /// </summary>
    public partial class FacilitatedOperationSpeechView : UserControl
    {
        public FacilitatedOperationSpeechView(FacilitatedOperationView facilitatedOperationView)
        {
            InitializeComponent();
            DataContext = new FacilitatedOperationSpeechViewModel(facilitatedOperationView, this);
        }
    }
}
