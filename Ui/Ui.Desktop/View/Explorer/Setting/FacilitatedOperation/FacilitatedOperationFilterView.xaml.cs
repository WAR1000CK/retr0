﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Explorer.Setting.FacilitatedOperation;

namespace Ui.Desktop.View.Setting.FacilitatedOperation
{
    /// <summary>
    /// Interaktionslogik für FacilitatedOperationFilterView.xaml
    /// </summary>
    public partial class FacilitatedOperationFilterView : UserControl
    {
        public FacilitatedOperationFilterView(FacilitatedOperationView facilitatedOperationView)
        {
            InitializeComponent();
            DataContext = new FacilitatedOperationFilterViewModel(facilitatedOperationView, this);
        }
    }
}
