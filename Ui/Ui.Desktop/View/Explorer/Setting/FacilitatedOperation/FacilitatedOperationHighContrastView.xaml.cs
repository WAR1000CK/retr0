﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Explorer.Setting.FacilitatedOperation;

namespace Ui.Desktop.View.Setting.FacilitatedOperation
{
    /// <summary>
    /// Interaktionslogik für FacilitatedOperationHighContrastView.xaml
    /// </summary>
    public partial class FacilitatedOperationHighContrastView : UserControl
    {
        public FacilitatedOperationHighContrastView(FacilitatedOperationView facilitatedOperationView)
        {
            InitializeComponent();
            DataContext = new FacilitatedOperationHighContrastViewModel(facilitatedOperationView, this);
        }
    }
}
