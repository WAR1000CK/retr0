﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Explorer.Setting.FacilitatedOperation;

namespace Ui.Desktop.View.Setting.FacilitatedOperation
{
    /// <summary>
    /// Interaktionslogik für FacilitatedOperationSpeechRecognitionView.xaml
    /// </summary>
    public partial class FacilitatedOperationSpeechRecognitionView : UserControl
    {
        public FacilitatedOperationSpeechRecognitionView(FacilitatedOperationView facilitatedOperationView)
        {
            InitializeComponent();
            DataContext = new FacilitatedOperationSpeechRecognitionViewModel(facilitatedOperationView, this);
        }
    }
}
