﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Explorer.Setting.FacilitatedOperation;

namespace Ui.Desktop.View.Setting.FacilitatedOperation
{
    /// <summary>
    /// Interaktionslogik für FacilitatedOperationCursorsAndPointersView.xaml
    /// </summary>
    public partial class FacilitatedOperationCursorsAndPointersView : UserControl
    {
        public FacilitatedOperationCursorsAndPointersView(FacilitatedOperationView facilitatedOperationView)
        {
            InitializeComponent();
            DataContext = new FacilitatedOperationCursorsAndPointersViewModel(facilitatedOperationView, this);
        }
    }
}
