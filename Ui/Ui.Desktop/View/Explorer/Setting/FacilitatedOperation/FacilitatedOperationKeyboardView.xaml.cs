﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Explorer.Setting.FacilitatedOperation;

namespace Ui.Desktop.View.Setting.FacilitatedOperation
{
    /// <summary>
    /// Interaktionslogik für FacilitatedOperationKeyboardView.xaml
    /// </summary>
    public partial class FacilitatedOperationKeyboardView : UserControl
    {
        public FacilitatedOperationKeyboardView(FacilitatedOperationView facilitatedOperationView)
        {
            InitializeComponent();
            DataContext = new FacilitatedOperationKeyboardViewModel(facilitatedOperationView, this);
        }
    }
}
