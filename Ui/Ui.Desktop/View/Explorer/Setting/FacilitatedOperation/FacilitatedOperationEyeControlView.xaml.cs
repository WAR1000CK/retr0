﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Explorer.Setting.FacilitatedOperation;

namespace Ui.Desktop.View.Setting.FacilitatedOperation
{
    /// <summary>
    /// Interaktionslogik für FacilitatedOperationEyeControlView.xaml
    /// </summary>
    public partial class FacilitatedOperationEyeControlView : UserControl
    {
        public FacilitatedOperationEyeControlView(FacilitatedOperationView facilitatedOperationView)
        {
            InitializeComponent();
            DataContext = new FacilitatedOperationEyeControlViewModel(facilitatedOperationView, this);
        }
    }
}
