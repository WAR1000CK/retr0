﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Apps;

namespace Ui.Desktop.View.Setting.Apps
{
    /// <summary>
    /// Interaktionslogik für AppsAutostartView.xaml
    /// </summary>
    public partial class AppsAutostartView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AppsAutostartView"/> class.
        /// </summary>
        /// <param name="appsView">The apps view.</param>
        public AppsAutostartView(AppsView appsView)
        {
            InitializeComponent();
            DataContext = new AppsAutostartViewModel(appsView, this);
        }
    }
}
