﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Apps;

namespace Ui.Desktop.View.Setting.Apps
{
    /// <summary>
    /// Interaktionslogik für AppsVideoPlaybackView.xaml
    /// </summary>
    public partial class AppsVideoPlaybackView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AppsVideoPlaybackView"/> class.
        /// </summary>
        /// <param name="appsView">The apps view.</param>
        public AppsVideoPlaybackView(AppsView appsView)
        {
            InitializeComponent();
            DataContext = new AppsVideoPlaybackViewModel(appsView, this);
        }
    }
}
