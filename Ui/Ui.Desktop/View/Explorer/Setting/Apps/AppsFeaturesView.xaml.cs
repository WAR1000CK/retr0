﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Apps;

namespace Ui.Desktop.View.Setting.Apps
{
    /// <summary>
    /// Interaktionslogik für AppsFeaturesView.xaml
    /// </summary>
    public partial class AppsFeaturesView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AppsFeaturesView"/> class.
        /// </summary>
        /// <param name="appsView">The apps view.</param>
        public AppsFeaturesView(AppsView appsView)
        {
            InitializeComponent();
            DataContext = new AppsFeaturesViewModel(appsView, this);
        }
    }
}
