﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Apps;

namespace Ui.Desktop.View.Setting.Apps
{
    /// <summary>
    /// Interaktionslogik für AppsStandardAppsView.xaml
    /// </summary>
    public partial class AppsStandardAppsView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AppsStandardAppsView"/> class.
        /// </summary>
        /// <param name="appsView">The apps view.</param>
        public AppsStandardAppsView(AppsView appsView)
        {
            InitializeComponent();
            DataContext = new AppsStandardAppsViewModel(appsView, this);
        }
    }
}
