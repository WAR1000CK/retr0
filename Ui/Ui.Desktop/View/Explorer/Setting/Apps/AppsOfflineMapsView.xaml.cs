﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Apps;

namespace Ui.Desktop.View.Setting.Apps
{
    /// <summary>
    /// Interaktionslogik für AppsOfflineMapsView.xaml
    /// </summary>
    public partial class AppsOfflineMapsView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AppsOfflineMapsView"/> class.
        /// </summary>
        /// <param name="appsView">The apps view.</param>
        public AppsOfflineMapsView(AppsView appsView)
        {
            InitializeComponent();
            DataContext = new AppsOfflineMapsViewModel(appsView, this);
        }
    }
}
