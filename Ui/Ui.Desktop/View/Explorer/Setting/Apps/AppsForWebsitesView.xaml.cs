﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Apps;

namespace Ui.Desktop.View.Setting.Apps
{
    /// <summary>
    /// Interaktionslogik für AppsForWebsitesView.xaml
    /// </summary>
    public partial class AppsForWebsitesView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AppsForWebsitesView"/> class.
        /// </summary>
        /// <param name="appsView">The apps view.</param>
        public AppsForWebsitesView(AppsView appsView)
        {
            InitializeComponent();
            DataContext = new AppsForWebsitesViewModel(appsView, this);
        }
    }
}
