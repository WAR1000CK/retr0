﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Apps;

namespace Ui.Desktop.View.Setting.Apps
{
    /// <summary>
    /// Interaktionslogik für AppsView.xaml
    /// </summary>
    public partial class AppsView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AppsView"/> class.
        /// </summary>
        /// <param name="mainWindow">The main window.</param>
        public AppsView(MainWindow mainWindow)
        {
            InitializeComponent();
            DataContext = new AppsViewModel(mainWindow, this);
        }
    }
}
