﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Explorer.Setting.Update;

namespace Ui.Desktop.View.Explorer.Setting.Update
{
    /// <summary>
    /// Interaktionslogik für UpdateWindowsSecurityView.xaml
    /// </summary>
    public partial class UpdateWindowsSecurityView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UpdateWindowsSecurityView"/> class.
        /// </summary>
        /// <param name="updateView">The update view.</param>
        public UpdateWindowsSecurityView(UpdateView updateView)
        {
            InitializeComponent();
            DataContext = new UpdateWindowsSecurityViewModel(updateView, this);
        }
    }
}
