﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Explorer.Setting.Update;

namespace Ui.Desktop.View.Explorer.Setting.Update
{
    /// <summary>
    /// Interaktionslogik für UpdateView.xaml
    /// </summary>
    public partial class UpdateView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UpdateView"/> class.
        /// </summary>
        /// <param name="mainWindow">The main window.</param>
        public UpdateView(MainWindow mainWindow)
        {
            InitializeComponent();
            DataContext = new UpdateViewModel(mainWindow, this);
        }
    }
}
