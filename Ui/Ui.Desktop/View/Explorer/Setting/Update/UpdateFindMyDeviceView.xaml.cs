﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Explorer.Setting.Update;

namespace Ui.Desktop.View.Explorer.Setting.Update
{
    /// <summary>
    /// Interaktionslogik für UpdateFindMyDeviceView.xaml
    /// </summary>
    public partial class UpdateFindMyDeviceView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UpdateFindMyDeviceView"/> class.
        /// </summary>
        /// <param name="updateView">The update view.</param>
        public UpdateFindMyDeviceView(UpdateView updateView)
        {
            InitializeComponent();
            DataContext = new UpdateFindMyDeviceViewModel(updateView, this);
        }
    }
}
