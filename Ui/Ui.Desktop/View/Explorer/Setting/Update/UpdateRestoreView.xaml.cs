﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Explorer.Setting.Update;

namespace Ui.Desktop.View.Explorer.Setting.Update
{
    /// <summary>
    /// Interaktionslogik für UpdateRestoreView.xaml
    /// </summary>
    public partial class UpdateRestoreView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UpdateRestoreView"/> class.
        /// </summary>
        /// <param name="updateView">The update view.</param>
        public UpdateRestoreView(UpdateView updateView)
        {
            InitializeComponent();
            DataContext = new UpdateRestoreViewModel(updateView, this);
        }
    }
}
