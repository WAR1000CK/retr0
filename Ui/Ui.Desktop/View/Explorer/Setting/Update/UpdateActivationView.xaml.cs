﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Explorer.Setting.Update;

namespace Ui.Desktop.View.Explorer.Setting.Update
{
    /// <summary>
    /// Interaktionslogik für UpdateActivationView.xaml
    /// </summary>
    public partial class UpdateActivationView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UpdateActivationView"/> class.
        /// </summary>
        /// <param name="updateView">The update view.</param>
        public UpdateActivationView(UpdateView updateView)
        {
            InitializeComponent();
            DataContext = new UpdateActivationViewModel(updateView, this);
        }
    }
}
