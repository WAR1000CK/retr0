﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Explorer.Setting.Update;

namespace Ui.Desktop.View.Explorer.Setting.Update
{
    /// <summary>
    /// Interaktionslogik für UpdateTransmissionOptimizationView.xaml
    /// </summary>
    public partial class UpdateTransmissionOptimizationView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UpdateTransmissionOptimizationView"/> class.
        /// </summary>
        /// <param name="updateView">The update view.</param>
        public UpdateTransmissionOptimizationView(UpdateView updateView)
        {
            InitializeComponent();
            DataContext = new UpdateTransmissionOptimizationViewModel(updateView, this);
        }
    }
}
