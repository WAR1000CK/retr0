﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Explorer.Setting.Update;

namespace Ui.Desktop.View.Explorer.Setting.Update
{
    /// <summary>
    /// Interaktionslogik für UpdateWindowsInsiderProgramView.xaml
    /// </summary>
    public partial class UpdateWindowsInsiderProgramView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UpdateWindowsInsiderProgramView"/> class.
        /// </summary>
        /// <param name="updateView">The update view.</param>
        public UpdateWindowsInsiderProgramView(UpdateView updateView)
        {
            InitializeComponent();
            DataContext = new UpdateWindowsInsiderProgramViewModel(updateView, this);
        }
    }
}
