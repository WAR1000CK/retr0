﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Explorer.Setting.Update;

namespace Ui.Desktop.View.Explorer.Setting.Update
{
    /// <summary>
    /// Interaktionslogik für UpdateForDevelopersView.xaml
    /// </summary>
    public partial class UpdateForDevelopersView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UpdateForDevelopersView"/> class.
        /// </summary>
        /// <param name="updateView">The update view.</param>
        public UpdateForDevelopersView(UpdateView updateView)
        {
            InitializeComponent();
            DataContext = new UpdateForDevelopersViewModel(updateView, this);
        }
    }
}
