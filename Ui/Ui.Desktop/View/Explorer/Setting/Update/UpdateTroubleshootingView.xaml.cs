﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Explorer.Setting.Update;

namespace Ui.Desktop.View.Explorer.Setting.Update
{
    /// <summary>
    /// Interaktionslogik für UpdateTroubleshootingView.xaml
    /// </summary>
    public partial class UpdateTroubleshootingView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UpdateTroubleshootingView"/> class.
        /// </summary>
        /// <param name="updateView">The update view.</param>
        public UpdateTroubleshootingView(UpdateView updateView)
        {
            InitializeComponent();
            DataContext = new UpdateTroubleshootingViewModel(updateView, this);
        }
    }
}
