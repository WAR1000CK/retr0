﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Explorer.Setting.Update;

namespace Ui.Desktop.View.Explorer.Setting.Update
{
    /// <summary>
    /// Interaktionslogik für UpdateWindowsUpdateView.xaml
    /// </summary>
    public partial class UpdateWindowsUpdateView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UpdateWindowsUpdateView"/> class.
        /// </summary>
        /// <param name="updateView">The update view.</param>
        public UpdateWindowsUpdateView(UpdateView updateView)
        {
            InitializeComponent();
            DataContext = new UpdateWindowsUpdateViewModel(updateView, this);
        }
    }
}
