﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Explorer.Setting.Update;

namespace Ui.Desktop.View.Explorer.Setting.Update
{
    /// <summary>
    /// Interaktionslogik für UpdateBackupView.xaml
    /// </summary>
    public partial class UpdateBackupView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UpdateBackupView"/> class.
        /// </summary>
        /// <param name="updateView">The update view.</param>
        public UpdateBackupView(UpdateView updateView)
        {
            InitializeComponent();
            DataContext = new UpdateBackupViewModel(updateView, this);
        }
    }
}
