﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Accounts;

namespace Ui.Desktop.View.Setting.Accounts
{
    /// <summary>
    /// Interaktionslogik für AccountsEmailAndAccountsView.xaml
    /// </summary>
    public partial class AccountsEmailAndAccountsView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AccountsEmailAndAccountsView"/> class.
        /// </summary>
        /// <param name="accountsView">The accounts view.</param>
        public AccountsEmailAndAccountsView(AccountsView accountsView)
        {
            InitializeComponent();
            DataContext = new AccountsEmailAndAccountsViewModel(accountsView, this);
        }
    }
}
