﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Accounts;

namespace Ui.Desktop.View.Setting.Accounts
{
    /// <summary>
    /// Interaktionslogik für AccountsSignupOptionView.xaml
    /// </summary>
    public partial class AccountsSignupOptionView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AccountsSignupOptionView"/> class.
        /// </summary>
        /// <param name="accountsView">The accounts view.</param>
        public AccountsSignupOptionView(AccountsView accountsView)
        {
            InitializeComponent();
            DataContext = new AccountsSignupOptionViewModel(accountsView, this);
        }
    }
}
