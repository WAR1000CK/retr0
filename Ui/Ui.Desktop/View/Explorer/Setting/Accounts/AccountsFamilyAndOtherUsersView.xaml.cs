﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Accounts;

namespace Ui.Desktop.View.Setting.Accounts
{
    /// <summary>
    /// Interaktionslogik für AccountsFamilyAndOtherUsersView.xaml
    /// </summary>
    public partial class AccountsFamilyAndOtherUsersView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AccountsFamilyAndOtherUsersView"/> class.
        /// </summary>
        /// <param name="accountsView">The accounts view.</param>
        public AccountsFamilyAndOtherUsersView(AccountsView accountsView)
        {
            InitializeComponent();
            DataContext = new AccountsFamilyAndOtherUsersViewModel(accountsView, this);
        }
    }
}
