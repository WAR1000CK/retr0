﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Accounts;

namespace Ui.Desktop.View.Setting.Accounts
{
    /// <summary>
    /// Interaktionslogik für AccountsSynchronizeSettingsView.xaml
    /// </summary>
    public partial class AccountsSynchronizeSettingsView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AccountsSynchronizeSettingsView"/> class.
        /// </summary>
        /// <param name="accountsView">The accounts view.</param>
        public AccountsSynchronizeSettingsView(AccountsView accountsView)
        {
            InitializeComponent();
            DataContext = new AccountsSynchronizeSettingsViewModel(accountsView, this);
        }
    }
}
