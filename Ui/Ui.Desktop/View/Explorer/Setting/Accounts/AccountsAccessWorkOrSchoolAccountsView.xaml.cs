﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Accounts;

namespace Ui.Desktop.View.Setting.Accounts
{
    /// <summary>
    /// Interaktionslogik für AccountsAccessWorkOrSchoolAccountsView.xaml
    /// </summary>
    public partial class AccountsAccessWorkOrSchoolAccountsView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AccountsAccessWorkOrSchoolAccountsView"/> class.
        /// </summary>
        /// <param name="accountsView">The accounts view.</param>
        public AccountsAccessWorkOrSchoolAccountsView(AccountsView accountsView)
        {
            InitializeComponent();
            DataContext = new AccountsAccessWorkOrSchoolAccountsViewModel(accountsView, this);
        }
    }
}
