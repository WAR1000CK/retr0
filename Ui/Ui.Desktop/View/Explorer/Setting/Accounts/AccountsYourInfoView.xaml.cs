﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Accounts;

namespace Ui.Desktop.View.Setting.Accounts
{
    /// <summary>
    /// Interaktionslogik für AccountsYourInfoView.xaml
    /// </summary>
    public partial class AccountsYourInfoView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AccountsYourInfoView"/> class.
        /// </summary>
        /// <param name="accountsView">The accounts view.</param>
        public AccountsYourInfoView(AccountsView accountsView)
        {
            InitializeComponent();
            DataContext = new AccountsYourInfoViewModel(accountsView, this);
        }
    }
}
