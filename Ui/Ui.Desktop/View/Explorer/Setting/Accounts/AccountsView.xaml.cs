﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Accounts;

namespace Ui.Desktop.View.Setting.Accounts
{
    /// <summary>
    /// Interaktionslogik für AccountsView.xaml
    /// </summary>
    public partial class AccountsView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AccountsView"/> class.
        /// </summary>
        /// <param name="mainWindow">The main window.</param>
        public AccountsView(MainWindow mainWindow)
        {
            InitializeComponent();
            DataContext = new AccountsViewModel(mainWindow, this);
        }
    }
}
