﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Sys;

namespace Ui.Desktop.View.Setting.Sys
{
    /// <summary>
    /// Interaktionslogik für SystemDisplayView.xaml
    /// </summary>
    public partial class SystemDisplayView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SystemDisplayView"/> class.
        /// </summary>
        /// <param name="systemView">The system view.</param>
        public SystemDisplayView(SystemView systemView)
        {
            InitializeComponent();
            DataContext = new SystemDisplayViewModel(systemView, this);
        }
    }
}
