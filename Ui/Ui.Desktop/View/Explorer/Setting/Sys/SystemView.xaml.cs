﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Sys;

namespace Ui.Desktop.View.Setting.Sys
{
    /// <summary>
    /// Interaktionslogik für PersonalizeView.xaml
    /// </summary>
    public partial class SystemView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SystemView"/> class.
        /// </summary>
        /// <param name="mainWindow">The main window.</param>
        public SystemView(MainWindow mainWindow)
        {
            InitializeComponent();
            DataContext = new SystemViewModel(mainWindow, this);
        }
    }
}
