﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Sys;

namespace Ui.Desktop.View.Setting.Sys
{
    /// <summary>
    /// Interaktionslogik für SystemMultitaskingView.xaml
    /// </summary>
    public partial class SystemMultitaskingView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SystemMultitaskingView"/> class.
        /// </summary>
        /// <param name="systemView">The system view.</param>
        public SystemMultitaskingView(SystemView systemView)
        {
            InitializeComponent();
            DataContext = new SystemMultitaskingViewModel(systemView, this);
        }
    }
}
