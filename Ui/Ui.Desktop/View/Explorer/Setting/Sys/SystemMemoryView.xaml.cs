﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Sys;

namespace Ui.Desktop.View.Setting.Sys
{
    /// <summary>
    /// Interaktionslogik für SystemMemoryView.xaml
    /// </summary>
    public partial class SystemMemoryView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SystemMemoryView"/> class.
        /// </summary>
        /// <param name="systemView">The system view.</param>
        public SystemMemoryView(SystemView systemView)
        {
            InitializeComponent();
            DataContext = new SystemMemoryViewModel(systemView, this);
        }
    }
}
