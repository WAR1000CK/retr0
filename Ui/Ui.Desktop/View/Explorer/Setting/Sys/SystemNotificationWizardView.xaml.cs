﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Sys;

namespace Ui.Desktop.View.Setting.Sys
{
    /// <summary>
    /// Interaktionslogik für SystemNotificationWizardView.xaml
    /// </summary>
    public partial class SystemNotificationWizardView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SystemNotificationWizardView"/> class.
        /// </summary>
        /// <param name="systemView">The system view.</param>
        public SystemNotificationWizardView(SystemView systemView)
        {
            InitializeComponent();
            DataContext = new SystemNotificationWizardViewModel(systemView, this);
        }
    }
}
