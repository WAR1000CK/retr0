﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Sys;

namespace Ui.Desktop.View.Setting.Sys
{
    /// <summary>
    /// Interaktionslogik für SystemNotificationView.xaml
    /// </summary>
    public partial class SystemNotificationView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SystemNotificationView"/> class.
        /// </summary>
        /// <param name="systemView">The system view.</param>
        public SystemNotificationView(SystemView systemView)
        {
            InitializeComponent();
            DataContext = new SystemNotificationViewModel(systemView, this);
        }
    }
}
