﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Sys;

namespace Ui.Desktop.View.Setting.Sys
{
    /// <summary>
    /// Interaktionslogik für SystemSoundView.xaml
    /// </summary>
    public partial class SystemSoundView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SystemSoundView"/> class.
        /// </summary>
        /// <param name="systemView">The system view.</param>
        public SystemSoundView(SystemView systemView)
        {
            InitializeComponent();
            DataContext = new SystemSoundViewModel(systemView, this);
        }
    }
}
