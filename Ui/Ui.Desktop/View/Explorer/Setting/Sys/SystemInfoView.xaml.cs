﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Sys;

namespace Ui.Desktop.View.Setting.Sys
{
    /// <summary>
    /// Interaktionslogik für SystemInfoView.xaml
    /// </summary>
    public partial class SystemInfoView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SystemInfoView"/> class.
        /// </summary>
        /// <param name="systemView">The system view.</param>
        public SystemInfoView(SystemView systemView)
        {
            InitializeComponent();
            DataContext = new SystemInfoViewModel(systemView, this);
        }
    }
}
