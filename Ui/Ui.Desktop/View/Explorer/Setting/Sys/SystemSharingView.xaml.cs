﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Sys;

namespace Ui.Desktop.View.Setting.Sys
{
    /// <summary>
    /// Interaktionslogik für SystemSharingView.xaml
    /// </summary>
    public partial class SystemSharingView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SystemSharingView"/> class.
        /// </summary>
        /// <param name="systemView">The system view.</param>
        public SystemSharingView(SystemView systemView)
        {
            InitializeComponent();
            DataContext = new SystemSharingViewModel(systemView, this);
        }
    }
}
