﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Sys;

namespace Ui.Desktop.View.Setting.Sys
{
    /// <summary>
    /// Interaktionslogik für SystemMainsOperationView.xaml
    /// </summary>
    public partial class SystemMainsOperationView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SystemMainsOperationView"/> class.
        /// </summary>
        /// <param name="systemView">The system view.</param>
        public SystemMainsOperationView(SystemView systemView)
        {
            InitializeComponent();
            DataContext = new SystemMainsOperationViewModel(systemView, this);
        }
    }
}
