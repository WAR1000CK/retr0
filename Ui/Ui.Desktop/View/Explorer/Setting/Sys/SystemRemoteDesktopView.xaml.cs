﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Sys;

namespace Ui.Desktop.View.Setting.Sys
{
    /// <summary>
    /// Interaktionslogik für SystemRemoteDesktopView.xaml
    /// </summary>
    public partial class SystemRemoteDesktopView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SystemRemoteDesktopView"/> class.
        /// </summary>
        /// <param name="systemView">The system view.</param>
        public SystemRemoteDesktopView(SystemView systemView)
        {
            InitializeComponent();
            DataContext = new SystemRemoteDesktopViewModel(systemView, this);
        }
    }
}
