﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Sys;

namespace Ui.Desktop.View.Setting.Sys
{
    /// <summary>
    /// Interaktionslogik für SystemClipboardView.xaml
    /// </summary>
    public partial class SystemClipboardView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SystemClipboardView"/> class.
        /// </summary>
        /// <param name="systemView">The system view.</param>
        public SystemClipboardView(SystemView systemView)
        {
            InitializeComponent();
            DataContext = new SystemClipboardViewModel(systemView, this);
        }
    }
}
