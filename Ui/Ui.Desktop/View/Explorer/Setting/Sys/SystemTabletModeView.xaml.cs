﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Sys;

namespace Ui.Desktop.View.Setting.Sys
{
    /// <summary>
    /// Interaktionslogik für SystemTabletMode.xaml
    /// </summary>
    public partial class SystemTabletModeView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SystemTabletModeView"/> class.
        /// </summary>
        /// <param name="systemView">The system view.</param>
        public SystemTabletModeView(SystemView systemView)
        {
            InitializeComponent();
            DataContext = new SystemTabletModeViewModel(systemView, this);
        }
    }
}
