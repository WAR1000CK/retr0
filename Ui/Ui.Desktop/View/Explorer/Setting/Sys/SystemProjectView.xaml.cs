﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Sys;

namespace Ui.Desktop.View.Setting.Sys
{
    /// <summary>
    /// Interaktionslogik für SystemProjectView.xaml
    /// </summary>
    public partial class SystemProjectView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SystemProjectView"/> class.
        /// </summary>
        /// <param name="systemView">The system view.</param>
        public SystemProjectView(SystemView systemView)
        {
            InitializeComponent();
            DataContext = new SystemProjectViewModel(systemView, this);
        }
    }
}
