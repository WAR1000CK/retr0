﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Devices;

namespace Ui.Desktop.View.Setting.Devices
{
    /// <summary>
    /// Interaktionslogik für DevicesBluetoothView.xaml
    /// </summary>
    public partial class DevicesBluetoothView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DevicesBluetoothView"/> class.
        /// </summary>
        /// <param name="devicesView">The devices view.</param>
        public DevicesBluetoothView(DevicesView devicesView)
        {
            InitializeComponent();
            DataContext = new DevicesBluetoothViewModel(devicesView, this);
        }
    }
}
