﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Devices;

namespace Ui.Desktop.View.Setting.Devices
{
    /// <summary>
    /// Interaktionslogik für DevicesAutomaticPlaybackView.xaml
    /// </summary>
    public partial class DevicesAutomaticPlaybackView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DevicesAutomaticPlaybackView"/> class.
        /// </summary>
        /// <param name="devicesView">The devices view.</param>
        public DevicesAutomaticPlaybackView(DevicesView devicesView)
        {
            InitializeComponent();
            DataContext = new DevicesAutomaticPlaybackViewModel(devicesView, this);
        }
    }
}
