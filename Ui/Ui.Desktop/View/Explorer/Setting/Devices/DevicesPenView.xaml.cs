﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Devices;

namespace Ui.Desktop.View.Setting.Devices
{
    /// <summary>
    /// Interaktionslogik für DevicesPenView.xaml
    /// </summary>
    public partial class DevicesPenView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DevicesPenView"/> class.
        /// </summary>
        /// <param name="devicesView">The devices view.</param>
        public DevicesPenView(DevicesView devicesView)
        {
            InitializeComponent();
            DataContext = new DevicesPenViewModel(devicesView, this);
        }
    }
}
