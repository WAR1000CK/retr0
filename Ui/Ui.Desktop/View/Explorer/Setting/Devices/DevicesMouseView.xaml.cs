﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Devices;

namespace Ui.Desktop.View.Setting.Devices
{
    /// <summary>
    /// Interaktionslogik für DevicesMouseView.xaml
    /// </summary>
    public partial class DevicesMouseView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DevicesMouseView"/> class.
        /// </summary>
        /// <param name="devicesView">The devices view.</param>
        public DevicesMouseView(DevicesView devicesView)
        {
            InitializeComponent();
            DataContext = new DevicesMouseViewModel(devicesView, this);
        }
    }
}
