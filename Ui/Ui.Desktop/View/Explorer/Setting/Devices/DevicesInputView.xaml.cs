﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Devices;

namespace Ui.Desktop.View.Setting.Devices
{
    /// <summary>
    /// Interaktionslogik für DevicesInputView.xaml
    /// </summary>
    public partial class DevicesInputView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DevicesInputView"/> class.
        /// </summary>
        /// <param name="devicesView">The devices view.</param>
        public DevicesInputView(DevicesView devicesView)
        {
            InitializeComponent();
            DataContext = new DevicesInputViewModel(devicesView, this);
        }
    }
}
