﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Devices;

namespace Ui.Desktop.View.Setting.Devices
{
    /// <summary>
    /// Interaktionslogik für DevicesUSBView.xaml
    /// </summary>
    public partial class DevicesUSBView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DevicesUSBView"/> class.
        /// </summary>
        /// <param name="devicesView">The devices view.</param>
        public DevicesUSBView(DevicesView devicesView)
        {
            InitializeComponent();
            DataContext = new DevicesUSBViewModel(devicesView, this);
        }
    }
}
