﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Devices;

namespace Ui.Desktop.View.Setting.Devices
{
    /// <summary>
    /// Interaktionslogik für DevicesView.xaml
    /// </summary>
    public partial class DevicesView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DevicesView"/> class.
        /// </summary>
        /// <param name="mainWindow">The main window.</param>
        public DevicesView(MainWindow mainWindow)
        {
            InitializeComponent();
            DataContext = new DevicesViewModel(mainWindow, this);
        }
    }
}
