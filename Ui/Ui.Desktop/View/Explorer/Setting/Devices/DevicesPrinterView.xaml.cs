﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Devices;

namespace Ui.Desktop.View.Setting.Devices
{
    /// <summary>
    /// Interaktionslogik für DevicesPrinterView.xaml
    /// </summary>
    public partial class DevicesPrinterView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DevicesPrinterView"/> class.
        /// </summary>
        /// <param name="devicesView">The devices view.</param>
        public DevicesPrinterView(DevicesView devicesView)
        {
            InitializeComponent();
            DataContext = new DevicesPrinterViewModel(devicesView, this);
        }
    }
}
