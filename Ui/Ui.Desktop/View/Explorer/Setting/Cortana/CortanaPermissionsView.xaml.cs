﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Cortana;

namespace Ui.Desktop.View.Setting.Cortana
{
    /// <summary>
    /// Interaktionslogik für CortanaPermissionsView.xaml
    /// </summary>
    public partial class CortanaPermissionsView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CortanaPermissionsView"/> class.
        /// </summary>
        /// <param name="cortanaView">The cortana view.</param>
        public CortanaPermissionsView(CortanaView cortanaView)
        {
            InitializeComponent();
            DataContext = new CortanaPermissionsViewModel(cortanaView, this);
        }
    }
}
