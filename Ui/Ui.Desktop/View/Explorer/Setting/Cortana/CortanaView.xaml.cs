﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Cortana;

namespace Ui.Desktop.View.Setting.Cortana
{
    /// <summary>
    /// Interaktionslogik für CortanaView.xaml
    /// </summary>
    public partial class CortanaView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CortanaView"/> class.
        /// </summary>
        /// <param name="mainWindow">The main window.</param>
        public CortanaView(MainWindow mainWindow)
        {
            InitializeComponent();
            DataContext = new CortanaViewModel(mainWindow, this);
        }
    }
}
