﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Cortana;

namespace Ui.Desktop.View.Setting.Cortana
{
    /// <summary>
    /// Interaktionslogik für CortanaTalkToCortanaView.xaml
    /// </summary>
    public partial class CortanaTalkToCortanaView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CortanaTalkToCortanaView"/> class.
        /// </summary>
        public CortanaTalkToCortanaView(CortanaView cortanaView)
        {
            InitializeComponent();
            DataContext = new CortanaTalkToCortanaViewModel(cortanaView, this);
        }
    }
}
