﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Setting.Cortana;

namespace Ui.Desktop.View.Setting.Cortana
{
    /// <summary>
    /// Interaktionslogik für CortanaForMoreinformationView.xaml
    /// </summary>
    public partial class CortanaForMoreinformationView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CortanaForMoreinformationView"/> class.
        /// </summary>
        public CortanaForMoreinformationView(CortanaView cortanaView)
        {
            InitializeComponent();
            DataContext = new CortanaForMoreinformationViewModel(cortanaView, this);
        }
    }
}
