﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Note;

namespace Ui.Desktop.View.Note
{
    /// <summary>
    /// Interaktionslogik für NoteView.xaml
    /// </summary>
    public partial class NoteView : UserControl
    {
        #region Private fields

        private static NoteViewModel _noteViewModel;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="NoteView"/> class.
        /// </summary>
        /// <param name="mainWindow">The main window.</param>
        public NoteView(MainWindow mainWindow)
        {
            InitializeComponent();
            _noteViewModel = new NoteViewModel(mainWindow, this);
            DataContext = _noteViewModel;
        }

        /// <summary>
        /// Gets the note viewmodel.
        /// </summary>
        /// <returns></returns>
        public static NoteViewModel GetNoteViewModel() => _noteViewModel;
    }
}
