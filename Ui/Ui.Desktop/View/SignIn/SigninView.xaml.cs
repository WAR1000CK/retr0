﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.SignIn;

namespace Ui.Desktop.View.SignIn
{
    /// <summary>
    /// Interaktionslogik für SigninView.xaml
    /// </summary>
    public partial class SigninView : UserControl
    {
        #region Private fields

        private static SigninViewModel _signinViewModel;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="SigninView"/> class.
        /// </summary>
        /// <param name="mainWindow">The main window.</param>
        public SigninView(MainWindow mainWindow)
        {
            InitializeComponent();
            _signinViewModel = new SigninViewModel(mainWindow, this);
            DataContext = _signinViewModel;
        }

        /// <summary>
        /// Gets the signin viewmodel.
        /// </summary>
        /// <returns></returns>
        public static SigninViewModel GetSigninViewModel() => _signinViewModel;
    }
}
