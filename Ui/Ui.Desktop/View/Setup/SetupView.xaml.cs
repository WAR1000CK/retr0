﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel;

namespace Ui.Desktop.View.Setup
{
    /// <summary>
    /// Interaktionslogik für SetupView.xaml
    /// </summary>
    public partial class SetupView : UserControl
    {
        #region Private fields

        private static SetupViewModel _setupViewModel;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="SetupView"/> class.
        /// </summary>
        /// <param name="mainWindow">The main window.</param>
        public SetupView(MainWindow mainWindow)
        {
            InitializeComponent();
            _setupViewModel = new SetupViewModel(mainWindow, this);
            DataContext = _setupViewModel;
        }

        /// <summary>
        /// Gets the setup viewmodel.
        /// </summary>
        /// <returns></returns>
        public static SetupViewModel GetSetupViewModel() => _setupViewModel;
    }
}
