﻿using System.Windows;
using System.Windows.Controls;
using Ui.Desktop.ViewModel.Registration;

namespace Ui.Desktop.View.Registration
{
    /// <summary>
    /// Interaktionslogik für RegistrationView.xaml
    /// </summary>
    public partial class RegistrationView : UserControl
    {
        #region Private fields

        private static RegistrationViewModel _registrationViewModel;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="RegistrationView"/> class.
        /// </summary>
        /// <param name="mainWindow">The main window.</param>
        public RegistrationView(MainWindow mainWindow)
        {
            InitializeComponent();
            _registrationViewModel = new RegistrationViewModel(mainWindow, this);
            DataContext = _registrationViewModel;
        }

        /// <summary>
        /// Gets the registration viewmodel.
        /// </summary>
        /// <returns></returns>
        public static RegistrationViewModel GetRegistrationViewModel() => _registrationViewModel;

        #region Events

        /// <summary>
        /// Handles the 1 event of the PasswordChanged control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void PasswordChanged_1(object sender, RoutedEventArgs e) => GetRegistrationViewModel().Password1 = ((PasswordBox)sender).Password;

        /// <summary>
        /// Handles the 2 event of the PasswordChanged control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void PasswordChanged_2(object sender, RoutedEventArgs e) => GetRegistrationViewModel().Password2 = ((PasswordBox)sender).Password;

        #endregion
    }
}
