﻿using System.Windows.Controls;
using Ui.Desktop.ViewModel.Bootup;

namespace Ui.Desktop.View.Bootup
{
    /// <summary>
    /// Interaktionslogik für BootupView.xaml
    /// </summary>
    public partial class BootupView : UserControl
    {
        #region Private fields

        private static BootupViewModel bootupViewModel;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="BootupView"/> class.
        /// </summary>
        /// <param name="mainWindow">The main window.</param>
        public BootupView(MainWindow mainWindow)
        {
            InitializeComponent();
            bootupViewModel = new BootupViewModel(mainWindow, this);
            DataContext = bootupViewModel;
        }

        /// <summary>
        /// Gets the bootup viewmodel.
        /// </summary>
        /// <returns></returns>
        public static BootupViewModel GetBootupViewModel() => bootupViewModel;
    }
}
