﻿using System.Windows.Media;
using Ui.FileManager.Enums;
using Ui.FileManager.ViewModels;

namespace Ui.FileManager
{
    public class Statusbar
    {
        #region Properties

        private MainWindow MainWindow { get; set; }
        private MainWindowViewModel MainWindowViewModel { get; set; }

        public int CurrentNumberOfEntries { get; set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="Statusbar"/> class.
        /// </summary>
        /// <param name="mainWindow">The main window.</param>
        /// <param name="mainWindowViewModel">The main window view model.</param>
        public Statusbar(MainWindow mainWindow, MainWindowViewModel mainWindowViewModel)
        {
            MainWindow = mainWindow;
            MainWindowViewModel = mainWindowViewModel;

            Reset();
        }

        /// <summary>
        /// Reset the statusbar content.
        /// </summary>
        public void Reset()
        {
            MainWindow.LblStatusbarPart0.Foreground = new SolidColorBrush(Colors.White);
            MainWindowViewModel.StatusbarMessage0 = $"{CurrentNumberOfEntries} Elemente   |";
            MainWindowViewModel.StatusbarMessage1 = "";
        }

        /// <summary>
        /// Display the content in statusbar.
        /// </summary>
        public void Content(string count = null, EntryType type = EntryType.None, string exception = null, string filesize = null)
        {
            Reset();

            // Show entry count
            MainWindowViewModel.StatusbarMessage0 = $"{count} Elemente   |";

            switch (type)
            {
                case EntryType.None:
                    break;
                case EntryType.Datei:
                    MainWindowViewModel.StatusbarMessage1 = $"1 Elemente ausgewählt ({filesize})";
                    break;
                case EntryType.Dateiordner:
                        MainWindowViewModel.StatusbarMessage1 = $"1 Elemente ausgewählt";
                    break;
                case EntryType.Error:
                    MainWindow.LblStatusbarPart0.Foreground = new SolidColorBrush(Colors.Red);
                    MainWindowViewModel.StatusbarMessage0 = $"{count}: {exception}";
                    break;
            }
        }
    }
}
