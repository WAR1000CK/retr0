﻿using System.IO;
using System.Windows;
using Ui.FileManager.ViewModels;

namespace Ui.FileManager.UserControls
{
    public partial class PropertiesView : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PropertiesView"/> class.
        /// </summary>
        /// <param name="fileInfo">The file information.</param>
        /// <param name="directoryInfo">The directory information.</param>
        public PropertiesView(FileInfo fileInfo, DirectoryInfo directoryInfo)
        {
            InitializeComponent();
            DataContext = new PropertiesViewModel(this, fileInfo, directoryInfo);

            string filename = fileInfo != null ? fileInfo.Name : directoryInfo.Name;
            this.Title = $"Eigenschaften von {filename}";

            if (this.ShowActivated == true)
                PropertiesViewModel.CalculateSize.RunWorkerAsync();

            this.ShowDialog();
        }
    }
}
