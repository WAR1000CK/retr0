﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using Ui.FileManager.ViewModels;

namespace Ui.FileManager
{
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
            DataContext = new MainWindowViewModel(this);
        }

        /// <summary>
        /// Gets the TreeView UIelement.
        /// </summary>
        public TreeView GetTreeView => treeView;

        /// <summary>
        /// Gets the ListView UIelement.
        /// </summary>
        public ListView GetListView => ListViewFiles;

        public void AddButtonToNavigationbar(string path)
        {
            // Get style template from xaml
            Style style = FindResource("PathNavigationButton") as Style;

            // Create buuton
            Button button = new Button()
            {
                Style = style,
                Content = path
            };
            // Click event
            button.Click += (s, e) =>
            {
                Button btn = e.Source as Button;

                MessageBox.Show("Path navigation in development...", btn.Content.ToString(), MessageBoxButton.OK, MessageBoxImage.Exclamation);

                // TODO 
                //       - Navigate to TreeView Node on button click.
                //       - Add all Directories and Files from Node to ListView.
            };

            // Add button to stackpanel
            PathNavigation.Children.Add(button);
        }

        /// <summary>
        /// Invokes the UI element from MainThread.
        /// </summary>
        public void InvokeUIElement(Action action, DispatcherPriority priority) => Dispatcher.Invoke(action, priority);
    }
}