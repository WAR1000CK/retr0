﻿using System;
using System.Collections.Generic;

namespace Ui.FileManager.CoreFeatures
{
    public struct SizeConverter
    {
        private const ushort CONVERT_VALUE = 0x400;

        public static string ToFilesize(ulong source)
        {
            List<string> suffixes = new List<string> { " B", " KB", " MB", " GB", " TB", " PB" };
            for (int i = 0; i < suffixes.Count; i++)
            {
                ulong temp = source / (ulong)Math.Pow(CONVERT_VALUE, i + 1);
                if (temp == 0)
                    return (source / (ulong)Math.Pow(CONVERT_VALUE, i)) + suffixes[i];
            }

            return source.ToString();
        }
    }
}
