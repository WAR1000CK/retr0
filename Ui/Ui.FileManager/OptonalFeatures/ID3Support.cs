﻿using Id3;

namespace Ui.FileManager
{
    public class ID3Support
    {
        public static string GetID3Tag(string path)
        {
            using (var mp3 = new Mp3(path, Mp3Permissions.Read))
            {
                Id3Tag tag = mp3.GetTag(Id3TagFamily.Version2X);
                return tag.Title;
            }
        }
    }
}
