﻿using System;
using System.ComponentModel;
using System.IO;
using System.Windows;
using System.Windows.Threading;
using Ui.FileManager.UserControls;
using static Ui.FileManager.CoreFeatures.SizeConverter;

namespace Ui.FileManager.ViewModels
{
    public class PropertiesViewModel : BaseViewModel
    {
        #region Properties

        public PropertiesView PropertiesView { private get; set; }
        public static BackgroundWorker CalculateSize { get; private set; }

        #region Generals

        public string Name { get; set; }
        public bool IsReadOnly { get; set; }
        public bool IsHidden { get; set; }
        public string CreationTime { get; set; }
        public string LastAccess { get; set; }
        public string LastWriteTime { get; set; }
        public FileAttributes FileAttributes { get; set; }
        public string Type { get => FileAttributes.ToString(); }
        public string FullPath { get; set; }

        private string _size;
        public string Size
        {
            get => _size;
            set
            {
                if (_size == value)
                    return;

                _size = value;
                OnPropertyChanged();
            }
        }

        private string _content;
        public string Content
        {
            get => _content;
            set
            {
                if (_content == value)
                    return;

                _content = value;
                OnPropertyChanged();
            }
        }

        #endregion

        #region Security



        #endregion

        #endregion

        #region Private fields

        string _tmpSize;
        string _tmpContent;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="PropertiesViewModel"/> class.
        /// </summary>
        /// <param name="propertiesView">The properties view.</param>
        /// <param name="fileInfo">The file information.</param>
        /// <param name="directoryInfo">The directory information.</param>
        public PropertiesViewModel(PropertiesView propertiesView, FileInfo fileInfo, DirectoryInfo directoryInfo)
        {
            PropertiesView = propertiesView;

            // Tab Generals
            Name = fileInfo != null ? fileInfo.Name : directoryInfo.Name;
            CreationTime = fileInfo != null ? $"{fileInfo.CreationTime}" : $"{directoryInfo.CreationTime}";
            LastAccess = fileInfo != null ? $"{fileInfo.LastAccessTime}" : $"{directoryInfo.LastAccessTime}";
            LastWriteTime = fileInfo != null ? $"{fileInfo.LastWriteTime}" : $"{directoryInfo.LastWriteTime}";
            FullPath = fileInfo != null ? fileInfo.FullName : directoryInfo.FullName;
            FileAttributes = fileInfo != null ? fileInfo.Attributes : directoryInfo.Attributes;
            IsReadOnly = (FileAttributes & FileAttributes.ReadOnly) == FileAttributes.ReadOnly ? true : false;
            IsHidden = (FileAttributes & FileAttributes.Hidden) == FileAttributes.Hidden ? true : false;

            // Calculate size in background
            CalculateSize = new BackgroundWorker
            {
                WorkerSupportsCancellation = true
            };
            CalculateSize.DoWork += delegate
            {
                Size = "Berechnung läuft...";
                Content = "Berechnung läuft...";

                if ((FileAttributes & FileAttributes.Directory) == FileAttributes.Directory)
                    _tmpSize = ToFilesize((ulong)DirectorySize(directoryInfo));
                else
                    _tmpSize = ToFilesize((ulong)fileInfo.Length);

                _tmpContent = CalculateContent();


            };
            CalculateSize.RunWorkerCompleted += delegate
            {
                Size = _tmpSize;
                Content = _tmpContent;
            };

            // Tab Security
        }

        // 
        private long DirectorySize(DirectoryInfo directoryInfo)
        {
            long size = 0;

            try
            {
                FileInfo[] fis = directoryInfo.GetFiles();
                foreach (FileInfo fi in fis) { size += fi.Length; }

                DirectoryInfo[] dis = directoryInfo.GetDirectories();
                foreach (DirectoryInfo di in dis) { size += DirectorySize(di); }
            }
            catch (Exception)
            { size = 0; }

            return size;
        }

        // 
        private long FileCountFromPath(string path)
        {
            try
            {
                if ((FileAttributes & FileAttributes.Directory) == FileAttributes.Directory)
                    return Directory.GetFiles(path, "*", SearchOption.AllDirectories).LongLength;
                else
                    return 0;
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message, ex.StackTrace, MessageBoxButton.OK, MessageBoxImage.Error);
                Size = ex.Message;
                return 0;
            }
        }

        // 
        private long DirectorieCountFromPath(string path)
        {
            try
            {
                if ((FileAttributes & FileAttributes.Directory) == FileAttributes.Directory)
                    return Directory.GetDirectories(path, "*", SearchOption.AllDirectories).LongLength;
                else
                    return 0;
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message, ex.StackTrace, MessageBoxButton.OK, MessageBoxImage.Error);
                Size = ex.Message;
                return 0;
            }
        }


        public string CalculateContent()
        {
            long tmpF = FileCountFromPath(FullPath);
            long tmpD = DirectorieCountFromPath(FullPath);

            if (tmpF == 0 && tmpD == 0)
            {
                InvokeUIElement(() => SetLabel(), DispatcherPriority.Normal);
                return _tmpSize;
            }

            return $"{tmpF} Dateien, {tmpD} Ordner";
        }

        // 
        private void SetLabel()
        {
            PropertiesView.LblContent.Content = "Größe auf\nDatenträger:";
            PropertiesView.LblContent.VerticalContentAlignment = VerticalAlignment.Center;
            PropertiesView.LblContentValue.VerticalContentAlignment = VerticalAlignment.Bottom;
        }


        public void InvokeUIElement(Action action, DispatcherPriority priority) => PropertiesView.Dispatcher.Invoke(action, priority);
    }
}
