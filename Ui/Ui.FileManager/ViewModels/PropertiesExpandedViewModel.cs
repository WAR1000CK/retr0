﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ui.FileManager.UserControls;

namespace Ui.FileManager.ViewModels
{
    public class PropertiesExpandedViewModel : BaseViewModel
    {
        #region Private fields

        private readonly MainWindow _mainWindow;

        #endregion

        public PropertiesExpandedViewModel(MainWindow mainWindow)
        {
            _mainWindow = mainWindow;
            IntializeWindow();
        }

        /// <summary>
        /// Intializes the <see cref="PropertiesExpandedView"/> window.
        /// </summary>
        private static void IntializeWindow()
        {
            PropertiesExpandedView propertiesExpanded = new PropertiesExpandedView();
            if (propertiesExpanded.ShowDialog() == true)
                propertiesExpanded.Show();
        }
    }
}
