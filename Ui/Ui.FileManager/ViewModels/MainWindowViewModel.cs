﻿using System.Collections.Generic;
using System.IO;
using Ui.FileManager.UserControls;
using UI.FileExplorer;

namespace Ui.FileManager.ViewModels
{
    public class Entry
    {
        #region Properties

        public FileInfo FileInfo { get; private set; }
        public DirectoryInfo DirectoryInfo { get; private set; }
        public FileAttributes Attributes { get; private set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="Entry"/> class.
        /// </summary>
        /// <param name="fileInfo">The file information.</param>
        /// <param name="directoryInfo">The directory information.</param>
        /// <param name="attributes">The attributes.</param>
        public Entry(FileInfo fileInfo, DirectoryInfo directoryInfo, FileAttributes attributes)
        {
            FileInfo = fileInfo;
            DirectoryInfo = directoryInfo;
            Attributes = attributes;
        }
    }

    public class MainWindowViewModel : BaseViewModel
    {
        #region Full Properties

        private string _customTitle = "Dieser PC";
        public string CustomTitle
        {
            get => _customTitle;
            set
            {
                if (_customTitle == value)
                    return;

                _customTitle = value;
                OnPropertyChanged();
            }
        }

        private string _statusbarMessage0;
        public string StatusbarMessage0
        {
            get { return _statusbarMessage0; }
            set
            {
                if (_statusbarMessage0 == value)
                    return;

                _statusbarMessage0 = value;
                OnPropertyChanged();
            }
        }

        private string _statusbarMessage1;
        public string StatusbarMessage1
        {
            get { return _statusbarMessage1; }
            set
            {
                if (_statusbarMessage1 == value)
                    return;

                _statusbarMessage1 = value;
                OnPropertyChanged();
            }
        }

        #endregion

        #region Properties

        public List<Entry> Entrys { get; private set; }
        public Treeview RefTreeView { get; private set; }
        public Listview RefListView { get; private set; }

        #region Relay commands

        // Menu Header: Datei
        public RelayCommand Datei_NeuesFensterÖffnen { get; private set; }
        public RelayCommand Datei_Hilfe { get; private set; }
        public RelayCommand Datei_Schließen { get; private set; }

        // Menu Header: Computer
        public RelayCommand Computer_Eigenschaften { get; private set; }
        public RelayCommand Computer_Öffnen { get; private set; }
        public RelayCommand Computer_Umbenennen { get; private set; }
        public RelayCommand Computer_AufMedienZugreifen { get; private set; }
        public RelayCommand Computer_NetzlaufwerkVerbinden { get; private set; }
        public RelayCommand Computer_NetzwerkadresseHinzufügen { get; private set; }
        public RelayCommand Computer_EinstellungenÖffnen { get; private set; }
        public RelayCommand Computer_ProgrammDeinstallierenOderÄndern { get; private set; }
        public RelayCommand Computer_Systemeigenschaften { get; private set; }
        public RelayCommand Computer_Verwalten { get; private set; }

        // Menu Header: Ansicht -> Bereiche
        public RelayCommand Ansicht_Bereiche_Navigationsbereich_Erweitern { get; private set; }
        public RelayCommand Ansicht_Bereiche_Navigationsbereich_AlleOrdnerAnzeigen { get; private set; }
        public RelayCommand Ansicht_Bereiche_Navigationsbereich_BibliothekenAnzeigen { get; private set; }
        public RelayCommand Ansicht_Bereiche_Vorschaufenster { get; private set; }
        public RelayCommand Ansicht_Bereiche_Detailbereich { get; private set; }

        // Menu Header: Ansicht -> Layout
        public RelayCommand Ansicht_Layout_ExtraGroßeSymbole { get; private set; }
        public RelayCommand Ansicht_Layout_GroßeSymbole { get; private set; }
        public RelayCommand Ansicht_Layout_MittelgroßeSymbole { get; private set; }
        public RelayCommand Ansicht_Layout_KleineSymbole { get; private set; }
        public RelayCommand Ansicht_Layout_Liste { get; private set; }
        public RelayCommand Ansicht_Layout_Details { get; private set; }
        public RelayCommand Ansicht_Layout_Kacheln { get; private set; }
        public RelayCommand Ansicht_Layout_Inhalt { get; private set; }

        // Menu Header: Ansicht -> AktuelleAnsicht
        public RelayCommand Ansicht_AktuelleAnsicht_SortierennNach { get; private set; }
        public RelayCommand Ansicht_AktuelleAnsicht_GruppierenNach { get; private set; }

        // Menu Header: Ansicht -> Ein/Ausblenden
        public RelayCommand Ansicht_EinAusblenden_Elementkontrollkästchen { get; private set; }
        public RelayCommand Ansicht_EinAusblenden_Dateinamenerweiterung { get; private set; }
        public RelayCommand Ansicht_EinAusblenden_AusgeblendeteElemente { get; private set; }

        // Menu Header: Ansicht
        public RelayCommand Ansicht_Optionen { get; private set; }

        // Menu Header: Verwaltung
        public RelayCommand Verwaltung_About { get; private set; }

        // SelectedItem
        public RelayCommand SelectedItem_SendenAn_Desktop { get; private set; }
        public RelayCommand SelectedItem_SendenAn_Dokumente { get; private set; }
        public RelayCommand SelectedItem_SendenAn_EMailEmpfänger { get; private set; }
        public RelayCommand SelectedItem_Ausschneiden { get; private set; }
        public RelayCommand SelectedItem_Kopieren { get; private set; }
        public RelayCommand SelectedItem_VerknüpfungErstellen { get; private set; }
        public RelayCommand SelectedItem_Löschen { get; private set; }
        public RelayCommand SelectedItem_Umbenennen { get; private set; }
        public RelayCommand SelectedItem_Eigenschaften { get; private set; }

        #endregion

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindowViewModel"/> class.
        /// </summary>
        /// <param name="mainWindow">The main window.</param>
        public MainWindowViewModel(MainWindow mainWindow)
        {
            Entrys = new List<Entry>();
            RefTreeView = new Treeview(this, mainWindow); // Initialize TreeView object and Events
            RefListView = new Listview(this, mainWindow); // Initialize ListView object and Events

            #region Initialize relay commands

            // Menu Header: Datei
            Datei_NeuesFensterÖffnen = new RelayCommand();
            Datei_Hilfe = new RelayCommand();
            Datei_Schließen = new RelayCommand();

            // Menu Header: Computer
            Computer_Eigenschaften = new RelayCommand(Eigenschaften);
            Computer_Öffnen = new RelayCommand();
            Computer_Umbenennen = new RelayCommand();
            Computer_AufMedienZugreifen = new RelayCommand();
            Computer_NetzlaufwerkVerbinden = new RelayCommand();
            Computer_NetzwerkadresseHinzufügen = new RelayCommand();
            Computer_EinstellungenÖffnen = new RelayCommand();
            Computer_ProgrammDeinstallierenOderÄndern = new RelayCommand();
            Computer_Systemeigenschaften = new RelayCommand();
            Computer_Verwalten = new RelayCommand();

            // Menu Header: Ansicht -> Bereiche
            Ansicht_Bereiche_Navigationsbereich_Erweitern = new RelayCommand();
            Ansicht_Bereiche_Navigationsbereich_AlleOrdnerAnzeigen = new RelayCommand();
            Ansicht_Bereiche_Navigationsbereich_BibliothekenAnzeigen = new RelayCommand();
            Ansicht_Bereiche_Vorschaufenster = new RelayCommand();
            Ansicht_Bereiche_Detailbereich = new RelayCommand();

            // Menu Header: Ansicht -> Layout
            Ansicht_Layout_ExtraGroßeSymbole = new RelayCommand();
            Ansicht_Layout_GroßeSymbole = new RelayCommand();
            Ansicht_Layout_MittelgroßeSymbole = new RelayCommand();
            Ansicht_Layout_KleineSymbole = new RelayCommand();
            Ansicht_Layout_Liste = new RelayCommand();
            Ansicht_Layout_Details = new RelayCommand();
            Ansicht_Layout_Kacheln = new RelayCommand();
            Ansicht_Layout_Inhalt = new RelayCommand();

            // Menu Header: Ansicht -> AktuelleAnsicht
            Ansicht_AktuelleAnsicht_SortierennNach = new RelayCommand();
            Ansicht_AktuelleAnsicht_GruppierenNach = new RelayCommand();

            // Menu Header: Ansicht -> Ein/Ausblenden
            Ansicht_EinAusblenden_Elementkontrollkästchen = new RelayCommand();
            Ansicht_EinAusblenden_Dateinamenerweiterung = new RelayCommand();
            Ansicht_EinAusblenden_AusgeblendeteElemente = new RelayCommand();

            // Menu Header: Ansicht
            Ansicht_Optionen = new RelayCommand();

            // Menu Header: Verwaltung
            Verwaltung_About = new RelayCommand();

            // SelectedItem
            SelectedItem_SendenAn_Desktop = new RelayCommand();
            SelectedItem_SendenAn_Dokumente = new RelayCommand();
            SelectedItem_SendenAn_EMailEmpfänger = new RelayCommand();
            SelectedItem_Ausschneiden = new RelayCommand();
            SelectedItem_Kopieren = new RelayCommand();
            SelectedItem_VerknüpfungErstellen = new RelayCommand();
            SelectedItem_Löschen = new RelayCommand(Löschen);
            SelectedItem_Umbenennen = new RelayCommand();
            SelectedItem_Eigenschaften = new RelayCommand(Eigenschaften);

            #endregion
        }

        /// <summary>
        /// Clear list of stored directories and files
        /// </summary>
        public void EntryClear() => Entrys.Clear();

        #region Relay command Methods

        private void Löschen()
        {
            //MessageBoxResult result = MessageBox.Show($"Möchten Sie\n\n{Path}\n\nwirklich löschen?", $"{FileName}", MessageBoxButton.YesNo, MessageBoxImage.Information);
            //if (result == MessageBoxResult.Yes)
            //{
            //    throw new NotImplementedException("Function not Implemented!");
            //}
        }

        private void Eigenschaften()
        {
            if (RefListView.SelectedItem != null)
                new PropertiesView(RefListView.FileInfo, RefListView.DirectoryInfo);
        }

        #endregion

    }
}
