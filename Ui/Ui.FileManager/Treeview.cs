﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Ui.FileManager.Enums;
using Ui.FileManager.ViewModels;

namespace Ui.FileManager
{
    public class Treeview
    {
        #region Properties

        private MainWindowViewModel MainWindowViewModel { get; set; }
        private MainWindow MainWindow { get; set; }
        private Statusbar Statusbar { get; set; }

        public string FullName { get; set; }

        #endregion

        #region Private fields

        private readonly object _dummyNode = null;

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="Treeview"/> class.
        /// </summary>
        /// <param name="mainWindowViewModel">The main window view model.</param>
        /// <param name="mainWindow">The main window.</param>
        public Treeview(MainWindowViewModel mainWindowViewModel, MainWindow mainWindow)
        {
            MainWindowViewModel = mainWindowViewModel;
            MainWindow = mainWindow;
            Statusbar = new Statusbar(mainWindow, mainWindowViewModel); // Initialize Statusbar 

            #region Events

            mainWindow.treeView.SelectedItemChanged += SelectedItemChanged;

            #endregion

            AddLogicalDrives();
        }

        // Adds all available drives to the tree view.
        private void AddLogicalDrives()
        {
            string[] drive = Directory.GetLogicalDrives();
            for (int i = 0; i < drive.Length; i++)
            {
                DriveInfo driveInfo = new DriveInfo(drive[i]);
                if (driveInfo.IsReady)
                {
                    TreeViewItem driveItem = new TreeViewItem
                    {
                        Header = $"{drive[i]}", // TODO: Change Displayname
                        Tag = drive[i],
                        FontSize = 14,
                        FontWeight = FontWeights.SemiBold
                    };

                    driveItem.Items.Add(_dummyNode);
                    driveItem.Expanded += new RoutedEventHandler(Expanded);
                    MainWindow.GetTreeView.Items.Add(driveItem);
                }
            }
        }


        // Handles the SelectedItemChanged event of the TreeView control.
        private void SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            TreeView tree = (TreeView)sender;
            TreeViewItem selectedItem = ((TreeViewItem)tree.SelectedItem);
            if (!selectedItem.IsSelected) return;

            #region Parent

            // Change Title
            MainWindowViewModel.CustomTitle = selectedItem.Header.ToString();

            // TEST
            MainWindow.PathNavigation.Children.Clear();
            MainWindow.AddButtonToNavigationbar(selectedItem.Header.ToString());

            MainWindow.GetListView.Items.Clear(); // Clear listView items
            MainWindowViewModel.EntryClear();       // Clear entry list

            FullName = "";
            string temp2 = "";
            while (true)
            {
                string temp1 = selectedItem.Header.ToString();
                if (temp1.Contains(@"\"))
                    temp2 = "";

                FullName = temp1 + temp2 + FullName;

                if (selectedItem.Parent.GetType().Equals(typeof(TreeView)))
                    break;

                selectedItem = ((TreeViewItem)selectedItem.Parent);
                temp2 = @"\";
            }

            #endregion

            // Start backgroundWorker to get all available directories and files from selected treeview node.
            if (MainWindowViewModel.RefListView.DirectoryWorker.IsBusy)
                MainWindowViewModel.RefListView.DirectoryWorker.CancelAsync();
            MainWindowViewModel.RefListView.DirectoryWorker.RunWorkerAsync(FullName);
        }

        // Handles the Expanded event of the Treeview control.
        private void Expanded(object sender, RoutedEventArgs e)
        {
            TreeViewItem item = (TreeViewItem)sender;
            if (item.Items.Count == 1 && item.Items[0] == _dummyNode)
            {
                try
                {
                    item.Items.Clear();

                    var dir = Directory.GetDirectories((string)item.Tag);
                    for (int i = 0; i < dir.Length; i++)
                    {
                        DirectoryInfo info = new DirectoryInfo(dir[i]);

                        if (!IsSystemAttribute(info, null) && !IsHiddenAttribute(info, null))
                        {
                            MainWindowViewModel.Entrys.Add(new Entry(null, new DirectoryInfo(dir[i]), info.Attributes));

                            TreeViewItem subitem = new TreeViewItem
                            {
                                Header = info.Name,
                                Tag = dir[i],
                                FontSize = 14,
                                FontWeight = FontWeights.Normal
                            };
                            subitem.Items.Add(_dummyNode);
                            subitem.Expanded += Expanded;
                            item.Items.Add(subitem);
                        }
                    }
                }
                catch (Exception ex)
                { Statusbar.Content(ex.Source, EntryType.None, ex.Message, null); }
            }
        }


        // Determines whether a file or directory is from the attribute system.
        private bool IsSystemAttribute(DirectoryInfo directoryInfo, FileInfo fileInfo)
        {
            if (directoryInfo != null)
                if ((directoryInfo.Attributes & FileAttributes.System) == FileAttributes.System)
                    return true;

            if (fileInfo != null)
                if ((fileInfo.Attributes & FileAttributes.System) == FileAttributes.System)
                    return true;

            return false;
        }

        // Determines whether a file or directory is from the attribute hidden.
        private bool IsHiddenAttribute(DirectoryInfo directoryInfo, FileInfo fileInfo)
        {
            if (directoryInfo != null)
                if ((directoryInfo.Attributes & FileAttributes.Hidden) == FileAttributes.Hidden)
                    return true;

            if (fileInfo != null)
                if ((fileInfo.Attributes & FileAttributes.Hidden) == FileAttributes.Hidden)
                    return true;

            return false;
        }


        /// <summary>
        /// 
        /// </summary>
        public void JumpToNode(TreeView treeView, string node)
        {
            bool done = false;
            ItemCollection ic = treeView.Items;

            while (!done)
            {
                bool found = false;

                foreach (TreeViewItem tvi in ic)
                {
                    if (node.Contains($"{tvi.Tag}"))
                    {
                        found = true;
                        tvi.IsExpanded = true;
                        ic = tvi.Items;
                        if (node == tvi.Tag.ToString()) done = true;

                        break;
                    }
                }

                done = (found == false && done == false);
            }
        }
    }
}
