﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Threading;
using Ui.FileManager.Enums;
using Ui.FileManager.ViewModels;
using static Ui.FileManager.CoreFeatures.SizeConverter;

namespace Ui.FileManager
{
    public class Listview
    {
        #region Properties

        private MainWindowViewModel MainWindowViewModel { get; set; }
        private MainWindow MainWindow { get; set; }
        private Statusbar Statusbar { get; set; }

        public BackgroundWorker FileWorker { get; private set; }
        public BackgroundWorker DirectoryWorker { get; private set; }
        public DirectoryInfo DirectoryInfo { get; private set; }
        public FileInfo FileInfo { get; private set; }

        [Bindable(true)]
        public object SelectedItem { get; set; }
        private string Filename { get => GetAnonymousType(SelectedItem, "Filename"); }
        public int SelectedIndex { get => MainWindow.GetListView.SelectedIndex; set { } }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="Listview"/> class.
        /// </summary>
        /// <param name="mainWindowViewModel">The main window view model.</param>
        /// <param name="mainWindow">The main window.</param>
        public Listview(MainWindowViewModel mainWindowViewModel, MainWindow mainWindow)
        {
            MainWindowViewModel = mainWindowViewModel;
            MainWindow = mainWindow;
            Statusbar = new Statusbar(mainWindow, mainWindowViewModel); // Initialize Statusbar 

            #region Events

            mainWindow.GetListView.SelectionChanged += SelectionChanged;
            mainWindow.GetListView.MouseDoubleClick += MouseDoubleClick;
            mainWindow.GetListView.MouseDown += MouseDown;

            #endregion

            #region Directory BackgroundWorker

            DirectoryWorker = new BackgroundWorker
            {
                WorkerSupportsCancellation = true,
                WorkerReportsProgress = true
            };
            // Handles the DoWork event
            DirectoryWorker.DoWork += (s, e) =>
            { IterateDirectories(e.Argument as string, s); };

            // Handles the ProgressChanged event
            DirectoryWorker.ProgressChanged += (s, e) =>
            { mainWindow.ProgressbarLoadingItems.Value = e.ProgressPercentage; };

            // Handles the RunWorkerCompleted event
            DirectoryWorker.RunWorkerCompleted += delegate
            {
                mainWindow.ProgressbarLoadingItems.Value = 0; // Reset Progressbar value
                try { FileWorker.RunWorkerAsync(mainWindowViewModel.RefTreeView.FullName); }
                catch (Exception) { }
            };

            #endregion

            #region File BackgroundWorker

            FileWorker = new BackgroundWorker
            {
                WorkerSupportsCancellation = true
            };
            // Handles the DoWork event
            FileWorker.DoWork += (s, e) => { IterateFiles(e.Argument as string); };

            // Handles the RunWorkerCompleted event
            FileWorker.RunWorkerCompleted += delegate
            {
                mainWindow.ProgressbarLoadingItems.IsIndeterminate = false;
                mainWindow.ProgressbarLoadingItems.Maximum = 100;
                mainWindow.ProgressbarLoadingItems.Value = 0; // Reset Progressbar value
            };

            #endregion
        }

        /// <summary>
        /// Separate thread <seealso cref="DirectoryWorker"/> iterates through all directories in the path <paramref name="arg"/>.
        /// </summary>
        public void IterateDirectories(string arg, object sender)
        {
            try
            {
                Statusbar.CurrentNumberOfEntries = 0; // Reset current entry counts

                ScrollTop();

                var dir = Directory.GetDirectories(arg);
                MainWindow.InvokeUIElement(() =>
                {
                    MainWindow.ProgressbarLoadingItems.Minimum = 0;
                    MainWindow.ProgressbarLoadingItems.Maximum = dir.Length;
                }, DispatcherPriority.Background);

                for (int i = 0; i < dir.Length; i++)
                {
                    (sender as BackgroundWorker).ReportProgress(i);

                    DirectoryInfo info = new DirectoryInfo(dir[i].ToString());

                    AddDirectory(info.Attributes, info);
                    MainWindowViewModel.Entrys.Add(new Entry(null, new DirectoryInfo(dir[i]), info.Attributes));
                }
            }
            catch (Exception ex)
            {
                MainWindow.InvokeUIElement(() =>
                {
                    // Refresh Statusbar
                    Statusbar.Content(ex.Source, EntryType.Error, ex.Message, null);
                }, DispatcherPriority.Background);
            }
        }

        /// <summary>
        /// Adds a directory to the UIElement <see cref="MainWindow.ListViewFiles"/>.
        /// </summary>
        private void AddDirectory(FileAttributes attributes, DirectoryInfo directoryInfo)
        {
            Statusbar.CurrentNumberOfEntries++;

            MainWindow.InvokeUIElement(() =>
            {
                // Adds a directory to the UIElement
                MainWindow.GetListView.Items.Add(new
                {
                    Filename = directoryInfo.Name,
                    LastWriteTime = $"{directoryInfo.LastWriteTime.ToShortDateString()} {directoryInfo.LastWriteTime.ToShortTimeString()}",
                    Attribut = attributes
                });

                // Refresh Statusbar
                Statusbar.Content($"{Statusbar.CurrentNumberOfEntries}", EntryType.None, null, null);
            }, DispatcherPriority.Background);
        }

        /// <summary>
        /// Separate thread <seealso cref="FileWorker"/> iterates through all directories in the path <paramref name="arg"/>.
        /// </summary>
        public void IterateFiles(string arg)
        {
            try
            {
                MainWindow.InvokeUIElement(() => {
                    MainWindow.ProgressbarLoadingItems.IsIndeterminate = true;
                }, DispatcherPriority.Normal);

                var file = Directory.GetFiles(arg);
                for (int i = 0; i < file.Length; i++)
                {
                    FileInfo info = new FileInfo(file[i].ToString());

                    AddFile(info.Attributes, info);
                    MainWindowViewModel.Entrys.Add(new Entry(new FileInfo(file[i]), null, info.Attributes));
                }
            }
            catch (Exception ex)
            {
                MainWindow.InvokeUIElement(() =>
                {
                    // Refresh Statusbar
                    Statusbar.Content(ex.Source, EntryType.Error, ex.Message, null);
                }, DispatcherPriority.Normal);
            }
        }

        /// <summary>
        /// Adds a file to the UIElement <see cref="MainWindow.ListViewFiles"/>.
        /// </summary>
        private void AddFile(FileAttributes attributes, FileInfo fileInfo)
        {
            Statusbar.CurrentNumberOfEntries++;

            MainWindow.InvokeUIElement(() =>
            {
                // Add item to Listview
                MainWindow.GetListView.Items.Add(new
                {
                    Filename = fileInfo.Name,
                    LastWriteTime = $"{fileInfo.LastWriteTime.ToShortDateString()} {fileInfo.LastWriteTime.ToShortTimeString()}",
                    Attribut = attributes,
                    Size = ToFilesize((ulong)fileInfo.Length),
                    FolderPath = fileInfo.FullName
                });

                ResizeGridViewColumn(MainWindow.test);

                // Refresh Statusbar
                Statusbar.Content($"{Statusbar.CurrentNumberOfEntries}", EntryType.None, null, null);
            }, DispatcherPriority.Background);
        }


        // Handles the SelectionChanged event of the ListViewFiles control.
        private void SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (SelectedIndex == -1) return;
            ListView listView = (ListView)sender;

            // Set the value for the first time
            SelectedItem = listView.SelectedItem;

            // TEST
            MainWindow.PathNavigation.Children.Clear();
            MainWindow.AddButtonToNavigationbar(Filename);

            foreach (Entry entry in MainWindowViewModel.Entrys)
            {
                if (SetFileInfo(entry, Filename)) break;
                if (SetDirectoryInfo(entry, Filename)) break;
            }
        }

        // Handles the MouseDoubleClick event of the ListViewFiles control.
        private void MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (SelectedIndex == -1) return;
            ListView tree = (ListView)sender;
            SelectedItem = tree.SelectedItem;

            foreach (Entry entry in MainWindowViewModel.Entrys)
            {
                if (IsEqualFilename(entry, Filename))
                {
                    Process.Start(entry.FileInfo.FullName);
                    break;
                }

                if (IsEqualDirectoryname(entry, Filename))
                {
                    MainWindowViewModel.EntryClear(); // Clear entry list
                    MainWindow.GetListView.Items.Clear();

                    MainWindowViewModel.RefTreeView.JumpToNode(MainWindow.GetTreeView, entry.DirectoryInfo.FullName);

                    // Change Title
                    MainWindowViewModel.CustomTitle = Filename;

                    // Reset Statusbar
                    Statusbar.Reset();

                    // Set path for FileWorker
                    MainWindowViewModel.RefTreeView.FullName = entry.DirectoryInfo.FullName;
                    // Start DirectoryWorker and FileWorker
                    DirectoryWorker.RunWorkerAsync(entry.DirectoryInfo.FullName);
                    break;
                }
            }
        }

        // Handles the MouseDown event of the ListViewFiles control.
        private void MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            MainWindow.GetListView.UnselectAll();
            SelectedItem = null;

            // Reset Statusbar
            Statusbar.Reset();

            // TODO: Navigate back and forward with mouse key
        }


        /// <summary>
        /// Sets the <seealso cref="FileInfo"/> from <paramref name="selectedItem"/>.
        /// </summary>
        private bool SetFileInfo(Entry entry, string selectedItem)
        {
            // Reset selected object
            FileInfo = null;

            // Check whether the current iteration is the selected item
            if (IsEqualFilename(entry, selectedItem))
            {
                // Get the file info from the selected item
                FileInfo = entry.FileInfo;

                // Refresh Statusbar
                Statusbar.Content($"{Statusbar.CurrentNumberOfEntries}", EntryType.Datei, null, ToFilesize((ulong)entry.FileInfo.Length));
                return true;
            }
            return false;
        }

        /// <summary>
        /// Check whether <seealso cref="Entry.FileInfo.Name"/> is the same as <paramref name="filename"/>
        /// </summary>
        private bool IsEqualFilename(Entry entry, string filename)
        {
            if (entry.FileInfo != null && entry.FileInfo.Name.Equals(filename))
                return true;

            return false;
        }

        /// <summary>
        /// Sets the <seealso cref="DirectoryInfo"/> from <paramref name="selectedItem"/>.
        /// </summary>
        private bool SetDirectoryInfo(Entry entry, string selectedItem)
        {
            // Reset selected object
            DirectoryInfo = null;

            if (IsEqualDirectoryname(entry, selectedItem))
            {
                // Get the directory info from the selected item
                DirectoryInfo = entry.DirectoryInfo;

                // Refresh Statusbar
                Statusbar.Content($"{Statusbar.CurrentNumberOfEntries}", EntryType.Dateiordner, null, null);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Check whether <seealso cref="Entry.DirectoryInfo.Name"/> is the same as <paramref name="filename"/>
        /// </summary>
        private bool IsEqualDirectoryname(Entry entry, string filename)
        {
            if (entry.DirectoryInfo != null && entry.DirectoryInfo.Name.Equals(filename))
                return true;

            return false;
        }


        // Anonymouses the type value.
        private string GetAnonymousType(object obj, string property)
        {
            return (string)obj.GetType().GetProperty(property).GetValue(obj);
        }

        // Scrolls the top.
        private void ScrollTop()
        {
            if (MainWindow.GetListView.Items.Count > 0)
                MainWindow.GetListView.ScrollIntoView(MainWindow.GetListView.Items[0]);
        }

        private void ResizeGridViewColumn(GridViewColumn column)
        {
            if (double.IsNaN(column.Width))
                column.Width = column.ActualWidth;

            column.Width = double.NaN;
        }
    }
}
